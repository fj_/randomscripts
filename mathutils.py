from itertools import izip, izip_longest, islice, chain, imap, count, repeat
from itertools import starmap, tee, cycle, combinations, groupby
import collections
import operator
import types
from bisect import bisect


def revxrange(a, b = None):
    if b is None:
        return xrange(a - 1, -1, -1)
    else:
        return xrange(b - 1, a - 1, -1)
    
def nextperm(lst):
    for i in revxrange(len(lst) - 1):
        if lst[i] < lst[i + 1]:
            for j in revxrange(i + 1, len(lst)):
                if lst[j] > lst[i]:
                    lst[i], lst[j] = lst[j], lst[i]
                    lst[i + 1:] = lst[:i:-1] # reverse
                    return True
    return False  
            
def trymove4(i,j,h,w):
    if i > 0: yield   (i - 1, j    )
    if j > 0: yield   (i    , j - 1)
    if i+1 < h: yield (i + 1, j    )
    if j+1 < w: yield (i    , j + 1)

def bisectf(f, l, r, eps):
    while True:
        m = (l + r) / 2
        if r - l < eps:
            return m
        v = f(m)
        if v == 0:
            return v
        elif v < 0:
            l = m
        else:
            r = m

def dotProduct2((x1, y1), (x2, y2)):
    return x1 * x2 + y1 * y2

def dotProduct3((x1, y1, z1), (x2, y2, z2)):
    return x1 * x2 + y1 * y2 + z1 * z2

class PrimeGenerator(object):
    __slots__ = ("primes", "bound")
    
    def __init__(self):
        self.primes = [2, 3, 5, 7]
        self.bound = 11
    
    def _AddPossiblePrime(self, value):
        for j in self.primes:
            if value % j == 0: return False
            if j * j > value: break
        self.primes.append(value)
        return True
        
    def __contains__(self, value):
        if value % 2 == 0:
            return value == 2
        
        if value >= self.bound:
            i = self.bound
            wasPrime = False
            while i <= value:
                wasPrime = self._AddPossiblePrime(i)
                i += 2
            self.bound = i
            return wasPrime
        else:
            index = bisect(self.primes, value)
            return index < len(self.primes) and self.primes[index] == value 
    
    def _extendTo(self, index):
        i = self.bound
        for _ in xrange(len(self.primes), index + 1):
            while not self._AddPossiblePrime(i):
                i += 2
            i += 2 
        self.bound = i

    def extendTo(self, index):
        if index >= len(self.primes):
            self._extendTo(index)
    
    def extendToValue(self, value):
        if value >= self.bound:
            i = self.bound
            wasPrime = False
            while i <= value:
                wasPrime = self._AddPossiblePrime(i)
                i += 2
            self.bound = i
            return wasPrime
            
    def getPrime(self, index):
        if index >= len(self.primes): 
            self._extendTo(index * 2 - index // 2)
        return self.primes[index]
        
    def __getitem__(self, index):
        if isinstance(index, types.SliceType):
            if None == index.step:
                self.extendTo(index.stop)
                return self.primes[index.start : index.stop]
            else:             
                return [self.getPrime(item) for item in xrange(index.start, index.stop, index.step)]
        else:
            return self.getPrime(index)
        
    def __iter__(self):
        i = 0
        while True:
            yield self.getPrime(i)
            i += 1
    
    def factorize(self, value):
        assert value >= 1
        factors = []
        if value == 1:
            return factors

        for prime in self:
            while True:
                d, m = divmod(value, prime)
                if m == 0:
                    factors.append(prime)
                    value = d
                    if value == 1:
                        return factors
                else:
                    break
            if value < prime * prime:
                factors.append(value)
                return factors


###### itertools recipes ########

def take(n, iterable):
    "Return first n items of the iterable as a list"
    return list(islice(iterable, n))

def enumerate(iterable, start=0):
    return izip(count(start), iterable)

def tabulate(function, start=0):
    "Return function(0), function(1), ..."
    return imap(function, count(start))

def consume(iterator, n):
    "Advance the iterator n-steps ahead. If n is none, consume entirely."
    collections.deque(islice(iterator, n), maxlen=0)

def nth(iterable, n, default=None):
    "Returns the nth item or a default value"
    return next(islice(iterable, n, None), default)

def quantify(iterable, pred=bool):
    "Count how many times the predicate is true"
    return sum(imap(pred, iterable))

def padnone(iterable):
    """Returns the sequence elements and then returns None indefinitely.

    Useful for emulating the behavior of the built-in map() function.
    """
    return chain(iterable, repeat(None))

def ncycles(iterable, n):
    "Returns the sequence elements n times"
    return chain.from_iterable(repeat(iterable, n))

def dotproduct(vec1, vec2):
    return sum(imap(operator.mul, vec1, vec2))

def flatten(listOfLists):
    return list(chain.from_iterable(listOfLists))

def repeatfunc(func, times=None, *args):
    """Repeat calls to func with specified arguments.

    Example:  repeatfunc(random.random)
    """
    if times is None:
        return starmap(func, repeat(args))
    return starmap(func, repeat(args, times))

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)

def grouper(n, iterable, fillvalue=None):
    "grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return izip_longest(fillvalue=fillvalue, *args)

def roundrobin(*iterables):
    "roundrobin('ABC', 'D', 'EF') --> A D E B F C"
    # Recipe credited to George Sakkis
    pending = len(iterables)
    nexts = cycle(iter(it).next for it in iterables)
    while pending:
        try:
            for next in nexts:
                yield next()
        except StopIteration:
            pending -= 1
            nexts = cycle(islice(nexts, pending))

def compress(data, selectors):
    "compress('ABCDEF', [1,0,1,0,1,1]) --> A C E F"
    return (d for d, s in izip(data, selectors) if s)

def combinations_with_replacement(iterable, r):
    "combinations_with_replacement('ABC', 2) --> AA AB AC BB BC CC"
    # number items returned:  (n+r-1)! / r! / (n-1)!
    pool = tuple(iterable)
    n = len(pool)
    if not n and r:
        return
    indices = [0] * r
    yield tuple(pool[i] for i in indices)
    while True:
        for i in reversed(range(r)):
            if indices[i] != n - 1:
                break
        else:
            return
        indices[i:] = [indices[i] + 1] * (r - i)
        yield tuple(pool[i] for i in indices)

def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))

def unique_everseen(iterable, key=None):
    "List unique elements, preserving order. Remember all elements ever seen."
    # unique_everseen('AAAABBBCCDAABBB') --> A B C D
    # unique_everseen('ABBCcAD', str.lower) --> A B C D
    seen = set()
    seen_add = seen.add
    if key is None:
        for element in iterable:
            if element not in seen:
                seen_add(element)
                yield element
    else:
        for element in iterable:
            k = key(element)
            if k not in seen:
                seen_add(k)
                yield element

def unique_justseen(iterable, key=None):
    "List unique elements, preserving order. Remember only the element just seen."
    # unique_justseen('AAAABBBCCDAABBB') --> A B C D A B
    # unique_justseen('ABBCcAD', str.lower) --> A B C A D
    return imap(next, imap(operator.itemgetter(1), groupby(iterable, key)))

class Grid(object):
    def __init__(self, width, height, initial = None, off_range = None):
        assert width > 0 and height > 0
        self.width = width
        self.height = height
        self.initial = initial
        self.off_range = off_range
        self.data = [initial for _ in range(width * height)]
        
    def calculate_offset(self, idx):
        row, col = idx
        width, height = self.width, self.height
        if 0 <= row < height and 0 <= col < width:
            return row * width + col
        return -1
    
    def __getitem__(self, idx):
        offset = self.calculate_offset(idx)
        return self.data[offset] if offset >= 0 else self.off_range
    
    def __setitem__(self, idx, value):
        offset = self.calculate_offset(idx)
        assert offset >= 0
        self.data[offset] = value
        
    def __iter__(self):
        width, height = self.width, self.height
        for row in xrange(height):
            base = row * width
            for col in xrange(width):
                yield (row, col), self.data[base + col]
                
    def iterrow(self, row):
        offset = self.calculate_offset((row, 0))
        if offset < 0: return
        for i in xrange(offset, offset + self.width):
            yield self.data[i]
            
    def iter8neighbours(self, pos):
        for delta in directions_delta:
            npos = v_add(pos, delta) 
            offset = self.calculate_offset(npos)
            if offset >= 0:
                yield npos, self.data[offset]
                
    def iter8neighbours_directions(self, pos):
        for delta, dir in zip(directions_delta, directions):
            npos = v_add(pos, delta) 
            offset = self.calculate_offset(npos)
            if offset >= 0:
                yield dir, self.data[offset]
        
    def __str__(self):
        def format_cell(cell):
            if cell == -1: return ' *'
            return '%02s' % cell
        def format_row(row):
            return ' '.join(format_cell(it) for it in self.iterrow(row))
        return '\n'.join(chain(
                (format_row(row) for row in xrange(self.height)),
                ['']))

def probOfStreak(numCoins, minHeads, headProb, saved=None):
    if saved == None: saved = {}
    ID = (numCoins, minHeads, headProb)
    
    if ID in saved:
        return saved[ID]
    else:
        if minHeads > numCoins or numCoins <= 0:
            result = 0
        else:
            result = headProb ** minHeads
            for firstTail in xrange(1, minHeads+1):
                pr = probOfStreak(numCoins-firstTail, minHeads, headProb, saved)
                result += (headProb ** (firstTail-1)) * (1 - headProb) * pr
        saved[ID] = result
        return result
