import pyaudio
from math import fmod

def generator1():
    melody_notes = 'IQNNNN!!]]!Q!IWJWQNN??!!W]WQNNN?'
    bass_notes = [3.0, 3.0, 4.75, 2.0]
    for t in xrange(2**30):
        w = t >> 9
        v = w >> 7
        p = (w >> 5) & 0x03
        a = 1.0 - fmod(t / 2048.0, 1.0)
        b = bass_notes[p] * t / 4.0
        d = a * fmod((14 * t * t) ^ t, 2048.0)
        m = (ord(melody_notes[((w >> 1) & 0x0F) | ((p / 3) << 4)]) / 33.0 * t) - t
        bass = fmod(b * 0.98, 80.0) + fmod(b, 80.0)
        drum = ((int(fmod(5.0 * t, 2048.0) * a) & 128) * ((0x53232323 >> ((w / 4) % 32)) & 1)) + ((int(d) & 127) * ((0xA444C444 >> ((w / 4) % 32)) & 1)) + (int(d * w) & 1)
        melody = (fmod(m, 32.0) + fmod(m * 1.99, 32.0) + fmod(m * 0.49, 32.0) + fmod(m * 0.97, 32.0) - 64.0) * (4.0 - a - a - a)
        sample = bass + (a * ((drum if v else 0.0) + (melody if v > 1 else 0.0)))
        sample = min(127, int(sample)) if sample >= 0.0 else 0
        yield sample

if __name__ == '__main__':
    print 'Yay'
    device = pyaudio.PyAudio()
    stream = device.open(format = pyaudio.paInt8, channels = 1, rate = 8000, output = True)
    for sample in generator1():
        stream.write(chr(sample))
    
