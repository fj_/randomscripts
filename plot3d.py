import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt

mpl.rcParams['legend.fontsize'] = 10

fig = plt.figure()
ax = fig.gca(projection='3d')
z = np.linspace(0, 20, 100)
x = np.sin(z)
y = np.sin(z - 0.1)
ax.plot(x, y, z, label='parametric curve')
ax.legend()

plt.show()
