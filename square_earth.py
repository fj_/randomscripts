import numpy as np

def grid(resolution):
    x, step = np.linspace(0.0, 1.0, resolution, endpoint=False, retstep=True)
    x += step / 2
    g = np.meshgrid(x, x, x)
    return np.stack(map(np.ravel, g), axis=1)


def force(reference, points):
    d = points - reference.reshape(1, 3)
    return np.sum(d / np.sum(np.abs(d) ** 3, axis=1, keepdims=True), axis=0) / points.shape[0]

import matplotlib.pyplot as plt
ax = plt.axes()

g = grid(50)
for x in np.linspace(0, 1, 10):
    ref = np.asarray([x, -0.01, 0.5])
    f = force(ref, g) * 0.1
    ax.arrow(x, 0, f[0], f[1], color='red')
    ax.arrow(x, 0, 0.5 - x, 0.5, color='green')
    print(ref,)


plt.show()

# circle1 = plt.Circle((0, 0), 0.2, color='r')
# circle2 = plt.Circle((0.5, 0.5), 0.2, color='blue')
# circle3 = plt.Circle((1, 1), 0.2, color='g', clip_on=False)
#
# fig, ax = plt.subplot()
# # (or if you have an existing figure)
# # fig = plt.gcf()
# # ax = fig.gca()
#
# ax.add_artist(circle1)
# ax.add_artist(circle2)
# ax.add_artist(circle3)


