from collections import namedtuple
from copy import deepcopy

Edge = namedtuple('Edge', 'type turn i1 i2')

class Node(object):
    def __init__(self, index, edge, color):
        self.index = index
        self.edges = [edge]
        self.color = color
        self.type = ''
    def tostring(self):
        if self.type: return self.type
        return ''.join(e.type + str(e.generation) for e in self.edges)
    def collapse(self, pos, type, turn):
        if not self.type:
            self.type = type
            self.turn = turn
            for edge in self.edges:
                other = edge.i1
                if other == self.index: other = edge.i2
                pos[other].collapse(pos, edge.type, edge.turn)
            self.edges = None

def pos2str(pos):
    return ':'.join(node.tostring() if node else '' for node in pos)


class PosCache(object):
    def __init__(self):
        self.cache = {}
    def get(self, pos):
        return self.cache.get(pos2str(pos))
    def set(self, pos, winner):
        key = pos2str(pos)
        assert key not in self.cache
        self.cache[key] = winner

poscache = PosCache()
        

def gettype(node):
    if node: return node.type
    return ''


def determineWinner(turn, results):
    player = 'xo'[turn & 1]
    draw = False
    for r in results:
        if r == player: return r
        elif r == '': draw = True 
    return '' if draw else 'ox'[turn & 1] # other player

def checkWinner(pos, turn):
    winner = poscache.get(pos)
    if winner is not None: return winner

    indices = []
    for i in range(3): 
        indices.append((i, i + 3, i + 6))
        i *= 3
        indices.append((i, i + 1, i + 2))
    indices.append((0, 4, 8))
    indices.append((2, 4, 6))
    winner = None
    winnerturn = 10
    for iii in indices:
        t = gettype(pos[iii[0]])
        if t and t == gettype(pos[iii[1]]) and t == gettype(pos[iii[2]]):
            maxturn = max(pos[iii[i]].turn for i in range(3))
            if not winner or maxturn < winnerturn:  
                winner = t
                winnerturn = maxturn
    
    if winner:
        poscache.set(pos, winner) 
        return winner
    else:
        return nextTurn(pos, turn) 
            
        

def collapse(pos, index, type, turn):
    pos = deepcopy(pos)
    pos[index].collapse(pos, type, turn)
    return checkWinner(pos, turn) 

def processCycle(pos, edge):
    turn = edge.turn
    r1 = collapse(pos, edge.i1, edge.type, turn)
    r2 = collapse(pos, edge.i2, edge.type, turn)
    turn += 1 # because collapse happens in the beginning of the next turn
    return determineWinner(turn, (r1, r2))
    
def nextTurn(pos, turn):
    winner = poscache.get(pos)
    if winner is not None: return winner
    
    free = [i for i in range(9) if not gettype(i)]
    lf = len(free)
    if lf == 0:
        # draw.
        return ''
    elif lf == 1:
        # collapse manually!
        i = free[0]
        assert pos[i] is None # or I don't know
        pos[i] = Node(turn, None, turn)
        pos[i].type = 'xo'[turn & 1]
        return checkWinner(pos, turn + 1)
    else:
        pass # !!!!!!!!!!
        
    
    

def addEdge(pos, edge):
    n1 = pos[edge.i1]
    n2 = pos[edge.i2]
    if n1 and n2:
        # don't append the edge yet
        n1c = n1.color
        n2c = n2.color        
        if n1.color != n2.color:
            n1.edges.append(edge)
            n2.edges.append(edge)
            # join subgraphs
            for n, i in enumerate(pos):
                if n.color == n1c: 
                    n.color = n2c
        else:
            # teh cycle detectored!            
            return processCycle(pos, edge)
    elif n1:
        n1.edges.append(edge)
        pos[edge.i2] = Node(edge.i2, edge, n1.color)
    elif n1:
        n2.edges.append(edge)
        pos[edge.i1] = Node(edge.i1, edge, n2.color)
    else:
        pos[edge.i1] = Node(edge.i1, edge, edge.turn)
        pos[edge.i2] = Node(edge.i2, edge, edge.turn)
    return nextTurn(pos, edge.turn + 1)
    
    
pos = [None]*9

print pos2str(pos)

