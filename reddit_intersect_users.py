import re
from pathlib import Path
import requests

def match_users(url):
    r = requests.get(url, headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'})
    assert r.status_code == 200
    s = r.text
    return set(m.group(1) for m in re.finditer(r'data-author="([^"]+)"', s))


s1 = match_users('https://www.reddit.com/r/relationships/comments/2kdqym/i28m_was_slapped_awake_by_my_girlfriend26f_of_2/?st=JA1OY1XN&sh=55da6571')
s2 = match_users('https://www.reddit.com/r/relationships/comments/7d6pa0/i33f_was_slapped_awake_by_my_boyfriend26m_of_2/?st=JA1OYMKM&sh=88bd5b2b')
print(s1)
print(s2)
print(s1 & s2)
