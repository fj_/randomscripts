import random

class Player(object):
    def __init__(self, name, skill = None):
        self.name = name
        self.skill = skill if skill is not None else random.random()
    def play(self, other):
        return self.skill + random.random() > other.skill + random.random() 
    def __str__(self):
        return '{}({})'.format(self.name, self.skill)
    def __repr__(self):
        return 'Player({}, {})'.format(repr(self.name), repr(self.skill))

class Two_vars_stats(object):
    def __init__(self, prefix, var1name, var1negation, var2name, var2negation):
        self.prefix = prefix
        self.var1name = var1name
        self.var2name = var2name
        self.var1negation = var1negation
        self.var2negation = var2negation
        self.matrix = [0 for _ in range(4)]
        
    def update(self, v1, v2):
        self.matrix[v1 * 2 + v2] += 1
        
    def get(self, v1, v2):
        return self.matrix[v1 * 2 + v2]
    
    def total(self):
        return sum(self.matrix)
    
    def printf(self):
        total = self.total()
        lines = []
        def addline(s, v):
            lines.append((s, '{:<5} ({:03.1f}%)'.format(v, v * 100.0 / total)))
        addline(self.var1name, self.matrix[2] + self.matrix[3]) 
        addline(self.var1negation, self.matrix[0] + self.matrix[1]) 
        addline(self.var2name, self.matrix[1] + self.matrix[3]) 
        addline(self.var2negation, self.matrix[0] + self.matrix[2])
        addline(self.var1name + ' and ' + self.var2name, self.matrix[3])
        addline(self.var1name + ' and ' + self.var2negation, self.matrix[2])
        addline(self.var1negation + ' and ' + self.var2name, self.matrix[1])
        addline(self.var1negation + ' and ' + self.var2negation, self.matrix[0])
        
        width = max(len(s1) for s1, _ in lines)
        for s1, s2 in lines:
            print '{} {:<{}} : {}'.format(self.prefix, s1, width, s2)
        
        
class Stats(object):
    def __init__(self):
        self.tournaments = 0
        self.situations = 0
        self.bo3_stats = Two_vars_stats('bo3 winner', 'is better', 'is worse', 'wins', 'loses')
        self.ext_stats = Two_vars_stats('ext winner', 'is better', 'is worse', 'wins', 'loses')
    def printf(self):
        print 'tournaments:', self.tournaments
        print 'situations:', self.situations
        
        def print_stats(name, stats):
            print
            stats.printf()
            better_wins = stats.get(True, True) + stats.get(False, False) 
            print name, 'better player wins : {:<5} ({:03.1f}%)'.format(
                    better_wins, better_wins * 100.0 / stats.total())
        print_stats('bo3', self.bo3_stats)
        print_stats('ext', self.ext_stats)
    
def best_of_N(n, p1, p2, p1_wins = 0, p2_wins = 0):
    wins_required = n // 2 + 1
    while p1_wins < wins_required and p2_wins < wins_required:
        # here the match model is defined
        if p1.play(p2): 
            p1_wins += 1
        else: 
            p2_wins += 1
    return p1_wins, p2_wins

def play_tournament(stats):
    stats.tournaments += 1
    # here the match model is defined
    players = [Player('P{}'.format(i + 1)) for i in range(4)]
    # print players
    # round1
    winners_bracket, losers_bracket = [], []
    def play(p1, p2):
        def order(x, y, cond):
            return (x, y) if cond else (y, x)
        p1_wins, p2_wins = best_of_N(3, p1, p2)
        p1_won = p1_wins > p2_wins
        winner, loser = order(p1, p2, p1_won)
        score = order(p1_wins, p2_wins, p1_won)
        return winner, loser, score
        
    for p1, p2 in ((players[0], players[1]), (players[2], players[3])):
        winner, loser, score = play(p1, p2)
        winner.prev_player = loser
        winner.prev_score = score
        winners_bracket.append(winner)
        losers_bracket.append(loser) 
    # round2 -- get winners braket loser and losers bracket winner as p1, p2
    _, p1, _ = play(*winners_bracket)
    p2, _, _ = play(*losers_bracket)
    
    if p1.prev_player == p2:
        # yay, we have a Situation!
        stats.situations += 1
        # play standard bo3
        p1_wins, p2_wins = best_of_N(3, p1, p2)
        winner_is_better = p1.skill > p2.skill
        winner_wins = p1_wins > p2_wins
        stats.bo3_stats.update(winner_is_better, winner_wins)
        # play extended series
        score = p1.prev_score
        p1_wins, p2_wins = best_of_N(7, p1, p2, *score)
        winner_is_better = p1.skill > p2.skill
        winner_wins = p1_wins > p2_wins
        stats.ext_stats.update(winner_is_better, winner_wins)
        
        

def main():
    stats = Stats()
    for i in range(1000000):
        if i % 10000 == 9999: print i + 1
        play_tournament(stats)
    stats.printf() 

if __name__ == '__main__':
    main()
             
"""
Sample results:
tournaments: 1000000
situations: 441521

bo3 winner is better           : 285002 (64.6%)
bo3 winner is worse            : 156519 (35.4%)
bo3 winner wins                : 268037 (60.7%)
bo3 winner loses               : 173484 (39.3%)
bo3 winner is better and wins  : 223023 (50.5%)
bo3 winner is better and loses : 61979 (14.0%)
bo3 winner is worse and wins   : 45014 (10.2%)
bo3 winner is worse and loses  : 111505 (25.3%)
bo3 better player wins : 334528 (75.8%)

ext winner is better           : 285002 (64.6%)
ext winner is worse            : 156519 (35.4%)
ext winner wins                : 339952 (77.0%)
ext winner loses               : 101569 (23.0%)
ext winner is better and wins  : 264632 (59.9%)
ext winner is better and loses : 20370 (4.6%)
ext winner is worse and wins   : 75320 (17.1%)
ext winner is worse and loses  : 81199 (18.4%)
ext better player wins : 345831 (78.3%)
"""