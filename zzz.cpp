#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <stdio.h>
#include <map>
#include <sstream>
#include <regex>
#include <mutex>

// header

#include <chrono>
#include <vector>

struct MetricsPackage;

void libmon_init(int args, const char * const argv[]);
void libmon_finit();
void libmon_push(MetricsPackage && stuff);


enum class MetricsItemType {
    Count,
    Frequency,
};


struct MetricsItem {
    std::string name;
    double value;
    MetricsItemType type;
};


struct MetricsPackage {
    std::string prefix;
    std::chrono::steady_clock::time_point time;
    std::vector<MetricsItem> items;
};


// implementation

using std::string;
using std::vector;
using std::map;

string sample_config = R"(
[sink/default_graphite]
ADDR = 127.0.0.1
PORT = 1234

[sink/default_monfile]
PATH = $CSHOME/mon/$SERVERNAME.${SERVERID}.$DATE.mon

[server] ; default sinks
sinks = default_graphite
[server/NETIO]
sinks = default_graphite default_monfile
)";


using SinkCfg = map<string, string>;


struct ServerCfg {
    string name;
    vector<SinkCfg*> sinks;
};


struct Config {
    map<string, SinkCfg> sinks;
    map<string, ServerCfg> servers;
};


/*
vector<string> regex_split(const string & s, const string & rx_str) {
    vector<string> result;
    std::regex rx(rx_str);
    bool first = true;
    auto it = std::sregex_iterator(s.begin(), s.end(), rx);
    auto end = std::sregex_iterator();
    auto it_prev = end;
    for (; it != end; ++it) {
        if (first) {
        }
        else {
            first = false;
        }
    }
    return result;
}
*/

static Config parse_config(string & config_str) {
    Config cfg = {};
    boost::property_tree::ptree tree;
    std::stringstream config_stream(config_str);
    boost::property_tree::read_ini(config_stream, tree);
    for (const auto & section : tree) {
        //if (section.first
    }
    return cfg;
}


// threading

static std::vector<MetricsPackage> gQueue;
static std::mutex gQueueMutex;
static Config gConfig;


void libmon_init(int argc, char * argv[]) {
    // blah blah load file
    gConfig = parse_config(sample_config);
}


void libmon_finit() {
    // stop the worker thread
}


void libmon_push(MetricsPackage && it) {
    static auto uninitialized_time = std::chrono::steady_clock::time_point();
    if (it.time == uninitialized_time) {
        it.time = std::chrono::steady_clock::now();
    }

    do { // lock
        std::lock_guard<std::mutex> lock(gQueueMutex);
        gQueue.push_back(std::move(it));
    } while (0);
}


static void libmon_worker() {
    while (true) { // TODO: check if we were asked to terminate
        std::vector<MetricsPackage> packages;
        do { // lock
            std::lock_guard<std::mutex> lock(gQueueMutex);
            std::swap(packages, gQueue);
        } while (0);

        for (const auto & it: packages) {
            // TODO: feed them to our sinks
        }
    }
}


int main(int argc, char * argv[]) {
    libmon_init(argc, argv);
    printf("Hello\n");
    libmon_finit();
    return 0;
}
