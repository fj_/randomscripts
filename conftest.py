from pathlib import Path

def pytest_ignore_collect(path, config):
    # whitelist-only so that we can doctest-modules
    p = Path(path)
    return p.name not in ['retokenizer.py']
