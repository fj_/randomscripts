# -*- coding: utf-8 -*-

def really_in(y, xs):
    for x in xs:
        if y is x: return True
    return False


from pprint import pprint


class Node(object):
    def __init__(self, data, left, right):
        self.data = data
        self.left = left
        self.right = right
    def __repr__(self):
        return str(self.data)

def tuplize(node):
    return (node.data, tuplize(node.left), tuplize(node.right)) if node else None

        
    

test_tree = Node(1, 
                 Node(2, 
                      Node(3, Node(4, None, None), None), 
                      Node(5, None, Node(6, None, None)),
                      ),
                 Node(7, None, None)
                 )

test_tree = Node(1, 
                 Node(2, 
                      Node(3, None, None), 
                      None),
                 Node(4, None, None)
                 )

test_tree = Node(1, 
                 Node(2, 
                      Node(4, 
                          Node(8, None, None), 
                          Node(9, None, None)),
                      Node(5, 
                          Node(10, None, None), 
                          Node(11, None, None))),
                 Node(3, 
                      Node(6, 
                          Node(12, None, None), 
                          Node(13, None, None)),
                      Node(7, 
                          Node(14, None, None), 
                          Node(15, None, None))),
                 )

def walk(root):
    pprint(tuplize(root))
    visited = set()
    sentinel = Node(0, root, None);
    p1 = sentinel; p2 = root;
    while p2 != sentinel:
        print p2
        visited.add(p2)
        if p2.left:
            p2.left, p2.right, p1, p2 = p2.right, p1, p2, p2.left 
        elif p2.right:
            visited.add(p2)
            print p2
            p2.right, p2.left, p1, p2 = p2.left, p1, p2, p2.right  
        else:
            visited.add(p2)
            print p2
            p1, p2 = p2, p1
    print sorted(i.data for i in visited)
    
    pprint(tuplize(root))

walk(test_tree)