// This thingie can pretend to be some other program in order to intercept and log
// everything that happens on its std streams. Like, for example, gpg2.exe as 
// launched via GPGME.
//
// The target should be renamed into `io_proxy_target.exe`, then this thing should be
// renamed into `<target>.exe`, and when run it would create `io_proxy.log`.
//
// Note that it might be buggy, exhibit weird behaviour, also it will not capture
// anything besides std streams (so it's might have to be modified for more extensive
// tinkering with gpg which uses additional descriptors).

#include <stdio.h>
#include <string>
#include <vector>
#include <sstream>
#include <windows.h>
//#include <assert.h>
#include <iostream>
#include <process.h> 

using std::string;
using std::vector;

HANDLE logfile;
CRITICAL_SECTION log_cs;

void escape(string & dst, const char * src, DWORD length)
{
    for (size_t i = 0; i < length; ++i)
    {
        unsigned char c = src[i];
        if (32 <= c && c < 127 && c != '\\')
        {
            dst += c;
        }
        else
        {
            char buf[5];
            _snprintf_s(buf, sizeof(buf), _TRUNCATE, "\\%02X", c);
            dst += buf;
        }
    }
}

void escape(string & dst, const string & src)
{
    escape(dst, &src[0], src.length());
}

template <typename T>
string to_string(const T & t)
{
    std::stringstream out;
    out << t;
    return out.str();
}

string str_printf(const char * format, ...)
{
    static volatile size_t static_size = 10;
    string buffer;
    size_t size = static_size;
    for (;;)
    {
        buffer.resize(size);
        va_list __argptr;
        va_start(__argptr, format);
        // since we use std::string, we don't care about null termination.
        // also, the upcoming standard (C++0x) demands that string contents were
        // indeed stored contiguously, while all known implementations already do.
#pragma warning (suppress: 4996) // no, I need _vsnprintf, not _vsnprintf_s.
        int len = _vsnprintf(&buffer[0], size, format, __argptr);
        va_end(__argptr);

        if (len >= 0)
        {
            size = min(min(size, (size_t)len + 3), 10);
            static_size = size;
            buffer.resize(len);
            return buffer;
        }
        size *= 2;
    }
}


void mylog(const char * type, const char * buffer, DWORD length)
{
    string s = str_printf("%s:%.*s\n", type, length, buffer);
    buffer = s.c_str();
    length = s.length();

    EnterCriticalSection(&log_cs);
    for (DWORD bytes_written = 0; bytes_written < length;)
    {
        DWORD n;
        if (!WriteFile(logfile, buffer + bytes_written, length - bytes_written, &n, NULL))
        {
            // Can't really report it here! Well, maybe with a messagebox...
            exit(1);
        }
        bytes_written += n;
    }
    LeaveCriticalSection(&log_cs);
}

void mylog(const char * type, const string & err)
{
    mylog(type, &err[0], err.length());
}


void myerror(const string & err, DWORD winerror = -1)
{
    char * msgptr = NULL;
    if (winerror == -1) winerror = GetLastError();

    FormatMessageA( 
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
            NULL,
            winerror,
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            (LPSTR)&msgptr,
            0,
            NULL 
            );

    if (msgptr)
    {
        int l = strlen(msgptr) - 1;
        while (l >= 0 && isspace(msgptr[l]))
        {
            msgptr[l] = '\0';
            l--;
        }
    }
    
    mylog("ERROR!", str_printf("%s (%s [%d])", err.c_str(), msgptr ? msgptr : "unknown error", winerror));
    if (msgptr) LocalFree(msgptr);
}

#define STRINGIFY1(x) #x
#define STRINGIFY(x) STRINGIFY1(x)
#define CHECK(action) do { if (!(action)) { myerror(__FILE__ ":" STRINGIFY(__LINE__) " \"" #action "\""); throw std::runtime_error( "An error" );}} while (0)

struct Connection
{
    const char * name;
    HANDLE read;
    HANDLE write;
};

unsigned int __stdcall transport_routine(__in void * lpParameter)
{
    Connection & con = *(Connection*)lpParameter;
    mylog(con.name, "Starting!");

    char buffer[512];
    DWORD bytes_read;
    string escaped;
    while (con.read && con.write)
    {
        // read
        if (!ReadFile(con.read, buffer, sizeof(buffer), &bytes_read, NULL))
        {
            DWORD err = GetLastError();
            if (err == ERROR_BROKEN_PIPE || err == ERROR_INVALID_HANDLE)
            {
                mylog(con.name, "<input closed>");
            }
            else
            {
                myerror(str_printf("Error while reading %s", con.name), err);
            }
            goto LABEL_BREAK;
        }
        if (bytes_read == 0)
        {
            // curiously enough, I get here only when I somewhat audaciously
            // close my own stdin handle.
            // But not always, that's why I need to check for ERROR_INVALID_HANDLE
            // up there too.
            mylog(con.name, "<EOF>");
            goto LABEL_BREAK;
        }
        escaped.assign(str_printf("[%d] ", bytes_read));
        escape(escaped, buffer, bytes_read);
        mylog(con.name, escaped);
        // Note that if we want to test it against Python, we have to use -u (unbuffered) 
        // flag, which then breaks Windows EOL handling in 2.7.2 and earlier, so for 
        // that purpose it might be necessary to remove all '\r' from the stream here.
        //convert_eols_to_unix(buffer, bytes_read);
        for (DWORD bytes_written = 0; bytes_written < bytes_read;)
        {
            DWORD n;
            if (!WriteFile(con.write, buffer + bytes_written, bytes_read - bytes_written, &n, NULL))
            {
                DWORD err = GetLastError();
                if (err == ERROR_BROKEN_PIPE)
                {
                    mylog(con.name, "<output closed>");
                }
                else
                {
                    myerror(str_printf("Error while writing %s", con.name), err);
                }
                goto LABEL_BREAK;
            }
            bytes_written += n;
        }
    }
LABEL_BREAK:
    CloseHandle(con.read);
    CloseHandle(con.write);
    return 0;
}

string get_exe_path()
{
    char buffer[MAX_PATH];
    DWORD n;
    CHECK(n = GetModuleFileNameA(NULL, buffer, sizeof(buffer)));
    int i = n - 1;
    for (; i >= 0; --i)
    {
        if (buffer[i] == '\\') break;
    }
    if (i > 0) return string(buffer, i);
    return string(buffer, n);
}

BOOL main_inner(int argc, const char *const *argv, const char *const *envp)
{
    InitializeCriticalSection(&log_cs);

    string exe_directory(get_exe_path()); 

    if (INVALID_HANDLE_VALUE == (logfile = 
        CreateFileA((exe_directory + "\\io_proxy.log").c_str(), GENERIC_WRITE, FILE_SHARE_READ, NULL, 
        OPEN_ALWAYS, 0, NULL)))
    {
        printf("Failed to open log: %d!\n", GetLastError());
        exit(1);
    }
    
    CHECK(INVALID_SET_FILE_POINTER != SetFilePointer(logfile, 0, NULL, FILE_END));

    string cmd = exe_directory + "\\io_proxy_target.exe";
    //const char * args = "dump_args.exe asddddddddddddddddddddddddddddddddddddddddddddd";
    //const char * args = "dir";
    //const char * args = "python -i";
    //const char * args = "io_proxy.exe";
    const char * args = GetCommandLineA();

    mylog("INFO", "executable directory: " + exe_directory);
    mylog("INFO", "commandline: " + string(args));

    HANDLE stdin_read, stdin_write, stdout_read, stdout_write, stderr_read, stderr_write;

    // Create pipes.

    CHECK(CreatePipe(&stdin_read, &stdin_write, NULL, 0));
    CHECK(CreatePipe(&stdout_read, &stdout_write, NULL, 0));
    CHECK(CreatePipe(&stderr_read, &stderr_write, NULL, 0));

    CHECK(SetHandleInformation(stdin_read, HANDLE_FLAG_INHERIT, HANDLE_FLAG_INHERIT));
    CHECK(SetHandleInformation(stdout_write, HANDLE_FLAG_INHERIT, HANDLE_FLAG_INHERIT));
    CHECK(SetHandleInformation(stderr_write, HANDLE_FLAG_INHERIT, HANDLE_FLAG_INHERIT));

    // Start threads
    Connection connections[] = {
        { "in ", GetStdHandle(STD_INPUT_HANDLE), stdin_write },
        { "out", stdout_read, GetStdHandle(STD_OUTPUT_HANDLE)},
        { "err", stderr_read, GetStdHandle(STD_ERROR_HANDLE)},
    };
    const int num_connections = sizeof(connections) / sizeof(Connection);

    HANDLE threads[num_connections] = {};

    for (int i = 0; i < num_connections; i++)
    {
        CHECK(NULL != 
            (threads[i] = (HANDLE)_beginthreadex(NULL, 4096, transport_routine, &connections[i], 0, NULL)));
    }
    
    mylog("INFO", "Threads launched");
    
    // Start process

    PROCESS_INFORMATION pi = {};
    STARTUPINFOA si = {};
    si.cb = sizeof(si);
    
    si.hStdInput = stdin_read;
    si.hStdOutput = stdout_write;
    si.hStdError = stderr_write;

    si.dwFlags = STARTF_USESTDHANDLES;

    char * args_copy = _strdup(args);
    if (!CreateProcessA(cmd.c_str(), args_copy, NULL, NULL, true,
        0, NULL, NULL, &si, &pi))
    {
        myerror("CreateProcess failed");
        return 1;
    }

    CHECK(CloseHandle(stdin_read));
    CHECK(CloseHandle(stdout_write));
    CHECK(CloseHandle(stderr_write));

    mylog("INFO", "Subprocess launched");


    DWORD wait_result =  WaitForSingleObject(pi.hProcess, INFINITE);

    if (wait_result != WAIT_OBJECT_0)
    {
        myerror(str_printf("WaitForMultipleObjects returned %d", wait_result));
    }

    mylog("INFO", "Subprocess terminated");

    // close the stdin handle. All other read handles should close automatically since 
    // the subprocess has terminated (but I can't force it, because there might be
    // some data left in the pipes!), so after that all transport threads should terminate.
    CloseHandle(connections[0].read);

    wait_result =  WaitForMultipleObjects(num_connections, threads, true, INFINITE);

    if (wait_result < WAIT_OBJECT_0 || wait_result >= WAIT_OBJECT_0 + num_connections)
    {
        myerror(str_printf("WaitForMultipleObjects returned %d", wait_result));
    }

    mylog("INFO", "All threads terminated");
    // why bother to actually close threads, we are quitting anyway!
    return TRUE;
}

int main(int argc, const char *const *argv, const char *const *envp)
{
    return main_inner(argc, argv, envp) ? 0 : 1;
}
