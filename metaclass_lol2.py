import functools
 
class Function(object):
    def __init__(self, f):
        self.f = f
    def __get__(self, instance, owner):
        # assigned to a class object
        if instance is None:
            # access via class
            return self.f
        # access via instance
        return functools.partial(self.f, instance)
    # enabling setter disables instance method overriding.
    def __set__(self, instance, value):
        print 'Setter, lol'
    def __call__(self, *args, **kwargs):
        # assigned to an instance
        return self.f(*args, **kwargs)
 
# to test that it works correctly
#def Function(f): return f
 
def meta_new(self, name, bases, attrs):
    print 'Meta', name, bases, attrs
    return type.__new__(Meta, name, bases, attrs)
 
 
class Meta(type):
    __new__ = Function(meta_new)
    
class Cls(object):
    __metaclass__ = Meta
    def __new__(cls):
        print 'Cls.__new__'
        return object.__new__(cls)
 
print 'class initialized'
 
o = Cls()
Cls.f = Function(lambda self: 'assigned to the class')
 
print o.f()
print Cls.f(o)
o.f = Function(lambda: 'assigned to the instance')
print o.f()
    
 