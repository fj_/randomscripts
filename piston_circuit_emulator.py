def print_led_digit(bits):
    assert len(bits) == 7
    it = iter(bits)
    def popbit(s):
        return s if it.next() == '1' else ' ' * len(s)
    print ' {} '.format(popbit('-' * 4))
    print '\n'.join(['{}    {}'.format(popbit('|'), popbit('|'))] * 2) 
    print ' {} '.format(popbit('-' * 4))
    print '\n'.join(['{}    {}'.format(popbit('|'), popbit('|'))] * 2) 
    print ' {} '.format(popbit('-' * 4))
    print

led_digits = [
        '1110111', # 0  0000
        '0010010', # 1  0001
        '1011101', # 2  0010
        '1011011', # 3  0011
        '0111010', # 4  0100 
        '1101011', # 5  0101
        '1101111', # 6  0110
        '1010010', # 7  0111
        '1111111', # 8  1000 
        '1111011', # 9  1001
        '1111110', # a  1010
        '0101111', # b  1011
        '1100101', # c  1100
        '0011111', # d  1101
        '1101101', # e  1110
        '1101100', # f  1111
        ]

for digit in led_digits:
    print_led_digit(digit) 
