using System;
using System.Windows.Forms;
using System.Web;

class CopyToClipboard
{
	[STAThreadAttribute]
	public static void Main(string[] argv)
	{
		string data;
		if (argv.Length > 0)
		{
			data = String.Join(" ", Array.ConvertAll<string, string>(argv, HttpUtility.UrlDecode));
		}
		else
		{
			data = Console.In.ReadToEnd();
		}
		Clipboard.SetText(data);
	}
}