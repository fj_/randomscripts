from string import Template
import getopt
import urllib
import sys
import webbrowser
import flickrapi
from subprocess import PIPE, Popen
import re

# settings

api_key = '862786ccd72a2abc35beca90ce16efd7'
api_secret = '7be5712875b412d7'
hijackWebbrowser = False
copyToClipboard = True
copyViaPipe = True
uploadAsPrivate = True

def HijackedWebbrowserCmdLine(url) :
    return [#'echo',
            'c:\\program files\\mozilla firefox\\firefox.exe',
            '-new-tab',
            url]

def CopyToClipboardCmdLine(text = None) :
    cmd = [#'echo',
        'c:\\program files\\FlickrUploader\\CopyToClipboard.exe']
    if text:
        cmd.append(text)
    return cmd

# utility functions

def EncodeParams(t):
    if isinstance(t, basestring):
        return re.sub("[&]", r"^\g<0>", t).encode('mbcs', 'strict')
    else:
        return [EncodeParams(s) for s in t]

def ShellExec(params, waitForCompletition = True, encodeParams = False, dataToSend = None):
    cmd = Popen(
        EncodeParams(params) if encodeParams else params,
        shell = True,
        stdin = PIPE, stdout = PIPE, stderr = PIPE)
    if waitForCompletition:
        return cmd.communicate(dataToSend)

def CopyToClipboard(code):
    if copyViaPipe:
        print ShellExec(CopyToClipboardCmdLine(), True, True, code)
    else:
        print ShellExec(CopyToClipboardCmdLine(urllib.quote(code)), True, True)
    
###########
    
class FlickrUploader:
    def __init__(self, clearCache = False):
        
        if hijackWebbrowser:
            def open(url, new, autoraise):
                print "Hijacked webbrowser.open:", str(url)
                ShellExec(HijackedWebbrowserCmdLine(url), False, True)
            savedOpen = webbrowser.open 
            webbrowser.open = open

        self.flickr = flickrapi.FlickrAPI(api_key, api_secret, format='etree')
        
        if (clearCache):
            print "Clearing the token cache..."
            self.flickr.token_cache.forget()

        print "Obtaining a token..."
        (token, frob) = self.flickr.get_token_part_one(perms='write')

        if hijackWebbrowser:
            webbrowser.open = savedOpen
            
        if not token: raw_input("Press ENTER after you authorized this program")
        token = self.flickr.get_token_part_two((token, frob))
        print "Done"
               
    def Upload(self, filename, photoId = None):
        if (photoId == None):
            def callback(percent, done):
                sys.stdout.write(".")
            print "Uploading: ", filename
            photoId = self.flickr.upload(filename, callback, 
                is_public = (0 if uploadAsPrivate else 1)).photoid[0].text
        print "Image uploaded, id =", photoId
        
        sizes = self.flickr.photos_getSizes(photo_id = photoId)[0]
        ss, sm, sl = None, None, None
        for size in sizes:
            attr = dict(size.items())
            if attr["label"] in ("Large", "Original"):
                sl = attr 
            elif attr["label"] == "Medium":
                sm = attr 
            elif attr["label"] == "Thumbnail":
                ss = attr
                
        def PrintSize(attr):
            if attr == None: return
            print "=====" + attr["label"].ljust(20, "=")
            
            res = Template('<a href="$url"><img src="$source" width="$width" height="$height"/></a>').substitute(attr)
            print res
            return (res, attr["source"])
        
        PrintSize(ss)
        PrintSize(sm)
        slcode = PrintSize(sl)
        
        print "="*25
        return slcode
#########
if __name__ == '__main__':
    opts, args = getopt.getopt(sys.argv[1:], "pc")
    pauseAfterExecution = ("-p", "") in opts
    clearCache = ("-c", "") in opts
    links = []
    
    uploader = FlickrUploader(clearCache)
    for s in args:
        print s 
        links.append(uploader.Upload(s))
    
    print "\n"
    for link in links:
        print link[1]
    print  "\n" + "="*25 + "\n"
    
    if copyToClipboard and len(links) > 0:
        print "Sending the code(s) for the large size(s) to the clipboard"
        CopyToClipboard("\r\n".join([link[0] for link in links])) 
        print "Done!"
    
    if pauseAfterExecution:
        raw_input("Press ENTER to quit") 
