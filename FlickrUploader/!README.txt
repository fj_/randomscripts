Description of the FlickrUploader utility, by Fj, last changed 21.05.2008 19:59


FlickrUploader is a command line utility for uploading images to Flickr, created primarily to suit my own needs, so if you are totally foreign to programming, you'd better not use it in case something goes wrong.

Installation: 
Download and install Python from http://www.python.org/download/ if you don't have it or have a version older than 2.5
Download and install .NET framework 2.0 from http://www.microsoft.com/downloads/details.aspx?FamilyID=0856EACB-4362-4B0D-8EDD-AAB15C5E04F5 if you don't have it (check C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\ if in doubt).
Create a folder "c:\program files\FlickrUploader" (spelled exactly like that!), extract/copy all files there, run "install.cmd".


Usage: 
1) Run "FlickrUploader <one or more image file names>" from the command line OR right-click on an image and select "Upload to Flickr" in the context menu OR select some images, right-click and choose "Send to -> Flickr.cmd".
2) If it's the first time you run the program, Flickr authorization webpage will be opened in the default browser, login to Flickr if needed, authorize the program, then alt-tab back to the script and press ENTER.
3) The program will complete logging-in and upload given file(s), displaying HTML code for thumbnail/medium/large sized versions of each. After all images were uploaded, it will copy the large-size codes into the clipboard and prompt for pressing ENTER again (unless if started from the command line).
4) ???
5) PROFIT!

Important notice: you can select multiple files and use "Upload to Flickr" command from the menu, but due to the specific Windows behaviour each file will be uploaded in its own command window and only one link will make it to the clipboard. Use "Send to" for uploading multiple files.

Uploaded images have Private status by default, yet HTML codes work anyway since they're using a direct link. Mind, however, that when you change the privacy status of an image the link goes broken.



Grim details (if something went wrong or if you want to customize stuff):

The program (i.e. the FlickrUploader.py script) uses flickrapi module to interact with Flickr API. I've put all necessary sources in the program directory to avoid explicit installation, though one may go to http://flickrapi.sourceforge.net/, download and install the latest version etc.

First of all, it took me some time to understand how does Flickr API authentication work. There are two keys, used to sign data, api_key and api_secret in the code. By the way: please, please, don't do anything at all with them, you can receive your very own instantly by going to http://www.flickr.com/services/api/keys/, there is no premoderation, so if you want to modify my program or create your own, you'd better do it and save me from the embarassment of having to acquire a new pair if you accidentally provoke a block.
So, an application calls some FlickrAPI function, receives session identifier, calls some other function, receives an url for user confirmation, the user goes and confirms the request, the application caches session identifier (in "%USERPROFILE%/.flickr" if you wondered) and goes on signing each API call with a certain combination of these three identities. If there was already a cached session identifier then the application verifies its validity instead and goes on immediately.
Everything is quite simple, no browser cookies or user password are involved etc. And, if I understand correctly, if (and only if) any other (malicious) application knows the cached session id, then it can fully impersonate our application, so, well, have it in mind.

In the beginning of the FlickrUploader.py script there are some settings, which need explaining:

api_key and api_secret: already explained above

hijackWebbrowser: I use Opera as primary browser and Firefox in a high sequrity mode, with cache/cookies clear on exit etc, so if I want to use other than my default Flickr account for uploading to, I can set this to True, construct the command line for Firefox invokation in the HijackedWebbrowserCmdLine function and the program will substitute this invokation for the standard "webbrowser.open" during the FlickrAPI logon. Note, however, that Python's Popen on Windows is slightly less stable than a box of stale dynamite: quotes around parameters (needed for paths with spaces) appear and disappear randomly, "&"s outside of quotes are treated as conditional execution operators etc, so getting it all right requires sertain trial and error.

copyToClipboard: copy HTML codes for the images to the clipboard. Disable if you don't have .NET framework 2.0  nor Mono

copyViaPipe: should be set to True, otherwise codes are passed to the CopyToClipboard.exe as command line parameters and there is a limit for command line length (10 codes were passed successfully, 100 were not)

uploadAsPrivate: True to have images to have Private visibility, False for Public. Private images are still visible when pointed at by a direct link (and when embedded in <img/> tag with a direct link) but do not show up in the Flickr photostream (unfortunately, they don't have a separate option for that). Changing visibility later breaks the links!

Also, the program recognizes two command line parameters:
-p causes script to wait for ENTER after the completition, so that command window doesn't disappear immediately.
-c clears cache and forces the reacquisition of the user permission.


Other files:
CopyToClipboard.cs/exe: copies parameters from the command line to the clipboard (performing url-decoding first, i.e. "%20"s are changed to spaces and so on), if none supplied uses stdin as a source. It compiles and works well on Mono for Windows, don't know about other platforms though. Btw C# compiler is included in the framework, so run "csc CopyToClipboard.cs" to recompile it.

__init__.py: well... I'm a bit confused about it, I used it during debugging to load the whole folder as a module in the interpreter, and that strange "import as" inside is for being able to reload FlickrUploader.py itself. It is not needed now and I have a feeling that there might be other, cleaner way of doing it.

install.cmd and stuff: it goes in the simplest way possible (and entirely sufficient for my own humble needs).
"Flickr.cmd" gets copied to the SendTo folder of the current user and invokes the script passing all the parameters (and -p switch).
"FlickrUploader.cmd" gets copied to the System32 folder in the Windows installation, it's in the PATH, so the command line invokation is enabled from anywhere.
"install to context menu.reg" gets executed silently and associates the script with image files (in a clean way, not damaging anything else). If you want to uninstall it, run regedit and delete HKEY_CLASSES_ROOT\SystemFileAssociations\image\shell\edit.FlickrUploader


If you want to install it not in the "c:\program files\FlickrUploader", you'll have to change paths in the following files: "Flickr.cmd", "FlickrUploader.cmd", "install to context menu.reg" and in the function CopyToClipboardCmdLine in "FlickrUploader.py"; then run install.cmd again.

If you have any questions (except ones like "where I download regedit?"), suggestions or thanks, you can reach me by email: fj.mail@gmail.com
