from binascii import hexlify
import sys
import re
from itertools import product

print('eh')

def read_pi():
    with open('d:/pi/Pi - Dec - Chudnovsky.txt') as f:
        f.read(2) # skip "3."
        s = f.read()
        print(len(s))
        return s

def read_pi_hex():
    with open('d:/pi/Pi - Hex - Chudnovsky.txt') as f:
        f.read(2) # skip "3."
        s = f.read()
        print(len(s))
        return s
    
def rand_capitalize(word):
    for caps in product(*([[0, 1]] * len(word))):
        yield ''.join(c.upper() if up else c for c, up in zip(word, caps))

pi = read_pi()
for word in ['unson'.upper()]:
    digits = list(ord(c) for c in word)
#     digits = list('{:02}'.format(ord(c)) for c in word)
    s = ''.join(map(str, digits))
    print(word, digits, s) 
    print(word, s, pi.find(s)) 
    
# print(re.search('00000000', s).start())           
