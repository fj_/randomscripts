import httplib, urllib, re, time
source = """
print "lol"
"""
# address = 'ideone.com:80' 
# address = '153.19.131.21:80' # DNS is closed.
address = '127.0.0.1:80' 

# for testing
location = None
# location = '/9r9az7zB'
# location = '/OfiZKZ5Z' 

if not location:
	params = urllib.urlencode({'lang': 4, 'file':source, 'run':1})
	headers = {"Content-type": "application/x-www-form-urlencoded",
	           "Accept": "text/plain"}
	conn = httplib.HTTPConnection(address)
	conn.request("POST", "/ideone/Index/submit", params, headers)
	response = conn.getresponse()
	status, location = response.status, response.getheader('location')
	print response.status, response.reason, 'redirect to:', location, response.getheaders()
	data = response.read()
	print data
	conn.close()

	assert status == 302 # moved

print 'Getting results...',

# wait a bit, for the app to run
time.sleep(1.0)

res = urllib.urlopen('http://' + address + location).read()

# quick and dirty extract output
match = re.search('<label>output:</label>\s*<pre class="box">'
				+ '(.*)'
				+ '</pre>\s*</div>\s*<div id="err'
				, res, re.DOTALL)
if match: 
	print 'Success\n', '=' * 80, '\n'
	print match.group(1)
else:
	print 'Something is wrong\n', '=' * 80, '\n'
	print res


