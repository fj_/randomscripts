from itertools import product
import heapq
from pprint import pprint

def souls_for_level(level):
    return level * (level + 1) / 2

def mimzee_bonus_with_max_dora():
    # assuming maxed Dora.
    # real_amount = amount * (0.81 + 0.11 * 10 * (1 + 0.5 * level)) 
    # real_amount = amount * (1.91 + 0.55 * level)
    # base_amount = amount * 1.91
    # bonus = (1.91 + 0.55 * level) / 1.91 = 1 +
    return 0.55 / 1.91

#print mimzee_bonus_with_max_dora()

class Bonuses(object):
    mammon = 0.05
    syalatas = 0.25
    libertas = 0.25
    mimzee = 0.287958115183
    

def chest_bonus(dora_level, mimzee_level):
    base_bonus = 1.0 + 0.01 * 10
    probability = 0.01 * (1 + 0.2 * dora_level)
    reward = 10 * (1 + 0.5 * mimzee_level)
    return probability * reward / base_bonus 


def multiplicative_bonus(souls, bonus_per_level, current_level = 0):
    for ancient_level in xrange(current_level, current_level + 100):
        ancient_souls = souls_for_level(ancient_level) - souls_for_level(current_level)
        free_souls = souls - ancient_souls
        if free_souls < 0: break        
        multiplier_1 = 1.0 + 0.1 * free_souls
        multiplier_2 = 1.0 + bonus_per_level * ancient_level
        total = multiplier_1 * multiplier_2
        print 'lvl: {:>2}, +souls: {:>2}, free souls = {:>2}, soul multiplier = {:1.4}, bonus multiplier = {:1.4}, total = {:1.7}x'.format(
                ancient_level, ancient_souls, free_souls, multiplier_1, multiplier_2, total)
    print


def gild_bonus(gilds):
    return 1 + 0.5 * gilds


def optimal_gilds(dps, gilds, mem={}):
    key = (dps, gilds) 
    if key in mem:
        return mem[key]
    
    dps, rest = dps[0], dps[1:]
    if not rest:
        best_dps = dps * gild_bonus(gilds) 
        best_solution = [gilds]
    else:
        best_dps = 0
        for g in xrange(0, gilds + 1):
            rest_dps, rest_solution = optimal_gilds(rest, gilds - g, mem)
            total_dps = rest_dps + dps * gild_bonus(g)
            if best_dps < total_dps:
                best_dps = total_dps
                best_solution = [g] + rest_solution
    mem[key] = (best_dps, best_solution)
    return best_dps, best_solution
             

def main():
    #multiplicative_bonus(18000, Bonuses.syalatas, 70)
    
    #multiplicative_bonus(3000, Bonuses.mammon, 30)
    
    #multiplicative_bonus(3000, Bonuses.mimzee, 40)
    
    #for d, m in product((23, 24, 25), (26, 27)):
    #    print d, m, chest_bonus(d, m)
    
    tb_dps = 4.62e67 / (37 * 0.5 + 1)
    ivan_dps = 5.51e67 / (33 * 0.5 + 1)
    s_dps = 9.91e66 / (1 * 0.5 + 1)
    
    mem = {}
    print optimal_gilds((tb_dps, ivan_dps, s_dps), 73, mem)
    print heapq.nlargest(10, mem.itervalues(), lambda (k, v): v[0])
    pprint(mem) 
    
if __name__ == '__main__':
    main()