"""Provides some statements as functions.
"""
import __builtin__

__all__ = ('print_', 'raise_')

print_ = getattr(__builtin__, 'print')

def raise_(exc, value = None, tb = None):
    raise exc, value, tb 