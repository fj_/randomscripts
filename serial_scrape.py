# adapted from https://github.com/rhelsing/worm_scraper
from scraperlib import Scraper
import lxml.html
from collections import namedtuple
from pprint import pprint
import re

class StartingUrls(object):
    pact = 'https://pactwebserial.wordpress.com/category/story/arc-1-bonds/1-01/'
    twig = 'https://twigserial.wordpress.com/2014/12/24/taking-root-1-1/'
    worm = 'https://parahumans.wordpress.com/2011/06/11/1-1/'

scraper = Scraper(request_interval=0.5)

def single(lst):
    assert len(lst) == 1
    return lst[0]


Chapter = namedtuple('Chapter', 'title content next')
def get_chapter(url):
    text = scraper.get(url)
    doc = lxml.html.fromstring(text)

    chapter_title = single(doc.cssselect('h1.entry-title')).text_content()

    next_chapter_elt = doc.cssselect('div.entry-content p a')[-1]
    if 'Next' in next_chapter_elt.text_content():
        next_chapter = next_chapter_elt.get('href')
        if next_chapter.startswith('//'):
            next_chapter = "https:" + next_chapter
    else:
        next_chapter = None

    chapter_content = single(doc.cssselect('div.entry-content'))

    return Chapter(chapter_title, chapter_content, next_chapter)

def pprint_elts(elts):
    for it in elts:
        print('{}: {!r}'.format(it, it.text_content()))

evil_contents = set()
def cleanup(chapter):
    # cleanup
    content = chapter.content

    def rm(elt, silent=False):
        if not silent:
            evil_contents.add(elt.text_content())
        elt.getparent().remove(elt)

    # various "Share" buttons
    for it in content.xpath('.//div'):
        rm(it)

    # Previous/next chapter div
    elt = content.cssselect('div.entry-content p')[0]
    s = elt.text_content()
    if (s == 'Next Chapter' or
            re.match('^Last Chapter.*Next Chapter$', s) or
            re.match('^Last Chapter.*End$', s)):
        # known bad content, remove silently
        rm(elt, True)
    else:
        print(s)
        pprint_elts(content.cssselect('div.entry-content p')[0:3])
        assert False

    return chapter._replace(content=content)
#     @to_remove = doc.css('div.entry-content p').first #gsub first p
#     @chapter_content = @chapter_content.to_s.gsub(@to_remove.to_s,"")

def get_chapters(starting_url):
    url = starting_url
    while url is not None:
        chapter = get_chapter(url)
        url = chapter.next
        yield chapter


def main():
    for chapter in get_chapters(StartingUrls.worm):
        print(chapter.title)
        cleanup(chapter)

    pprint(evil_contents)



if __name__ == '__main__':
    main()


