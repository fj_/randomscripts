#!/usr/bin/env python2.7

from os import path as os_path
import mmap
import time
from contextlib import closing
from collections import namedtuple
import re

class Time_interval_measurer(object):
    def __init__(self, tag = None):
        self.tag = tag
        self.cnt = 0
        self.max = 0.0
        self.total = 0.0
        self.prev_time = None
    @property
    def avg(self):
        return self.total / self.cnt
    def start(self):
        assert self.prev_time is None, 'Timer already started'
        self.prev_time = time.time()
    def stop(self):
        assert self.prev_time is not None, 'Timer not started'
        dt = time.time() - self.prev_time
        self.prev_time = None
        self.max = max(self.max, dt)
        self.total += dt
        self.cnt += 1
    

def time_multipart_function(f, description, results):
    timers = [Time_interval_measurer(res) for res in results]
    timer_total = Time_interval_measurer('total')
    iterations = 10
    for _ in range(iterations):
        it = f()
        timer_total.start()
        for timer in timers:
            timer.start()
            res = next(it)
            timer.stop()
            assert res == timer.tag, 'Expected {!r}, got {!r}'.format(timer.tag, res)
        timer_total.stop()
        sentinel = object()
        res = next(it,  sentinel)
        assert res is sentinel, 'Expected stop iteration, got {!r}'.format(res) 
    
    print '{}x iterations on {}:'.format(iterations, description)
    timers.append(timer_total)
    for timer in timers:
        print '{}: avg = {:2.5f}, max = {:2.5f}'.format(timer.tag, timer.avg, timer.max)
    print
    
def scan_str_factory(fname, s, binary = False):
    def wrapped():
        with open(fname, 'r' + ('b' if binary else 'U')) as f:
            data = f.read()
            yield 'opening'
            yield data.find(s) 
    return wrapped

def scan_str_mmap_factory(fname, s):
    def wrapped():
        with open(fname, 'r') as f:
            with closing(mmap.mmap(f.fileno(), 0, access = mmap.ACCESS_READ)) as data:
                yield 'opening' 
                yield data.find(s) 
    return wrapped

def re_str_factory(fname, s, binary = False):
    rx = re.compile(re.escape(s))
    def wrapped():
        with open(fname, 'r' + ('b' if binary else 'U')) as f:
            data = f.read()
            yield 'opening'
            yield rx.search(data).start() 
    return wrapped

def re_str_mmap_factory(fname, s):
    rx = re.compile(re.escape(s))
    def wrapped():
        with open(fname, 'r') as f:
            with closing(mmap.mmap(f.fileno(), 0, access = mmap.ACCESS_READ)) as data:
                yield 'opening' 
                yield rx.search(data).start() 
    return wrapped

def read_str_factory(fname, binary = False):
    def wrapped():
        with open(fname, 'r' + ('b' if binary else 'U')) as f:
            data = f.read()
        yield len(data)
    return wrapped

def read_str_mmap_factory(fname, binary = False):
    def wrapped():
        with open(fname, 'r') as f:
            with closing(mmap.mmap(f.fileno(), 0, access = mmap.ACCESS_READ)) as data:
                data = data.read(len(data))
                yield len(data)
    return wrapped
    
def make_test_file(fname, item_count):
    if not os_path.exists(fname):
        print 'creating test file {!r}'.format(fname)
        with open(fname, 'wb') as f:
            for i in range (item_count):
                f.write('{:08d}\r\n'.format(i))
        print 'done'


def main():
    Params = namedtuple('Params', 'file_name item_count search_str res_text res_binary')
    files = (
             Params('test_mmap_small.txt',  1000000, '00999999',  8999991,  9999990),
             Params('test_mmap_big.txt',   10000000, '09999999', 89999991, 99999990),
            )
    for params in files:
        make_test_file(params.file_name, params.item_count)
    print 'testing'

    def test_stuffs(args):
        time_multipart_function(scan_str_factory(args.file_name, args.search_str),
                                'open().read()', 
                                ['opening', args.res_text])
        time_multipart_function(scan_str_factory(args.file_name, args.search_str, True),
                                'open(as binary).read()', 
                                ['opening', args.res_binary])
        time_multipart_function(scan_str_mmap_factory(args.file_name, args.search_str),
                                'mmap', 
                                ['opening', args.res_binary])

    def test_re_stuffs(args):
        time_multipart_function(re_str_factory(args.file_name, args.search_str),
                                'open().read()', 
                                ['opening', args.res_text])
        time_multipart_function(re_str_factory(args.file_name, args.search_str, True),
                                'open(as binary).read()', 
                                ['opening', args.res_binary])
        time_multipart_function(re_str_mmap_factory(args.file_name, args.search_str),
                                'mmap', 
                                ['opening', args.res_binary])

    def test_read_stuffs(args):
        time_multipart_function(read_str_factory(args.file_name),
                                'open().read()', 
                                [args.res_text + 9])
        time_multipart_function(read_str_factory(args.file_name, True),
                                'open(as binary).read()', 
                                [args.res_binary + 10])
        time_multipart_function(read_str_mmap_factory(args.file_name),
                                'mmap', 
                                [args.res_binary + 10])
    for params in files:
        test_re_stuffs(params)

if __name__ == '__main__':
    main()