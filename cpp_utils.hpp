#include <windows.h>
#include <string>
#include <sstream>

using std::string;
using std::stringstream;

template <typename T>
string to_string(const T & t)
{
    std::stringstream out;
    out << t;
    return out.str();
}



string str_printf(const char * format, ...)
    __attribute__ ((format (printf, 1, 2)));

static string str_printf(const char * format, ...)
{
    string buffer;
    size_t size = 128;
    for (;;)
    {
        buffer.resize(size);
        va_list __argptr;
        va_start(__argptr, format);
        // since we use std::string, we don't care about null termination.
        // also, the upcoming standard (C++0x) demands that string contents were
        // indeed stored contiguously, while all known implementations already do.
        int len = vsnprintf(&buffer[0], size, format, __argptr);
        va_end(__argptr);

        if (len < 0)
        {
            // old-style snprintf return -1 if insufficient size
            size *= 2;
            continue;
        }
        if ((size_t)len >= size)
        {
            // C99 snprintf returns required buffer size
            size = len + 1;
            continue;
        }
        buffer.resize(len);
        return buffer;
    }
}

string format_last_error_str(const string & activity)
{
    char * msgptr;
    int last_error = GetLastError();
    DWORD chars = FormatMessageA( 
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
        last_error,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
        (LPSTR)&msgptr,
        0,
        NULL 
        );

    stringstream ss;
    if (chars == 0)
    {
        ss << "\"" << activity << "\" failed (" << last_error << 
            "), then FormatMessage failed too (" << GetLastError() << ")!";
        return ss.str();
    }
    else
    {
        ss << "\"" << activity << "\" failed (" << last_error << 
                "): " << string(msgptr, chars);
    }
    return ss.str();
}

#define CHECK_NZ(src) do {if (0 == (src)) { \
    MessageBoxA(NULL, format_last_error_str(#src).c_str(), "Error!", 0); \
    abort(); } } while (0)

static inline void safe_close_handle(HANDLE & handle)
{
    if (handle)
    {
#if _DEBUG
        CHECK_NZ(CloseHandle(handle));
#else
        CloseHandle(handle);
#endif
        handle = NULL;
    }
}

template <typename T, int len>
static inline int array_length(const T (&arr)[len])
{
    return len;
}

static string py_substring(const string & s, int start, int end = INT_MAX)
{
    if (start < 0) start += s.length();
    if (start < 0) start = 0; // say we want "abc"[-10:]
    if (end < 0) end += s.length();
    if (end > (int)s.length()) end = s.length();
    if (start >= end) return "";
    return s.substr(start, end - start);
}

template <typename TKey, typename TValue>
TValue * find(std::map<TKey, TValue> & map, const TKey & key)
{
    typename std::map<TKey, TValue>::iterator it(map.find(key)); // o_O
    if (it != map.end())
    {
        return &it->second;
    }
    return NULL;
}

template <typename TKey, typename TValue>
bool find(std::map<TKey, TValue> & map, const TKey & key, TValue & value)
{
    TValue * val = find(map, key);
    if (NULL == val) return false;
    value = *val;
    return true;
}
