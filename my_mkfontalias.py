#!/usr/bin/python
#
# This is a short python utility to assist people in de-uglification of 
# their true type fonts. Basically, creates a fonts.alias file from the 
# fonts.dir file found in the directory.
#
# Consider this software to be under GPL.
#
# Roman Sulzhyk, Starmedia Networks, 2000. roman@starmedia.net
# --
# 8 Feb 2008: Lower case needed in format names when using Xming tools.
# Colin Harrison colin <dot> harrison <at> virgin <dot> net
# --
# 20.05.2000: Added common MS webfonts as fontnames to map:
# Verdana, Tahoma, Impact, Arial Black, Comic Sans MS, Georgia, Trebuchet MS.
# R.K.Aa. dark@c2i.net 
# --
# 21.05.2000: Added 'cheating' - sizes 6,7 and 8 now map to 9 point fonts.
# --
import sys, string, os

_font_sizes = range(6, 17) + [ 18, 24 ]
_infile = 'fonts.dir'
_outfile = 'fonts.alias'

# Resolution
_res = [ '75', '75' ]

# Do 'cheating' to make sizes 6,7,8 as 9 bit fonts
_cheat_map = { 6 : 9,
               7 : 9,
               8 : 9 }

# The fonts to map. Format name : alias
_font_map = { 
        'consolas' : 'Consolas',
        'arial' : 'Arial',
        'times new roman' : 'Times New Roman',
        'verdana' : 'Verdana',
        'tahoma' : 'Tahoma',
        'impact' : 'Impact',
        'arial black' : 'Arial Black',
        'comic sans ms' : 'Comic Sans MS',
        'georgia' : 'Georgia',
        'trebuchet ms' : 'Trebuchet MS',
        'courier new' : 'Courier New' }

# Read in the fonts.
try:
    # Strip the first line
    fonts = open ( _infile ).readlines()[1:]
except IOError, val:
    sys.stderr.write ( 'Cannot read %s (%s) - are you sure you are in the '
                       'fonts directory?\n' % (_infile, val) )
    sys.exit(1)

# Now, create the output
_aliases = []
for line in fonts:
    try:
        # Get rid of the first entry, but mind that other may have 
        # spaces in them
        font = string.strip(string.join ( string.split ( line, ' ' )[1:], ' '))
    except IndexError:
        sys.stderr.write ( 'Cannot parse %s line: %s\n' % (_infile, line ) )
        sys.exit(1)

    entries = string.split ( font, '-' )

    if len(entries) != 15:
        # Seems to be invalid
        sys.stderr.write ( 'Invalid font: %s\n' % (font) )
        sys.exit(1)

    name = entries[2]

    map = _font_map.get ( name, None )

    if map:
        # Create a bunch of aliases, for each size
        for size in _font_sizes:
            # Do the 'cheating' - fallback to size if not in the cheat map
            real_size = _cheat_map.get ( size, size )

            name = string.join ( entries[:7] + [ str(real_size), 
                                                 str(real_size * 10) ] + 
                                 entries[9:], '-' )

            alias = string.join ( entries[:2] + [map] + entries[3:7] + 
                                 [ str(size), str(size * 10) ] + 
                                  _res + entries[11:], '-' )

            # Add the entry to the aliases
            _aliases.append ( '"%s" "%s"' % (alias, name) )

# Boast
print 'Created %s aliases' % len(_aliases)

# Backup the existing file
_bak = _outfile + '.bak' 
if os.path.exists ( _outfile ) and not os.path.exists ( _bak ):
    try:
        os.rename ( _outfile, _bak )
    except OSError, val:
        sys.stderr.write ( 'Cannot backup %s to %s: %s\n' % (_outfile, _bak, 
                           val) )
        sys.exit(1)
    else:
        print 'Backed up existing %s to %s' % (_outfile, _bak) 

# Ok, write the file
try:
    _out = open ( _outfile, 'w' )
except IOError, val:
    sys.stderr.write ( 'Cannot open %s for writing: %s\n' % (_outfile, val) )
    sys.exit(1)

_out.write ( string.join ( _aliases, '\n' ) )
_out.close()

print 'Wrote aliases to %s' % _outfile
