# -*- coding: utf-8 -*-

from pprint import pprint


class Node(object):
    def __init__(self, data, left, right):
        self.data = data
        self.left = left
        self.right = right
    def __repr__(self):
        return str(self.data)

def tuplize(node):
    return (node.data, tuplize(node.left), tuplize(node.right)) if node else None

        
    

test_tree = Node(1, 
                 Node(2, 
                      Node(3, Node(4, None, None), None), 
                      Node(5, None, Node(6, None, None)),
                      ),
                 Node(7, None, None)
                 )

test_tree = Node(1, 
                 Node(2, 
                      Node(3, None, None), 
                      None),
                 Node(4, None, None)
                 )

test_tree = Node(1, 
                 Node(2, 
                      Node(4, 
                          Node(8, None, None), 
                          Node(9, None, None)),
                      Node(5, 
                          Node(10, None, None), 
                          Node(11, None, None))),
                 Node(3, 
                      Node(6, 
                          Node(12, None, None), 
                          Node(13, None, None)),
                      Node(7, 
                          Node(14, None, None), 
                          Node(15, None, None))),
                 )

def walk(root):
    pprint(tuplize(root))
    visited = set()
    sentinel = Node(0, root, None);
    p1 = sentinel; p2 = root;
    while p2 != sentinel:
        print p2
        visited.add(p2)
        if p2.left:
            p2.left, p2.right, p1, p2 = p2.right, p1, p2, p2.left 
        elif p2.right:
            visited.add(p2)
            print p2
            p2.right, p2.left, p1, p2 = p2.left, p1, p2, p2.right  
        else:
            visited.add(p2)
            print p2
            p1, p2 = p2, p1
    print sorted(i.data for i in visited)
    
    pprint(tuplize(root))

walk(test_tree)

#####################



class Node(object):
    def __init__(self, data, left, right):
        self.data = data
        self.left = left
        self.right = right
    def has_single_child(self):
        return bool(self.left) + bool(self.right) == 1 # xor
    def get_single_child(self):
        assert self.has_single_child()
        return self.left or self.right
    def set_single_child(self, value):
        assert self.has_single_child()
        if self.left: self.left = value
        else: self.right = value
    single_child = property(get_single_child, set_single_child)
    def __repr__(self):
        return str(self.data)
        
    

test_tree = Node(1, 
                 Node(2, 
                      Node(3, Node(4, None, None), None), 
                      Node(5, None, Node(6, None, None)),
                      ),
                 Node(7, None, None)
                 )

test_tree = Node(1, 
                 Node(2, 
                      Node(3, None, None), 
                      None),
                 Node(4, None, None)
                 )

def walk(node):
    fake_root = Node('fake root', node, None)
    
    def trampoline(node, prev_node):
        while node:
            print 'Node:', node, 'Prev_node:', prev_node
            node, prev_node = rec(node, prev_node)
    
    def is_child(node, prev_node):
        return prev_node and (prev_node.left is node or prev_node.right is node)
    
    def rec(node, prev_node):
        # end?
        if node is fake_root:
            print 'end'
            return None, None
        
        # no children
        if not (node.left or node.right):  
            print 'Data:', node.data
            return prev_node, node
        
        # single child
        if node.has_single_child(): 
            print 'single child'
            print node, prev_node, node.single_child
            if prev_node is node.single_child: # go up
                # restore 
                prev_node, node.single_child = node.single_child, prev_node
                return prev_node, node
            else: # go down
                print 'Data:', node.data
                # setup
                next_node, node.single_child = node.single_child, prev_node
                return next_node, node
        
        # two children
        if prev_node is node.left: # go right
            print 'two children, go right'
            # restore
            prev_node, node.left = node.left, prev_node
            # setup
            next_node, node.right = node.right, prev_node
            return next_node, node
        elif prev_node is node.right: # go up
            print 'two children, go up'
            # restore 
            prev_node, node.right = node.right, prev_node
            return prev_node, node
        else: # go left
            print 'two children, go left'
            print 'Data:', node.data
            # setup
            next_node, node.left = node.left, prev_node
            return next_node, node

    trampoline(fake_root.left, fake_root)

walk(test_tree)