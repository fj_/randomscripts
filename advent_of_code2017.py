#!/usr/bin/env python3.6

from pathlib import Path
from collections import Counter, defaultdict, deque, namedtuple
from pprint import pprint, pformat
from typing import Sequence, Tuple, Callable, List, Union, NamedTuple
import itertools, re, functools, shutil, errno, sys, time, hashlib, json, ast, string
import requests
import numpy as np
import networkx as nx
from blist import blist

# line_profiler support
try:
    profile
except:
    def profile(f):
        return f

quiet = False


def npv2(x, y):
    return np.array([x, y])


def args_to_int(f):
    @functools.wraps(f)
    def wrapper(*args):
        return f(*map(int, args))
    return wrapper


def gcd(a, b):
	if a < b: a, b = b, a
	while b:
		a, b = b, a % b
	return a


def pad_map_with_border(mm, sentinel=None):
    '''Pad a rectangular map with a border made of `None`s or provided sentinel values'''
    width = len(mm[0])
    assert all(width == len(row) for row in mm)
    res = []
    res.append([sentinel for _ in range(width + 2)])
    for row in mm:
        rr = [sentinel]
        rr.extend(row)
        rr.append(sentinel)
        res.append(rr)
    res.append([sentinel for _ in range(width + 2)])
    return res


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args, fillvalue=fillvalue)


def log_seq(seq):
    for n, it in enumerate(seq):
        print(n, it)
        yield it


# cw starting from north, y points down.
directions4 = ((0, -1), (1, 0), (0, 1), (-1, 0))
# for dx, dy in directions4:
#     nx, ny = x + dx, y + dy

def addv2(v1, v2):
    return v1[0] + v2[0], v1[1] + v2[1]


class AnswerDb:
    def __init__(self, name):
        self.file = Path(f'input/{name}_answerdb.txt')
        self.warnings = []

    def load(self):
        if not self.file.exists():
            return {}
        return json.loads(self.file.read_text())

    def save(self, answers):
        self.file.write_text(json.dumps(answers, sort_keys=True, indent=4))

    def answer(self, id, answer):
        assert answer is not None
        answers = self.load()
        orig = answers.get(id)
        if orig is not None and answer != orig:
            warning = f'Different answer for {id!r}, was:\n{pformat(orig)}\nnew:\n{pformat(answer)}'
            self.warnings.append(warning)
            print(warning)
        if orig is None or answer != orig:
            answers[id] = answer
            self.save(answers)

    def print_warnings(self):
        for w in self.warnings:
            print(w + '\n')


answer_db = AnswerDb('ac2017')

def fetch_input(n):
    file = Path('input') / 'ac2017d{}_input.txt'.format(n)
    try:
        data = file.read_text()
        print('Problem read from {!r}'.format(file.as_posix()))
        return data
    except IOError as exc:
        if exc.errno != errno.ENOENT:
            raise

    session_cookie = Path('ac2017_session.txt').read_text().strip()
    url = 'http://adventofcode.com/2017/day/{}/input'.format(n)
    print('Fetching problem from {!r}'.format(url))
    r = requests.get(url, cookies={'session': session_cookie})
    assert r.status_code == 200
    r.raw.decode_content = True
    file.write_text(r.text)
    print('Problem cached in {!r}'.format(file.as_posix()))
    return r.text


def split_data(s):
    data = list(filter(None, map(str.strip, s.split('\n'))))
    if len(data) == 1:
        [data] = data
    return data


def get_data(n):
    data = fetch_input(n)
    data = split_data(data)
    return data


def solve(n, second=False):
    data = get_data(n)
    t = time.perf_counter()
    res = globals()['problem' + str(n)](data, second)
    if res is not None:
        print(f'Solved in {time.perf_counter() - t}')
        answer_db.answer(f'{n}_{int(second)}', res)
    return res


def collect_problems():
    res = []
    for k, v in globals().items():
        m = re.match(r'problem(\d+)$', k)
        if m:
            res.append(int(m.group(1)))
    return sorted(res)


###########

def problem1(data, second):
    if second:
        return sum(int(c) for i, c in enumerate(data) if data[(i + len(data) // 2) % len(data)] == c)
    return sum(int(c) for i, c in enumerate(data) if data[(i + 1) % len(data)] == c)


def problem2(data, second):
    z = 0
    if second:
        for s in data:
            ns = [int(c) for c in s.split()]
            for n in ns:
                for n2 in ns:
                    if n != n2 and n // n2 * n2 == n:
                        # print(n, n2, n // n2)
                        z += n // n2
    else:
        for s in data:
            ns = [int(c) for c in s.split()]
            z += max(ns) - min(ns)
    return z


def problem3(data, second):
    data = int(data)

    def side(generation):
        return generation * 2 - 1

    def cells_inside(generation):
        return side(generation) ** 2

    def base(step):
        for gen in range(1, 1000):
            if cells_inside(gen) >= step:
                return gen - 1, cells_inside(gen - 1)

    def solve(data):
        gen, base_cells = base(data)
        remainder = data - base_cells
        def get_other_coord(remainder):
            while remainder > gen * 2:
                remainder -= gen * 2
            return abs(remainder - gen)
        return gen + get_other_coord(remainder)

    def coord(start, step):
        gen, base_cells = base(step)
        remainder = step - base_cells - 1
        base_side = gen * 2
        # print(step, gen, base_cells, base_side, remainder)
        if remainder < base_side:
            return addv2(start, (gen, remainder - gen + 1))
        remainder -= base_side

        if remainder < base_side:
            return addv2(start, (gen - remainder - 1, gen))
        remainder -= base_side

        if remainder < base_side:
            return addv2(start, (-gen, gen - remainder - 1))
        remainder -= base_side

        if remainder < base_side:
            return addv2(start, (remainder - gen + 1, -gen))

        assert False, remainder


    def solve2(data):
        size = 500
        start = size, size
        grid = np.zeros([size * 2, size * 2], dtype='int32')
        grid[start] = 1
        def gen_step():
            for step in range(2, 10000):
                yield coord(start, step)
        for i, z in enumerate(gen_step()):
            value = np.sum(grid[z[0] - 1 : z[0] + 2, z[1] - 1 : z[1] + 2].flat)
            grid[z] = value
            if value > data:
                return int(value)

    if second:
        return solve2(data)
    return solve(data)


def problem4(data, second):
    # data = ''
    def is_valid(passphrase):
        l = passphrase.split()
        if second:
            l = [''.join(sorted(w)) for w in l]
        return len(l) == len(set(l))

    return sum(is_valid(p) for p in data)


def problem5(data, second):
    # data = '0 3 0 1 -3'.split()
    offsets = [int(x) for x in data]
    pos = 0
    for step in range(0, 1_000_000_000):
        if not 0 <= pos < len(data): return step
        offset = offsets[pos]
        if second and offset >= 3:
            offsets[pos] = offset - 1
        else:
            offsets[pos] = offset + 1
        pos += offset
    else:
        assert False


def problem6(data, second):
    # data = '0 2 7 0'
    # if second: return
    banks = list(map(int, data.split()))
    lb = len(banks)
    seen = set()

    def step():
        idx = banks.index(max(banks))
        amount = banks[idx]
        banks[idx] = 0
        split_all = amount // lb
        if split_all:
            for i in range(lb):
                banks[i] += split_all
        amount -= split_all * lb
        for _ in range(amount):
            idx = (idx + 1) % len(banks)
            banks[idx] += 1
        return tuple(banks)

    for redist in range(100000000):
        result = step()
        if result in seen:
            if second: break
            return redist + 1
        seen.add(result)
    target = result
    for redist in range(100000000):
        result = step()
        if result == target:
            return redist + 1


def problem7(data, second):
#     data = '''pbga (66)
# xhth (57)
# ebii (61)
# havc (66)
# ktlj (57)
# fwft (72) -> ktlj, cntj, xhth
# qoyq (66)
# padx (45) -> pbga, havc, qoyq
# tknk (41) -> ugml, padx, fwft
# jptl (61)
# ugml (68) -> gyxo, ebii, jptl
# gyxo (61)
# cntj (57)'''.split('\n')
    d = {}
    class Node:
        def __init__(self, weight):
            self.weight = weight
            self.children_names = []

        def compute_total_weight(self, d):
            self.children = [d[s] for s in self.children_names]
            self.total_weight = self.weight + sum(c.compute_total_weight(d) for c in self.children)
            return self.total_weight

        def find_imbalance(self):
            if not self.children:
                return
            if all(self.children[0].total_weight == c.total_weight for c in self.children):
                return
            counter = Counter(c.total_weight for c in self.children)
            rcounter = defaultdict(list)
            for k, v in counter.items():
                rcounter[v].append(k)
            odd_weight, = rcounter[1] # must be unique
            odd_child = next(c for c in self.children if c.total_weight == odd_weight)
            result = odd_child.find_imbalance()
            if result is not None:
                return result
            correct_weight = next(c.total_weight for c in self.children if c.total_weight != odd_weight)
            return odd_child.weight + (correct_weight - odd_child.total_weight)

    parents = {}

    for line in data:
        s1, _, s2 = line.partition(' -> ')
        name, weight = s1.split()
        weight = int(weight.strip('()'))
        node = d[name] = Node(weight)
        if s2:
            node.children_names.extend(filter(None, (s.strip() for s in s2.split(','))))
        for child in node.children_names:
            assert child not in parents
            parents[child] = name

    while name in parents:
        name = parents[name]

    root = name
    if not second:
        return root

    print(d[root].compute_total_weight(d))
    print(d[root].find_imbalance())


##########


def problem(data, second):
    data = ''
    if second: return


###########

def solve_all():
    global quiet
    quiet = True
    for i in collect_problems():
        print('###', i)
        print(solve(i))
        if i == 5:
            print('Skipping')
            continue
        print(solve(i, True))


def main():
    problems = collect_problems()
    p = problems[-1]
    print(solve(p))
    print(solve(p, True))

if __name__ == '__main__':
    print('Hello', sys.version)
    # solve_all()
    print('--------------\n\n')
    main()
    # problem17(get_data(17), True)
    # import cProfile
    # cProfile.run('problem13(get_data(13), True)', sort='cumtime')
    # t = time.clock()
    # problem13(get_data(13), True)
    # print(time.clock() - t)
