#import psyco
#psyco.full()

import time


_lastClock = 0
def Measure(msg = None):
    global _lastClock
    t = time.clock()
    if msg != None:
        print msg, t - _lastClock
    _lastClock = t
    
def FindPrimesSimple(n):
    primes = [3]
    for i in xrange(2, n / 2):
        current = i * 2 + 1
        j = 3
        while j * j <= current:
            if current % j == 0:
                break
            j += 2
        else:
            primes.append(current)

            
    primes.insert(0, 2)
    return primes

def FindPrimesReusing(n):
    primes = [3]
    for i in xrange(2, n/2):
        current = i * 2 + 1
        for j in primes:
            if j * j > current:
                primes.append(current)
                break
            if current % j == 0:
                break
    primes.insert(0, 2)
    return primes
        
Measure("Starting: ")
n = 2000000
p = FindPrimesSimple(n)
Measure("Simple: ")
print p[:100]
Measure()
p = FindPrimesReusing(n)
Measure("Reusing: ")
print p[:100]
