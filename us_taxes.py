"""based on http://www.irs.gov/taxstats/indtaxstats/article/0,,id=133521,00.html
in particular Tax Generated, Classified by: Tax Rate and Size of Adjusted Gross Income, in 2008
http://www.irs.gov/pub/irs-soi/08in35tr.xls
"""

payers_str = """\
217,955
365,765
587,434
1,137,799
1,651,758
2,342,620
2,542,013
2,747,013
2,880,415
3,266,667
8,354,766
8,078,487
14,031,669
10,881,138
19,044,011
11,672,302
13,816,581
3,466,191
574,677
139,660
59,078
85,689
21,238
13,403"""

taxes_str = """\
4,553
46,472
68,738
187,187
330,361
618,635
1,008,780
1,419,491
1,892,034
2,532,470
8,784,435
12,351,214
32,408,731
37,948,614
107,402,789
102,841,250
240,148,659
184,072,786
92,512,758
42,310,594
25,763,527
64,697,267
35,640,067
86,304,057"""

def parse_data(s):
    return [int(v.replace(',', '')) for v in s.split()]

payers, taxes = parse_data(payers_str), parse_data(taxes_str)
total_payers, total_taxes = sum(payers), sum(taxes)
curr_payers, curr_taxes = 0, 0
for p, t in zip(payers, taxes):
    curr_payers += p
    curr_taxes += t
    print('{0:>4.1f}% payers paid {1:>4.1f}% taxes'.format(100.0 * curr_payers / total_payers, 100.0 * curr_taxes / total_taxes))
