from utils import Timeit
import os

test_file = 'test.out' 

def write1(cnt):
	for i in range(cnt):
		with open(test_file, 'a') as f:
			f.write(str(i) + '\n')

def write2(cnt):
	with open(test_file, 'a') as f:
		for i in range(cnt):
			f.write(str(i) + '\n')
			f.flush()

def write3(cnt):
	with open(test_file, 'a') as f:
		for i in range(cnt):
			f.write(str(i) + '\n')

cnt = 40000

def test(writer):
	with Timeit(writer.__name__):
		writer(cnt)
	with Timeit(writer.__name__):
		writer(cnt)
	os.remove(test_file)
	
test(write1)
test(write2)
test(write3)