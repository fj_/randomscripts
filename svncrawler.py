"""
Should be rewritten to use crawler.py. But just in case I need it suddenly, 
I'll leave it here.
"""

import urllib
from urlparse import *
from htmllib import HTMLParser
import formatter
from itertools import *
import re
import os

class Anchor(object):
	__slots__ = ("href", "name", "type", "data")
	def __init__(self, href, name, type, data = ""):
		self.href = href
		self.name = name
		self.type = type
		self.data = data
	
	def __repr__(self):
		def strAttr(attr):
			val = self.__getattribute__(attr)
			if val is not None and val != "":
				return attr + "=\"" + str(val) + "\""
			
		return "<a " + \
			" ".join([s for s in (strAttr("href"), strAttr("name"), strAttr("type"))
			if s is not None]) + \
			">" + self.data + "</a>"
		
	
class AnchorsExtractor(HTMLParser):
	def __init__(self):
		HTMLParser.__init__(self, formatter.NullFormatter())
		
	def reset(self):
		HTMLParser.reset(self)
		self.anchors = []
		self.savedAnchor = None

	def anchor_bgn(self, href, name, type):
		self.savedAnchor = Anchor(href, name, type)
		self.save_bgn()
	
	def anchor_end(self):
		self.savedAnchor.data = self.save_end()
		self.anchors.append(self.savedAnchor)
		self.savedAnchor = None
		
	def parseFile(self, data):
		"""
		Works with any file-like object, that supports iteration 
		"""
		self.reset()
		for l in data:
			self.feed(l)
		self.close()
		anchors = self.anchors
		self.reset()
		return anchors


def parseTTData(f):
	alist = AnchorsExtractor().parseFile(f)
	for i, a in enumerate(alist):
		if a.data == "Last log entry" and a.href.endswith("?sortby=log#dirlist"):
			if i + 1 < len(alist) and a.href.startswith(alist[i + 1].href):
				i += 1 # parent directory
			break
	else:
		print "Failed to extract the beginning of the list!"
		return

	dirs = []
	files = []
	
	for i in xrange(i + 1, len(alist) - 1, 2):
		a1, a2 = alist[i], alist[i + 1]
		if a1.data[-1] == "/":
			#directory
			dirs.append((a1.data[:-1], a1.href))
		else:
			#file
			url = a2.href
			url = re.sub("&view=markup", "", url)
			files.append((a1.data, url))

	return dirs, files

def walkTheRep(host, path, localPath, dirsAndFilesExtractor):
	f = urllib.urlopen(host + path)
	try:
		dirs, files = dirsAndFilesExtractor(f)
	finally:
		f.close()
		
	yield (localPath, dirs, files)
	for d in dirs:
		for triple in walkTheRep(host, d[1], localPath + "\\" + d[0], dirsAndFilesExtractor):
			yield triple

def replicateRep(url, localPath, dirsAndFilesExtractor):
	r = urlparse(url)
	host = urlunparse((r[0], r[1], '', '', '', ''))
	for localPath, _, files in walkTheRep(host, r[2], localPath, dirsAndFilesExtractor):
		print ">>> " + localPath
		if not os.path.exists(localPath):
			os.makedirs(localPath)
		os.chdir(localPath)
		for f in files:
			print f
			urllib.urlretrieve(host + f[1], f[0])


######################################

if __name__ == '__main__':
	
	localRoot = ""
	remoteRoot = ""
	
	def replicateItem(url):
		assert url.startswith(remoteRoot)
		
		if url[-1] == '/':
			relative = url[len(remoteRoot):-1]
		else:
			relative = url[len(remoteRoot):]
			
		relative = relative.replace("/", "\\")
		replicateRep(url, 
				localRoot + relative,
				parseTTData)
	
	
	items = """
"""
	
	for item in items.split("\n"):
		item = item.strip()
		if not item.startswith('#') and item != "":
			replicateItem(item)