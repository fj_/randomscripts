from itertools import *
from stat import S_IREAD
import sys
import os
import os.path as path
from subprocess import *
import shutil

def UEncode(t):
    if isinstance(t, basestring):
        return t.encode('mbcs', 'strict')
    else:
        return [s.encode('mbcs', 'strict') for s in t]

def CreateHardlink(fname, newfname):
#    print fname, newfname
#    return 

    cmd = Popen(
        UEncode([
        #"echo",
        "fsutil", "hardlink", "create",
        newfname, fname]),
        shell = True,
        stdin = PIPE, stdout = PIPE, stderr = PIPE)
    cmd.communicate()

    if cmd.returncode != 0:
        raise Exception('ERROR: Command returned ' + str(cmd.returncode))

def MakeCopyName(srcPath):
    dirname, basename = path.split(srcPath)
    if basename == '': 
        Exception("Source path doesn't contain file or directory name "
                  " (try removing trailing slash): " 
                  + basename)
    i = 1;
    name, ext = path.splitext(basename)
    while True:
        newPath = path.join(dirname, name + " copy " + str(i) + ("." + ext if ext else ""))
        if not path.exists(newPath): return newPath 
        i += 1
    


def Duplicate(srcPath, dstPath = None):
    if not path.exists(srcPath):
        raise Exception("File or directory does not exist: " + srcPath)
    
    if dstPath == None:
        dstPath = MakeCopyName(srcPath)
    
    if path.exists(dstPath):
        raise Exception("File or directory already exists: " + dstPath)
    
    print dstPath
    if path.isfile(srcPath):
        CreateHardlink(srcPath, dstPath)
        os.chmod(srcPath, S_IREAD)
        os.chmod(dstPath, S_IREAD)
    else:
        os.makedirs(dstPath)
        for f in os.listdir(srcPath):
            Duplicate(path.join(srcPath, f), path.join(dstPath, f))
        
if __name__ == '__main__':
    Duplicate(*sys.argv[1:])
