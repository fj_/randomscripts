// ==UserScript==
// @name         Automate Distance-Incremental
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        http://raw.githack.com/Jacorb90/Distance-Incremental/master/main.html
// @grant        none
// ==/UserScript==

(function() {
    "use strict";
	var pfx = "my_unique_prefix_";

	var perks = [];

	function create_ui() {
        var mydiv = document.createElement("div");
		mydiv.style.cssText = "overflow-x: auto;";

		function create_checkbox(name) {
			var obj = document.createElement("INPUT");
			var id = pfx + "checkbox_" + name;
			obj.setAttribute("type", "checkbox");
			obj.setAttribute("id", id);
			mydiv.appendChild(obj);
			var label = document.createElement("label");
			label.setAttribute("for", id);
			label.innerHTML = name;
			mydiv.appendChild(label);
			mydiv.innerHTML += "&nbsp;&nbsp;";
			return id;
		}

		function create_perk(button_id, checkbox_name) {
			perks.push({
				button_id: button_id,
				checkbox_id: create_checkbox(checkbox_name),
			})
		}

		create_perk("perk1", "perkGod");
		create_perk("perk2", "perkHoly");
		create_perk("perk3", "perkSaint");
		create_perk("perk4", "perkGlory");

		document.querySelector("#mainContainer > div.band.lb").appendChild(mydiv);
	}

	function activate_perk(perk, n) {
		// make sure everything is loaded.
		var btn = document.getElementById("inftabbtn");
		if (!btn) return;

		var checkbox = document.getElementById(perk.checkbox_id);
		n += 1; // perks are 1-based.
		if (checkbox.checked != tmp.inf.asc.perkActive(n)) {
			tmp.inf.asc.activatePerk(n);
		}
	}

    function tick() {
		perks.forEach(activate_perk);
	}

    create_ui();
	setInterval(tick, 100);
})();
