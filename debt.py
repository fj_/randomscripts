from scipy.optimize import fsolve


def debt0_30_iter(rate):
    amount = 30_000
    for i in range(30):
        amount = amount * (1 + rate) - 2_000
        yield amount


def debt0_30(rate):
    for amount in debt0_30_iter(rate):
        pass
    return amount


def debt30_60_iter(rate):
    amount = 0
    for i in range(30):
        amount = amount * (1 + rate) + 1_000
        yield amount
    for i in range(30):
        amount = amount * (1 + rate) - 2_000
        yield amount

def debt30_60(rate):
    for amount in debt30_60_iter(rate):
        pass
    return amount

[rate_lending] = fsolve(debt0_30, 0.05)
[rate_saving] = fsolve(debt30_60, 0.05)

print(f'rate_lending={rate_lending*100:0.2f}%, rate_saving={rate_saving*100:0.2f}%')

print(f'avg citizen\'s debt = ${sum(debt0_30_iter(rate_lending)) / 60 :0.2f}')
print(f'avg bank\'s debt (per citizen) = ${sum(debt30_60_iter(rate_saving)) / 60 :0.2f}')

def print_debt_list(header, lst):
    print(f'\n=== {header} ===', *['{:02} - ${:0.2f}'.format(*it) for it in enumerate(lst)], sep = '\n')

print_debt_list('citizen debts', debt0_30_iter(rate_lending))
print_debt_list('citizen savings', debt30_60_iter(rate_saving))
