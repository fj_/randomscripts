#include <stdio.h>
#include <windows.h>
#include <intrin.h>

// Use PerformanceCounter to extrapolate system time with 100ns precision.

// Note that the real frequency of PerformanceCounter is volatile (since
// processor clock frequency can fluctuate depending on temperature, voltage,
// load etc), so for example on one piece of hardware I tested it, it deviated
// from SystemTime by about 30ms per hour.

// If that's unacceptable, one should add the ability to calculate an
// adjustment, and also to resynchronize completely in case something weird
// happens. One complication is that a single synchronization has a 15ms error
// in the first place (and you probably don't want to wait for hundrends of
// milliseconds to more or less accurately pinpoint the phase of the SystemTime
// clock).

typedef unsigned __int64 uint64_t;

inline int bsr64(uint64_t x)
// unlike BitScanReverse returns 1-based index of the MSB of x
// or zero if x is zero.
{
    DWORD res;
    if (BitScanReverse(&res, x >> 32)) return res + 33;
    if (BitScanReverse(&res, x & ((DWORD)-1))) return res + 1;
    return 0;
}

uint64_t mul_div_64_unsafe(uint64_t src, uint64_t multiplier, uint64_t divisor)
{
    // multiplier * divisor must fit into uint64 (and of course the result must
    // fit into uint64 too).
    //
    // For example, here I want to multiply stuff by the ratio of 10^7 (100ns
    // intervals per second) to PerformanceFrequency which usually is the same
    // as clock frequency, i.e. ~3*10^9. The worst-case intermeidate result is
    // 3 * 10^15, which is comfortably distant from the 1.8 * 10^19 limit (and
    // would remain distant until we have terahertz processors which for some
    // reason expose their clock frequencies as is)!
    //
    // Still, check if we the value is too big and gracefully degrade
    // precision, just to be safe.
    uint64_t whole = (src / divisor) * multiplier;
    uint64_t frac;
    int bitlen = bsr64(multiplier) + bsr64(divisor); // 1-based indices
    if (bitlen > 64)
    {
        unsigned int shift = bitlen - 64;
        if (multiplier > divisor)
        {
            frac = ((src % divisor) * (multiplier >> shift) / divisor) << shift;
        }
        else
        {
            frac = (((src % divisor) >> shift) * multiplier / divisor) << shift;
        }
    }
    else
    {
        frac = (src % divisor) * multiplier / divisor;
    }
    return whole + frac;
}

void synchronize_counters(FILETIME & system_time, LARGE_INTEGER & perf_counter)
{
    LARGE_INTEGER prev_time = {system_time.dwLowDateTime, system_time.dwHighDateTime};
    for (int i = 0; i < 10; i++)
    {
        GetSystemTimeAsFileTime(&system_time);
        LARGE_INTEGER cur_time = {system_time.dwLowDateTime, system_time.dwHighDateTime};
        if (cur_time.QuadPart == prev_time.QuadPart) return;
        prev_time = cur_time;
        QueryPerformanceCounter(&perf_counter);
    }
}

class Precise_timer
{
    LARGE_INTEGER perf_frequency;
    FILETIME ref_system_time;
    LARGE_INTEGER ref_perf_counter;
public:
    Precise_timer() : ref_system_time(), ref_perf_counter()
    {
        QueryPerformanceFrequency(&perf_frequency);
        for (int i = 0; i < 100000; i++)
        {
            synchronize_counters(ref_system_time, ref_perf_counter);
        }
    }

    FILETIME get_estimated_system_time(int & deviation)
    {
        LARGE_INTEGER counter = {};
        FILETIME real_time = {};
        synchronize_counters(real_time, counter);
        //QueryPerformanceCounter(&counter);
        counter.QuadPart -= ref_perf_counter.QuadPart;
        counter.QuadPart = mul_div_64_unsafe(counter.QuadPart, 10000000, perf_frequency.QuadPart);
        LARGE_INTEGER tmp = { ref_system_time.dwLowDateTime, ref_system_time.dwHighDateTime };
        counter.QuadPart += tmp.QuadPart;
        FILETIME res = {counter.LowPart, counter.HighPart};

        LARGE_INTEGER tmp2 = {real_time.dwLowDateTime, real_time.dwHighDateTime };
        deviation = (int)(tmp2.QuadPart - counter.QuadPart);
        return res;
    }
};

int extract_ticks(const FILETIME& ft)
{
    ULARGE_INTEGER tmp = {ft.dwLowDateTime, ft.dwHighDateTime};
    return tmp.QuadPart % 10000000;
}

int main()
{
    Precise_timer t;
    int prev_ticks = 0;

    for (;;)
    {
        printf("\n\n\n\n\n*********\n\n");
        for (int i = 0; i < 100; i++)
        {
            FILETIME ft_system, ft_local;
            SYSTEMTIME lt;
            int ticks;
            int deviation;

            ft_system = t.get_estimated_system_time(deviation);
            FileTimeToLocalFileTime(&ft_system, &ft_local);
            FileTimeToSystemTime(&ft_local, &lt);
            ticks = extract_ticks(ft_local);

            printf("%04d/%02d/%02d %02d:%02d:%02d.%03d (%07d) : %07d  | %8d\n",
                lt.wYear, lt.wMonth, lt.wDay,
                lt.wHour, lt.wMinute, lt.wSecond, lt.wMilliseconds,
                ticks, ticks - prev_ticks, deviation);
            prev_ticks = ticks;
        }
        Sleep(1000 * 5);
    }

    printf("lol\n");
}
