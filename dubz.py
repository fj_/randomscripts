dubz_db = {}

def get_dubz_list(n):
    def collect():
        def in_base_iter(n, base):
            while n:
                n, d = divmod(n, base)
                yield d
        for base in xrange(2, n - 1):
            lst = list(in_base_iter(n, base))
            if len(lst) < 2: continue
            digit = lst[0]
            if not all(digit == it for it in lst): continue
            yield len(lst), base, digit
    d = {}
    
    for length, base, digit in collect():
        d.setdefault(length, []).append((base, digit))
        c = chr(ord('0') + digit)
        if c > '9': c = chr(ord('a') + digit)
        try:
            reconstructed = None
            reconstructed = int(c * length, base)
        except:
            pass
        assert reconstructed is None or n == reconstructed 
    return sorted(d.iteritems())

def add_to_db(n, dubz_list):
    assert n not in dubz_db
    score = 0.0
    for length, lst in dubz_list:
        score += 0.5 * (length - 2)
        for base, _ in lst:
            score += 1
            score += 0.5 * dubz_db[base][0]
    dubz_db[n] = score, dubz_list
    return score 

def format_dubz(n, dubz_list, print_digit = False, recursive = False):        
    names = ['', '', 'dubz', 'tripz', 'quadz', 'pentz', 'sezzz', 'septz', 'zoundz']
    def format_list(lst):
        if len(lst) > 2:
            return ', '.join(lst[:-1]) + ', and ' + lst[-1]
        elif len(lst) == 2:
            return ' and '.join(lst)
        else:
            return lst[0]
        
    def pluralize(noun, number):
        if number > 1: return noun + 's'
        return noun
    
    def format_dubzlist(dubz_list):
        res = []
        base_format = '{} ({})' if print_digit else '{}'
        def format_base(base, digit):
            s = base_format.format(*(base, digit))
            if recursive:
                subscore, subdubz = dubz_db[base]
                if subscore:
                    s += ' (which itself is ' + format_dubzlist(subdubz) + ')'
            return s
            
        for length, lst in dubz_list:
            res.append('{} in {} {}'.format(
                    names[min(length, len(names) - 1)],
                    pluralize('base', len(lst)),
                    format_list([format_base(base, digit) for base, digit in lst])))
        return format_list(res)  
    
    return '{} is {}'.format(n, format_dubzlist(dubz_list))

for i in xrange(200):
#    print i, '=' * 20
    dubz_list = get_dubz_list(i)
    score = add_to_db(i, dubz_list) 
    if score > 1:
        print '%2.1f: ' % score, format_dubz(i, dubz_list)
        
        