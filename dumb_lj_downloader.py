import contextlib
import re
import time
import urllib
import random

import os
from os import path as os_path

last_request = None
def delay():
    global last_request
    t = time.clock()
    if last_request is not None:
        delay = (random.random() + 1) * 3
        elapsed = t - last_request
        remaining = delay - elapsed
        if remaining > 0:
            print 'Sleeping for', remaining 
            time.sleep(remaining)
            t = time.clock()
    last_request = t

    
def urlopen(url):
    delay()     
    print 'Downloading', url 
    return contextlib.closing(urllib.urlopen(url))


def main():
    local_dir = 'dump'
    entry = 'insert last entry here'
    if not os_path.exists(local_dir):
        os.makedirs(local_dir)
        
    while entry:
        with urlopen(entry) as inp:
            data = inp.read()
            entry = inp.geturl()
        local_file = os_path.basename(entry)
        if not re.match(r'\d+\.html', local_file):
            local_file = 'end.html'
        local_file = os_path.join(local_dir, local_file)
        print 'Writing', local_file
        with open(local_file, 'w') as out:
            out.write(data)

        # <link href='http://www.livejournal.com/go.bml?journal=xxxx&amp;itemid=123123&amp;dir=prev' rel='Previous' />
        prev_url = re.search(r'''<link href='([^']+)' rel='Previous' ''', data) #  
        entry = None if prev_url is None else prev_url.group(1) 


if __name__ == '__main__':
    main()
    print 'yo'
    
