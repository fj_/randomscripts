import time, threading, contextlib, logging
import sqlite3
import requests

log = logging.getLogger(__name__)


def open_default_db():
    return sqlite3.connect('url_cache.sqlite', isolation_level=None, check_same_thread=not synchronized)



class DummyLock(contextlib.AbstractContextManager):
    acquire = release = __exit__ = lambda *args: None


class Scraper(contextlib.AbstractContextManager):
    def __init__(self, name='url_cache_default.sqlite', request_interval=2.0, headers=None, synchronized=False):
        self.name = name
        self.request_interval = request_interval
        self.last_request_time = None
        self.headers = headers if headers is not None else {
                'User-Agent': 'Very Descriptive User Agent String',
        }

        self.db = sqlite3.connect('url_cache.sqlite', isolation_level=None, check_same_thread=not synchronized)
        self.db.execute('''create table if not exists cached_requests
        (url text not null,
         text text not null,
         primary key (url))''')

        if synchronized:
            self.lock = threading.Lock()
        else:
            self.lock = None #DummyLock()


    def close(self):
        self.db.close()


    def __exit__(self, *args):
        self.close()


    def _sleepy_get(self, url):
        if self.request_interval is not None and self.last_request_time is not None:
            sleep_for = self.request_interval - (time.time() - self.last_request_time)
            if sleep_for > 0:
                log.info('Sleeping for {}'.format(sleep_for))
                time.sleep(sleep_for)
        log.info('Getting {!r}'.format(url))

        self.lock.release()
        try:
            result = requests.get(url, headers=self.headers)
        finally:
            self.lock.acquire()

        self.last_request_time = time.time()
        return result


    def _cached_get(self, url):
        with self.lock:
            res = self.db.execute('select text from cached_requests where url=?',
                    (url,)).fetchone()
            if res is not None:
                return res[0]
            res = self._sleepy_get(url)
            res.raise_for_status()
            text = res.text
            self.db.execute(
                    'insert or replace into cached_requests ' +
                    '(url, text) values (?, ?)',
                    (url, text))
            return text

    get = _cached_get

