from itertools import *
graph = { 1 : [2], 2 : [3, 10], 3 : [4, 20], 4: [5], 5: [8], 10 : [11], 11 : [4],
          20 : [21], 21 : [5, 22], 22 : [], 8 : [9], }
start = 1
stop = 8

def shiftgroup(list, groupSize):
    """shift([1, 2, 3, 4, 5], 3) -> [(1, 2, 3), (2, 3, 4), (3, 4, 5)]"""
    group = list[:groupSize - 1]
    for c in list[groupSize - 1:]:
        group.append(c)
        yield tuple(group)
        del group[0]

class Graph:
    def __init__(self):
        self.vertices = set()
        self.edgesOut = {}
        self.edgesIn = {}

    def addVertex(self, v):
        if v not in self.vertices:
            self.vertices.add(v)
            self.edgesOut[v] = set()
            self.edgesIn[v] = set()

    def addEdge(self, fro, to):
        self.addVertex(fro)
        self.addVertex(to)
        self.edgesOut[fro].add(to)
        self.edgesIn[to].add(fro)

    def wave(self, start, usedEdges = None):
        if usedEdges == None: usedEdges = set()
        visited = {start : None}
        front = []
        back = [start]
        while back:
            for v1 in back:
                for v2 in chain([v2 for v2 in self.edgesOut[v1] if (v1, v2) not in usedEdges],
                                [v2 for v2 in self.edgesIn[v1] if (v2, v1) in usedEdges]):
                    if v2 in visited: continue
                    front.append(v2)
                    visited[v2] = v1
                
            back, front = front, back
            del front[:]

        return visited

    def shortestPath(self, start, stop):
        visited = graph.wave(start)

        v = stop
        if not visited[v]: return None
        path = []
        
        while v:
            path.append(v)
            v = visited[v]
            
        path.reverse()
        return path
        
def prepareGraph(graph):
    r = Graph()
    for v1, val in graph.iteritems():
        v1 = str(v1)
        r.addEdge(v1 + "i", v1 + "o")
        for v2 in map(str, val):
            r.addEdge(v1 + "o", v2 + "i")
            r.addEdge(v2 + "o", v1 + "i")
    return r

graph = prepareGraph(graph)
start = str(start) + "o"
stop = str(stop) + "i"
#print graph.vertices
#print graph.edgesOut

sp = graph.shortestPath(start, stop)
print sp
usedEdges = list(shiftgroup(sp, 2))
print usedEdges
print  

splitPoints = set()
while sp:
    visited = graph.wave(start, usedEdges)
#    print visited.keys()
    while sp and start in visited: 
        start = sp.pop(0)
    splitPoints.add(int(start[:-1]))
    
print splitPoints