#!/usr/bin/env python

import sys
try:
    set()
except:
    from sets import Set as set

printable = set([chr(i) for i in range(32, 127)])
printable.update('\r\n')
printable.difference_update('\\')

MIN_RUN_LENGTH = 2
	
def escape_runs(data):
	"""The idea is to ignore random printable symbols between unprintable
	unless the run of printable symbols is long enough"""
	def escape(c): return '\\%02x' % ord(c)
	is_printable_run = False
	printable_buffer = []
	for c in data:
		if c in printable:
			if is_printable_run:
				yield c
			elif len(printable_buffer) + 1 >= MIN_RUN_LENGTH:
				is_printable_run = True
				for b in printable_buffer: yield b
				del printable_buffer[:]
				yield c
			else:
				printable_buffer.append(c)
		else:
			is_printable_run = False
			if printable_buffer:
				for b in printable_buffer: yield escape(b)
				del printable_buffer[:]
			yield escape(c)
	# if we have something left in the printable buffer then it's most probably not printable
	for b in printable_buffer: yield escape(b)
	
if __name__ == '__main__':
    assert len(sys.argv) == 2

    f = open(sys.argv[1], 'rb')
    data = f.read()
    f.close()

    print ''.join(escape_runs(data))