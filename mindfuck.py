# WTF Python

def one():
    numbers = range(6)
    numbers = (i for i in numbers if i % 2)
    numbers = (i for i in numbers if i % 3)
    return numbers


def two():
    numbers = range(6)
    for p in (2, 3):
        numbers = (i for i in numbers if i % p)
    return numbers


def three():
    numbers = range(6)
    numbers = (i for i in numbers if i % 2)
    numbers = (i for j in range(1) for i in numbers if i % 3)
    return numbers


print('one', *one())
print('two', *two())
print('three', *three())
