import random

def memoized(f):
    d = {}
    def wrapper(*args):
        if args not in d:
            d[args] = f(*args)
        return d[args]
    return wrapper

@memoized
def cnt(ncnt, nsum, nmax):
    if ncnt == 1:
        return 1 if nsum <= nmax else 0
    if nsum == 0:
        return 1
    return sum(cnt(ncnt - 1, nsum - cur, nmax) 
               for cur in range(min(nsum, nmax) + 1))

def select(ncnt, nsum, nmax, n):
    if ncnt == 1:
        assert n == 0
        assert nsum <= nmax
        return [nsum]
    for cur in range(nmax + 1):
        c = cnt(ncnt - 1, nsum - cur, nmax)
        if n < c:
            return [cur] + select(ncnt - 1, nsum - cur, nmax, n)
        n -= c
    assert False, 'overflow, %d remaining' % n

def select_random(ncnt, nsum, nmax):
    total = cnt(ncnt, nsum, nmax)
    n = random.randrange(0, total)
    r = select(ncnt, nsum, nmax, n)
#    print '%d of %d' % (n, total)
    return r  
    
if __name__ == '__main__':
    try:
        for i in range(28):
            print '%3d' % i, select(3, 7, 5, i)
    except AssertionError as err:
        print err
    
    for i in range(10):
        print select_random(20, 200, 20)

    print select_random(1, 15, 20)   
    for i in range(20):
        print select_random(2, 1, 1)
    