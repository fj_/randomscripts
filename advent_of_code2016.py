#!/usr/bin/env python3.6

from pathlib import Path
from collections import Counter, defaultdict, deque, namedtuple
from pprint import pprint
from typing import Sequence, Tuple, Callable, List
import itertools, re, functools, shutil, errno, sys, time, hashlib, string
import requests
import numpy as np
from blist import blist

# line_profiler support
try:
    profile
except:
    def profile(f):
        return f

quiet = False

short_inputs = {
    5: 'ffykfhsq', 13: 1364, 14: 'yjdafjpo', 16: '11100010111110100', 17: 'edjrjqaa',
    19: 3012210,
}


def v2(x, y):
    return np.array([x, y])


def args_to_int(f):
    @functools.wraps(f)
    def wrapper(*args):
        return f(*map(int, args))
    return wrapper


def gcd(a, b):
	if a < b: a, b = b, a
	while b:
		a, b = b, a % b
	return a


def pad_map_with_border(mm, sentinel=None):
    '''Pad a rectangular map with a border made of `None`s or provided sentinel values'''
    width = len(mm[0])
    assert all(width == len(row) for row in mm)
    res = []
    res.append([sentinel for _ in range(width + 2)])
    for row in mm:
        rr = [sentinel]
        rr.extend(row)
        rr.append(sentinel)
        res.append(rr)
    res.append([sentinel for _ in range(width + 2)])
    return res


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args, fillvalue=fillvalue)


def log_seq(seq):
    for n, it in enumerate(seq):
        print(n, it)
        yield it


class ReTokenizer(object):
    def __init__(self, tokens: Sequence[Tuple[str, Callable]], flags=0, not_found=object()):
        self.not_found = not_found
        self.groups = []
        group_cnt = 1
        regexes = []
        for rx_str, handler in tokens:
            rx = re.compile(rx_str, flags)
            regexes.append(rx_str)
            self.groups.append((group_cnt, rx.groups, handler))
            group_cnt += rx.groups + 1
        self.rx = re.compile('|'.join('({})'.format(rx) for rx in regexes), flags)

    def _process_match(self, m, expected_length=None):
        for start, group_count, handler in self.groups:
            m_end = m.end(start)
            if m_end < 0:
                # no match
                continue
            if expected_length is not None and m_end != expected_length:
                # incomplete match
                assert False, 'Incomplete match {!r} on {!r}'.format(m.group(start), m.string)
            groups = [m.group(i) for i in range(start + 1, start + 1 + group_count)]
            return handler(*groups)
        assert False # wtf

    def match(self, s):
        'Tries to match the whole string'
        m = self.rx.match(s)
        assert m is not None, 'Failed to match {!r}'.format(s)
        r = self._process_match(m, len(s))
        assert r is not self.not_found, 'Failed to match {!r}'.format(s)
        return r

    def find_all(self, s):
        'Call handlers on all matches in the string, return a list of results'
        res = []
        for m in self.rx.finditer(s):
            mm = self._process_match(m)
            assert mm != self.not_found
            res.append(mm)
        return res


def neighbours4(x, y):
    for dx, dy in (( 0, -1), (0, 1), (-1, 0), (1, 0)):
        yield x + dx, y + dy


@profile
def bfs_step(front, visited, goal, neighbors: Callable):
    '''Returns path, visited or None, visited if goal unreachable or unspecified'''
    sentinel = object()
    front.append(sentinel)
    while True:
        parent_node = front.popleft()
        if parent_node is sentinel:
            # goal not found at this step
            return None
        for node in neighbors(parent_node):
            if node not in visited:
                visited[node] = parent_node
                front.append(node)
                if goal(node):
                    return node


def bfs_extract_path(node, visited):
    path = []
    while node is not None:
        path.append(node)
        node = visited[node]
    path.reverse()
    return path


def bfs(start, goal, neighbors: Callable):
    '''Returns (path, visited) or (None, visited) if goal unreachable or unspecified'''
    visited = {s: None for s in start}
    front = deque(start)
    step = 1
    while front:
        found = bfs_step(front, visited, goal, neighbors)
        if found is not None:
            return bfs_extract_path(found, visited), visited
        if not quiet: print(f'BFS: step={step}, visited={len(visited)}')
        step += 1
    return None, visited


def bfs_bidirectional(start: Sequence, end: Sequence, neighbors: Callable):
    '''Returns (path, visited) or (None, visited) if goal unreachable or unspecified'''
    visited1 = {s: None for s in start}
    visited2 = {s: None for s in end}
    assert not (visited1.keys() & visited2.keys())
    front1 = deque(start)
    front2 = deque(end)
    step = 1
    def swap_states():
        nonlocal front1, front2, visited1, visited2
        front1, front2 = front2, front1
        visited1, visited2 = visited2, visited1

    while front1:
        found = bfs_step(front1, visited1, lambda s: s in visited2, neighbors)
        if found is not None:
            break
        l1, l2 = len(visited1), len(visited2)
        if not (step & 1):
            l1, l2 = l2, l1
        if not quiet: print(f'BFSBD: step={step}, visited={l1} + {l2}')
        step += 1
        swap_states()

    if found:
        if not (step & 1):
            swap_states()
        p2 = bfs_extract_path(found, visited2)
        p2 = p2[-2::-1] # reverse and drop the first element
        return bfs_extract_path(found, visited1) + p2

    return None


def fetch_input(n):
    file = Path('input') / 'ac2016d{}_input.txt'.format(n)
    try:
        data = file.read_text()
        print('Problem read from {!r}'.format(file.as_posix()))
        return data
    except IOError as exc:
        if exc.errno != errno.ENOENT:
            raise

    session_cookie = Path('ac2016_session.txt').read_text().strip()
    url = 'http://adventofcode.com/2016/day/{}/input'.format(n)
    print('Fetching problem from {!r}'.format(url))
    r = requests.get(url, cookies={'session': session_cookie})
    assert r.status_code == 200
    r.raw.decode_content = True
    file.write_text(r.text)
    print('Problem cached in {!r}'.format(file.as_posix()))
    return r.text


def split_data(s):
    return list(filter(None, map(str.strip, s.split('\n'))))


def get_data(n):
    data = short_inputs.get(n)
    if data is not None:
        return data
    data = fetch_input(n)
    data = split_data(data)
    if len(data) == 1:
        [data] = data
    return data


def solve(n, second=False):
    data = get_data(n)
    t = time.perf_counter()
    res = globals()['problem' + str(n)](data, second)
    print(f'Solved in {time.perf_counter() - t}')
    return res


def collect_problems():
    res = []
    for k, v in globals().items():
        m = re.match(r'problem(\d+)$', k)
        if m:
            res.append(int(m.group(1)))
    return sorted(res)


##########


def problem1(s, ss):
    def run(commands):
        l = np.array([[0, -1], [1, 0]])
        r = l @ l @ l
        d = v2(0, 1)
        p = v2(0, 0)
        visited = set()
        visited.add(str(p))
        for cmd in commands:
            rot = {'L':l, 'R':r}[cmd[0]]
            dist = int(cmd[1:])
            d = rot @ d
            for _ in range(dist):
                p += d
                if ss:
                    strp = str(p)
                    if strp in visited:
                        return p
                    else:
                        visited.add(strp)
        return p
    #s = 'R8, R4, R4, R8'
    p = run(it.strip() for it in s.split(','))
    return abs(p[0]) + abs(p[1])


def problem2(s, ss):
    if ss:
        keypad = pad_map_with_border((
                '  1  ',
                ' 234 ',
                '56789',
                ' ABC ',
                '  D  '), ' ')
        p = v2(1, 3)
    else:
        keypad = pad_map_with_border((
                '123',
                '456',
                '789',
                ), ' ')
        p = v2(2, 2)

    dirs = {'U': v2(0, -1), 'D': v2(0, 1), 'R': v2(1, 0), 'L': v2(-1, 0)}
    def key_p(p):
        return keypad[p[1]][p[0]]
    def move(p, d):
        pp = p + dirs[d]
        return pp if key_p(pp) != ' ' else p

    _ = '''ULL
RRDDD
LURDL
UUUUD'''
    r = ''
    for line in s:
        for c in line:
            p = move(p, c)
#             print(c, p)
        r += key_p(p)
    return r


def problem3(s, ss):
    data = [[int(r) for r in row.split()] for row in s]
    if ss:
        data = itertools.chain.from_iterable(zip(*data))
        data = grouper(data, 3)
    return sum(True for a, b, c in data if a + b > c and a + c > b and b + c > a)


def problem4(s, ss):
    _ = '''aaaaa-bbb-z-y-x-123[abxyz]
a-b-c-d-e-f-g-h-987[abcde]
not-a-real-room-404[oarel]
totally-real-room-200[decoy]'''
    rooms = [re.match(r'^(.*)-(\d+)\[(.*)]', r).groups() for r in s]
    def check(room):
        c = Counter(c for c in room[0] if c != '-')
        d = ((-b, a) for a, b in c.most_common())
        d = ''.join(it[1] for it in sorted(d))[:5]
        return d == room[2]
    def decrypt(s, n):
        res = []
        for c in s:
            if c == '-': res.append(' ')
            else:
                o = ord(c) - ord('a')
                assert o <= ord('z')
                res.append(chr((o + n) % 26 + ord('a')))
        return ''.join(res)
    s = 0
    rooms = [(room[0], int(room[1])) for room in rooms if check(room)]
    rooms = [(decrypt(s, n), n) for s, n in rooms]
    if ss:
        for s, sector in rooms:
            if 'north' in s:
                print(s)
                return sector
    return sum(v for k, v in rooms)

def problem5(s, ss):
    # s = 'abc'
    import hashlib
    res = [None] * 8
    hash = hashlib.md5(s.encode('ascii'))
    for idx in range(0, 1000000000):
        h = hash.copy()
        h.update(str(idx).encode('ascii'))
        h = h.hexdigest()
        if h.startswith('00000'):
            pos, char = int(h[5], 16), h[6]
            if pos <= 7 and res[pos] is None:
                res[pos] = char
                if all(c is not None for c in res):
                    return ''.join(res)
            print(idx, h, ''.join('_' if c is None else c for c in res))


def problem6(s, ss):
    res = ''
    for line in zip(*s):
        if ss:
            res += Counter(line).most_common()[-1][0]
        else:
            res += Counter(line).most_common(1)[0][0]
    return ''.join(res)


def problem7(data, second):
    def parse(s):
        res = [[], []]
        current = ']'
        start = 0
        for m in re.finditer('\]|\[|$', s):
            data = s[start : m.start()]
            sep = m.group()
            if current == '[':
                assert sep == ']'
                res[1].append(data)
            else:
                assert sep == '[' or sep == ''
                res[0].append(data)
            current = sep
            start = m.end()
        return res

    def has_abba(s):
        for i in range(0, len(s) - 3):
            if s[i] != s[i + 1] and s[i] == s[i + 3] and s[i + 1] == s[i + 2]:
                return True
        return False

    def find_babs(s):
        for i in range(0, len(s) - 2):
            if s[i] != s[i + 1] and s[i] == s[i + 2]:
                yield s[i : i + 3]

    count = 0
    for s in data:
        supernet, hypernet = parse(s)
        if second:
            abas = set(s for s in supernet for s in find_babs(s))
            wanted_babs = set(s[1] + s[0] + s[1] for s in abas)
            babs = set(s for s in hypernet for s in find_babs(s))
            valid = bool(babs & wanted_babs)
        else:
            valid = any(map(has_abba, supernet)) and not any(map(has_abba, hypernet))
        if valid:
            count += 1
    return count


def problem8(data, second):
    # data = 'rect 3x2', 'rotate column x=1 by 1', 'rotate row y=0 by 4', 'rotate column x=1 by 1'
    # screen = np.zeros([3, 7], dtype=np.int)
    screen = np.zeros([6, 50], dtype=np.int)

    @args_to_int
    def rect(x, y):
        screen[0 : y, 0 : x] = 1

    @args_to_int
    def rotate_column(x, by):
        col = screen[:, x]
        by = -by
        col[:] = np.append(col[by:], col[:by])

    @args_to_int
    def rotate_row(y, by):
        row = screen[y, :]
        by = -by
        row[:] = np.append(row[by:], row[:by])

    tokenizer = ReTokenizer([
            (r'rect (\d+)x(\d+)', rect),
            (r'rotate column x=(\d+) by (\d+)', rotate_column),
            (r'rotate row y=(\d+) by (\d+)', rotate_row),
            ])

    for line in data:
        # print(line)
        tokenizer.match(line.strip())

    if second:
        return '\n'.join(''.join('#' if c else ' ' for c in row) for row in screen)
    return np.sum(screen)


def problem9(data, second):
    data = re.sub(r'\s+', '', ''.join(data))
    rx = re.compile(r'\((\d+)x(\d+)\)')

    def decompressed_len(data):
        pos = 0
        n = 0
        while True:
            m = rx.search(data, pos)
            if not m:
                return n + len(data) - pos
            n += m.start() - pos
            l, c = map(int, m.groups())
            c_s, c_e = m.end(), m.end() + l
            if second:
                n += decompressed_len(data[c_s:c_e]) * c
            else:
                n += l * c
            pos = c_e
    return decompressed_len(data)


def problem10(data, second):
    _data = '''value 5 goes to bot 2
bot 2 gives low to bot 1 and high to bot 0
value 3 goes to bot 1
bot 1 gives low to output 1 and high to bot 0
bot 0 gives low to output 2 and high to output 0
value 2 goes to bot 2'''.split('\n')

    bot_insns = {}
    value_seq = []
    bots = defaultdict(list)
    result = []

    def give(bot, value):
        bots[bot].append(value)
        bot_insns.get(bot, lambda:'')()

    def bot_action(bot, low, high):
        def action():
            values = bots[bot]
            assert len(values) <= 2
            if len(values) != 2: return
            # distribute values
            del bots[bot]
            values.sort()
            if values == [17, 61]:
                result.append(bot)
            # print('{} gives {} to {}, {} to {}'.format(bot, values[0], low, values[1], high))
            give(low, values[0])
            give(high, values[1])
        bot_insns[bot] = action

    def value_distribution(v, bot):
        value_seq.append((int(v), bot))

    tokenizer = ReTokenizer([
            (r'(bot \d+) gives low to ((?:bot|output) \d+) and high to ((?:bot|output) \d+)', bot_action),
            (r'value (\d+) goes to (bot \d+)', value_distribution),
    ])

    for line in data:
        # print(line)
        tokenizer.match(line.strip())

    for v, b in value_seq:
        give(b, v)
        # pprint(bots)

    if second:
        def o(n):
            return bots['output ' + str(n)][0]
        return o(0) * o(1) * o(2)
    return result[0]


def problem11(data, second, test=False):
    _data = '''The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.
The second floor contains a hydrogen generator.
The third floor contains a lithium generator.
The fourth floor contains nothing relevant'''.split('.')
    if second:
        data[0] += ' a elerium generator a elerium-compatible microchip a dilithium generator a dilithium-compatible microchip.'
    name_cnt, floor_cnt, state = None, None, None
    def parse():
        nonlocal name_cnt, state, floor_cnt
        microchips = {}
        generators = {}
        tokenizer = ReTokenizer([
                (r'\ba (\w+) generator\b', lambda s: generators.__setitem__(s, top_floor)),
                (r'\ba (\w+)-compatible microchip\b', lambda s: microchips.__setitem__(s, top_floor))])
        for top_floor, line in enumerate(data):
            tokenizer.find_all(line)

        floor_cnt = top_floor + 1
        names = {n: i for i, n in enumerate(sorted(set(microchips).union(generators)))}
        name_cnt = len(names)
        assert name_cnt == len(microchips) == len(generators)
        state = [-1] * (2 * name_cnt + 1)
        for n, f in microchips.items():
            state[names[n]] = f
        for n, f in generators.items():
            state[names[n] + name_cnt] = f
        state[-1] = 0 # floor
        state = tuple(state)

    def stuff_on_floor(state, n):
        return [i for i in range(len(state) - 1) if state[i] == n]

    def take_stuff(stuff):
        yield from itertools.combinations(stuff, 1)
        yield from itertools.combinations(stuff, 2)

    def hash_state(state):
        r = [state[i] + state[i + name_cnt] * 10 for i in range(name_cnt)]
        r.sort()
        r.append(state[-1])
        return tuple(r)

    def unhash_state(state):
        r = [None] * (name_cnt * 2 + 1)
        for i in range(name_cnt):
            r[i] = state[i] % 10
            r[i + name_cnt] = state[i] // 10
        r[-1] = state[-1]
        return tuple(r)

    def is_valid_floor(state, floor):
        if not any(state[i] == floor for i in range(name_cnt, name_cnt * 2)):
            # no generators
            return True
        if all(state[i] != floor or state[i + name_cnt] == floor for i in range(0, name_cnt)):
            # all chips are shielded
            return True
        return False

    def gen_next_state(state):
        def gen_next_state_dir(state, new_floor):
            stuff = stuff_on_floor(state, state[-1])
            for taken in take_stuff(stuff):
                new_state = list(state)
                new_state[-1] = new_floor
                for it in taken:
                    new_state[it] = new_floor
                new_state = tuple(new_state)
                if is_valid_floor(new_state, state[-1]) and is_valid_floor(new_state, new_state[-1]):
                    yield hash_state(new_state)
        state = unhash_state(state)
        if state[-1] < floor_cnt - 1:
            yield from gen_next_state_dir(state, state[-1] + 1)
        if state[-1] > 0:
            yield from gen_next_state_dir(state, state[-1] - 1)

    parse()
    state = hash_state(state)
    goal_state = hash_state([floor_cnt - 1] * (name_cnt * 2 + 1))

    if not test:
        path, _ = bfs([state], lambda s: s == goal_state, gen_next_state)
        return len(path) - 1

    t = time.clock()
    path2 = bfs_bidirectional([state], [goal_state], gen_next_state)
    print(time.clock() - t)
    t = time.clock()
    path1, _ = bfs([state], lambda s: s == goal_state, gen_next_state)
    print(time.clock() - t)
    assert len(path1) == len(path2)
    assert path1[0] == path2[0]
    assert path1[-1] == path2[-1]
    return 0


def problem12(data, second):
    _data = '''cpy 41 a
inc a
inc a
dec a
jnz a 2
dec a'''.split('\n')
    # if second:
    #     return

    class Env:
        def __init__(self, code):
            self.r = [0] * 4
            self.ip = 0
            self.code = code

        def step(self):
            if not (0 <= self.ip < len(self.code)):
                return False
            insn = self.code[self.ip]
            self.ip += 1
            insn(self)
            return True

    # peephole optimization
    data = '\n'.join(s.strip() for s in data)
    data = re.sub(r'^inc ([a-d])\ndec ([a-d])\njnz \2 -2$', 'zadd \\2 \\1\njmp_trap\njmp_trap', data, flags=re.MULTILINE)
    # return data
    data = data.split('\n')

    reg_name2idx = {n : i for i, n in enumerate('abcd')}

    jmp_trap_insn = lambda e: None

    def checked_jmp(e, offset):
        e.ip = e.ip + offset - 1
        assert e.code[e.ip] != jmp_trap_insn

    def cpy_imm(x, y):
        src = int(x)
        dst = reg_name2idx[y]
        return lambda e: e.r.__setitem__(dst, src)

    def cpy_reg(x, y):
        src = reg_name2idx[x]
        dst = reg_name2idx[y]
        return lambda e: e.r.__setitem__(dst, e.r[src])

    def inc_dec(insn, r):
        arg = {'inc': 1, 'dec': -1}[insn]
        r = reg_name2idx[r]
        return lambda e: e.r.__setitem__(r, e.r[r] + arg)

    def jnz(r, offset):
        r = reg_name2idx[r]
        offset = int(offset)
        def f(e):
            if e.r[r]:
                checked_jmp(e, offset)
        return f

    def jnz_imm(x, offset):
        x = int(x)
        offset = int(offset)
        if x:
            return lambda e: checked_jmp(e, offset)
        else:
            return lambda e: None

    def zadd(x, y):
        src = reg_name2idx[x]
        dst = reg_name2idx[y]
        def f(e):
            assert e.r[src] > 0
            e.r[dst] += e.r[src]
            e.r[src] = 0
        return f

    tokenizer = ReTokenizer([
            (r'cpy (-?\d+) ([a-d])', cpy_imm),
            (r'cpy ([a-d]) ([a-d])', cpy_reg),
            (r'(inc|dec) ([a-d])', inc_dec),
            (r'jnz ([a-d]|) (-?\d+)', jnz),
            (r'jnz (-?\d+) (-?\d+)', jnz_imm),
            (r'zadd ([a-d]) ([a-d])', zadd),
            (r'jmp_trap', lambda: jmp_trap_insn),
    ])

    code = []
    for line in data:
        code.append(tokenizer.match(line.strip()))

    e = Env(code)
    if second:
        e.r[2] = 1
    while e.step():
        pass
    return e.r[0]


def problem13(data, second):
    # data = 10
    # goal = [(7, 4)]
    goal = [(31, 39)]

    offsets4 = (( 0, -1), (0, 1), (-1, 0), (1, 0))

    def iswall(x, y):
        if x < 0 or y < 0:
            return True
        res = x*x + 3*x + 2*x*y + y + y*y + data
        return bin(res).count('1') & 1

    def print_grid(size_x, size_y):
        for y in range(size_y):
            print(''.join('#' if iswall(x, y) else ' ' for x in range(size_x)))

    def gen_next_state(pos):
        for offset in offsets4:
            x, y = offset[0] + pos[0], offset[1] + pos[1]
            if not iswall(x, y): yield x, y

    # print_grid(10, 10)
    if second:
        visited = {(0, 0): None}
        front = deque([(0, 0)])
        for step in range(50):
            _ = bfs_step(front, visited, lambda s: False, gen_next_state)
        return len(visited) - (1 if iswall(0, 0) else 0)

    path, _ = bfs([(0, 0)], lambda s: s == goal, gen_next_state)
    if path:
        return len(path) - 1


def problem14(data, second):
    # data = 'abc'
    buffer = deque()
    hash = hashlib.md5(data.encode('ascii'))
    def get_hash(idx):
        h = hash.copy()
        h.update(str(idx).encode('ascii'))
        if second:
            for _ in range(2016):
                h = hashlib.md5(h.hexdigest().encode('ascii'))
        return h.hexdigest()

    hexdigits = ''.join(f'{c:x}' for c in range(16))
    rx = re.compile('|'.join(f'{c}{{3,}}' for c in hexdigits))
    def get_runs(s):
        matches = rx.findall(s)
        r3 = matches[0][0] if matches else ''
        r5 = frozenset(s[0] for s in matches if len(s) >= 5)
        return r3, r5

    # print(get_runs('122333444455555566666666'))

    def gen():
        r3s, r5s = deque(), deque()
        idx_end = 0
        for idx in range(0, 100000000):
            while idx_end - idx <= 1000:
                r3, r5 = get_runs(get_hash(idx_end))
                r3s.append(r3)
                if r5:
                    r5s.append((idx_end, r5))
                idx_end += 1
            r3 = r3s.popleft()
            assert len(r3s) == 1000
            if r3:
                # only discard garbage now
                while r5s and r5s[0][0] <= idx:
                    r5s.popleft()

                def check():
                    for r5idx, r5 in r5s:
                        for c in r3:
                            if c in r5:
                                return idx, c, r5idx

                res = check()
                if res is not None:
                    yield res


    res = list(log_seq(itertools.islice(gen(), 64)))
    # for n, (i1, c, i2) in enumerate(res):
    #     s1, s2 = get_hash(i1), get_hash(i2)
    #     # print(f'{n:2} {i1:05}', s1, s2, c)
    #     assert i2 - i1 < 1000
    #     assert c * 3 in s1, c * 5 in s2
    return res[-1]


def problem15(data, second):
    # data = ['Disc #1 has 5 positions; at time=0, it is at position 4.',
    #         'Disc #2 has 2 positions; at time=0, it is at position 1.']
    if second:
        data.append(f'Disc #{len(data)+1} has 11 positions; at time=0, it is at position 0.')

    Disc = namedtuple('Disc', 'pos_count pos')
    discs = []

    @args_to_int
    def parse_disc(num, pos_count, time, pos):
        assert num == len(discs) + 1
        assert time == 0
        discs.append(Disc(pos_count, pos))

    tokenizer = ReTokenizer([
            (r'Disc #(\d+) has (\d+) positions; at time=(\d+), it is at position (\d+).', parse_disc),
            ])

    for line in data:
        # print(line)
        tokenizer.match(line.strip())

    def solve_ax_eq_b_mod_m(a, b, m):
        a = a % m
        b = b % m
        tmp = 0
        for x in range(m):
            if tmp == b:
                return x
            elif tmp == 0 and x != 0:
                return None
            tmp = (tmp + a) % m
        assert False

    time_offset = 0
    period = 1
    for t, disc in enumerate(discs):
        disk_offset = (disc.pos + t + 1 + time_offset) % disc.pos_count
        x = solve_ax_eq_b_mod_m(period, -disk_offset, disc.pos_count)
        assert x is not None
        time_offset += x * period
        period = period * disc.pos_count // gcd(period, disc.pos_count)
    return time_offset


def problem16(data, second):
    disksize = 272
    if second: disksize = 35651584

    # data = '10000'
    # disksize = 20

    while len(data) < disksize:
        data = data + '0' + ''.join(reversed(data.translate(str.maketrans('10', '01'))))
        # print(len(data))

    data = data[:disksize]
    print('generated')

    while len(data) % 2 == 0:
        data = ''.join('1' if data[i] == data[i + 1] else '0' for i in range(0, len(data), 2))
        print(len(data))

    return data


@profile
def problem17(data, second):
    # data = 'ulqzkmiv'
    basehash = hashlib.md5(data.encode('ascii'))

    class LinkedListStr(namedtuple('LinkedListStrBase', 's prev len hash')):
        __slots__ = ()
        def __new__(Cls, s):
            return super().__new__(Cls, s, None, len(s), hash(s))
        def __len__(self):
            return self.len
        def __hash__(self):
            return self.hash
        def __eq__(self, other):
            return self.s == other.s and self.len == other.len and self.prev == other.prev
        def rev_iter(self):
            x = self
            while x is not None:
                yield x.s
                x = x.prev
        def __str__(self):
            return ''.join(self.rev_iter())[::-1]
        def append(self, s):
            return super().__new__(self.__class__, s, self, self.len + len(s), hash((self.hash, s)))

    class State():
        __slots__ = 'x y path hash'.split()

        def __init__(self, x, y, path, hash):
            self.x = x
            self.y = y
            self.path = path
            self.hash = hash

        def __eq__(self, other):
            return self.x == other.x and self.y == other.y and self.path == other.path

        def __hash__(self):
            return hash((self.x, self.y, self.path))

    @profile
    def neighbors(state):
        hbase = state.hash
        state.hash = None
        if state.x == 3 and state.y == 3:
            return
        hs = hbase.hexdigest()

        dirs = (
            (0, b'U',  0, -1),
            (1, b'D',  0,  1),
            (2, b'L', -1,  0),
            (3, b'R',  1,  0),
        )

        for i, d, dx, dy in dirs:
            c = hs[i]
            if c not in 'bcdef':
                continue
            nx = state.x + dx
            ny = state.y + dy
            if 0 <= nx < 4 and 0 <= ny < 4:
                h = hbase.copy()
                h.update(d)
                yield State(nx, ny, state.path.append(d.decode('ascii')), h)

    def goal(state):
        return state.x == state.y == 3

    if second:
        path, visited = bfs([State(0, 0, LinkedListStr(''), basehash)], lambda s: False, neighbors)
        assert path is None
        return max(len(s.path) for s in visited.keys() if s.x == s.y == 3)

    path, visited = bfs([State(0, 0, LinkedListStr(''), basehash)], goal, neighbors)
    assert path is not None
    return str(path[-1].path)


def problem18(data, second):
    # data = '.^^.^.^^^^'
    # row_cnt = 10
    row_cnt = 40
    if second:
        row_cnt = 400000

    def step(row : np.ndarray):
        res = np.roll(row, -1) ^ np.roll(row, 1)
        res[-1] = 0
        return res

    def to(s):
        return np.array([1 if c == '^' else 0 for c in s + '.'])

    def fro(row):
        return ''.join('^' if c else '.' for c in row[:-1])

    first = row = to(data)
    safe = 0
    for i in range(row_cnt):
        safe += len(row) - sum(row) - 1
        # print(format(i, '3'), fro(row))
        row = step(row)
    return safe


def problem19(data, second):

    def slow(n):
        elves = [i + 1 for i in range(n)]
        pos = 1
        while len(elves) > 1:
            del elves[pos]
            pos = (pos + 1) % len(elves)
        return elves[0]

    def fast(n):
        n = n + 1
        kill_first = 0
        power = 1
        res = 0
        while n > 1:
            if kill_first:
                res += power
            power *= 2
            kill_first = not (n % 2)
            n = (n + 1) // 2
        return res + 1

    def part2_slow(n):
        elves = blist(i + 1 for i in range(n))
        pos = 0
        while len(elves) > 1:
            target = (pos + len(elves) // 2) % len(elves)
            del elves[target]
            pos = (pos + (pos < target)) % len(elves)
        return elves[0]

    def part2_fast(n):
        low = 1
        while True:
            high = low * 3
            if n <= high:
                break
            low = high
        if (n - low) * 2 <= n:
            return n - low
        return (n - low) * 2



    for n in range(1, 101):
        s, f = slow(n), fast(n)
        assert s == f, f'{s}, {f}'

    if not second:
        return fast(data)

    for n in range(1, 101):
        s, f = slow(n), fast(n)
        assert s == f, f'{s}, {f}'

    return part2_fast(data)

def problem20(data, second):
    # data = ['5-8', '0-2', '4-7']
    # if second: return

    lst = []
    for line in data:
        a, b = line.split('-')
        lst.append((int(a), int(b) + 1))
    m = 0
    lst.sort()
    if not second:
        for a, b in lst:
            if a > m:
                break
            if b > m:
                m = b
        return m

    cnt = 0
    lst.append((4294967295 + 1, 0))
    for a, b in lst:
        if a > m:
            cnt += a - m
        if b > m:
            m = b
    return cnt


def problem21(data, second):
    # data = [
    #     'swap position 4 with position 0',
    #     'swap letter d with letter b',
    #     'reverse positions 0 through 4',
    #     'rotate left 1 step',
    #     'move position 1 to position 4',
    #     'move position 3 to position 0',
    #     'rotate based on position of letter b',
    #     'rotate based on position of letter d',
    # ]
    # if second: return

    def run_encryption(s, commands, in_reverse=False):
        s = list(s)

        @args_to_int
        def swap_pos(i, j):
            s[i], s[j] = s[j], s[i]

        def swap_letter(a, b):
            i, j = s.index(a), s.index(b)
            s[i], s[j] = s[j], s[i]

        def rotate_impl(s, cnt):
            cnt = cnt % len(s)
            if not cnt: return s
            return s[-cnt:] + s[:-cnt]

        def rotate(dir, cnt):
            nonlocal s
            dir = {'left': -1, 'right': 1}[dir]
            if in_reverse:
                dir *= -1
            s = rotate_impl(s, dir * int(cnt))

        def rotate_letter(c):
            nonlocal s
            def find_preimages():
                for ii in range(len(s)):
                    preimage = rotate_impl(s, ii)
                    i = preimage.index(c)
                    r = rotate_impl(preimage, i + 1 + (i >= 4))
                    if r == s:
                        yield preimage
            if in_reverse:
                pp = list(find_preimages())
                [s] = pp
            else:
                i = s.index(c)
                rotate('right', i + 1 + (i >= 4))

        @args_to_int
        def reverse(i, j):
            j += 1
            s[i : j] = reversed(s[i : j])

        @args_to_int
        def move(i, j):
            if in_reverse:
                i, j = j, i
            c = s.pop(i)
            s.insert(j, c)

        tokenizer = ReTokenizer([
                (r'swap position (\d+) with position (\d+)', swap_pos),
                (r'swap letter (\w+) with letter (\w+)', swap_letter),
                (r'rotate (\w+) (\d+) steps?', rotate),
                (r'rotate based on position of letter (\w+)', rotate_letter),
                (r'reverse positions (\d+) through (\d+)', reverse),
                (r'move position (\d+) to position (\d+)', move),
                ])

        for line in commands:
            tokenizer.match(line)

        return ''.join(s)

    if not second:
       return run_encryption('abcdefgh', data)

    s = s_prev = 'fbgdceah'

    for line in reversed(data):
        s = run_encryption(s, [line], True)
        assert run_encryption(s, [line]) == s_prev
        s_prev = s
    # print(run_encryption('bacdefgh', data))
    return ''.join(s)


def problem22(data, second):
    data_ = split_data('''root@blah
    Filesystem            Size  Used  Avail  Use%
/dev/grid/node-x0-y0   10T    8T     2T   80%
/dev/grid/node-x0-y1   11T    6T     5T   54%
/dev/grid/node-x0-y2   32T   28T     4T   87%
/dev/grid/node-x1-y0    9T    7T     2T   77%
/dev/grid/node-x1-y1    8T    0T     8T    0%
/dev/grid/node-x1-y2   11T    7T     4T   63%
/dev/grid/node-x2-y0   10T    6T     4T   60%
/dev/grid/node-x2-y1    9T    8T     1T   88%
/dev/grid/node-x2-y2    9T    6T     3T   66%
    ''')

    # if second: return
    assert data[0].startswith('root')
    assert data[1].startswith('Filesystem')

    Node = namedtuple('Node', 'id x y size used avail perc')

    def parse_size(s):
        assert s[-1] == 'T'
        return int(s[:-1])

    def parse_node(s):
        id, size, used, avail, perc = s.split()
        zzz, x, y = id.split('-')
        assert zzz == '/dev/grid/node'
        assert x.startswith('x') and y.startswith('y')
        x, y = int(x[1:]), int(y[1:])
        return Node(id, x, y, parse_size(size), parse_size(used), parse_size(avail), perc)

    nodes = []
    for line in data[2:]:
        nodes.append(parse_node(line))

    if not second:
        return
        viable_pair_cnt = 0
        for a in nodes:
            for b in nodes:
                if not a.used: continue
                if a == b: continue
                if a.used <= b.avail:
                    viable_pair_cnt += 1

        return viable_pair_cnt

    min_size = min(n.size for n in nodes)
    size_x, size_y = max(n.x for n in nodes) + 1, max(n.y for n in nodes) + 1
    grid = np.full((size_x, size_y), ' ', dtype='S1', order='C')
    for n in nodes:
        grid[n.x, n.y] = (
            'G' if n.x == size_x - 1 and n.y == 0 else
            '_' if n.used == 0 else
            '.' if n.used <= min_size else
            '#')

    def print_grid(grid):
        print(b'\n'.join(b''.join(row) for row in grid).decode('ascii'))

    # print_grid(grid)
    path = len('............#.........') + 16 * 2 + 6
    path += (size_x - 1) + (size_x - 2) * 4
    return path


def asembunny(data):
    reg_name2idx = {n : i for i, n in enumerate('abcd')}

    Insn = namedtuple('Insn', 'code arg1 arg2')
    Insn.__str__ = lambda self: f'{self.code} {self.arg1} {self.arg2 if self.arg2 is not None else " "}'
    Insn.__new__.__defaults__ = (None,)

    Mulj = namedtuple('Insn', 'code arg1 arg2z dest tmp')

    class Env:
        def __init__(self, code):
            self.reset()
            self.code_cache = {}
            self.compile(code)

        def reset(self):
            self.r = [0] * 4
            self.ip = 0
            self.ops, self.skipped, self.compilations = 0, 0, 0

        def get(self, name):
            r = reg_name2idx.get(name)
            if r is not None:
                return self.r[r]
            return int(name)

        def set(self, name, value):
            r = reg_name2idx.get(name)
            if r is not None:
                self.r[r] = value

        def offset_to_addr(self, offset):
            return self.get(offset) + self.ip - 1

        def step(self):
            if not (0 <= self.ip < len(self.compiled)):
                return False
            insn = self.compiled[self.ip]
            # print(f'-- {self.ip:3} {self.source[self.ip]} {insn.insn} {self.r}')
            self.ip += 1
            insn(self)
            self.ops += 1
            return True

        def dump_code(self):
            for i, insn in enumerate(self.compiled):
                print(f'{i:3} {insn.insn}')

        def _match_seq(self, code, pos, *args):
            for arg in args:
                if pos >= len(code) or code[pos].code != arg:
                    return False
                pos += 1
            return True

        def optimize_addj(self, code):
            # inc a
            # dec c
            # jnz c -2
            for i in range(len(code) - 2):
                if not self._match_seq(code, i, 'inc', 'dec', 'jnz'):
                    continue
                if code[i + 1].arg1 != code[i + 2].arg1 or code[i + 2].arg2 != '-2':
                    continue
                code[i] = Insn('addj', code[i + 1].arg1, code[i].arg1)

        def optimize_mulj(self, code):
            # 0  cpy b c
            # 1  addj c a
            # 2  ----
            # 3  ----
            # 4  dec d
            # 5  jnz d -5
            for i in range(len(code) - 4):
                if not self._match_seq(code, i, 'cpy', 'addj', 'dec', 'jnz', 'dec', 'jnz'):
                    continue
                b, c, a, d = code[i].arg1, code[i].arg2, code[i + 1].arg2, code[i + 4].arg1
                assert len(set([b, c, a, d])) == 4
                assert code[i + 1].arg1 == c
                assert code[i + 5].arg1 == d and code[i + 5].arg2 == "-5"

                code[i] = Mulj('mulj', b, d, a, c)

        def compile(self, source):
            self.source = source = tuple(source)

            if source in self.code_cache:
                self.compiled = self.code_cache[source]
                return

            optimized = list(source)
            self.optimize_addj(optimized)
            self.optimize_mulj(optimized)
            self.compiled = []
            for it in optimized:
                self.compiled.append(compile_insn(it))

            self.code_cache[source] = self.compiled
            self.compilations += 1
            # self.dump_code()

        def out(self, x):
            print(f'out: {x!r}')

    def compile_insn(insn):
        def jnz(e):
            if e.get(insn.arg1):
                e.ip = e.offset_to_addr(insn.arg2)

        def tgl(e):
            addr = e.offset_to_addr(insn.arg1)
            if not (0 <= addr < len(e.source)):
                return
            src = list(e.source)
            t = src[addr]
            if t.arg2 is None:
                code = 'dec' if t.code == 'inc' else 'inc'
            else:
                code = 'cpy' if t.code == 'jnz' else 'jnz'

            src[addr] = Insn(code, t.arg1, t.arg2)
            e.compile(src)

        def addj(e):
            src = e.get(insn.arg1)
            dst = e.get(insn.arg2)
            assert src > 0
            e.set(insn.arg1, 0)
            e.set(insn.arg2, src + dst)
            e.skipped += src * 3 - 1
            e.ip = e.offset_to_addr(3)

        def mulj(e):
            arg1v = e.get(insn.arg1)
            arg2v = e.get(insn.arg2z)
            destv = e.get(insn.dest)
            assert arg1v > 0 and arg2v > 0
            e.set(insn.arg2z, 0)
            e.set(insn.tmp, 0)
            e.set(insn.dest, arg1v * arg2v + destv)
            e.skipped += arg2v * (arg1v * 3 + 2)
            e.ip = e.offset_to_addr(6)

        d = {
            'cpy': lambda e: e.set(insn.arg2, e.get(insn.arg1)),
            'inc': lambda e: e.set(insn.arg1, e.get(insn.arg1) + 1),
            'dec': lambda e: e.set(insn.arg1, e.get(insn.arg1) - 1),
            'jnz': jnz,
            'tgl': tgl,
            'addj': addj,
            'mulj': mulj,
            'out': lambda e: e.out(e.get(insn.arg1))
            }

        f = d[insn.code]
        f.insn = insn
        return f

    arg = '([a-d]|-?[0-9]+)'
    tokenizer = ReTokenizer([
            (rf'(inc|dec|tgl|out) {arg}', Insn),
            (rf'(cpy|jnz) {arg} {arg}', Insn),
    ])

    code = []
    for line in data:
        code.append(tokenizer.match(line.strip()))

    e = Env(code)
    return e


def problem23(data, second):
    data_ = split_data('''
cpy 2 a
tgl a
tgl a
tgl a
cpy 1 a
dec a
dec a
''')
    #  if second:
    #     return
    e = asembunny(data)

    e.r[0] = 12 if second else 7
    while e.step():
        if e.ops % 10000 == 0:
            print(f'ops={e.ops}, skipped={e.skipped}, compilations={e.compilations}, r={e.r}')
        pass
    print(f'ops={e.ops}, skipped={e.skipped}, compilations={e.compilations}, r={e.r}')
    return e.r[0]


def problem24(data, second):
    global quiet
    quiet = True

    data_ = split_data('''
###########
#0.1.....2#
#.#######.#
#4.......3#
###########''')
    # if second: return
    assert all(len(s) == len(data[0]) for s in data)

    nodepos = {}
    for y, line in enumerate(data):
        for x, c in enumerate(line):
            if c.isdigit():
                assert c not in nodepos
                nodepos[c] = (x, y)

    def distances():
        d = {}
        def n(state):
            for x, y in neighbours4(*state):
                if data[y][x] != '#':
                    yield x, y
        for node, node_xy in nodepos.items():
            _, visited = bfs([node_xy], lambda s: False, n)
            for n2, xy in nodepos.items():
                d[(node, n2)] = len(bfs_extract_path(xy, visited)) - 1
        return d

    d = distances()

    def iter_pathlengths():
        rest_nodes = [c for c in nodepos.keys() if c != '0']
        for path in itertools.permutations(rest_nodes):
            current = '0'
            l = 0
            for n in path:
                l += d[current, n]
                current = n
            if second:
                l += d[current, '0']
            yield l

    return min(iter_pathlengths())


def problem25(data, second):
    data_ = ''
    if second: return
    e = asembunny(data)
    # e.dump_code()

    found = False
    def out(i):
        nonlocal found
        if i != len(r) % 2:
            e.ip = -1
        r.append(i)

        if len(r) > 100:
            found = True
            e.ip = -1

    e.out = out

    for i in range(1000):
        e.reset()
        e.r[0] = i
        r = []
        while e.step():
            # if e.ops % 10000 == 9999:
            #     print(f'ops={e.ops}, skipped={e.skipped}, compilations={e.compilations}, r={e.r}')
            pass
        # print(f'ops={e.ops}, skipped={e.skipped}, compilations={e.compilations}, r={e.r}')
        print(f'{i}: {len(r)}')
        if len(r) > 100:
            return i







####

def problem(data, second):
    data = ''
    if second: return


###########

def solve_all():
    global quiet
    quiet = True
    for i in collect_problems():
        print('###', i)
        if i == 5:
            print('Skipping')
            continue
        print(solve(i))
        print(solve(i, True))


def main():
    problems = collect_problems()
    p = problems[-1]
    print(solve(p))
    print(solve(p, True))

if __name__ == '__main__':
    print('Hello')
    # problem17(get_data(17), True)
    # import cProfile
    # cProfile.run('problem13(get_data(13), True)', sort='cumtime')
    # t = time.clock()
    # problem13(get_data(13), True)
    # print(time.clock() - t)
    solve_all()
    # main()
