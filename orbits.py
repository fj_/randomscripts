import sys
import math
from math import sqrt
import Image, ImageDraw

posinf = float('+inf')
G = 6.67428e-11
earthMass = 6e24
sunMass = 1.98892e30
sun_mu = G * sunMass
day = 60.0 * 60 * 24

#http://en.wikipedia.org/wiki/Earth
#Semi-major axis = 149,597,887.5 km
#Eccentricity = 0.016710219
#
#http://en.wikipedia.org/wiki/Earth's_orbit
#Semi-major axis = 149,598,261 km
#Eccentricity = 0.01671123
 
earthSMA = 1.495978875e11
earthEcc = 0.016710219

earthSMA = 1.49598261e11
earthEcc = 0.01671123

def solve_newton(f, df, guess, eps, convergenceguard = 0.5):
    convstep = posinf;
    while True:
        v = f(guess)
        if abs(v) < eps: return guess
        newguess = guess - v / df(guess)
        newstep = abs(newguess - guess)
        if newstep > convstep:
            raise Exception('Method does not converge or converges too slowly')
        guess = newguess
        convstep = newstep * convergenceguard  
    

def orbitcoords(semimajor_axis, eccentricity, mu, t):
    """ returns (x, y) """
    semi_latus_rectum = semimajor_axis * (1 - eccentricity ** 2)
    mean_anomaly = math.sqrt(mu / semimajor_axis ** 3) * t
    eaf = lambda e: e - mean_anomaly - eccentricity * math.sin(e) 
    eadf = lambda e: 1 - eccentricity * math.cos(e)
    eccentric_anomaly = solve_newton(eaf, eadf, 0.0, 10.0e-10)
    true_anomaly = 2 * math.atan(math.sqrt((1 + eccentricity) / (1 - eccentricity))
                                 * math.tan(eccentric_anomaly / 2))
    r = semi_latus_rectum / (1 + eccentricity * math.cos(true_anomaly))
#    v = math.sqrt(mu * (2 / r - 1 / semimajor_axis))
    x = r * math.cos(true_anomaly)
    y = r * math.sin(true_anomaly)
    return (x, y)

# half-assed vector ops
def v_sq_len(v):
    return sum(x * x for x in v)
def v_len(v):
    return sqrt(v_sq_len(v))
def v_add(x, y):
    return tuple(x1 + y1 for x1, y1 in zip(x, y)) 
def v_sub(x, y):
    return tuple(x1 - y1 for x1, y1 in zip(x, y)) 
def v_mul(x, s):
    return tuple(x1 * s for x1 in x) 

def gravity_acceleration(mass, distance):
    """distance is a vector, returns a vector"""
    dst = v_len(distance)
    return v_mul(distance, -G * mass * dst ** -3)

def mass_from_acceleration_distance(acceleration, distance):
    """distance is a vector, returns a vector"""
    if len(acceleration): acceleration = v_len(acceleration)
    if len(distance): distance = v_len(distance)
    return (acceleration * distance ** 2) / G 

def center_of_mass(c1, m1, c2, m2):
    m = (m1 + m2)
    q1, q2 = m1 / m, m2 / m
    return v_add(v_mul(c1, q1), v_mul(c2, q2))  

def period(sma, mu):
    return 2 * math.pi * math.sqrt(sma ** 3 / mu)

def period2(sma, m1, m2):
    return 2 * math.pi * math.sqrt(sma ** 3 / (G * (m1 + m2)))

def mass_from_sma_period(sma, period):
    mu =  (sma ** 3) * (2 * math.pi / period) ** 2
    return mu / G

def calculate_L4(sun, planet):
    s2p = v_sub(planet, sun)
    half = v_add(sun, v_mul(s2p, 0.5))
    rccw = (-s2p[1], s2p[0])
    return v_add(half, v_mul(rccw, sqrt(3)/2))

def calculate_accelerations():
    sun = (0, 0)
    earth = orbitcoords(earthSMA, 0.0, sun_mu, 0)
    cm = center_of_mass(sun, sunMass, earth, earthMass)
    l4 = calculate_L4(sun, earth)
#    print sun
#    print earth
#    print cm
#    print l4
#    print v_len(v_sub(l4, sun)), v_len(v_sub(l4, earth)), v_len(v_sub(earth, sun)) 
    sun2l4 = v_sub(l4, sun)
    earth2l4 = v_sub(l4, earth)
    cm2l4 = v_sub(l4, cm) 
    a2 = gravity_acceleration(sunMass, sun2l4)
    a3 = gravity_acceleration(earthMass, earth2l4)
    ac = v_add(a2, a3)
    assert 0.0 == cm2l4[0] * ac[1] - cm2l4[1] * ac[0] # that's why it's L4
    # calculate fake mass from the rotation period (equal to Earth's)
    # as yet another way to self-check.
    earthPeriod = period2(earthSMA, sunMass, earthMass)
    cmMass = mass_from_sma_period(v_len(cm2l4), earthPeriod)
    cmMass1 = mass_from_acceleration_distance(ac, cm2l4)
    print cmMass - cmMass1
    print cmMass, cmMass1
    assert 0 == cmMass - cmMass1 
    print period(earthSMA, sun_mu) / day
    print period(v_len(cm2l4), cmMass * G) / day
    print period(v_len(cm2l4), cmMass1 * G) / day
    
#    print cmMass, cmMass1
#    print ac
#    print gravity_acceleration(cmMass, cm2l4)
#    print gravity_acceleration(cmMass1, cm2l4)
#    print gravity_acceleration(sunMass, cm2l4)
#    print gravity_acceleration(sunMass - earthMass, cm2l4) # lol
#    print sun, earth, l4

def draw_orbits():
    sma = earthSMA
    ecc = 0.03
    
    year = period(sma, sun_mu)
    
    #print period / day

    size = 800
    def to_px(v):
        def transform(p):
            return (p + sma * 1.3) / (sma * 2.6) * size
        return map(transform, v)
    
    im = Image.new('RGB', (size, size), 'white')
    draw = ImageDraw.Draw(im)
    def circle(xy, r, color):
        x, y = xy
        draw.ellipse((x - r, y - r, x + r, y + r), color)
        
    def planet_orbit(orbit, color):
        for i, p in enumerate(orbit):
            pprev = orbit[i - 1]
            draw.line(tuple(pprev + p), color)
            
    
    def planet_image(orbit, color):
        for p in orbit:
            circle(p, 2.0, color)
    
    
    circle(to_px((0, 0)), 15, 'yellow')
    
    def generate_orbits(steps):
        c1, c2 = [], []
        for i in range(steps):
            t = i * year / steps
            c1.append(to_px(orbitcoords(sma, ecc, sun_mu, t)))
            c2.append(to_px(orbitcoords(sma, 0.0, sun_mu, t)))
        return c1, c2
        
    orb1, orb2 = generate_orbits(16)
    orb1fine, orb2fine = generate_orbits(256)
    planet_orbit(orb1fine, '#FFB0B0')
    planet_orbit(orb2fine, '#B0B0FF')
    planet_image(orb1, '#FF0000')
    planet_image(orb2, '#0000FF')

    sma2 = sma * 0.5
    year2 = period(sma2, sun_mu)
    for k in range(20):
        ecc = 0.019 * k
        c1 = []
        steps = 256
        for i in range(steps):
            t = i * year2 / steps
            c1.append(to_px(orbitcoords(sma2, ecc, sun_mu, t)))
        planet_orbit(c1, '#404000')
    
    im.show()
    im.save('tmp/orbits.png')

if __name__ == '__main__':
    calculate_accelerations()
    draw_orbits()