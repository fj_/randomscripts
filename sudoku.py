def possible_ranges():
    small, large = 0, 0
    for i in range(9):
        small += i + 1
        large += 9 - i
        print(f'{i + 1}: {small}..{large}')


def triangle_number(n):
    return (n + 1) * n // 2


def triangle_numbers():
    for i in range(1, 10):
        print(f'1..{i} {triangle_number(i)}')
    for i in range(1, 10):
        print(f'{i}..9 {triangle_number(9) - triangle_number(i - 1)}')

def possible_sums(total, numbers, minbound=0):
    if numbers == 1:
        if minbound < total and total < 10:
            yield [total]
        return
    for i in range(minbound + 1, min(9, total - minbound)):
        for lst in possible_sums(total - i, numbers - 1, i):
            lst.append(i)
            yield lst

triangle_numbers()
print(list(possible_sums(31, 6)))

