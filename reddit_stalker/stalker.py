import json
# import sqlite3
from pprint import pprint
from datetime import timedelta, datetime
import os
from os import path as os_path
import itertools
from itertools import izip

EPOCH = datetime(1970, 1, 1) 
def utctotimestamp(dt) : 
    return (dt - EPOCH).total_seconds()

discard_after = timedelta(7).total_seconds()
user_name = ''
test_file_path = ''

try:
    # override stuff there
    from config import *
except:
    print 'fail'
    pass

storage_file_name = 'storage_{}.json'.format(user_name)


class Comment(object):
    def __init__(self, type, author, body, created_utc, link_url, subreddit, parent, is_new = None, amends = None, notes = None):
        self.__dict__.update(dict(
                type = type,
                author = author, 
                body = body, 
                created_utc = created_utc, 
                link_url = link_url,
                subreddit = subreddit,
                parent = parent, 
                is_new = is_new,
                amends = amends,
                notes = notes
                ))
    def __delattr__(self, name):
        raise AttributeError('Can\'t delete attributes, assign None instead')
    def __setattr__(self, name, value):
        if not hasattr(self, name):
            raise AttributeError('Attribute %r not found, can\'t create new attributes.' % name)
    def __repr__(self):
        return '{}({} in {} at {}: {!r}...)'.format(
                self.type, self.author, self.subreddit, self.created_utc, self.body[:40])
    equality_fields = ['type', 'author', 'body', 'created_utc', 'link_url', 'subreddit', 'parent']
    def __eq__(self, other):
        if not isinstance(other, Comment): return False
        d1 = self.__dict__
        d2 = other.__dict__
        return all(d1[name] == d2[name] for name in self.equality_fields)
    def __hash__(self):
        d = self.__dict__
        return sum(hash(d[name]) for name in self.equality_fields)

def pairwise(iterable):
    's -> (s0,s1), (s1,s2), (s2, s3), ...'
    a, b = itertools.tee(iterable)
    next(b, None)
    return izip(a, b)

def load_raw_data():
    if test_file_path:
        with open(test_file_path, 'r') as f: 
            return json.load(f)
    elif user_name:
        return ''
    else:
        assert False, 'No input source specified'

def parse_comments(data):
    result = []
    assert data['kind'] == 'Listing'
    for item in data['data']['children']:
        try:
            item_kind = item['kind'] 
            item = item['data']
            if item_kind == 't1': # comment, apparently
                item_id = item['id'] # comment id
                link_kind, link_id = item['link_id'].split('_') # post id, apparently
                assert link_kind == 't3'
                subreddit = item['subreddit']
                link_url = (
                        'http://www.reddit.com/r/{}/comments/{}/lol_title/{}'
                        .format(subreddit, link_id, item_id))
                result.append(Comment(
                        'Comment', item['author'], item['body'], 
                        item['created_utc'], link_url, subreddit, item['parent_id']))
            elif item_kind == 't3': # post
                text = item['title']
                selftext = item.get('selftext', '')
                if selftext: text += '\n' + selftext

                subreddit = item['subreddit']
                link_url = 'http://www.reddit.com' + item['permalink']
                result.append(Comment(
                        'Post', item['author'], text,
                        item['created_utc'], link_url, subreddit, None))
            else:
                assert False, 'Unknown kind of item %r' % item_kind
        except:
            print 'Error while parsing item:'
            print '========================='
            pprint(item) 
            print '========================='
            raise
    return result

def load_storage():
    if not os_path.exists(storage_file_name): return []
    with open(storage_file_name, 'r') as f: 
        data = json.load(f)
    result = []
    for item in data:
        try:
            result.append(Comment(**item))
        except:
            print 'Error while parsing item:'
            print '========================='
            pprint(item) 
            print '========================='
            raise
    return result

def save_storage(data):
    unwrapped = [it.__dict__ for it in data]
    if os_path.exists(storage_file_name):
        backup = storage_file_name + '.bak'
        if os_path.exists(backup):
            os.remove(backup)
        os.rename(storage_file_name, backup)
    with open(storage_file_name, 'w') as f:
        json.dump(unwrapped, f, indent = 4)

# def update_storage(storage, new_data, now):
#     for msg in storage:
#         msg.is_new = False
#     for msg in new_data:
#         msg.is_new = True
#     
#     if not len(new_data): return
# 
#     assert all(x.created_utc < y.created_utc for x, y in pairwise(new_data))
#     new_data_set = set(new_data)
#     
#     # find deleted messages in storage
#     earliest_new_data = new_data[0].created_utc
#     for it in storage:
#         if it.created_utc > earliest_new_data and it not in new_data_set:
            
    
def drop_older_messages(messages, now, description):
    filtered = filter(lambda comment: now - comment.created_utc < discard_after, 
            messages)
    cnt = len(messages) - len(filtered)
    if cnt:
        print '{} of {} messages ({}) dropped as too old'.format(cnt, len(messages), description)
    return filtered


def main():
    data = load_raw_data()
    now = utctotimestamp(datetime.utcnow())
    
    parsed = parse_comments(data)
    storage = load_storage()
    parsed = drop_older_messages(parsed, now, 'fetched from remote')
    storage = drop_older_messages(storage, now, 'fetched from storage')
    
#    pprint(storage)
#    pprint(parsed)
#    save_storage(parsed)

if __name__ == '__main__':
    main()