import requests, time, pickle, datetime
from pathlib import Path
import re
import json
import sys
from pprint import pprint
from collections import Counter
import sqlite3
from addict import Dict as AdDict

# modhash = None
# def login_if_necessary(username, password):
#     global modhash
#     if modhash is not None: continue
#     sleepy_get_json('https://ssl.reddit.com/api/login')

def sleepy_get_json(url, request_interval=2.0, last_get_time=[None]):
    t = last_get_time[0]
    if t is not None:
        sleep_for = request_interval - (time.time() - t)
        if sleep_for > 0:
            print('Sleeping for {}'.format(sleep_for))
            time.sleep(sleep_for)
    print('Getting {!r}'.format(url))
    headers = {
            'User-Agent': 'Very Descriptive User Agent String',
    }
    result = requests.get(url, headers=headers).json()
    last_get_time[0] = time.time()
    return result

url_database = None

def cached_get_json(url):
    global url_database
    if url_database is None:
        url_database = sqlite3.connect(str(Path(__file__).parent / 'url_cache.sqlite'))
        url_database.isolation_level = None
        url_database.execute('''create table if not exists cached_requests
        (url text not null,
         response text not null,
         primary key (url))''')
#         pprint(url_database.execute('select * from cached_requests').fetchall())

    res = url_database.execute('select response from cached_requests where url=?',
            (url,)).fetchone()
    if res is not None:
        result = json.loads(res[0])
        error = result.get('error') if isinstance(result, dict) else None
        if not error or error == 403: # cache banned user pages
            print('Using cached {!r}'.format(url))
            return result
    res = sleepy_get_json(url)
    url_database.execute(
            'insert or replace into cached_requests ' +
            '(url, response) values (?, ?)',
            (url, json.dumps(res)))
    return res

def get_json(url, cached=True):
    return (cached_get_json if cached else sleepy_get_json)(url)

def print_flowing(s, file, maxcol=100):
    from formatter import DumbWriter
    w = DumbWriter(maxcol=maxcol, file=file)
    for it in s.split('\n'):
        w.send_flowing_data(it)
        w.send_paragraph(1)


def pprint_json(js):
    print(json.dumps(js, indent=4, sort_keys=True))


class Forbidden(Exception):
    pass


def fetch_page(url, count=1000, sort=None, cached=False):
    after = None
    result = []
    while count > 0:
        limit = min(count, 100)  # 100 is max allowed value
        first_params_separator = '&' if '?' in url else '?'
        url += f'{first_params_separator}limit={limit}'
        if sort is not None:
            url += '&sort=' + sort
        if after:
            url += '&after=' + after
            cached = True
        r = get_json(url, cached)
        if r.get('error') == 403:
            raise Forbidden(url)
        result += r['data']['children']
        count -= limit
        after = r['data'].get('after')
        if not after: break
    return result


def fetch_comments(user, count, sort=None, cached=False):
    return fetch_page(f'http://www.reddit.com/user/{user}/comments.json', count, sort, cached)


def typeless_id(item_id):
    if '_' in item_id:
        _, item_id = item_id.split('_')
    return item_id


def comment_url(data, item_id=None):
    if item_id is None:
        item_id = data['id']
    return 'http://www.reddit.com/r/{}/comments/{}/x/{}'.format(
            data['subreddit'], typeless_id(data['link_id']), typeless_id(item_id))

def fetch_parent_comment(data):
    r = get_json(comment_url(data, data['parent_id']) + '.json')
    stuff = r[1]['data']['children']
    if stuff:
        return stuff[0]['data']

def persist(identification, func):
    file_name = 'stalker2.{}.pickle'.format(identification)
    if Path(file_name).exists():
        with open(file_name, 'rb') as f:
            data = pickle.load(f)
    else:
        data = func()
        with open(file_name, 'wb') as f:
            pickle.dump(data, f)
    return data

def print_comment(comment, file=sys.stdout):
    time = str(datetime.datetime.utcfromtimestamp(comment['created_utc']))
    score = '+{}/-{}'.format(comment['ups'], comment['downs'])
    print('=' * 40, file=file)
    print(time, score, comment_url(comment), file=file)
    if 'parent_comment' in comment:
        print('In reply to:', comment['parent_comment']['author'], file=file)
    print_flowing(comment['body'], file=file)
    print(file=file)
    file.flush()


def main():
    user = 'bitcointip'
    comments = persist(user, lambda: fetch_comments(user, 5000))
    assert all(it['kind'] == 't1' for it in comments)
    comments = [it['data'] for it in comments]
    print(len(comments))
    def extract_btc_amount(comment):
        s = comment['body']
        match = re.search(r';([0-9.,]+)\s*BTC', s)
        assert match, s
        return float(match.group(1))
    stuff = [(extract_btc_amount(comment), comment) for comment in comments]
    stuff.sort(reverse=True)
    for amount, comment in stuff[:20]:
        print(amount)
        print_comment(comment)
#     print_comment(comments[-2])
    sys.exit()


    for it in comments:
        print_comment(it)
        print('=' * 40)

    print('yo')

def main2():
    user = 'zergling_Lester'
    # user = 'IlForte'
    print(f'Stalking {user}')
#     subreddit = settings['subreddit']
    if 0:
        comments = persist(user + '.top', lambda: fetch_comments(user, 5000, 'top'))
    else:
        comments = persist(user, lambda: fetch_comments(user, 5000))

    assert all(it['kind'] == 't1' for it in comments)
#     for it in reversed(comments):
#         after = it['data'].get('after')
#         if after: print after

    comments = [it['data'] for it in comments]
    print(len(comments))

    def attach_parent(comment):
        parent = fetch_parent_comment(comment)
        if parent is not None:
            comment['parent_comment'] = parent
        return comment
    # comments = map(attach_parent, comments)

    with open(Path(__file__).parent / 'text_dump.txt', 'w', encoding='utf-8') as f:
        for i, it in enumerate(comments):
            print(i)
            print_comment(it, file=f)

    print('yo')

def get_modifiers(text):
    '\n\n##Non-Beta##\n###Ironclad###\n\n###Modifiers:###\n* **Brewmaster:** Start with 5 copies of Alchemize and the White Beast Statue.\n\n* **Certain Future:** The map contains only one path.\n\n* **Big Game Hunter:** Elite enemies are now swarming the Spire and drop better rewards.\n\n***  \n##Beta##'
    m = re.search('\n##Non-Beta##\n.*\n###Modifiers:###\n(.*)\n##Beta##\n', text, re.DOTALL).group(1)
    r = re.findall(r'\* \*\*([^:]*):\*\*', m)
    assert len(r) == 3
    return r

def all_pairs(mods):
    for mmm in mods:
        mmm = sorted(mmm)
        yield mmm[0], mmm[1]
        yield mmm[0], mmm[2]
        yield mmm[1], mmm[2]


def main3():
    # Inspecting search results
    subreddit = 'slaythespire'
    question = requests.utils.quote('title:"Daily Run Discussion"')
    url = f'https://www.reddit.com/r/{subreddit}/search.json?q={question}&include_over_18=on&restrict_sr=on&sort=new'
    res = fetch_page(url, cached=True)
    print(f'Got {len(res)} results')
    mods = [get_modifiers(it['data']['selftext']) for n, it in enumerate(res[30:90])] # :137
    pprint(Counter(m for mmm in mods for m in mmm))
    cnt = Counter(all_pairs(mods))
    pprint(cnt.most_common(10))
    print('yo')


def main4():
    # list abandoned subreddits
    cached = True
    now = time.time()
    url = 'https://www.reddit.com/subreddits/popular.json'
    res = fetch_page(url, cached=cached)
    print(f'Got {len(res)} results')
    # print(json.dumps(res[0], indent=4, sort_keys=True))
    inactive_subs = []
    for it in res:
        sub = AdDict(it['data'])
        sub.freeze()
        print(sub.url, sub.subscribers)
        mods = fetch_page('https://www.reddit.com' + sub.url + 'about/moderators.json', cached=cached)
        #pprint_json(mods)
        def is_active(user):
            try:
                comments = fetch_comments(user, 1, cached=cached)
            except Forbidden:
                # banned!
                return False

            if not len(comments):
                return False

            comment = comments[0]['data']
            created_utc = comment['created_utc']
            age = now - created_utc
            assert age > -60 * 60 # OK if comment newer than 1 hour after we started scanning
            return age < 60 * 60 * 24 * 60

        mods = [m['name'] for m in mods]
        if not any(filter(is_active, mods)):
            print('Inactive!')
            inactive_subs.append(sub)
        for sub in inactive_subs:
            print(f'{sub.url} subscribers: {sub.subscribers}')



if __name__ == '__main__':
    main2()
