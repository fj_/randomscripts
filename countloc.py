from __future__ import with_statement
import fnmatch
import sys
import os
def CountLOC(wildcard, path = ''):
    loc = 0
    nonEmptyLoc = 0
    for dirpath, dirnames, filenames in os.walk(path):
    	print dirpath
        for filename in fnmatch.filter(filenames, wildcard):
            with open(os.path.join(dirpath, filename)) as f:
                for line in f:
                    line = line.strip()
                    if len(line) > 0: nonEmptyLoc += 1
                    loc += 1
    return (loc, nonEmptyLoc)
        

if __name__ == '__main__':
    print sys.argv[1:]
    print CountLOC(*sys.argv[1:])
    #print CountLOC('*.py')