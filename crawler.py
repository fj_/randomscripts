# -*- coding: utf-8 -*-
"""A library for crawling and dumping site contents.

I made it when I needed to dump a repository published via ViewVC,
then massively refactored when I needed to replicate an UTF-8 encoded 
directory, and wget doesn't work with unicode very well.

It is not in a stable state, must be run customized manually and requires
careful monitoring. I suspect that it still doesn't work with unicode or
special characters very well. It turned out that we simply can't have nice 
things, 'from __future__ import unicode_literals' breaks doctest for example,
so the correct choice seems to be to declare that we use utf-8 encoded plain 
strings and that is all. Well, not all, we need to convert them to unicode
when working with OS (done in `dump`)!

"""

import contextlib

from htmllib import HTMLParser

import time
import codecs
from urlparse import urlparse, urlunparse, urljoin
import urllib
from urllib import quote as urlquote, unquote as urlunquote
from urllib import urlretrieve

def urlopen(url): return contextlib.closing(urllib.urlopen(url))

import formatter
from pprint import pprint
from utils import Progress_printer, format_float_to_width, parse_simple_list

# explicitly import filesystem stuff so that I can test it
from os.path import exists as path_exists, join as path_join, basename as path_basename
from os import makedirs

class Anchor(object):
	__slots__ = ('href', 'name', 'type', 'data')
	def __init__(self, href, name, type, data = ''):
		self.href = href
		self.name = name
		self.type = type
		self.data = data
	
	def __repr__(self):
		def strAttr(attr):
			val = self.__getattribute__(attr)
			if val is not None and val != '':
				return ' ' + attr + '=\'' + str(val) + '\''
			return ''
			
		return ('<a' + 
			''.join(strAttr(s) for s in ('href', 'name', 'type')) + 
			'>' + self.data + '</a>')
		
	
class Anchors_extractor(HTMLParser):
	def __init__(self):
		HTMLParser.__init__(self, formatter.NullFormatter())
		
	def reset(self):
		HTMLParser.reset(self)
		self.anchors = []
		self.savedAnchor = None

	def anchor_bgn(self, href, name, type):
		self.current_anchor = Anchor(href, name, type)
		self.save_bgn()
	
	def anchor_end(self):
		self.current_anchor.data = self.save_end()
		self.anchors.append(self.current_anchor)
		self.current_anchor = None
		
	def parse_file(self, data):
		'''
		Works with any file-like object, that supports iteration 
		'''
		self.reset()
		for l in data:
			self.feed(l)
		self.close()
		anchors = self.anchors
		self.reset()
		return anchors

def normalize_link(baseurl, url):
	"""Strip queries and fragments, also urlunparse might do some normalization
	
	>>> normalize_link('http://example.com/base/', '/test/test.html?x=y#z')
	'http://example.com/test/test.html'
	"""
	url = urljoin(baseurl, url)
	parsed = urlparse(url)
	url = urlunparse(list(parsed[:3]) + [''] * 3)
	return url.lower()


def get_internal_deeper_links(url, anchors):
	"""Assume that the structure is encoded in the path only, i.e. no 
	queries or fragments. Filter the links so that only the deeper links to the same
	host are returned. Oh, and yield pairs (name, url)
	"""
	for a in anchors:
		if a.href:
			link = normalize_link(url, a.href)
			if link:
				if link == url:
					pass # ignore, probably was fragment/query
				elif link.startswith(url):
					yield (link, a.data)
				elif url.startswith(link):
					pass # link back, silently ignore
				else:
					print "Warning! Link points somewhere else entirely: ", a
			

def walk(url, path, dirs_and_files_extractor):
	"""dirs_and_files_extractor takes url, a list of anchors 
	and must return (dirs, files), where each is a list of (url, name).
	
	Yields (path, local_path, dirs, files)"""
	with urlopen(url) as f:
		# don't check headers, assume utf-8 encoding
		uf = (f)
		anchors = Anchors_extractor().parse_file(uf)
	dirs, files = dirs_and_files_extractor(url, anchors)
	yield (url, path, dirs, files)
	for url, name in dirs:
		#maybe should add escaping and stuff 
		new_path = path_join(path, name)
		for r in walk(url, new_path, dirs_and_files_extractor):
			yield r


def dump(walker):
	progress = Progress_printer()
	for url, path, dirs, files in walker:
		path = path.decode('utf-8') # OS likes unicode
		print url + ' -> "' + path + '"'
		if not path_exists(path):
			makedirs(path)
		for f in files:
			full_name = path_join(path, f[1].decode('utf-8'))
			file_name = path_basename(full_name)
			
			# --------- This part does nice reporting.
			megabyte = 1024.0 ** 2 
			report_interval = 0.1
			start_time = time.clock()
			report_time = [start_time - 2 * report_interval]
			saved_total_bytes = [0]
			def do_report(size_bytes, total_bytes, last = False):
				if not saved_total_bytes[0] and total_bytes: saved_total_bytes[0] = total_bytes 
				downloaded_mb = size_bytes / megabyte
				total_mb = total_bytes / megabyte 
				downloading_for = report_time[0] - start_time
				speed_mb = downloaded_mb / downloading_for if downloading_for > 0.5 else 0.0 
				percent = min(1.00, downloaded_mb / total_mb) if total_mb else 0.0
				percent = int(percent * 100)
				s = u'"%s" %02d%% of %sMB, %sMB/s' % (
					file_name, percent, 
					format_float_to_width(total_mb, 4), format_float_to_width(speed_mb, 4))
				progress.update(s, last)
				
			def report_hook(blockcnt, blocksize, total):
				curr_time = time.clock()
				if curr_time - report_time[0] >= report_interval:
					report_time[0] = curr_time
					do_report(blockcnt * blocksize, total)
			
			# ---------- end of nice reporting, real work follows.
					
			try:
				urlretrieve(f[0], full_name, report_hook)
			except:
				print '... Interrupted!'
				raise
			
			# some more nice reporting
			report_time[0] = time.clock()
			stb = saved_total_bytes[0]
			do_report(stb, stb, True)

######################################

def simple_extractor(url, anchors):
	dirs, files = [], []
	for link in get_internal_deeper_links(url, anchors):
		(dirs if link[0].endswith('/') else files).append(link)
	return dirs, files

def prepare_url(url):
	"""normalize url, urldecode and then urlencode the path part,
	return (url, path) where path is the last path component of the url.
	Note: valid paths end with '/'!
	
	>>> prepare_url('http://example.com/base/а%20б/index.html?s=z#xxx')
	('http://example.com/base/%d0%b0%20%d0%b1/index.html', '\\xd0\\xb0 \\xd0\\xb1')
	"""
	url = urlparse(url)
	path = urlunquote(url[2])
	# in 'a/b/c' we need b, add empty strings to front in case of bad url.  
	localpath = (['', ''] + path.rsplit('/', 2))[-2]
	url = urlunparse(list(url[:2]) + [urlquote(path)] + [''] * 3).lower()
	return (url, localpath)
	
	
	

if __name__ == '__main__':
#	import crawler_test
#	crawler_test.run()
# 	sys.exit()
	urls = parse_simple_list("""
	
	""")
	pprint(urls)
	for url in urls:
		url, local = prepare_url(url)
		print 'Downloading ' + url + ' -> "' + local + '"'
		dump(walk(url, local, simple_extractor))
