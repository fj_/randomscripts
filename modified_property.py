    def set_flag_on_modification(private_name, default_value = None, doc = None):
        '''Creates a property and a function which returns True if the property 
        has been modified since the last call.
        
        >>> class Cls(object):
        ...    field, field_modified = set_flag_on_modification('_field', 'default')
        ...
        >>> o = Cls()
        >>> o.field_modified()
        False
        >>> o.field
        'default'
        >>> o.field = 'asd'
        >>> o.field_modified()
        True
        >>> o.field_modified()
        False
        >>> o.field = 'asdf'
        >>> o.field_modified()
        True
        >>> o.field = 'asdf'
        >>> o.field_modified()
        False
        >>> o1 = Cls()
        >>> o1.field
        'default'
        >>> o1.field = 'default'
        >>> o1.field_modified()
        False
        '''
        private_flag_name = private_name + '_modified'
        def value_getter(self):
            return self.__dict__.setdefault(private_name, default_value)
        def value_setter(self, value):
            old_value = self.__dict__.get(private_name, default_value)
            self.__dict__[private_name] = value
            if value != old_value: self.__dict__[private_flag_name] = True
        def check_modified(self):
            value = self.__dict__.setdefault(private_flag_name, False)
            if value: self.__dict__[private_flag_name] = False
            return value
        return (property(value_getter, value_setter, doc),
                check_modified)
    
    if __name__ == '__main__':
        import doctest
        doctest.testmod(verbose = True)
