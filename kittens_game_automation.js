// ==UserScript==
// @name         Kittens Game Automation
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        http://bloodrizer.ru/games/kittens/
// @grant        none
// ==/UserScript==


var autocfg;
var autocfgOrder;
var autocfgMenuAddition;

/*
    ui = '<div id="farRightColumn" class="column"> zzz ' +
        '<a id="scriptOptions" onclick="selectOptions()"> | autokitty </a>' +
        '<div id="optionSelect" style="display:none; margin-top:-400px; margin-left:-100px; width:200px" class="dialog help">';

        if (value === true || value === false) {
            console.error(attr);
            console.error('<button onclick="autoCheckboxClicked("' + attr + '")>' + attr + '</button></br>');
            ui += '<button onclick="autoCheckboxClicked("' + attr + '")">' + attr + '</button></br>';
        }
    ui += '</div>';
    autocfgMenuAddition = ui;

    //$("#footerLinks").append(autocfgMenuAddition);

*/

function selectOptions() {
    $("#optionSelect").toggle();
}


function autoCheckboxClicked(name) {
    var newValue = !autocfg[name];
    autocfg[name] = newValue;
    gamePage.msg('Auto ' + name  + ' is now ' + newValue);
}


//--------------

function autoHunt() {
    var catpower = game.resPool.get('manpower');
    if (catpower.value / catpower.maxValue > 0.95) {
        $("a:contains('Send hunters')").click();
    }
}


function autoObserve() {
    var checkObserveBtn = document.getElementById("observeBtn");
    if (checkObserveBtn !== null) {
        checkObserveBtn.click();
    }
}


function autoPray() {
    var faith = game.resPool.get('faith');
    if (faith.value / faith.maxValue > 0.1) {
        $("a:contains('Praise the sun!')").click();
    }
}


function craftSome(resourceName) {
    if (game.workshop.getCraft(resourceName).unlocked) {
        amount = 0.05 * game.workshop.getCraftAllCount(resourceName);
        if (amount > 0) {
            game.craft(it, Math.max(amount, 1), true, true);
        }
    }
}

function wantToCraft(resourceName) {
	var res = game.resPool.get('steel');
	//res =
}

function autoCraftResources() {
    var resources = [
        ["catnip",      "wood"],
        ["wood",        "beam"],
        ["minerals",    "slab"],
        ["coal",        "steel"],
        ["iron",        "plate"],
        ["oil",         "kerosene"],
        ["uranium",     "thorium"],
        ["unobtainium", "eludium"],
    ];

    for (var i = 0; i < resources.length; i++) {
        var from = resources[i][0];
        var to = resources[i][1];
        var curRes = game.resPool.get(from);
        if (curRes.value < curRes.maxValue * 1.1 && curRes.value > curRes.maxValue * 0.95) {
            craftSome(to);
        }
    }

    if (game.workshop.getCraft('alloy').unlocked) {
        var steel = game.resPool.get('steel');
        var titanium = game.resPool.get('titanium');
        var alloy = game.resPool.get('alloy');
        if (titanium.value < titanium.maxValue * 1.1 && titanium.value > titanium.maxValue * 0.90 &&
            alloy.value * 5 < steel.value) {
            craftSome('alloy');
        }
    }
}


function autoCraftFurDerivatives() {
    if (!game.workshop.getCraft('parchment').unlocked) return;

    var furs = game.resPool.get('furs');
    var fursPertick = game.getResourcePerTick('furs', true);
    var fursToZero = furs.value / -fursPertick;

    var catpower = game.resPool.get('manpower');
    var catpowerPertick = game.getResourcePerTick('manpower', true);
    var catpowerToCap = catpower.maxValue / catpowerPertick;

    if (furs.value > 2000 &&
        ((catpowerToCap > 0 && fursToZero > Math.max(5 * 600, catpowerToCap * 5)) || (fursToZero <= 0))) {
        craftSome('parchment');
    }

    if (game.resPool.get('parchment').value > autocfg.minParchment) craftSome('manuscript');
    if (game.resPool.get('manuscript').value > autocfg.minManuscript) craftSome('compedium');
    if (game.resPool.get('compedium').value > autocfg.minCompedium && game.resPool.get('blueprint').value < autocfg.maxBlueprint) craftSome('blueprint');
}


function autoTrade() {
    var titanium = game.resPool.get('titanium');
    var uranium = game.resPool.get('uranium');
    var gold = game.resPool.get('gold');
    var steel = game.resPool.get('steel');

    trades = 1;

    var tradesPerSecond = game.getResourcePerTick('gold', true) * 5 / 15;
    if (tradesPerSecond > 0) {
        trades = Math.ceil(tradesPerSecond * game.opts.autocfg.targetSecondsPerTrade);
    }

    function tradeWith(who) {
        who = game.diplomacy.get(who);
        if (who.unlocked) {
			var tr = trades;
			while (tr > 0) {
				if (game.diplomacy.hasMultipleResources(who, tr)) {
					game.diplomacy.tradeMultiple(who, tr);
					return true;
				}
				tr = Math.floor(tr / 2);
			}
		}
    }

    if (gold.value > gold.maxValue * 0.95 || (autocfg.aggressiveTrade && gold.value > trades * 15)) {
        while (true) {
            if (titanium.value < titanium.maxValue * 0.95) {
                if (tradeWith('zebras')) break;
            }
            if (titanium.value > titanium.maxValue * 0.8 && uranium.value < uranium.maxValue * 0.5) {
                if (tradeWith('dragons')) break;
            }
            if (game.opts.autocfg.tradeOverride) {
                if (tradeWith(game.opts.autocfg.tradeOverride)) break;
            }
            if (tradeWith('griffins')) break; // get iron and spice in early game, don't waste slabs in late game.
            break;
        }
    }
}


function autoParty() {
    if (game.science.get("drama").researched) {
        var catpower = game.resPool.get('manpower').value;
        var culture = game.resPool.get('culture').value;
        var parchment = game.resPool.get('parchment').value;

        if (catpower > 1500 && culture > 5000 && parchment > 2500) {
            var minDays = 0;
            if (game.prestige.getPerk("carnivals").researched) {
                minDays = 800;
            }
            if (game.calendar.festivalDays <= minDays) {
                game.village.holdFestival(1);
                // it's done in UI so we have to be honest and do it ourselves
                game.resPool.addResEvent("manpower", -1500);
                game.resPool.addResEvent("culture", -5000);
                game.resPool.addResEvent("parchment", -2500);
            }
        }

    }
}


function initAutocfg() {
    if (autocfg !== undefined && autocfg === game.opts.autocfg) return;
    autocfg = game.opts.autocfg || {};
    game.opts.autocfg = autocfg;
    autocfgOrder = [];

    function set(attr, value) {
        if (!(attr in autocfg)) {
            autocfg[attr] = value;
        }
        autocfgOrder.push(attr);
    }

    set('interval', 5);
    set('master', true);
    set('pray', true);
    set('hunt', true);
    set('craftResources', true);
    set('craftFurDerivatives', true);
    set('trade', true);
    set('aggressiveTrade', false);
    set('targetSecondsPerTrade', 30);
    set('tradeOverride', '');
    set('party', false);
    set('minParchment', 100);
    set('minManuscript', 25);
    set('minCompedium', 50);
    set('maxBlueprint', 0);
}

var tickerCountdown = 0;

function update() {
    if (tickerCountdown > 0) {
        tickerCountdown -= 1;
        return;
    }
    else {
        tickerCountdown = autocfg.interval;
    }

    if (!autocfg.master) return;
    autoObserve();
    if (autocfg.pray) autoPray();
    if (autocfg.hunt) autoHunt();
    if (autocfg.craftResources) autoCraftResources();
    if (autocfg.craftFurDerivatives) autoCraftFurDerivatives();
    if (autocfg.trade) autoTrade();
    if (autocfg.party) autoParty();
}


console.log("autokitty: waiting for the game to start...");
var waitForGame = setInterval(function() {
    if (!game) return;
    if (!game.timer) return;
    if (game.timer.ticks <= 2) return; // wait for resources to initialize

    clearInterval(waitForGame);
    initAutocfg();
    game.timer.addEvent(update, 1);
    console.log("autokitty: started!");
}, 100);
