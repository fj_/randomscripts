#!/usr/bin/python2.6

MARGIN = 50

def line_generator_sux(words):
    lst = []
    length = 0
    for word in words:
        if len(lst):
            length += 1
        length += len(word)
        if length > MARGIN: 
            yield ' '.join(lst)
            # Problem number 1: repeat of the initialization code and the step code
            # (combined and simplified, but still).
            lst = [word]
            length = len(word)
        else:
            lst.append(word)
    # Problem number 2: I have to see if I have any leftovers and duplicate a bit 
    # of code again
    if lst:
        yield ' '.join(lst)


# If our algorithm can be naturally expressed as 
# "do this to the first word, do other thing to the rest of the words,"
# then we can make it much nicer manually.
def line_generator_selfcontained1(lst):
    rest = iter(lst)
    word = next(rest) # will terminate the generator if lst is empty
    exhausted = False
    while not exhausted:
        lst = [word]
        length = len(word)
        for word in rest:
            length = length + 1 + len(word)
            if length > MARGIN:
                break
            lst.append(word)
        else:
            exhausted = True
        yield ' '.join(lst)

# or refactor certain parts into a combinator
def line_generator1(word, rest, sentinel):
    lst = [word]
    length = len(word)
    for word in rest:
        length = length + 1 + len(word)
        if length > MARGIN:
            break
        lst.append(word)
    else:
        word = sentinel
    return (' '.join(lst), word)

def group_reduce1(generator, enumerable):
    iterator = iter(enumerable)
    word = next(iterator)
    sentinel = object()
    while word is not sentinel:
        result, word = generator(word, iterator, sentinel)
        yield result 


# Or here's a better variant, it supports a general iteration
# while you can also manually do `word = next(words) ... for word in words: ...`

def line_generator(words):
    word = next(words)
    lst = [word]
    length = len(word)
    if length > MARGIN:
        return lst
    for word in words:
        length = length + 1 + len(word)
        if length > MARGIN:
            break
        lst.append(word)
    return ' '.join(lst)

def split_ten(iterator):
    total = next(iterator)
    lst = [total]
    for n in iterator:
        total += n
        if total >= 10: break
        lst.append(n)
    return lst

class _GroupReduceIterator(object):
    def __init__(self, first, rest):
        self.first = first
        self.rest = rest
        self.state = 0
        # 0 -- initial
        # 1 -- got item from first
        # 2 -- got item from rest 
        # 3 -- exhausted
    def __iter__(self): return self
    def next(self): #@ReservedAssignment
        if self.state == 0:
            self.state = 1
            return self.first
        try:
            first = self.first = next(self.rest)
            self.state = 2
            return first
        except: # except all exceptions because that's how all generators work.
            self.state = 3
            raise
        
def group_reduce(f, enumerable):
    '''Splits a sequence into non-empty runs of items and reduces each run
    using the provided function. When the function returns, the last item it
    consumed is reused as the first item in the next run, unless it was the only
    item. The return value is yielded. The function is guaranteed that the 
    iterator provided to it contains at least one value.
    
    >>> def split_ten(iterator):
    ...     total = next(iterator)
    ...     lst = [total]
    ...     for n in iterator:
    ...         total += n
    ...         if total >= 10: break
    ...         lst.append(n)
    ...     return lst
    ...
    >>> list(group_reduce(split_ten, [1, 7, 2, 11, 11, 1, 8]))
    [[1, 7], [2], [11], [11], [1, 8]]
    
    >>> list(group_reduce(lambda iterator: None, [1, 2, 3]))
    Traceback (most recent call last):
        ...
    AssertionError: Function failed to consume an item

    >>> list(group_reduce(lambda iterator: next(iterator), [1, 2, 3]))
    [1, 2, 3]
    '''
    rest = iter(enumerable)
    iterator = _GroupReduceIterator(next(rest), rest)
    del rest
    yield f(iterator)
    while 1:
        if iterator.state == 0:
            assert False, 'Function failed to consume an item'
        elif iterator.state == 1:
            # special-case the first item.
            next(iterator)
            iterator.state = 0
        elif iterator.state == 2:
            iterator.state = 0
        else:
            break
        yield f(iterator)


########################

def str_diff(s1, s2):
    import difflib
    return ''.join(difflib.unified_diff(s1.splitlines(True), s2.splitlines(True)))

def generate_words():
    import random
    rng = random.Random(0)
    def gen(start, length):
        start = ord(start)
        return [chr(start + i) * rng.randint(1, 10) for i in xrange(length)]
    lst = gen('a', 26) + gen('A', 26) + gen('0', 10)
    m = len(lst) // 2
    lst[m:m] = ['longword' * 10, 'anotherone' * 10]   
    return lst

words = generate_words()


def run_line_generator(generator):
    return '\n\n'.join('\n'.join(generator(words[:i])) 
            for i in range(-15, 0) + [len(words)] + range(0, 15))

reference = run_line_generator(line_generator_sux)
print 'testing yo'
#test = run_line_generator(line_generator_selfcontained1)
#print str_diff(reference, test)
assert reference == run_line_generator(line_generator_selfcontained1)
assert reference == run_line_generator(lambda words: 
        group_reduce1(line_generator1, words))
#assert reference == run_line_generator(lambda words: 
#        group_reduce(line_generator, words))

print 'doctesting'
import doctest
doctest.testmod() 

print 'Done!'

