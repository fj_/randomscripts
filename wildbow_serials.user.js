// ==UserScript==
// @name         Enhance your wildbow!
// @namespace    http://tampermonkey.net/
// @version      0.5
// @description  Make twig/ward readable on mobile.
// @author       You
// @match        https://www.parahumans.net/*
// @match        https://twigserial.wordpress.com/*
// @match        https://palewebserial.wordpress.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict'

    function injectCSS(str) {
        var node = document.createElement('style');
        node.innerHTML = str;
        document.body.appendChild(node);
    }


    // remove long string of &nbsp; between Prev and Next links that make mobile Chrome and Firefox
    // zoom out content.
    function patchPrevNext() {
        console.log('patching prev/next')
        var paragraphs = document.querySelectorAll(".entry-content p")
        for (var i = 0; i < paragraphs.length; i++) {
            var p = paragraphs[i]
            //  there are ordinary spaces between &nbsp; sometimes.
            var s = p.innerHTML.replace(/ *(&nbsp; *){10,}/, ' ')
            if (s != p.innerHTML) {
                console.log('found:', s)
                if (s.includes('<strong>')) {
                    // "ward" wraps the whole contents in one <strong> tag, split it to make justifyContent do the thing
                    // replace once to deal with current chapter where "next" is not a link.
                    s = s.replace(/<\/a>/, '</a></strong><strong>')
                }
                p.innerHTML = s
                // And make the Next link align right
                p.style.display = "flex"
                p.style.justifyContent = "space-between"
            }
        }
    }


    // Twig: Wordpress doesn't allow to permanently remove the Follow Bar even if you're logged in, and it
    // pops up annoyingly when touch-scrolling, so force the issue.
    function deleteFollowBar() {
        var el = document.getElementById("actionbar")
        if (el) {
            el.parentNode.removeChild(el)
            // or el.style.display = "none" if that turns out to be too brutal
        }
    }


    function miscFixes() {
        // In "Twig" header has a border which is not included in size calculation, so it ends up too wide.
        var header_image = document.querySelector(".header-image img")
        if (header_image) {
            header_image.style.boxSizing = "border-box"
        }
    }


    // Edit to taste or comment-out the calls below.

    function customAdjustmentsTwig() {
        console.log('custom adjustments for Twig')
        var el = document.querySelector(".entry-content")
        if (el) {
            // Default is 1.4rem
            el.style.fontSize = "1.3rem"
            // Default is 1.714
            el.style.lineHeight = "1.55"
        }

        el = document.getElementById("page")
        if (el) {
            // Default margins are 0.8em, 0.8em
            el.style.paddingLeft = "0.4em"
            el.style.paddingRight = "0.4em"
        }
    }


    // Edit to taste or comment-out the call below.
    function customAdjustmentsWard() {
        console.log('custom adjustments for Ward')
        // I'm a man of habit so just set everything the same as for Twig
        // (font sizes look different because Twig uses font-size: 62.5% on the root)
        // also set text color to black

        injectCSS(`
.entry-header,
.entry-summary,
.entry-content,
.entry-footer,
.page-content {
	margin-right: 0.4em;
	margin-left: 0.4em;
}

.site-header {
	padding: 0.4em;
}

body {
    font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 0.9rem;
    line-height: 1.55;
    padding: 0 0;
}

.site {
    background-color: #FFFFFF;
}

p {
	margin: 0 0 1.5em;
}

body,
blockquote cite,
blockquote small,
.main-navigation a,
.menu-toggle,
.dropdown-toggle,
.social-navigation a,
.post-navigation a,
.pagination a:hover,
.pagination a:focus,
.widget-title a,
.site-branding .site-title a,
.entry-title a,
.page-links > .page-links-title,
.comment-author,
.comment-reply-title small a:hover,
.comment-reply-title small a:focus {
    color: #111;
}
`)
    }

    // Edit to taste or comment-out the call below.
    function customAdjustmentsPale() {
        console.log('custom adjustments for Pale')
        // try to make it look the same as Ward

        injectCSS(`
body {
    font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 0.9rem;
    line-height: 1.55;
    padding: 0 0;
}

#page, body.custom-background, .commentlist>li.comment, .commentlist .children>li.bypostauthor, .commentlist .children li.comment {
    background: #FFFFFF;
}

p {
	margin: 0 0 1.5em;
}

body,
blockquote cite,
blockquote small,
.main-navigation a,
.menu-toggle,
.dropdown-toggle,
.social-navigation a,
.post-navigation a,
.pagination a:hover,
.pagination a:focus,
.widget-title a,
.site-branding .site-title a,
.entry-title a,
.page-links > .page-links-title,
.comment-author,
.comment-reply-title small a:hover,
.comment-reply-title small a:focus {
    color: #111;
}
`)
    }

    patchPrevNext()
    deleteFollowBar()
    miscFixes()

    if (location.href.includes('twigserial')) {
        customAdjustmentsTwig()
    }
    else if (location.href.includes('parahumans')) {
        customAdjustmentsWard()
    }
    else if (location.href.includes('palewebserial')) {
        customAdjustmentsPale()
    }

})();
