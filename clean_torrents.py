"""Use uTorrent's list of active torrents to remove all downloaded dirs that are no longer used.
Oh, and the list can be acquired by selecting all torrents and pressing ctrl-c =)

"""
import os
import os.path as os_path
import shutil

def parse_list(s):
	names = (l.split('\t', 1)[0].strip() for l in s.split('\n'))
	return [n for n in names if n]

def get_paths(path):
	paths = [(p, os_path.join(path, p)) for p in os.listdir(path)]  
	return [pair for pair in paths if os_path.isdir(pair[1])]

def filter_paths(paths, preserve):
	preserve = set(preserve)
	def f((path, abspath)):
		if path in preserve:
			preserve.remove(path)
			return False
		return True
	paths = filter(f, paths)
	assert len(preserve) == 0, 'OMG some paths not found: %s' % str(preserve)
	return paths
		

def run(lst, path):
	lst = parse_list(lst)
	#print lst
	paths = get_paths(path)
	#print paths
	paths = filter_paths(paths, lst)
	#print paths
	
	dest_dir = os_path.join(path, '_removed')
	try: os.mkdir(dest_dir)
	except: pass
	
	for p, absp in paths:
		if absp == dest_dir: continue
		print p
		shutil.move(absp, dest_dir)
	

if __name__ == '__main__':
	import test