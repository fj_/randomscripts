#!/usr/bin/env python3
# Echo client program
import socket, time

HOST = input('host:')
PORT = 50000
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
t = time.clock()
n = 0
for i in range(int(10e9)):
    data = str(i)
    s.sendall(data.encode())
    resp = s.recv(100000).decode()
    assert resp == data
    n += 1
    t1 = time.clock()
    if t1 - t > 1:
        print(n / (t1 - t))
        n = 0
        t = t1
