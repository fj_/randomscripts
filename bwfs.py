from collections import defaultdict
labels = defaultdict(lambda:0)

neighbours = {
        0 : [1, 5], 
        1 : [0, 6, 2],
        2 : [1, 3],
        3 : [2, 4],
        4 : [3, 5],
        5 : [4, 1],
        6 : [1],
        } 

def bwfs(v, i = 1, d = 0):
    print d, i, v, labels[v]
    if not labels[v]: yield v
    labels[v] = i
    for u in neighbours[v]:
        if 0 < labels[u] < i:
            for r in bwfs(u, i, d + 1):
                yield r
    for u in neighbours[v]:
        if labels[u] == 0:
            for r in bwfs(u, i + 1, d + 1):
                yield r
            
print list(bwfs(0))


