from Tkinter import Tk, Label, Text, Frame, Checkbutton, IntVar, Button,\
    StringVar, Menubutton, Menu
from ScrolledText import ScrolledText
from tkFont import Font
from time import time as gettime
import sys

#
# CONSTANTS
#

USE_SIMULATED_PORT = True


class Simulated_port(object):
    def __init__(self, filename = 'PANEL.DAT'):
        with open(filename, 'r') as f:
            self.data = f.readlines()
        self.line_iter = iter(self.data)
        self.last_time = None         # set on first read()
    
    def read(self):
        t = gettime()
        if self.last_time or t - self.last_time > 1.0:
            while True:
                # skip empty lines, return on eof
                l = next(self.line_iter, None)
                if l is None:
                    return bytes()
                l = l.strip()
                if l:
                    break
            res = [int(s, 16) for s in l.split()]
            return bytes(res)
        return bytes()
    
    def close(self):
        pass


def generator_coroutine(f):
    def constructor():
        input = object() # gensym lol
        generator = f(input)
        primed = []
        # primed: in Py3k it would have been a boolean flag that is set to True 
        # after the generator has been run to its first input request
        
        # note that wrapper is an iterator that is created for every input batch.
        def wrapper(data):
            while not len(primed):
                res = generator.send(None)
                if res is not input:
                    yield res
                else:
                    primed.append(None)
            for c in data:
                res = generator.send(c)            
                while res is not input:
                    yield res
                    res = generator.send(None)
        return wrapper
    return constructor

@generator_coroutine
def Decoder(input):
    while True:
        buffer = []
        ff_cnt = 0
        while True:
            c = yield input
            if c == 0x0D and ff_cnt >= 3:
                break
            if c == 0xFF:
                ff_cnt += 1
            buffer.append(c)
        yield buffer

    
class Loopy_thing(object):
    def __init__(self):
        self.t = gettime()
        self.cnt1 = 0
        self.cnt2 = 0
    
    def update(self):
        t = gettime()
        while self.t < t:
            c = chr(ord('a') + self.cnt1) 
            yield c * 5 + '%02d' % (self.cnt1 + self.cnt2 * 3) + c * 5
            self.t += 0 + 0.2
            self.cnt1 += 1
            if self.cnt1 > 8:
                yield 'x'
                self.t += 2
                self.cnt1 = 0
                self.cnt2 += 1
                if self.cnt2 > 10:
                    self.cnt2 = 0
    

class Gui(object):
    def __init__(self, port):
        self.port = port
        self.root = root = Tk()
        root.title('Panel data decoder')
        root.protocol("WM_DELETE_WINDOW", self.close)
        
        self.text_font = Font(root, ("Courier New", 10))
        
        self.text = Text(root, font = self.text_font, state = 'disabled')
        self.text.pack(fill = 'both', expand = True)
        
        self.text.tag_configure('hi', background='yellow')
        self.text.tag_lower('hi') # shouldn't override selection
        
        
        self.loopy_thing = Loopy_thing()
        self.current_line = 1 

    def timer_callback(self):
        self.update()
        self.root.after(20, self.timer_callback)
    
    def update(self):
        for line in self.loopy_thing.update():
            self.replace_line(self.current_line, line)
            if line == 'x': self.current_line = 1
            else: self.current_line += 1
            
    def replace_line(self, line, s):
        text = self.text
        text.configure(state = 'normal')
        text.tag_remove('hi', '0.0', 'end')
        
        last_line = int(text.index('end').split('.')[0])
        to_insert = line - last_line + 1
        if to_insert > 0:
            text.insert('end', '\n' * to_insert)
        
        lstart = '%d.0' % line
        lend = '%d.end' % line
        text.delete(lstart, lend)
        text.insert(lstart, s, 'hi')
        text.configure(state = 'disabled')
        
        
    def run(self):
        self.timer_callback()
        return self.root.mainloop()

    def close(self):
        if self.root:
            self.root.destroy()
            self.root = None

if __name__ == '__main__':
    gui = Gui(None)
    gui.run();