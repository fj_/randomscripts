#!/usr/bin/env python3

from advent_of_code_utils import *
utils_init(2019, globals())

###########


@dataclass
class Command:
    code: int
    name: str
    arglen: int
    func: Callable


def fast_run_till_output(m, input_data, break_on_consumed_input):
    pc = 0
    relative_base = 0
    output = None
    consumed_input = False

    def _in(p1):
        nonlocal consumed_input
        m[p1] = input_data.popleft()
        consumed_input = True

    def _out(p1):
        nonlocal output
        output = m[p1]

    def _jt(p1, p2):
        nonlocal pc
        if m[p1]:
            pc = m[p2]

    def _jf(p1, p2):
        nonlocal pc
        if not m[p1]:
            pc = m[p2]

    def _arb(p1):
        nonlocal relative_base
        relative_base += m[p1]

    def _end():
        nonlocal pc
        pc = -1

    from operator import setitem
    commands_list = [
        Command(1, 'add', 3, lambda p1, p2, p3: setitem(m, p3, m[p1] + m[p2])),
        Command(2, 'mul', 3, lambda p1, p2, p3: setitem(m, p3, m[p1] * m[p2])),
        Command(3, 'in ', 1, _in),
        Command(4, 'out', 1, _out),
        Command(5, 'jt ', 2, _jt),
        Command(6, 'jf ', 2, _jf),
        Command(7, 'slt', 3, lambda p1, p2, p3: setitem(m, p3, m[p1] < m[p2])),
        Command(8, 'seq', 3, lambda p1, p2, p3: setitem(m, p3, m[p1] == m[p2])),
        Command(9, 'arb', 1, _arb),
        Command(99, 'end', 0, _end),
    ]

    commands = {cmd.code: cmd for cmd in commands_list}

    while pc >= 0:
        pmode, cmd = divmod(m[pc], 100)
        cmd = commands[cmd]
        args = m[pc + 1 : pc + cmd.arglen + 1]
        # apply pmode
        for i in range(len(args)):
            pmode, p = divmod(pmode, 10)
            if p == 0:
                pass
            elif p == 1:
                args[i] = pc + i + 1
            elif p == 2:
                args[i] += relative_base
            else:
                assert False
        # print(pc, cmd.name, args, m[pc : pc + cmd.arglen + 1])
        pc += cmd.arglen + 1
        cmd.func(*args)
        if output is not None:
            yield output
            output = None
        if consumed_input and break_on_consumed_input:
            consumed_input = False
            yield None


class FixedInput:
    def __init__(self, input_data):
        self.queue = deque(input_data)

    def popleft(self):
        if len(self.queue) == 1:
            return self.queue[0]
        return self.queue.popleft()

    def set(self, value):
        self.queue.clear()
        self.queue.append(value)


class NonblockingInput:
    def __init__(self, input_data, empty_value=-1):
        self.empty_value = empty_value
        self.queue = deque(input_data)

    def popleft(self):
        if len(self.queue):
            return self.queue.popleft()
        return self.empty_value

    def extend(self, values):
        self.queue.extend(values)

    def __len__(self):
        return len(self.queue)


class IntCode:

    @staticmethod
    def parse_data(data):
        # always create a private copy
        return list(map(int, data.split(','))
            if isinstance(data, str) else data
        )


    def __init__(self, data, input_data=[], max_memory=3000, id=None, break_on_consumed_input=False):
        self.data = self.parse_data(data)
        self.data.extend(0 for _ in range(len(self.data), max_memory))
        self.input_data = deque(input_data) if isinstance(input_data, list) else input_data
        self.runner = fast_run_till_output(self.data, self.input_data, break_on_consumed_input)
        self.id = id
        self.finished = False
        self._eof_sentinel = object()


    def add_input(self, *inputs):
        self.input_data.extend(inputs)


    def run_till_output(self):
        res = next(self.runner, self._eof_sentinel)
        if res is self._eof_sentinel:
            self.finished = True
            return None
        return res


    def run(self):
        output_data = []
        while (r := self.run_till_output()) is not None:
            output_data.append(r)
        return output_data


def problem1(data, second):
    # if second: return
    # pprint(data)
    # data_ = split_data(''' ''')
    data = np.fromiter(map(int, data), dtype=int)
    total = 0
    while len(data):
        data //= 3
        data -= 2
        data = data[data > 0]
        total += np.sum(data)
        if not second:
            break
    return total


def problem2(data, second):
    # if second: return
    # data = split_data('''1,9,10,3,2,3,11,0,99,30,40,50''')
    ic = IntCode(data)

    if not second:
        ic.data[1] = 12
        ic.data[2] = 2
        ic.run()
        return ic.data[0]


    for noun, verb in itertools.product(range(100), range(100)):
        ic = IntCode(data)
        ic.data[1] = noun
        ic.data[2] = verb
        ic.run()
        if ic.data[0] == 19690720:
            return 100 * noun + verb


def problem3(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83''')

    def parse(wire):
        def p(it):
            direction, length = it[0], it[1:]
            assert direction in 'UDLR'
            return direction, int(length)
        return [p(it) for it in wire.split(',')]

    def to_points(wire):
        r = {}
        pos = (0, 0)
        dd = {'U' : (1, 0), 'D' : (-1, 0), 'R' : (0, 1), 'L' : (0, -1)}
        counter = 1
        for d, l in wire:
            d = dd[d]
            for _ in range(l):
                pos = addv2(pos, d)
                r[pos] = r.get(pos, counter)
                counter += 1
        return r

    p1 = to_points(parse(data[0]))
    p2 = to_points(parse(data[1]))
    intersections = set(p2.keys()).intersection(p1.keys())
    # print(intersections)
    if second:
        return min([p1[p] + p2[p] for p in intersections])
    else:
        return min([abs(y) + abs(x) for x, y in intersections])


def problem4(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data(''' ''')
    rmin, rmax = map(int, data.split('-'))
    cnt = 0
    def test(si):
        same = False
        for a, b in pairwise(si):
            if a > b:
                return False
            if a == b:
                same = True
        return same


    for i in range(rmin, rmax):
        si = str(i)
        if test(si) and (not second or any(len(m[0]) == 2 for m in re.findall(r'((.)\2+)', str(i)))):
            cnt += 1
    return cnt


def problem5(data, second):
    # if second: return
    # data = split_data('''1,9,10,3,2,3,11,0,99,30,40,50''')
    if second:
        r = IntCode(data, [5]).run()
    else:
        r = IntCode(data, [1]).run()
    return r[-1]


def problem6(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''
COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN
''')
    g = networkx.DiGraph()

    for it in data:
        g.add_edge(*it.split(')'))

    def rec(n):
        d = g.nodes[n].get('d', None)
        if d is not None: return d
        p = list(g.predecessors(n))
        if len(p) == 0:
            d = 0
        else:
            [p] = p
            d = rec(p) + 1
        g.nodes[n]['d'] = d
        return d

    if not second:
        return sum(rec(n) for n in g)

    g = g.to_undirected()
    return networkx.shortest_path_length(g, 'YOU', 'SAN') - 2


def problem7(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data(''' ''')

    m = 0
    for phases in itertools.permutations(range(5, 10) if second else range(5)):
        inp = 0
        last = None
        ics = []
        for phase in phases:
            ics.append(IntCode(data, [phase]))
        while True:
            for ic in ics:
                ic.add_input(inp)
                inp = ic.run_till_output()
                if inp is None:
                    break
            if inp is None:
                break
            last = inp
            if not second:
                break
        if last is not None and last > m:
            m = last

    return m


def problem8(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data(''' ''')
    width, height = 25, 6
    depth = len(data) // (width * height)
    # print(width, height, depth)
    assert width * height * depth == len(data)
    layers = np.fromiter(data, dtype=np.int8)
    layers.shape = -1, height, width

    if not second:
        l1 = min(layers, key=lambda l: (l == 0).sum())
        return (l1 == 1).sum() * (l1 == 2).sum()

    image = np.ones_like(layers[0]) * 2
    for l in layers:
        np.copyto(image, l, where=(image == 2))
    print(image)


def problem9(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data(''' ''')
    ic = IntCode(data, [2 if second else 1])
    r = ic.run()
    assert len(r) == 1, repr(r)
    return r[0]


def problem10(data, second):
    from fractions import Fraction
    # if second: return
    # pprint(data)
    data_ = split_data('''.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##''')
    width, height = len(data[0]), len(data)
    positions = []
    for y, row in enumerate(data):
        for x, col in enumerate(row):
            if col == '#':
                positions.append((x, y))
    visible = []
    for pos in positions:
        v = set()
        visible.append(v)
        for pos2 in positions:
            if pos2 == pos:
                continue
            dx, dy = pos2[0] - pos[0], pos2[1] - pos[1]
            adx, ady = map(abs, (dx, dy))
            d = math.gcd(adx, ady)
            dx //= d
            dy //= d
            v.add((dx, dy))
    if not second:
        return max(len(v) for v in visible)
    location = max((len(v), i) for i, v in enumerate(visible))[1]
    pos = positions[location]
    print(pos)
    polar = defaultdict(list)
    for i, pos2 in enumerate(positions):
        if i == location:
            continue
        dx, dy = pos2[0] - pos[0], pos2[1] - pos[1]
        polar[int(round(math.atan2(-dx, dy) * 1_000_000))].append(((dx ** 2 + dy ** 2), pos2))
    polar = sorted((k, list(reversed(sorted(v)))) for k, v in polar.items())
    i = 0
    for j in range(199): # we start from the second asteroid because of how atan2 works
        r, (x, y) = polar[i][1].pop()
        # print(j, x, y)
        if not polar[i][1]:
            del polar[i]
        else:
            i += 1
        if i > len(polar):
            i = 0
    return x * 100 + y


def problem11(data, second):
    # if second: return
    # pprint(data)
    # data_ = split_data(''' ''')

    field = {0: second}
    ic = IntCode(data, FixedInput([field[0]]))
    direction = -1j
    position = 0
    while True:
        if (color := ic.run_till_output()) is None:
            break
        field[position] = color
        if (rotation := ic.run_till_output()) is None:
            break
        direction *= 1j if rotation else -1j
        position += direction
        ic.input_data.set(field.get(position, 0))
    if second:
        min_x, w, min_y, h = map(int, complex_2d_aabbox(field.keys()))
        grid = [[' '] * w for _ in range(h)]
        for z, v in field.items():
            grid[int(z.imag) - min_y][int(z.real) - min_x] = '█' if v else '.'
        print('\n'.join(''.join(row) for row in grid))
        return 'CBLPJZCU'
    else:
        return len(field)


def problem12(data, second):
    # if second: return
    data_ = split_data('''<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>''')
    data_ = split_data('''<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>''')

    pos = np.array(ReTokenizer('<x={int}, y={int}, z={int}>').match_all(data))
    vel = np.zeros_like(pos)
    if not second:
        for turn in range(1000):
            vel += np.sum(np.sign(pos - pos[:, np.newaxis]), axis=1)
            pos += vel
        pot = np.sum(np.abs(pos), axis=1)
        kin = np.sum(np.abs(vel), axis=1)
        return np.sum(pot * kin)

    def period(pos, vel):
        pos = np.copy(pos)
        vel = np.copy(vel)
        start = tuple(pos) + tuple(vel)
        for turn in range(1, 1000000):
            vel += np.sum(np.sign(pos - pos[:, np.newaxis]), axis=1)
            pos += vel
            if start == tuple(pos) + tuple(vel):
                return turn
    p1 = period(pos[:,0], vel[:,0])
    p2 = period(pos[:,1], vel[:,1])
    p3 = period(pos[:,2], vel[:,2])
    pp = p1 * p2 // math.gcd(p1, p2)
    pp = pp * p3 // math.gcd(pp, p3)
    return pp


def problem13(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data(''' ''')
    screen = {}
    if not second:
        ic = IntCode(data)
        while True:
            x, y, t = ic.run_till_output(), ic.run_till_output(), ic.run_till_output()
            if x is None:
                break
            assert y is not None and t is not None
            screen[(x, y)] = t
        return sum(1 for t in screen.values() if t == 2)

    ic = IntCode(data, FixedInput([0]))
    ic.data[0] = 2
    score = 0
    ball_x = paddle_x = 0
    while True:
        x, y, t = ic.run_till_output(), ic.run_till_output(), ic.run_till_output()
        if x is None:
            break
        assert y is not None and t is not None

        def display():
            min_x, w, min_y, h = np_2d_aabbox(np.array(list(screen.keys()), dtype=int))
            grid = [[' '] * w for _ in range(h)]
            for (x, y), t in screen.items():
                grid[y - min_y][x - min_x] = {
                    0: '.',
                    1: '█',
                    2: '#',
                    3: '=',
                    4: 'O',
                }[t]
            print(score)
            print('\n'.join(''.join(row) for row in grid))

        if x == -1:
            assert y == 0
            score = t
        else:
            screen[(x, y)] = t
            if t == 4:
                ball_x = x
            elif t == 3:
                paddle_x = x

        if t == 4:
            mov = ball_x - paddle_x
            if abs(mov):
                mov = mov // abs(mov)
            ic.input_data.set(mov)
            # display()

    return(score)


def problem14(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX''')

    def amount_what_split(s):
        a, w = s.strip().split(' ')
        return int(a.strip()), w.strip() # change order!

    reactions = {}
    amounts = {}

    for line in data:
        src, dst = line.split('=>')
        a, w = amount_what_split(dst)
        amounts[w] = a
        reactions[w] = list(map(amount_what_split, src.split(',')))

    def needs_ore(a):
        needed_ore = 0
        leftovers = {}
        def rec(w, a):
            nonlocal needed_ore
            if w == 'ORE':
                needed_ore += a
                return
            a -= leftovers.get(w, 0)
            r_cnt = math.ceil(a / amounts[w])
            leftovers[w] = r_cnt * amounts[w] - a
            for src_a, src_w in reactions[w]:
                needed = r_cnt * src_a
                rec(src_w, needed)
        rec('FUEL', a)
        return needed_ore

    if not second:
        return needs_ore(1)

    has_ore = 1000000000000
    left = has_ore // needs_ore(1)
    right = left * 2
    while right > left:
        m = (left + right + 1) // 2
        # print(left, m, right)
        if needs_ore(m) <= has_ore:
            left = m
        else:
            right = m - 1
    # print(has_ore)
    # print(needs_ore(left - 1))
    # print(needs_ore(left))
    # print(needs_ore(left + 1))
    return left


def problem15(data, second):
    # if second: return
    # pprint(data)
    # data_ = split_data(''' ''')
    ic = IntCode(data, FixedInput([]))
    oxy = None
    pos = 0j
    g = networkx.Graph()
    g.add_node(pos, t='.')
    unexplored = set()
    dirs = {-1j: 1, 1j: 2, 0j - 1: 3, 0j + 1: 4 }
    def look_around(pos):
        for d in dirs.keys():
            pp = pos + d
            if pp not in g.nodes:
                unexplored.add(pp)
                g.add_node(pp, t='?')
                g.add_edge(pos, pp)
    look_around(0j)

    def dist_c(a, b):
        x = b - a
        return abs(x.real) + abs(x.imag)

    while unexplored:
        target = min(unexplored, key=lambda p: dist_c(pos, p))
        # print(pos, target)
        path = networkx.shortest_path(g, source=pos, target=target)
        for p, pp in pairwise(path):
            ic.input_data.set(dirs[pp - p])
            st = ic.run_till_output()
            unexplored.discard(pp)
            if st == 0:
                g.nodes[pp]['t'] = '█'
            elif st == 1:
                pos = pp
                look_around(pos)
            elif st == 2:
                oxy = pp
                pos = pp
                look_around(pos)
            else:
                assert False
    if not second:
        return networkx.shortest_path_length(g, 0j, oxy)
    return max(networkx.single_source_shortest_path_length(g, oxy).values()) - 1 # the path is to the last wall


def problem16(data, second):
    # if second: return
    # pprint(data)
    # data = split_data('''80871224585914546619083218645595''')
    data = np.fromiter(data, dtype=int)
    n = len(data)
    if not second:
        matrix = np.zeros((n, n), dtype=int)
        matrix[0] = np.tile(np.array([1, 0, -1, 0]), (n + 3) // 4)[:n]
        for i in range(1, n):
            r = i + 1
            idx = np.repeat(np.arange(n // r), r)[:n - i]
            matrix[i, i:] = matrix[0][idx]

        for phase in range(100):
            data = matrix @ data
            np.abs(data, out=data)
            data %= 10
        return ''.join(map(str, data[:8]))

    needed_idx = int(''.join(map(str, data[:7])))
    # print(needed_idx, n * 10_000 - needed_idx)
    data = np.flip(np.tile(data, 10_000)[needed_idx:]).copy()
    for phase in range(100):
        np.cumsum(data, out=data)
        np.abs(data, out=data)
        data %= 10
    return ''.join(map(str, np.flip(data)[:8]))


def problem17(data, second):
    # if second: return

    orig_data = data
    ic = IntCode(orig_data, max_memory=10_000)

    data = split_data(''.join(map(chr, ic.run())))
    data_ = split_data('''..#..........
..#..........
##O####...###
#.#...#...#.#
##O###O###O##
..#...#...#..
..#####...^..''')

    walls = np.array([[d != '.' for d in row] for row in data], dtype=int)
    if not second:
        nb = sum(np.roll(walls, d, axis=(0, 1)) for d in ((-1, 0), (1, 0), (0, -1), (0, 1), (0, 0)))
        return sum(x * y for y, x in np.argwhere(nb == 5))

    # print('\n'.join(data))

    [pos] = flatten(
        ((ir, ic) for ic, d in enumerate(row) if d == '^') for ir, row in enumerate(data))
    insns = ['R']
    dir = (0, 1)
    length = 0
    while True:
        npos = addv2(pos, dir)
        try:
            if walls[npos]:
                pos = npos
                length += 1
                continue
        except:
            pass

        insns.append(length)
        length = 0

        ndir = cmulv2(dir, (1, 0))
        try:
            if walls[addv2(pos, ndir)]:
                dir = ndir
                insns.append('R')
                continue
        except:
            pass
        ndir = cmulv2(dir, (-1, 0))
        try:
            if walls[addv2(pos, ndir)]:
                dir = ndir
                insns.append('L')
                continue
        except:
            pass

        break
    s = ','.join(map(str, insns)) + ','
    A = 'R,6,L,10,R,8,R,8,'
    B = 'R,12,L,10,R,6,L,10,'
    C = 'R,12,L,8,L,10,'
    S = s.replace(A, 'A,').replace(B, 'B,').replace(C, 'C,')
    S, A, B, C = [s.strip(',') for s in (S, A, B, C)]
    inp = f'{S}\n{A}\n{B}\n{C}\nn\n'

    ic = IntCode(orig_data, max_memory=10_000, input_data=[ord(c) for c in inp])
    ic.data[0] = 2
    return ic.run()[-1]


def problem18(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''#################
#i.G..c...e..H.p#
########.########
#j.A..b...f..D.o#
########@########
#k.E..a...g..B.n#
########.########
#l.F..d...h..C.m#
#################''')
    # print('\n'.join(data))
    data = ndarray_from_chargrid(data)
    player = tuple(np.argwhere(data == '@')[0])
    data[player] = '.'
    pos_to_key = {}
    pos_to_door = {}

    all_keys = 0
    for p, c in np.ndenumerate(data):
        if (idx := string.ascii_lowercase.find(c)) >= 0:
            pos_to_key[p] = 1 << idx
            all_keys |= 1 << idx
        elif (idx := string.ascii_uppercase.find(c)) >= 0:
            pos_to_door[p] = 1 << idx
        else:
            assert c in '.#'

    g = grid_to_graph(data)

    if second:
        g.remove_node(player)
        for d in directions4:
            g.remove_node(addv2(player, d))
        assert networkx.is_forest(g)
        start_locations = [
                addv2(player, ( 1, 1)),
                addv2(player, ( 1,-1)),
                addv2(player, (-1, 1)),
                addv2(player, (-1,-1)),
        ]
    else:
        start_locations = [player]

    locations = list(pos_to_key.keys())
    locations.extend(start_locations)

    with timeit_block('precompute paths'):
        sp = precompute_shortest_paths(g, locations)

    @dataclass(frozen=True)
    class Move:
        target_key: int
        needed_keys: int
        length: int

    moves = {}

    for source, edges in sp.adjacency():
        mm = moves[source] = {}
        for target, attrs in edges.items():
            target_key = pos_to_key.get(target)
            # exclude starting positions
            if target_key is None:
                continue
            needed_keys = 0
            for p in attrs['path']:
                if (door := pos_to_door.get(p)) is not None:
                    needed_keys |= door
            mm[target] = Move(target_key, needed_keys, attrs['weight'])


    @dataclass(frozen=True)
    class Node:
        keys: int
        positions: Tuple[Vector2, ...]

    SUCCESS = 'success'
    g2 = networkx.DiGraph()
    def rec(node: Node):
        if node.keys == all_keys:
            g2.add_edge(node, SUCCESS, weight=0)
            return
        # print(node)
        moved = False
        for source_idx, source_pos in enumerate(node.positions):
            # This is a huge hack for performance, works on my input at least
            if moved:
                break
            for pos, move in moves[source_pos].items():
                # print(pos, move)
                if node.keys & move.target_key:
                    # already have the key
                    continue
                if move.needed_keys & ~node.keys:
                    # missing some keys
                    continue
                newpos = tuple(pos if idx == source_idx else src for idx, src in enumerate(node.positions))
                nn = Node(node.keys | move.target_key, newpos)
                need_rec = nn not in g2.nodes
                g2.add_edge(node, nn, weight=move.length)
                if need_rec:
                    moved = True
                    rec(nn)

    start = Node(0, tuple(start_locations))

    with timeit_block('construct game graph'):
        rec(start)
        print(len(g2.nodes))

    return networkx.shortest_path_length(g2, start, SUCCESS, weight='weight')


def problem19(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data(''' ''')
    data = IntCode.parse_data(data)
    def run(y, x):
        ic = IntCode(data, input_data=[x, y])
        [res] = ic.run()
        # print(y, x, res)
        return res

    # s = 0
    # for y in range(50):
    #     for x in range(50):
    #         res = run(y, x)
    #         print(' #'[res], end='')
    #         s += res
    #     print()
    # return s

    beam = []
    y, startx, endx = 0, 0, 0
    while True:
        x = startx
        while not run(y, x):
            x += 1
            if x > y * 3 + 3:
                # the beam is discontinuous at first
                x = -1
                break
        if x >= 0:
            startx = x
            x = max(endx, x + 1)
            while run(y, x):
                x += 1
            endx = x
        else:
            endx = startx

        beam.append((startx, endx))
        y += 1

        if second:
            if y < 100:
                continue
            # print(beam[-1])
            _, prev_end_x = beam[-100]
            if prev_end_x - startx >= 100:
                return (y - 100) + startx * 10_000

        elif y == 50:
            return sum(max(0, min(50, b) - a) for a, b in beam)


def problem20(data, second):
    data = ndarray_from_chargrid(get_raw_data())
    # if second: return
    # for row in data: print(''.join(row))
    # data_ = split_data(''' ''')

    g = networkx.Graph()
    portals_inner = {}
    portals_outer = {}

    for p, c in ndenumerate_slice(data, np.s_[2:-2, 2:-2]):
        if c == '.':
            for d in directions4:
                n = addv2(p, d)
                dn = data[n]
                if dn == '.':
                    g.add_edge(p, n)
                elif dn in string.ascii_uppercase:
                    nn = addv2(n, d)
                    dnn = data[nn]
                    is_inner = 3 < nn[0] < len(data) - 3 and 3 < nn[1] < len(data[0]) - 3
                    name = dn + dnn if nn > n else dnn + dn
                    portals = portals_inner if is_inner else portals_outer
                    assert name not in portals, (name, portals)
                    portals[name] = p

    start, end = portals_outer['AA'], portals_outer['ZZ']
    del portals_outer['AA']
    del portals_outer['ZZ']

    if not second:
        for name, pos in portals_outer.items():
            if name in ('AA', 'ZZ'):
                continue
            g.add_edge(pos, portals_inner[name])
        return networkx.shortest_path_length(g, start, end)

    def add_coord(p, i):
        return p[0], p[1], i

    portals_inner_pos = frozenset(portals_inner.values())
    portals_outer_pos = frozenset(portals_outer.values())

    locations = list(portals_inner_pos)
    locations.extend(portals_outer_pos)
    locations.append(start)
    locations.append(end)

    spg = precompute_shortest_paths(g, locations)

    g2 = networkx.Graph()
    LEVELS = len(portals_inner)

    for p in (start, end):
        for p2, attrs in spg[p].items():
            if p2 in portals_inner_pos:
                g2.add_edge(p, add_coord(p2, 0), weight=attrs['weight'])

    spg.remove_node(start)
    spg.remove_node(end)

    for lvl in range(1, LEVELS):
        subgraph = networkx.relabel_nodes(spg, lambda n: add_coord(n, lvl))
        g2.update(subgraph)
        for name, pos in portals_inner.items():
            g2.add_edge(add_coord(pos, lvl - 1), add_coord(portals_outer[name], lvl), weight=1)

    return networkx.dijkstra_path_length(g2, start, end)


def problem21(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data(''' ''')
    ic = IntCode(data)
    if not second:
        program = '''\
NOT A J
NOT B T
OR T J
NOT C T
OR T J
AND D J
WALK
'''
    else:
        program = '''\
NOT A J
NOT B T
OR T J
NOT C T
OR T J
AND D J
NOT J T
OR H T
OR E T
AND T J
RUN
'''

    ic.add_input(*(ord(c) for c in program))
    output = ic.run()
    print(''.join(map(chr, output[:-1])))
    return output[-1]


def problem22(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''
deal into new stack
cut -2
deal with increment 7
cut 8
cut -4
deal with increment 7
cut 3
deal with increment 9
deal with increment 3
cut -1
''')

    DECK_SIZE = 10007
    CARD_POS = 2019
    # DECK_SIZE = 10
    # CARD_POS = 5

    if not second:
        cards = np.arange(DECK_SIZE)
        tmp = np.zeros_like(cards)

        def flip(_):
            nonlocal cards
            cards = np.flip(cards)

        def cut(n):
            nonlocal cards
            cards = np.roll(cards, -n)

        def deal(n):
            nonlocal cards, tmp
            indices = np.arange(len(cards))
            indices *= n
            indices %= len(cards)
            tmp[indices] = cards
            cards, tmp = tmp, cards

        ReTokenizer((
            ('deal into new stack', flip),
            ('cut {int}', cut),
            ('deal with increment {int}', deal),
        )).match_all(data)

        # print(cards)
        [[idx]] = np.nonzero(cards == CARD_POS)
        return idx

    rt = ReTokenizer()

    def homomatrix(a, b):
        return np.array(((a, b), (0, 1)), dtype=object)

    @rt.add_dataclass('deal into new stack')
    class Flip:
        def idx(self, k):
            return (-k - 1) % DECK_SIZE

        def tr(self, ax, b):
            return -ax, -b - 1

        def trm(self):
            return homomatrix(-1, -1)


    @rt.add_dataclass('cut {}')
    class Cut:
        n : int
        def idx(self, k):
            return (k + self.n) % DECK_SIZE

        def tr(self, ax, b):
            return ax, b + self.n

        def trm(self):
            return homomatrix(1, self.n)


    @rt.add_dataclass('deal with increment {}')
    class Deal:
        n : int

        @property
        def modinverse(self):
            return pow(self.n, -1, DECK_SIZE)

        def idx(self, k):
            return (k * self.modinverse) % DECK_SIZE

        def tr(self, ax, b):
            return ax * self.modinverse, b * self.modinverse

        def trm(self):
            return homomatrix(self.modinverse, 0)


    ops = rt.match_all(data)
    ops.reverse()

    p = 2496
    for op in ops:
        p = op.idx(p)
    assert p == 2019

    ax, b = 1, 0
    m = homomatrix(1, 0)
    for op in ops:
        ax, b = map(lambda x: x % DECK_SIZE, op.tr(ax, b))
        m = op.trm() @ m
        m %= DECK_SIZE

    def compute(ax, b, x):
        return (ax * x + b) % DECK_SIZE

    assert compute(ax, b, 2496) == 2019

    def homocompute(m, x):
        return (m @ (x, 1))[0] % DECK_SIZE

    assert homocompute(m, 2496) == 2019

    def fastpow(m, n):
        res = np.identity(2, dtype=object)
        while n:
            if n & 1:
                res = m @ res
                res %= DECK_SIZE
            n >>= 1
            m = m @ m
            m %= DECK_SIZE
        return res

    DECK_SIZE = 119315717514047
    SHUFFLES = 101741582076661

    m = homomatrix(1, 0)
    for op in ops:
        m = op.trm() @ m
        m %= DECK_SIZE

    m = fastpow(m, SHUFFLES)
    return homocompute(m, 2020)


def problem23(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data(''' ''')
    computers = [IntCode(data, input_data=NonblockingInput([i]), id=i, break_on_consumed_input=True) for i in range(50)]
    queues = defaultdict(deque)

    def run_comp(comp):
        idle_tries = 0
        while idle_tries < 3:
            had_input = len(comp.input_data)
            output = comp.run_till_output()
            # print(f'{comp.id=} {had_input=} {output=}')
            assert not comp.finished
            if output is None:
                if not had_input:
                    idle_tries += 1
                continue

            assert (output2 := comp.run_till_output()) is not None
            assert (output3 := comp.run_till_output()) is not None

            queues[output].append(output2)
            queues[output].append(output3)
            # print(f'{comp.id:2} -> {output:2}: {output2}, {output3}')

    saved_packet = None
    prev_y = None
    while True:
        if len(queues):
            k, v = queues.popitem()
            comp = computers[k]
            comp.add_input(*v)
            run_comp(comp)
        else:
            for comp in computers:
                run_comp(comp)
            if not len(queues):
                x, y = saved_packet
                if y == prev_y:
                    return y
                prev_y = y
                queues[0] = saved_packet

        if 255 in queues:
            q = queues.pop(255)
            if not second:
                return q.popleft()
            saved_packet = q[-2], q[-1]


def problem24(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''....#
#..#.
#..##
..#..
#....''')

    field = np.fromiter((c == '#' for c in ''.join(data)), dtype=np.int32).reshape(5, 5)
    field = np.pad(field, 1)
    powers = np.fromiter((2 ** i for i in range(25)), dtype=np.int32).reshape(5, 5)
    powers = np.pad(powers, 1)
    seen = set()
    seen.add(np.sum(field * powers))
    if not second:
        for turn in range(2**25):
            nb = sum(np.roll(field, coord, axis=(0, 1)) for coord in directions4)
            nb += field * 10
            field[1:-1, 1:-1] = np.isin(nb[1:-1, 1:-1], (1, 2, 11))
            biodiversity = np.sum(field * powers)
            if biodiversity in seen:
                return biodiversity
            seen.add(biodiversity)

    def add_coord(p, i):
        return p[0], p[1], i

    target_t = 200
    g = networkx.Graph()
    for depth in range(-target_t // 2 - 3, target_t // 2 + 3):
        for p in itertools.product(range(5), range(5)):
            if p == (2, 2):
                continue
            for d in directions4:
                n = addv2(p, d)
                if n == (2, 2):
                    continue
                nn = n
                if n[0] < 0:
                    nn = (1, 2)
                elif n[0] > 4:
                    nn = (3, 2)
                if n[1] < 0:
                    nn = (2, 1)
                elif n[1] > 4:
                    nn = (2, 3)
                nd = depth - (nn != n) # up a level
                g.add_edge(add_coord(p, depth), add_coord(nn, nd))

    def pp(active):
        grid = np.zeros((target_t, 5, 5), dtype=int)
        if isinstance(active, set):
            for i, j, d in active:
                grid[d, i, j] = 1
        else:
            for (i, j, d), n in active.items():
                grid[d, i, j] = n

        print(grid)
        print('\n', '*' * 20, '\n\n')


    active = set()
    for i, row in enumerate(data):
        for j, c in enumerate(row):
            if c == '#':
                active.add((i, j, 0))

    for t in range(target_t):
        new_active = defaultdict(int)
        for p in active:
            new_active[p] += 100
            for n in networkx.neighbors(g, p):
                new_active[n] += 1
        # pp(new_active)
        active = set(p for p, v in new_active.items() if v in (1, 2, 101))
    # pp(active)

    return len(active)


def problem25(data, second):
    if second: return
    # pprint(data)
    # data_ = split_data(''' ''')

    # Items in your inventory:
    # - astrolabe
    # - ornament
    # - weather machine
    # - food ration

    return 4206594
    ic = IntCode(data, max_memory=10_000)
    buffer = ''
    while (c := ic.run_till_output()) is not None:
        c = chr(c)
        print(c, end='')
        buffer += c
        if buffer.endswith('Command?'):
            buffer = ''
            cmd = input()
            ic.input_data.extend(ord(c1) for c1 in cmd)
            ic.input_data.append(10)


##########

def problem(data, second):
    if second: return
    pprint(data)
    data_ = split_data(''' ''')


##########

if __name__ == '__main__':
    print('Hello')
    solve_all()
    # solve_latest(22)
    # solve_latest()
