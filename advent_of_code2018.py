#!/usr/bin/env python3

from advent_of_code_utils import *
utils_init(2018, globals())

###########

def problem1(data, second):
    # data = '+3 +3 +4 -2 -4'.split()
    data = [int(s) for s in data]
    if not second: return sum(data)
    f = 0
    freqs = {f}
    for it in itertools.cycle(data):
        f += it
        if f in freqs:
            return f
        freqs.add(f)


def problem2(data, second):
    if second:
        hashes = defaultdict(dict)
        for s in data:
            for i in range(len(s)):
                hashdict = hashes[i]
                h = s[ : i] + s[i + 1 : ]
                if h in hashdict:
                    return h
                hashdict[h] = s
        assert False
    twos, threes = 0, 0
    for s in data:
        cntr = Counter(s)
        if 2 in cntr.values():
            twos += 1
        if 3 in cntr.values():
            threes += 1
    return twos * threes


def problem3(data, second):
    data_ = split_data('''
        #1 @ 1,3: 4x4
        #2 @ 3,1: 4x4
        #3 @ 5,5: 2x2''')
    # if second: return
    rt = ReTokenizer()
    rt.add_dataclass_old('#{id:int} @ {x:int},{y:int}: {w:int}x{h:int}')
    claims = rt.match_all(data)

    arr = np.zeros((1001, 1001), np.int32)
    for c in claims:
        arr[c.y : c.y + c.h, c.x : c.x + c.w] += 1

    if not second:
        return int(np.sum(arr > 1))

    res = []
    for c in claims:
        if np.sum(arr[c.y : c.y + c.h, c.x : c.x + c.w]) == c.h * c.w:
            res.append(c.id)
    [res] = res
    return res


def problem4(data, second):
    # data = split_data('')
    # if second: return
    data.sort()
    sched = ReTokenizer([
        ('.*?:{int}] Guard #{int} begins shift', None),
        ('.*?:{int}] {:(falls|wakes)} .*', None),
    ]).match_all(data)

    guards = defaultdict(list)
    for m, act in sched:
        if isinstance(act, int):
            shift = []
            guards[act].append(shift)
        elif act == 'falls':
            shift.append(m)
        elif act == 'wakes':
            m2 = shift.pop()
            shift.append((m2, m))

    if not second:
        _, g, shifts = max(
            (sum(m2 - m for m, m2 in flatten(shifts)), g, shifts)
            for g, shifts in guards.items())
        minutes = np.zeros(60, dtype=np.int32)
        for m, m2 in flatten(shifts):
            minutes[m:m2] += 1
        # print(minutes)
        return g * int(np.argmax(minutes))

    lst = []
    for g, shifts in guards.items():
        minutes = np.zeros(60, dtype=np.int32)
        for m, m2 in flatten(shifts):
            minutes[m:m2] += 1
        am = np.argmax(minutes)
        lst.append((minutes[am], am, g))
    _, m, g = max(lst)
    return m * g


def problem5(data, second):
    # data = split_data('')
    # if second: return
    from blist import blist

    def react(data_str: str):
        data = blist(data_str)
        while True:
            oldlen = len(data)
            i = 0
            while i < len(data) - 1:
                if data[i] != data[i + 1] and data[i].lower() == data[i + 1].lower():
                    del data[i : i + 2]
                    i = max(0, i - 1)
                else:
                    i += 1
            if oldlen == len(data):
                break
        return data

    data = react(data)
    if not second:
        return len(data)

    data = ''.join(data)
    units = set(x.lower() for x in data)
    # print(units)
    return min(
        len(react(re.sub(f'[{u}{u.upper()}]', '', data)))
        for u in units)


def problem6(data, second):
    # data = split_data('')
    # if second: return
    data = ReTokenizer('{int}, {int}').match_all(data)

    min_x = min(d[0] for d in data)
    min_y = min(d[1] for d in data)
    max_x = max(d[0] for d in data)
    max_y = max(d[1] for d in data)
    size_x = max_x - min_x + 1
    size_y = max_y - min_y + 1
    field = np.zeros((size_x, size_y), np.int8)
    field_turn = np.zeros((size_x, size_y), np.int8)
    front = []
    infinitely_large = set()
    turn = 1
    for i, (x, y) in enumerate(data):
        x -= min_x
        y -= min_y
        front.append((x, y))
        field[x, y] = i + 1
        field_turn[x, y] = turn

    if second:
        dist_x = np.zeros(size_x, dtype=np.int32)
        dist_y = np.zeros(size_y, dtype=np.int32)
        for x, y in front:
            dist_x[0 : x] += np.arange(x, 0, -1)
            dist_x[x + 1 : size_x] += np.arange(1, size_x - x)
            dist_y[0 : y] += np.arange(y, 0, -1)
            dist_y[y + 1 : size_y] += np.arange(1, size_y - y)
        total = dist_x[:, np.newaxis] + dist_y
        return np.sum(total < 10000)

    while front:
        newfront = []
        turn += 1
        while front:
            x, y = front.pop()
            color = field[x, y]
            if color < 0:
                continue
            for dx, dy in directions4:
                nx, ny = x + dx, y + dy
                if 0 <= nx < size_x and 0 <= ny < size_y:
                    if field[nx, ny] == 0:
                        newfront.append((nx, ny))
                        field[nx, ny] = color
                        field_turn[nx, ny] = turn
                    elif field_turn[nx, ny] == turn and field[nx, ny] != color:
                        field[nx, ny] = -1
                else:
                    infinitely_large.add(color)
        front = newfront
    # pprint(field)
    return max(size for i, size in Counter(map(int, field.flat)).items()
            if i > 0 and i not in infinitely_large)


def problem7(data, second):
    # if second: return
    data = ReTokenizer('Step {id} must be finished before step {id} can begin.').match_all(data)
    prereqs = defaultdict(set)
    enables = defaultdict(list)
    for before, after in data:
        prereqs[after].add(before)
        enables[before].append(after)

    available = [c for c in string.ascii_uppercase if c not in prereqs]
    assert available
    tasks = []
    def topotasks():
        while available:
            available.sort(reverse=True)
            task = available.pop()
            yield task
            for it in enables[task]:
                prereqs[it].remove(task)
                if not prereqs[it]:
                    available.append(it)

    if not second:
        return ''.join(topotasks())

    time = 0
    workers = 5
    in_progress = []
    heapify(available)

    while available or in_progress:
        while available and workers:
            task = heappop(available)
            workers -= 1
            heappush(in_progress, (time + 61 + ord(task) - ord('A'), task))

        time, task = heappop(in_progress)
        workers += 1

        for it in enables[task]:
            prereqs[it].remove(task)
            if not prereqs[it]:
                available.append(it)
    return time


def problem8(data, second):
    # if second: return
    data = list(map(int, data.split()))
    data.reverse()

    def readrec(data):
        nc = data.pop()
        nm = data.pop()
        children = [readrec(data) for _ in range(nc)]
        metadata = [data.pop() for _ in range(nm)]
        return children, metadata

    tree = readrec(data)
    assert not data

    def sumrec(tree):
        return sum(tree[1]) + sum(sumrec(c) for c in tree[0])

    def sumrec2(tree):
        cs, ms = tree
        if not cs:
            return sum(ms)
        s = 0
        for m in ms:
            m -= 1
            if 0 <= m < len(cs):
                s += sumrec2(cs[m])
        return s

    return sumrec2(tree) if second else sumrec(tree)


def problem9(data, second):
    # data = '13 players; last marble is worth 7999 points'
    # if second: return
    player_count, last_marble = ReTokenizer('{int} players; last marble is worth {int} points'
            ).fullmatch(data)
    print(data)
    if second:
        last_marble *= 100
    players = [0] * player_count
    player = 0
    current = 0
    marbles = blist([0])
    for m in range(1, last_marble + 1):
        if not m % 23:
            players[player] += m
            current = (current - 7) % len(marbles)
            players[player] += marbles.pop(current)
        else:
            current = (current + 2) % len(marbles)
            marbles.insert(current, m)
        player = (player + 1) % len(players)
    return max(players)


def problem10(data, second):
    if second: return
    data = ReTokenizer('position=< *{int}, *{int}> velocity=< *{int}, *{int}>').match_all(data)
    pos = np.zeros((len(data), 2), dtype=np.int32)
    vel = np.copy(pos)
    for i, (x, y, dx, dy) in enumerate(data):
        pos[i] = x, y
        vel[i] = dx, dy

    def visualize(pos):
        min_x, w, min_y, h = np_2d_aabbox(pos)
        sq = np.full((h, w), ord('.'), np.uint8)
        for x, y in pos:
            sq[y - min_y, x - min_x] = ord('#')
        for row in sq:
            print(''.join(chr(i) for i in row))
        print()

    min_w = np_2d_aabbox(pos)[1]
    counter = 0
    while True:
        pos += vel
        counter += 1
        w = np_2d_aabbox(pos)[1]
        # print(w)
        if w < min_w:
            min_w = w
            continue
        # visualize(pos)
        pos -= vel
        visualize(pos)
        pos -= vel
        # visualize(pos)
        return counter - 1


def problem11(data, second):
    # if second: return
    # print(data)
    serial = int(data)
    rack_id = np.arange(11, 11 + 300, dtype=np.int32)
    power = np.arange(1, 301, dtype=np.int32)[:,np.newaxis] * rack_id
    power += serial
    power *= rack_id
    power //= 100
    power %= 10
    power -= 5
    sat = power.cumsum(axis=0).cumsum(axis=1)
    best = None
    for size in (range(1, 300) if second else [3]):
        sums = sat[:-size, :-size] + sat[size:, size:] - sat[size:, :-size] - sat[:-size, size:]
        y, x = np.unravel_index(np.argmax(sums), sums.shape)
        val = sums[y, x]
        if not best or best < val:
            best = val
            x += 2 # I'm probably missing some sums...
            y += 2
            best_answer = f'{x},{y},{size}' if second else f'{x},{y}'
    print(best)
    return best_answer


def problem12(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''\
initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #''')
    state = data[0].split(':')[1].strip()
    rules = dict(ReTokenizer('{:([.#]{5})} => {:([.#])}').match_all(data[2:]))
    offset = 10
    state = '.' * offset + state + '.' * offset

    def step(state: str):
        s = []
        s.append('..')
        for i in range(2, len(state) - 2):
            new = rules.get(state[i - 2 : i + 3], '.')
            s.append(new)
        s.append('..' + '.' * ('#' in s[-5:]))
        return ''.join(s)

    if not second:
        for i in range(20):
            state = step(state)
        return sum(i - offset for i, p in enumerate(state) if p == '#')

    for turn in range(1, 9999):
        oldstate = state
        state = step(state)
        # print(state)
        if state[1:] == oldstate:
            break

    return sum(i - offset + 50_000_000_000 - turn for i, p in enumerate(state) if p == '#')


def problem13(data, second):
    data = get_raw_data()
    data_ = r'''
/->-\
|   |  /----\
| /-+--+-\  |
| | |  | v  |
\-+-/  \-+--/
  \------/'''
    # if second: return
    data = [s for s in data.split('\n') if s.strip()]

    @dataclass
    class Cart:
        pos: Tuple
        direction: int # up, then clockwise
        turn_counter = 0
        def move(self):
            self.pos = addv2(self.pos,
                ((-1, 0), (0, 1), (1, 0), (0, -1))[self.direction])


    carts = []
    positions = set()
    for row, line in enumerate(data):
        for m in re.finditer('[>v<^]', line):
            c = m.group()
            pos = row, m.start()
            carts.append(Cart(pos, '^>v<'.index(c)))
            positions.add(pos)
        line = re.sub('[<>]', '-', line)
        line = re.sub('[v^]', '|', line)
        data[row] = line

    def move(c: Cart):
        tile = data[c.pos[0]][c.pos[1]]
        if tile == '\\':
            c.direction = 3 - c.direction
        elif tile == '/':
            c.direction = c.direction ^ 1
        elif tile == '+':
            c.direction = (c.direction + [-1, 0, 1][c.turn_counter % 3]) % 4
            c.turn_counter += 1
        elif tile == '-':
            assert c.direction in (1, 3)
        elif tile == '|':
            assert c.direction in (0, 2)
        else:
            assert False
        positions.remove(c.pos)
        c.move()
        if c.pos in positions:
            return c.pos
        positions.add(c.pos)

    while True:
        if len(carts) < 2:
            p = carts[0].pos
            return f'{p[1]},{p[0]}'
        carts.sort(key=lambda c: c.pos)
        # print(carts)
        idx = 0
        while idx < len(carts):
            crash = move(carts[idx])
            if not crash:
                idx += 1
                continue
            if not second:
                return f'{crash[1]},{crash[0]}'
            del carts[idx]
            for i2, c in enumerate(carts):
                if c.pos == crash:
                    del carts[i2]
                    break
            else:
                assert False
            if i2 < idx:
                idx -= 1
            positions.remove(crash)


def problem14(data, second):
    # if second: return
    # pprint(data)
    # data = '59414'
    if second:
        data = [int(c) for c in data]
    else:
        data = int(data)

    lst = [3, 7]
    pos1, pos2 = 0, 1
    def step(append=lambda c: lst.append(c)):
        nonlocal pos1, pos2, lst
        s = lst[pos1] + lst[pos2]
        if s >= 10:
            append(s // 10)
        append(s % 10)
        pos1 = (pos1 + lst[pos1] + 1) % len(lst)
        pos2 = (pos2 + lst[pos2] + 1) % len(lst)

    if not second:
        target_len = 10 + data
        while len(lst) < target_len:
            step()
        return ''.join(map(str, lst[data : data + 10]))

    class Found(Exception):
        ''
    def append(c):
        lst.append(c)
        if lst[-len(data):] == data:
            raise Found()

    try:
        while True:
            step(append)
    except Found:
        pass

    return len(lst) - len(data)


def problem15(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''\
#######
#.G...#
#...EG#
#.#.#G#
#..G#E#
#.....#
#######''')

    data_ = split_data('''\
#######
#G..#E#
#E#E.E#
#G.##.#
#...#E#
#...E.#
#######''')

    data_ = split_data('''\
#######
#E..EG#
#.#G.E#
#E.##E#
#G..#.#
#..E#.#
#######''')

    data = [list(s) for s in data]

    @dataclass
    class Unit:
        kind: str
        pos: np.array
        hp = 200
        attack: int = 3

        @cached_property
        def target(self):
            return 'E' if self.kind == 'G' else 'G'


    def get(idx):
        return data[idx[0]][idx[1]]

    def assign(idx, value):
        data[idx[0]][idx[1]] = value

    def pos2unit(pos):
        return next(u for u in units if u.pos == pos and u.hp > 0)

    dirs4 = (
            (-1,  0),
            ( 0, -1),
            ( 0,  1),
            ( 1,  0))

    def move(unit):
        passable = '.' + unit.target
        def neighbors(pos):
            for d in dirs4:
                np = addv2(d, pos)
                if get(np) in passable:
                    yield np

        goals, visited = bfs_full([unit.pos], lambda pos: get(pos) == unit.target, neighbors)
        if not goals:
            return

        # pprint(visited)
        def next_to_goals():
            for g in goals:
                # print(g, list(neighbors(g)))
                for n in neighbors(g):
                    if n in visited:
                        p = bfs_extract_path(n, visited)
                        yield -len(p), p[-1][0], p[-1][1], p[1]

        step = min(next_to_goals())[-1]
        # print(f'move {unit.pos} -> {step}')
        assign(unit.pos, '.')
        unit.pos = step
        assign(unit.pos, unit.kind)
        return True

    class ElfDied(Exception):
        ''

    def attack(unit):
        targets = []
        for d in dirs4:
            np = addv2(d, unit.pos)
            if get(np) == unit.target:
                u = pos2unit(np)
                targets.append((u.hp, np, u))
        if not targets:
            return
        u = min(targets)[-1]
        u.hp -= unit.attack
        if u.hp <= 0:
            if second and u.kind == 'E':
                raise ElfDied()
            assign(u.pos, '.')
        return True

    def print_state():
        for line in data:
            print(''.join(line))
        for u in units:
            print(f'{u.kind} {u.hp}')
        print()

    def do_turn():
        nonlocal units
        for u in units:
            if len({u.kind for u in units if u.hp > 0}) < 2:
                return True
            if u.hp <= 0:
                continue
            if attack(u):
                continue
            if move(u):
                attack(u)
        units = sorted((u for u in units if u.hp > 0), key=lambda u: u.pos)

    units = []
    def run_simulation(elf_attack):
        del units[:]
        for row_idx, row in enumerate(data):
            for col_idx, c in enumerate(row):
                if c in 'EG':
                    units.append(Unit(c, (row_idx, col_idx), attack=elf_attack if c == 'E' else 3))

        for turn in range(0, 99999):
            res = do_turn()
            # print_state()
            # print(f'***** turn: {turn}, units: {len(units)} *****')
            if res:
                return turn * sum(u.hp for u in units if u.hp > 0)

    # print_state()
    backup = copy.deepcopy(data)
    for elf_attack in range(3, 999):
        print(f'\n\nelf_attack={elf_attack}\n')
        data = copy.deepcopy(backup)
        try:
            return run_simulation(elf_attack)
        except ElfDied:
            pass


##########

def problem16(data, second):
    # if second: return
    # pprint(data)
    it = iter(data)

    samples = []
    program = []

    def parse_samples():
        while True:
            s = next(it)
            if not s.startswith('Before:'):
                return s
            before = ast.literal_eval(s.split(':')[1].strip())
            s = next(it)
            cmd = list(map(int, s.split()))
            assert len(cmd) == 4
            s = next(it)
            assert s.startswith('After:')
            after = ast.literal_eval(s.split(':')[1].strip())
            samples.append((before, cmd, after))

    def parse_program(s):
        while s:
            cmd = list(map(int, s.split()))
            assert len(cmd) == 4
            program.append(cmd)
            s = next(it, None)

    parse_program(parse_samples())

    def execute(r, insn, a, b, c):
        if insn < 8:
            op = [operator.add, operator.mul, operator.and_, operator.or_][insn >> 1]
            a = r[a]
            if not insn & 1:
                b = r[b]
        elif insn < 10:
            if not insn & 1:
                a = r[a]
            op = lambda a, b: a
        else:
            insn -= 10
            op = [operator.gt, operator.eq][insn // 3]
            rr = insn % 3
            if rr in (1, 2):
                a = r[a]
            if rr in (0, 2):
                b = r[b]
        r[c] = op(a, b)
        return r

    possible_mappings = [set(range(16)) for _ in range(16)]
    total_count = 0
    for before, op, after in samples:
        count = 0
        mm = set()
        for insn in range(16):
            if execute(copy.copy(before), insn, *op[1:]) == after:
                count += 1
                mm.add(insn)
        if count >= 3:
            total_count += 1
        possible_mappings[op[0]] &= mm

    if not second:
        return total_count

    front = [m for i, m in enumerate(possible_mappings) if len(m) == 1]
    while front:
        m = front.pop()
        for m2 in possible_mappings:
            if len(m2) > 1:
                m2 -= m
                if len(m2) == 1:
                    front.append(m2)
    # print(possible_mappings)
    assert all(len(m) == 1 for m in possible_mappings)
    mapping = {i: m.pop() for i, m in enumerate(possible_mappings)}

    r = [0] * 16
    for insn, a, b, c in program:
        execute(r, mapping[insn], a, b, c)
    return r[0]


##########

def problem17(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''
x=495, y=2..7
y=7, x=495..501
x=501, y=3..7
x=498, y=2..4
x=506, y=1..2
x=498, y=10..13
x=504, y=10..13
y=13, x=498..504''')
    data = ReTokenizer('{id}={int}, {id}={int}..{int}').match_all(data)
    regions = np.zeros((len(data), 4), dtype=np.int32)
    for idx, (c1, d1, c2, d2, d3) in enumerate(data):
        assert c1 in 'xy' and c2 in 'xy' and c1 != c2
        if c1 == 'x':
            regions[idx] = (d2, d3, d1, d1)
        else:
            regions[idx] = (d1, d1, d2, d3)
    min_y, max_y = np.min(regions[:, 0]), np.max(regions[:, 1])
    min_x, max_x = np.min(regions[:, 2]), np.max(regions[:, 3])
    min_x -= 1
    max_x += 1
    min_y -= 1 # for proper flow, don't forget to ignore the first column.
    field = np.zeros((max_y - min_y + 1, max_x - min_x + 1), dtype=np.uint8)
    regions[:, 0] -= min_y
    regions[:, 1] -= min_y - 1
    regions[:, 2] -= min_x
    regions[:, 3] -= min_x - 1
    for y1, y2, x1, x2 in regions:
        field[y1:y2, x1:x2] = 1
    F_EMPTY = 0
    F_WALL = 1
    F_STILL = 2
    F_FLOWING = 3

    def free_flow_below(y, x):
        if y >= field.shape[0]:
            return True
        c = field[y, x]
        if c in (F_WALL, F_STILL):
            return False
        if c == F_FLOWING:
            return True
        assert c == F_EMPTY
        field[y, x] = F_FLOWING
        if free_flow_below(y + 1, x):
            return True
        ldir = free_flow_direction(y, x, -1)
        rdir = free_flow_direction(y, x, 1)
        if ldir is None or rdir is None:
            return True
        field[y, x - ldir : x + rdir + 1] = F_STILL
        return False

    def free_flow_direction(y, x, d):
        for steps in range(0, 999999):
            x += d
            c = field[y, x]
            if c in (F_WALL, F_STILL):
                return steps
            if c == F_FLOWING:
                return None # hole ahead then
            assert c == F_EMPTY
            field[y, x] = F_FLOWING
            if free_flow_below(y + 1, x):
                return None

    origin = 500 - min_x
    rlimit = sys.getrecursionlimit()
    sys.setrecursionlimit(2000)
    try:
        free_flow_below(0, origin)
    finally:
        sys.setrecursionlimit(rlimit)
    # with open('field.txt', 'w') as f:
    #     np.savetxt(f, field, fmt='%d')
    if second:
        return np.sum(field[1:] == F_STILL)
    return np.sum(field[1:] > F_WALL)



def problem18(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''
.#.#...|#.
.....#|##|
.|..|...#.
..|#.....#
#.#|||#|#|
...#.||...
.|....|...
||...#|.#|
|.||||..|.
...#.|..|.''')

    world = np.zeros((len(data), len(data[0])), dtype=np.uint8)
    new_world = np.zeros_like(world)
    nb = np.zeros_like(world)
    nb_lumberyards = np.zeros_like(world)

    for row_idx, row in enumerate(data):
        for col_idx, c in enumerate(row):
            world[row_idx, col_idx] = {'.': 0, '|': 1, '#': 10}[c]

    def print_world():
        for row in world:
            print(''.join({0: '.', 1: '|', 10: '#'}[c] for c in row))

    def step():
        nonlocal world, new_world
        np_add_8neighbors(world, nb)
        np.divmod(nb, 10, nb_lumberyards, nb)

        new_world += (world == 0) * (nb >= 3)
        np.add(new_world, 1, out=new_world, where=(world == 1) & (nb_lumberyards < 3))
        np.add(new_world, 10, out=new_world, where=(world == 1) & (nb_lumberyards >= 3)) # add ly
        np.add(new_world, 10, out=new_world, where=(world == 10) & (nb_lumberyards > 0) & (nb > 0))
        world, new_world = new_world, world
        new_world[...] = 0

    seen = {}
    the_range = 1_000_000_000 if second else 10
    turn = 0
    skipped = False
    while turn < the_range:
        step()
        s = world.tostring()
        if s in seen and not skipped:
            cycle = turn - seen[s]
            remaining = the_range - turn - 1
            skip = remaining // cycle * cycle
            turn += skip
            assert turn < the_range
            skipped = True
        else:
            seen[s] = turn
        turn += 1

    return np.sum(1, where=(world == 1)) * np.sum(1, where=(world == 10))


def problem19(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''
#ip 0
seti 5 0 1
seti 6 0 2
addi 0 1 0
addr 1 2 3
setr 1 0 0
seti 8 0 4
seti 9 0 5''')
    ip_register = ReTokenizer('#ip {int}').fullmatch(data.pop(0))[0]
    program = ReTokenizer('{id} {int} {int} {int}').match_all(data)
    insns = {
        'addr': lambda r, a, b: r[a] + r[b],
        'addi': lambda r, a, b: r[a] + b,
        'mulr': lambda r, a, b: r[a] * r[b],
        'muli': lambda r, a, b: r[a] * b,
        'banr': lambda r, a, b: r[a] & r[b],
        'bani': lambda r, a, b: r[a] & b,
        'borr': lambda r, a, b: r[a] | r[b],
        'bori': lambda r, a, b: r[a] | b,
        'setr': lambda r, a, b: r[a],
        'seti': lambda r, a, b: a,
        'gtir': lambda r, a, b: a > r[b],
        'gtri': lambda r, a, b: r[a] > b,
        'gtrr': lambda r, a, b: r[a] > r[b],
        'eqir': lambda r, a, b: a == r[b],
        'eqri': lambda r, a, b: r[a] == b,
        'eqrr': lambda r, a, b: r[a] == r[b],
    }
    assert len(insns) == 16
    ip = 0
    r = [0] * 6
    if second:
        r[0] = 1
    while 0 <= ip < len(program):
        if ip == 1:
            return sum(x for x in range(1, r[3] + 1) if r[3] % x == 0)
        r[ip_register] = ip
        insn, a, b, c = program[ip]
        # print(ip, insn, a, b, c, r, end=' ')
        r[c] = insns[insn](r, a, b)
        # print(r)
        ip = r[ip_register] + 1
    return r[0]


def problem20(data, second):
    # if second: return
    # pprint(data)
    data_ = '^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$'
    data_ = '^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$'
    data = list(reversed(data))
    data_pos = 1
    def pop():
        nonlocal data_pos
        val = data[data_pos]
        data_pos += 1
        # print(data_pos, val)
        return val

    class Field:
        def __init__(self):
            self.pos = (0, 0)
            self.doors = set()
            self.rooms = set([self.pos])

        def join(self, other):
            offset = (self.pos[0] - other.pos[0], self.pos[1] - other.pos[1])
            self.doors.update(addv2(p, offset) for p in other.doors)
            self.rooms.update(addv2(p, offset) for p in other.rooms)

        def __str__(self):
            arr = np.array(list(self.rooms))
            min_y, h, min_x, w = np_2d_aabbox(arr)
            res = [['#'] * w for _ in range(h)]
            for y, x in self.rooms:
                res[y - min_y][x - min_x] = '.'
            for y, x in self.doors:
                res[y - min_y][x - min_x] = '+'
            res[self.pos[0] - min_y][self.pos[1] - min_x] = 'X'
            return '\n'.join(''.join(l) for l in res)


    dirs = {'N': (1, 0), 'S': (-1, 0), 'E': (0, -1), 'W': (0, 1)}

    def parse_seq(field: Field):
        while True:
            # print(data[:data_pos])
            # print(field)
            c = pop()
            d = dirs.get(c)
            if d:
                field.pos = addv2(field.pos, d)
                field.doors.add(field.pos)
                field.pos = addv2(field.pos, d)
                field.rooms.add(field.pos)
            elif c == ')':
                alternatives = []
                done = False
                orig = copy.deepcopy(field)
                more_alts = parse_seq(field)
                while more_alts:
                    alt = copy.deepcopy(orig)
                    alternatives.append(alt)
                    more_alts = parse_seq(alt)
                for alt in alternatives:
                    field.join(alt)
            elif c == '|':
                return True
            elif c in '^(':
                return False
            else:
                assert False, c

    field = Field()
    assert not parse_seq(field)
    # print(field)

    def neighbors(pos):
        for d in dirs.values():
            p = addv2(pos, d)
            if p in field.doors:
                yield addv2(p, d)

    start = [field.pos]
    visited = {s: None for s in start}
    front = deque(start)
    step = 0
    if second:
        while front:
            bfs_full_step(front, visited, lambda pos: False, neighbors)
            step += 1
            if step >= 999:
                break
        return len(field.rooms) - len(visited)
    else:
        while front:
            bfs_full_step(front, visited, lambda pos: False, neighbors)
            step += 1
        return step - 1


def problem21(data, second):
    # if second: return
    for i, s in enumerate(data[1:]):
        print(f'{i:2} {s}')
    print()

    ip_register = ReTokenizer('#ip {int}').fullmatch(data.pop(0))[0]
    program = ReTokenizer('{id} {int} {int} {int}').match_all(data)
    insns = {
        'addr': lambda r, a, b: r[a] + r[b],
        'addi': lambda r, a, b: r[a] + b,
        'mulr': lambda r, a, b: r[a] * r[b],
        'muli': lambda r, a, b: r[a] * b,
        'banr': lambda r, a, b: r[a] & r[b],
        'bani': lambda r, a, b: r[a] & b,
        'borr': lambda r, a, b: r[a] | r[b],
        'bori': lambda r, a, b: r[a] | b,
        'setr': lambda r, a, b: r[a],
        'seti': lambda r, a, b: a,
        'gtir': lambda r, a, b: a > r[b],
        'gtri': lambda r, a, b: r[a] > b,
        'gtrr': lambda r, a, b: r[a] > r[b],
        'eqir': lambda r, a, b: a == r[b],
        'eqri': lambda r, a, b: r[a] == b,
        'eqrr': lambda r, a, b: r[a] == r[b],
    }
    assert len(insns) == 16
    ip = 0
    r = [0] * 6
    if not second:
        r[0] = 2159153
    cnt = 0

    def fixup25():
        if r[3] > 2:
            r[3] = r[4] // 256

    seen = set()
    prev_seen = None
    def fixup28():
        nonlocal prev_seen
        # if not len(seen) & 0xFF:
        #     print(ip, insn, a, b, c, r, len(seen), cnt)
        x = r[1]
        if x in seen:
            r[0] = prev_seen
            return True
        seen.add(x)
        prev_seen = x

    exec_pre = {
        #20: lambda: (r.__setitem__(4, r[2]), print(f'hello {r}')),
        25: fixup25,
        28: fixup28,
    }

    while 0 <= ip < len(program):
        r[ip_register] = ip
        insn, a, b, c = program[ip]
        if ip in exec_pre:
            # print(f'Hook at {ip}')
            if exec_pre[ip]():
                break

        r[c] = insns[insn](r, a, b)
        ip = r[ip_register] + 1
        cnt += 1
        if cnt > 2000000:
            break
    return r[0]


def problem22(data, second):
    # if second: return
    # pprint(data)
    # data = split_data('''d: 510\nt: 10,10''')
    _, depth = data[0].split(': ')
    depth = int(depth)
    _, xy = data[1].split(': ')
    target_x, target_y = map(int, xy.split(','))
    width, height = target_x * 2, target_y + 3
    erosion_map = np.zeros((height, width), dtype=np.int32)
    modulo = 20183
    for y in range(1, height):
        erosion_map[y][0] = (y * 48271 + depth) % modulo
    for x in range(1, width):
        erosion_map[0][x] = (x * 16807 + depth) % modulo
    for y in range(1, height):
        for x in range(1, width):
            erosion_map[y][x] = (erosion_map[y - 1][x] * erosion_map[y][x - 1] + depth) % modulo

    def erosion_to_terrain(er):
        t = np.zeros_like(er, dtype=np.int8)
        np.mod(er, 3, out=t)
        return t

    t_map = erosion_to_terrain(erosion_map)
    t_map[target_y][target_x] = 0 # rocky
    # for row in t_map:
    #     print(''.join('.=|'[c] for c in row))

    if not second:
        return(np.sum(t_map[0 : target_y + 1, 0 : target_x + 1]))

    @dataclass(frozen=True)
    class Node:
        x: int
        y: int
        t: int

    g = networkx.Graph()

    with timeit_block('building_graph'):
        for (y, x), terrain in np.ndenumerate(t_map):
            for t in range(3):
                if t == terrain:
                    continue

                cur = Node(x, y, t)
                for ot in range(3):
                    if ot != t and ot != terrain:
                        g.add_edge(cur, Node(x, y, ot), weight=7)

                for dy, dx in directions4:
                    nx, ny = x + dx, y + dy
                    if 0 <= nx < width and 0 <= ny < height and t != t_map[ny][nx]:
                        g.add_edge(cur, Node(nx, ny, t))

    # for n in networkx.dijkstra_path(g, Node(0, 0, 1), Node(target_x, target_y, 1)):
    #     print(n, t_map[n.y][n.x])

    return networkx.dijkstra_path_length(g, Node(0, 0, 1), Node(target_x, target_y, 1))


def problem23(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''pos=<10,12,12>, r=2
pos=<12,14,12>, r=2
pos=<16,12,12>, r=4
pos=<14,14,14>, r=6
pos=<50,50,50>, r=200
pos=<10,10,10>, r=5''')
    bots4 = np.asarray(ReTokenizer('pos=<{int},{int},{int}>, r={int}').match_all(data), dtype=np.int32)
    largest_idx = np.argmax(bots4[:, 3])
    r = bots4[largest_idx, 3]
    pos = bots4[largest_idx, :3]
    dd = np.sum(np.abs(bots4[:, :3] - pos), axis=1)
    if not second:
        return np.sum(dd <= r)

    try:
        import z3
    except Exception as exc:
        print('z3 not installed?')
        import traceback
        traceback.print_exception(*sys.exc_info())
        return

    o = z3.Optimize()

    def z3abs(x):
        return z3.If(x >= 0, x, -x)
    var_x, var_y, var_z = z3.Int('x'), z3.Int('y'), z3.Int('z')
    ranges = []
    sum_target = z3.Int('sum_target')
    print('generating model')
    for i, v in enumerate(bots4):
        x, y, z, r = map(int, v)
        ranges.append(z3.If(z3abs(var_x - x) + z3abs(var_y - y) + z3abs(var_z - z) <= r, 1, 0))
    o.add(sum_target == sum(ranges))
    print('solving')
    z3.set_param('parallel.enable', True)
    o.maximize(sum_target)
    r = o.minimize(var_x + var_y + var_z)
    print(o.check())
    print(o.model())
    return int(r.value().as_long())


def problem24(data, second):
    # if second: return
    # pprint(data)
    data_ = split_data('''Immune System:
17 units each with 5390 hit points (weak to radiation, bludgeoning) with an attack that does 4507 fire damage at initiative 2
989 units each with 1274 hit points (immune to fire; weak to bludgeoning, slashing) with an attack that does 25 slashing damage at initiative 3

Infection:
801 units each with 4706 hit points (weak to radiation) with an attack that does 116 bludgeoning damage at initiative 1
4485 units each with 2961 hit points (immune to radiation; weak to fire, cold) with an attack that does 12 slashing damage at initiative 4''')

    IMMUNE_SYSTEM='Immune System:'
    INFECTION='Infection:'
    rt = ReTokenizer()
    rt.add_tuple(IMMUNE_SYSTEM)
    rt.add_tuple(INFECTION)
    #{attrs:(\([^)]*\))}
    UnitBase = rt.add_dataclass_old(r'{n:int} units each with {hp:int} hit points {attrs:(\([^)]*\))}?'
            '\s*with an attack that does {a_str:int} {a_type:id} damage at initiative {initiative:int}'
            , frozen=False)
    data = rt.match_all(data)
    assert data[0] == (IMMUNE_SYSTEM,)
    units, immune, infection = [], [], []
    current = immune

    @dataclass
    class Unit(UnitBase):
        is_immune: bool
        weak: Set[str] = dataclass_field(default_factory=set)
        immune: Set[str] = dataclass_field(default_factory=set)
        selected: bool = False
        attacking: Any = None

        @property
        def effective_power(self):
            return self.n * self.a_str

        def damage_against(self, enemy):
            if self.a_type in enemy.immune:
                return 0
            p = self.effective_power
            if self.a_type in enemy.weak:
                p *= 2
            return p

        @property
        def alive(self):
            return self.n > 0


    for d in data[1:]:
        if d == (INFECTION,):
            assert current == immune
            current = infection
            continue
        s : str = d.attrs
        d = Unit(**dataclass_asdict(d), is_immune=current is immune)
        if s is not None:
            for ss in s.strip('()').split(';'):
                what, _, rest = ss.partition('to')
                what = what.strip()
                assert what in ('weak', 'immune')
                getattr(d, what).update(it.strip() for it in rest.split(','))

        current.append(d)

    def run(infection, immune, boost=0):
        infection = copy.deepcopy(infection)
        immune = copy.deepcopy(immune)

        for u in immune:
            u.a_str += boost

        while infection and immune:
            attacked = False
            units = infection + immune
            units.sort(reverse=True, key=lambda u: (u.effective_power, u.initiative))
            for u in units:
                u.selected = False
                u.attacking = None
            for u in units:
                enemy_force = (immune, infection)[u.is_immune]
                enemies = [e for e in enemy_force if not e.selected]
                if not enemies:
                    continue
                e = max(enemies, key=lambda e: (u.damage_against(e), e.effective_power, e.initiative))
                if u.damage_against(e):
                    u.attacking = e
                    e.selected = True
                    # print(u.n, 'attacks', e.n)
            # attack phase
            units.sort(reverse=True, key=lambda u: u.initiative)
            for u in units:
                a = u.attacking
                if u.alive and a:
                    dmg = u.damage_against(a) // a.hp
                    if dmg:
                        attacked = True
                    a.n -= dmg
            immune = [u for u in immune if u.alive]
            infection = [u for u in infection if u.alive]
            if not attacked:
                return False, 0
        return immune, sum(u.n for u in (immune or infection))

    if not second:
        return run(infection, immune)[1]

    # invariants: l unsuccessful, r successful
    l, r = 0, 100
    while True:
        m = (l + r) // 2
        print(l, m, r)
        if m == l:
            return run(infection, immune, r)[1]
        success, n = run(infection, immune, m)
        if success:
            r = m
        else:
            l = m


def problem25(data, second):
    if second: return
    pprint(len(data))
    data_ = split_data('''1,-1,0,1
2,0,-1,0
3,2,-1,0
0,0,3,1
0,0,-1,-1
2,3,-2,0
-2,2,0,0
2,-2,0,-1
1,-1,0,-1
3,2,0,2''')
    data = np.asarray([[int(t) for t in s.split(',')] for s in data], dtype=np.int32)
    print(data)
    g = networkx.Graph()
    print('building graph')
    for i, di in enumerate(data):
        g.add_node(i)
        for j in range(i + 1, len(data)):
            dj = data[j]
            dst = np.sum(np.abs(di - dj))
            if dst <= 3:
                # print(i, j, di, dj)
                g.add_edge(i, j)
    print('finding connected components')
    return networkx.number_connected_components(g)



##########

def problem(data, second):
    if second: return
    pprint(data)
    data_ = split_data(''' ''')


##########

if __name__ == '__main__':
    print('Hello')
    # solve_all([])
    solve_latest()
