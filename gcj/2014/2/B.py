import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *
from copy import copy, deepcopy
from pprint import pprint
from glob import glob

taskname = 'B'
input = None

def readstr():
    return next(input).strip()

def readintlist():
    lst = map(int, readstr().split())
    return lst

def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]

def merge(left, right):
    count = 0
    result = []
    left_idx = 0
    right_idx = 0
    while left_idx < len(left) and right_idx < len(right):
        if left[left_idx] <= right[right_idx]:
            result.append(left[left_idx])
            left_idx += 1
        else:
            result.append(right[right_idx])
            right_idx += 1
            count += len(left) - left_idx
    result += left[left_idx:]
    result += right[right_idx:]
    return result, count
    
def sort_count_swaps(lst):
    if len(lst) <= 1:
        return lst, 0
    pivot = len(lst) // 2
    left, inv_left = sort_count_swaps(lst[:pivot])
    right, inv_right = sort_count_swaps(lst[pivot:])
    merged, inv_merged = merge(left, right)
    return merged, inv_left + inv_right + inv_merged

def count_swaps_slow(lst):
    lst = lst[:]
    count = 0
    for i in xrange(len(lst) - 1):
        for j in xrange(i, len(lst) - 1):
            if lst[j] > lst[j + 1]:
                lst[j], lst[j + 1] = lst[j + 1], lst[j] 
                count += 1
    return count

lst = [6, 1, 2, 7, 3, 4, 5]
print sort_count_swaps(lst)[1], count_swaps_slow(lst)
sys.exit() 

def required_swaps(lst, max_idx, pivot_idx):
    if pivot_idx < max_idx:
        left = lst[0 : pivot_idx]
        right = lst[pivot_idx : max_idx] + lst[max_idx + 1 : ]
    else:
        left = lst[0 : max_idx] + lst[max_idx + 1 : pivot_idx + 1]
        right = lst[pivot_idx + 1 : ]
    right.reverse()
#    swaps_left = sort_count_swaps(left)[1]
    swaps_left1 = count_swaps_slow(left)
#    swaps_right = sort_count_swaps(right)[1] 
    swaps_right1 = count_swaps_slow(right)
#    print left, swaps_left, swaps_left1 
#    print right, swaps_right, swaps_right1 
#    assert swaps_left == swaps_left1 and swaps_right == swaps_right1 
    return (abs(max_idx - pivot_idx) + 
            swaps_left1 +
            swaps_right1)
        

def solvecase():
    N = readint()
    lst = readintlist()
    assert N == len(lst)
    max_idx = max(zip(lst, xrange(len(lst))))[1]
    return min(required_swaps(lst, max_idx, pivot_idx) for pivot_idx in xrange(len(lst)))

def solve(input_name, output_name):
    global input
    tstart = time.clock()
    input = open(input_name, 'r')
    output = open(output_name, 'w')
    casecount = readint()
    
    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s)) 
        print >>output, s
        print s 
        
    input.close()
    output.close()
    print '%s solved in %.3f' % (input_name, time.clock() - tstart)

def main():
    input_names = glob(taskname + '-*.in')
    assert len(input_names)
    input_names.sort(reverse = True)
    for input_name in input_names:
        solve(input_name, input_name.replace('.in', '.out')) 
                
if __name__ == '__main__':
    main()
