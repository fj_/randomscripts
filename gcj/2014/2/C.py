import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *
from copy import copy, deepcopy
from pprint import pprint
from glob import glob

taskname = 'C'
input = None

def readstr():
    return next(input).strip()

def readintlist():
    lst = map(int, readstr().split())
    return lst

def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]

def maxflow(graph, reverse_graph, capacity, source, sink):
    flow = dict((edge, 0) for edge, _ in capacity.iteritems())
    def dfs():
        visited = {source: None}
        stack = [source]
        while stack:
            node = stack.pop()
            if node == sink:
                result = []
                while True:
                    point = visited[node]
                    if point is None:
                        return result
                    result.append(point)
                    node = point[0]
                
            for next in graph.get(node, []):
                if next in visited:
                    continue
                edge = (node, next)
                if capacity[edge] > flow[edge]:
                    visited[next] = (node, edge, True)
                    stack.append(next)
            for next in reverse_graph.get(node, []):
                if next in visited:
                    continue
                edge = (next, node)
                if flow[edge] > 0:
                    visited[next] = (node, edge, False)
                    stack.append(next)
        return None
    while True:
        path = dfs()
        if path is None:
            return sum(flow[(source, node)] for node in graph[source])
        # assume capacities are 0, 1
        for node, edge, direct in path:
            if direct:
                assert flow[edge] == 0
                flow[edge] += 1
            else:
                assert flow[edge] == 1
                flow[edge] -= 1


    
def solvecase():
    #graph, reverse_graph, capacity, source, sink
    width, height, building_count = readintlist()
    buildings = [readintlist() for _ in xrange(building_count)]
    uid = iter(xrange(1, 100000000)).next
    grid = [[uid() for _ in xrange(width)] for _ in xrange(height)]
    for x0, y0, x1, y1 in buildings:
        for x in xrange(x0, x1 + 1):
            for y in xrange(y0, y1 + 1):
                grid[y][x] = None
#    print '\n'.join('|'.join('{:3}'.format(i if i is not None else '') for i in row) for row in grid)
    graph, reverse_graph, capacities = {}, {}, {}
    
    def iter_neighbours(x, y):
        for dx, dy in ((0, 1), (1, 0), (0, -1), (-1, 0)):
            nx, ny = x + dx, y + dy
            if 0 <= nx < width and 0 <= ny < height:
                yield nx, ny
    
    def add_edge(node1, node2):
        graph.setdefault(node1, []).append(node2)
        reverse_graph.setdefault(node2, []).append(node1)
        capacities[(node1, node2)] = 1 
                                    
    for y in xrange(0, height):
        for x in xrange(0, width):
            node = grid[y][x]
            if node is None: continue
            add_edge(node, -node)
            for nx, ny in iter_neighbours(x, y):
                node2 = grid[ny][nx]
                if node2 is None: continue
                add_edge(-node, node2)
                
    source, sink = uid(), uid()
    for x in xrange(0, width):
        node = grid[0][x]
        if node is not None:
            add_edge(source, node)
        node = grid[-1][x]
        if node is not None:
            add_edge(-node, sink)
            
    return maxflow(graph, reverse_graph, capacities, source, sink)

def solve(input_name, output_name):
    global input
    tstart = time.clock()
    input = open(input_name, 'r')
    output = open(output_name, 'w')
    casecount = readint()
    
    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s)) 
        print >>output, s
        print s 
        
    input.close()
    output.close()
    print '%s solved in %.3f' % (input_name, time.clock() - tstart)

def main():
    input_names = glob(taskname + '-*.in')
    assert len(input_names)
    input_names.sort(reverse = True)
    for input_name in input_names:
        solve(input_name, input_name.replace('.in', '.out')) 
                
if __name__ == '__main__':
    main()
