import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *
from copy import copy, deepcopy
from pprint import pprint
from glob import glob

taskname = 'A'
input = None

def readstr():
    return next(input).strip()

def readintlist():
    lst = map(int, readstr().split())
    return lst

def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]

def get_move_results(s):
    return frozenset(
            s[:i] + s[i + 1:]
            for i in xrange(len(s) - 1)
            if s[i] == s[i + 1])
    
def get_move_table(s):
    current = set([s])
    result = [current]
    while current:
        result.append(current)
        current = frozenset.union(*(get_move_results(s) for s in current))
    return result


def solvecase():
    cnt = readint()
    assert cnt == 2
    s1, s2 = readstr(), readstr()
    table1, table2 = get_move_table(s1), get_move_table(s2)
    print len(table1), len(table2)
    for total_moves in xrange(len(table1) + len(table2)):
        print '>>>', total_moves
        for c1 in xrange(min(len(table1), total_moves + 1)):
            c2 = total_moves - c1
            if c2 >= len(table2): continue
            print c1, c2
            if table1[c1] & table2[c2]:
                return total_moves 
    return 'Fegla Won'

def solve(input_name, output_name):
    global input
    tstart = time.clock()
    input = open(input_name, 'r')
    output = open(output_name, 'w')
    casecount = readint()
    
    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s)) 
        print >>output, s
        print s 
        
    input.close()
    output.close()
    print '%s solved in %.3f' % (input_name, time.clock() - tstart)

def main():
    input_names = glob(taskname + '-*.in')
    assert len(input_names)
    input_names.sort(reverse = True)
    for input_name in input_names:
        solve(input_name, input_name.replace('.in', '.out')) 
                
if __name__ == '__main__':
    main()
