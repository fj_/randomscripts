import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *
from copy import copy, deepcopy
from pprint import pprint
from glob import glob
from collections import defaultdict

taskname = 'B'
input = None

def readstr():
    return next(input).strip()

def readintlist():
    lst = map(int, readstr().split())
    return lst

def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]

def delete(edges, node, parent):
    return 1 + sum(delete(edges, e, node) for e in edges[node] if e != parent)

def fbify(edges, node, parent):
    children = [e for e in edges[node] if e != parent]
#    print parent, node, children
    if len(children) == 0:
        return 0
    if len(children) == 1:
        return delete(edges, children.pop(), node)
    return min(
            sum(delete(edges, c, node) for c in children[ : idx1]) +
            fbify(edges, children[idx1], node) +  
            sum(delete(edges, c, node) for c in children[idx1 + 1 : idx2]) +
            fbify(edges, children[idx2], node) + 
            sum(delete(edges, c, node) for c in children[idx2 + 1 : ])
            for idx1 in xrange(len(children) - 1)
            for idx2 in xrange(idx1 + 1, len(children)))

def solvecase():
    node_count = readint()
    edges = defaultdict(list)
    for _ in xrange(node_count - 1):
        a, b = readintlist()
        edges[a].append(b)
        edges[b].append(a)
    return min(fbify(edges, root, None) for root in xrange(1, node_count + 1))

def solve(input_name, output_name):
    global input
    tstart = time.clock()
    input = open(input_name, 'r')
    output = open(output_name, 'w')
    casecount = readint()
    
    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s)) 
        print >>output, s
        print s 
        
    input.close()
    output.close()
    print '%s solved in %.3f' % (input_name, time.clock() - tstart)

def main():
    input_names = glob(taskname + '-*.in')
    assert len(input_names)
    input_names.sort(reverse = True)
    for input_name in input_names:
        solve(input_name, input_name.replace('.in', '.out')) 
                
if __name__ == '__main__':
    main()
