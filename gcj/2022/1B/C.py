import builtins
import pdb
from random import randint
import random
import sys
import re
import time
from collections import Counter, defaultdict, namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'C'


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    possibilities = set(range(1, 256))
    for turns in range(3000):
        chance_of_0 = Counter()
        distinct_bits = {}
        bitdict = defaultdict(set)
        checked = set()
        for x in range(1, 255):
            if x in checked:
                continue
            lbitdict = set()
            for r in range(8):
                rx = (x >> r) | (x << (8 - r)) & 0xFF
                checked.add(rx)
                for p in possibilities:
                    xp = p ^ rx
                    if not xp:
                        chance_of_0[x] += 1
                    bits = bin(xp).count('1')
                    bitdict[bits].add(xp)
                    lbitdict.add(bits)
            distinct_bits[x] = len(lbitdict)
        if chance_of_0:
            bestbits = min(distinct_bits.values())
            candidates = [x for x, b in distinct_bits.items() if b == bestbits]
            print(f'{bestbits=}', flush=True, file=sys.stderr)
            guess = random.choice(candidates)
        else:
            guess = randint(1, 255)

        print(f'{guess:08b}', flush=True)
        print(f'guessing {guess:08b}', flush=True, file=sys.stderr)
        bits = readint()
        # print(f'bits {bits}', flush=True, file=sys.stderr)
        if bits == 0:
            print(f'Guessed in {turns}', flush=True, file=sys.stderr)
            return
        if bits < 0:
            sys.exit(1)
        possibilities = bitdict[bits]


def main():
    tstart = time.perf_counter()

    print('Hello', flush=True, file=sys.stderr)
    casecount = readint()
    print(f'{casecount}', flush=True, file=sys.stderr)

    for case in range(1, casecount + 1):
        solvecase()


if __name__ == '__main__':
    main()
