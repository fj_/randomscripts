import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'B'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    ncust, nprod = readintlist()
    custprod = [readintlist() for _ in range(ncust)]
    assert len(custprod) == ncust
    assert all(len(cust) == nprod for cust in custprod)
    def fromto(from1, to1, min1, max1):
        one = abs(from1 - min1) + (max1 - min1) + abs(to1 - max1)
        two = abs(from1 - max1) + (max1 - min1) + abs(to1 - min1)
        return min(one, two)
    best = {0: 0}
    for prods in custprod:
        new = {}
        min1, max1 = min(prods), max(prods)
        for p in prods:
            if p in new:
                continue
            new[p] = min(fromto(oldp, p, min1, max1) + cost for oldp, cost in best.items())
        best = new
    return min(best.values())


def main():
    tstart = time.perf_counter()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        # example file exists, redirect stdout to example output as well
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.perf_counter() - tstart))


if __name__ == '__main__':
    main()
