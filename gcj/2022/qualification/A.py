import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'A'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    r, c = readintlist()
    def row1(c):
        return '-'.join('+' * (c + 1))
    def row2(c):
        return '.'.join('|' * (c + 1))

    res = ['']
    res.append('..' + row1(c - 1))
    res.append('..' + row2(c - 1))
    for _ in range(r - 1):
        res.append(row1(c))
        res.append(row2(c))
    res.append(row1(c))
    return '\n'.join(res)


def main():
    tstart = time.perf_counter()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        # example file exists, redirect stdout to example output as well
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.perf_counter() - tstart))


if __name__ == '__main__':
    main()
