import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *
from copy import copy, deepcopy
from pprint import pprint
from glob import glob

taskname = 'C'
input = None

def readstr():
    return next(input).strip()

def readintlist():
    lst = map(int, readstr().split())
    return lst

def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]

Hiker = namedtuple('Hiker', 'position speed')

#def argmin(lst, predicate):
#    return min(enumerate(lst), 

#def solve_rec(hikers, encounters, following_hiker=None):
#    if encounters >= len(hikers):
#        # just run straight through 
#        return len(hikers)
#    if following_hiker is None:
#        following_hiker = 

def solve_dumb(hikers):
    if len(hikers) <= 1:
        return 0
    if len(hikers) != 2: return -1
    assert len(hikers) == 2
    hikers.sort(key=lambda (p, v): (p, -v)) # closest first, slowest first.
    
    h1, h2 = hikers
    time_when_following = (360 - h1.position) * h1.velocity
    if h2.velocity == h1.velocity:
        return 0
    rel_velocity = h1.velocity * h2.velocity / (h1.velocity - h2.velocity)
    if rel_velocity < 0:
        catch_up_in =  

def solvecase():
    n_groups = readint()
    hikers = []
    for _ in xrange(n_groups):
        position, n_hikers, speed = readintlist()
        for i in xrange(n_hikers):
            hikers.append(Hiker(position, speed + i))
    return solve_dumb(hikers)
    

def solve(input_name, output_name):
    global input
    tstart = time.clock()
    input = open(input_name, 'r')
    output = open(output_name, 'w')
    casecount = readint()
    
    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s)) 
        print >>output, s
        print s 
        
    input.close()
    output.close()
    print '%s solved in %.3f' % (input_name, time.clock() - tstart)

def main():
    input_names = glob(taskname + '-*.in')
    assert len(input_names)
    input_names.sort(reverse = True)
    for input_name in input_names:
        solve(input_name, input_name.replace('.in', '.out')) 
                
if __name__ == '__main__':
    main()
