import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *
from copy import copy, deepcopy
from pprint import pprint
from glob import glob
import math

taskname = 'C'
input = None

def readstr():
    return next(input).strip()

def readintlist():
    lst = map(int, readstr().split())
    return lst

def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]

def add_coin(current, denomination, count):
    values = [denomination * (i + 1) for i in xrange(count)]
    tmp = list(current)
    current.update(values)
    current.update(c + v for c in tmp for v in values)

def add_coin2(current, denomination, count):
    result = current
    for i in xrange(count):
        result |= current << (denomination * (i + 1))
    return result

def verify(lst, num):
    mask = 2
    zzz = []
    for i in xrange(1, 1000000000):
        if num & mask:
            zzz.append(i)
        mask <<= 1
        if mask > num:
            break
    assert sorted(lst) == sorted(zzz)

def iter_bits(n):
    i = 1
    mask = 2
    while True:
        if mask > n:
            break
        yield i, n & mask                 
        mask <<= 1
        i += 1

def lowest_bit_set(x):
    '''
    >>> for i in xrange(1, 10):
    ...     print i, lowest_bit_set(i) 
    1 0
    2 1
    3 0
    4 2
    5 0
    6 1
    7 0
    8 3
    9 0
    '''
    low = (x & -x)
    return int(math.log(low, 2))

def highest_bit_set(x):
    '''
    >>> for i in xrange(1, 10):
    ...     print i, highest_bit_set(i)
    1 0
    2 1
    3 1
    4 2
    5 2
    6 2
    7 2
    8 3
    9 3
    '''
    return int(math.log(x, 2))


def solvecase():
    c, d, v = readintlist()
    denominations = readintlist()
    assert len(denominations) == d
    current = 1
    for coin in denominations:
        print math.log(current, 2), 'adding', coin
        current = add_coin2(current, coin, c)
    coins_added = 0
    # phase 1: fill the holes
    while True:
        min_empty = None
        b = highest_bit_set(current)
        if b == 0:
            min_empty = 1
        else:
            mask = (2 << b) - 1
            x = current ^ b
            if x == 0:
                break
            
        
#        x = ~current
#        if x == 0:        
#        if min_empty > v:
#            return coins_added
#        print math.log(current, 2), 'adding', min_empty
#        coins_added += 1
#        current = add_coin2(current, min_empty, c)
    print 'phase2'
    # phase2, grow exponentially
    for max_set_bit, _ in iter_bits(current):
        pass
    while max_set_bit < v:
        max_set_bit = (max_set_bit + 1) * c + max_set_bit
        coins_added += 1
    return coins_added

def solve(input_name, output_name):
    global input
    tstart = time.clock()
    input = open(input_name, 'r')
    output = open(output_name, 'w')
    casecount = readint()
    
    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s)) 
        print >>output, s
        print s 
        
    input.close()
    output.close()
    print '%s solved in %.3f' % (input_name, time.clock() - tstart)

def main():
    input_names = glob(taskname + '-*.in')
    assert len(input_names)
    input_names.sort(reverse = True)
    for input_name in input_names:
        solve(input_name, input_name.replace('.in', '.out')) 
                
if __name__ == '__main__':
    main()
