import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint
from math import sin, cos, pi, atan2, sqrt

TASKNAME = 'D'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


cube = [[0, 0, 0],
        [1, 0, 0],
        [1, 0, 1],
        [0, 0, 1],
        [0, 1, 0],
        [1, 1, 0],
        [1, 1, 1],
        [0, 1, 1]]


def v3_translate(vector, points):
    return [[a + b for a, b in zip(vector, p)] for p in points]


def v3_matmul(matrix, points):
    return [[sum(a * b for a, b in zip(p, row)) for row in matrix] for p in points]


def v3_roty(a):
    return [[ cos(a), 0,  sin(a)],
            [      0, 1,       0],
            [-sin(a), 0,  cos(a)]]


def v3_rotz(a):
    return [[ cos(a),-sin(a), 0],
            [ sin(a), cos(a), 0],
            [      0,      0, 1]]


def tri_area(p1, p2, p3):
    return abs(p1[0] * (p2[1] - p3[1]) + p2[0] * (p3[1] - p1[1]) + p3[0] * (p1[1] - p2[1])) / 2


def bsearch(f, l, r, target):
    while True:
        m = (l + r) / 2
        if not (l < m < r):
            return m
        if f(m) < target:
            l = m
        else:
            r = m

###

def shadow_area(s):
    # convert to 2d
    s = [[x[0], x[2]] for x in s]
    return (
        tri_area(s[0], s[1], s[3]) +
        tri_area(s[1], s[5], s[3]) +
        tri_area(s[7], s[5], s[3]) +
        tri_area(s[5], s[6], s[7]))


def solvecase():
    max_angle = atan2(1, sqrt(2)/2)
    cube2 = cube
    cube2 = v3_translate([-0.5, -0.5, -0.5], cube2)
    cube2 = v3_matmul(v3_roty(pi / 4), cube2)
    def f(angle):
        return shadow_area(v3_matmul(v3_rotz(angle), cube2))

    target = float(readstr())
    a = bsearch(f, 0.0, max_angle, target)
    sides = [[0.5, 0.0, 0.0],
             [0.0, 0.5, 0.0],
             [0.0, 0.0, 0.5]]
    sides = v3_matmul(v3_rotz(a), v3_matmul(v3_roty(pi / 4), sides))
    return '\n' + '\n'.join(' '.join(str(c) for c in side) for side in sides)


def draw(points):
    from mpl_toolkits.mplot3d import Axes3D
    from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)
    ax.set_zlim(-1, 1)
    ax.scatter3D([p[0] for p in points],
                 [p[1] for p in points],
                 [p[2] for p in points])
    plt.show()


def main(interactive=False):
    # max_angle = atan2(1, sqrt(2)/2)
    # cube2 = cube
    # # cube2 = [cube[i] for i in [0, 1, 5, 6, 7, 3]]
    # cube2 = v3_translate([-0.5, -0.5, -0.5], cube2)
    # cube2 = v3_matmul(v3_roty(pi / 4), cube2)
    # cube2 = v3_matmul(v3_rotz(max_angle), cube2)

    # draw(cube2)
    # print(shadow_area(cube2))

    tstart = time.clock()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        if not interactive:
            print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.clock() - tstart))


if __name__ == '__main__':
    main()