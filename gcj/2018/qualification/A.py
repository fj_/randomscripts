import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'A'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr() -> str:
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    shield, program = readstr().split()
    shield = int(shield)
    current_power = 1
    damage = 0
    power = []
    for c in program:
        if c == 'C':
            current_power *= 2
        else:
            damage += current_power
        power.append(current_power)
    hacks = 0
    while damage > shield:
        hack_idx = program.rfind('CS')
        if hack_idx < 0:
            return 'IMPOSSIBLE'
        hacks += 1
        power[hack_idx] //= 2
        damage -= power[hack_idx]
        program = program[:hack_idx] + 'SC' + program[hack_idx + 2:]

    return hacks


def main(interactive=False):
    tstart = time.clock()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        if not interactive:
            print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.clock() - tstart))


if __name__ == '__main__':
    main()