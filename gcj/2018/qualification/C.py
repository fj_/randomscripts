import pdb
import sys
import re
import time
from collections import namedtuple, defaultdict
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'C'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    a = readint()
    if a == 10:
        rows, cols = 3, 4
    elif a == 20:
        rows, cols = 4, 5
    elif a == 200:
        rows, cols = 12, 17
    holes = [[0 for _ in range(cols)] for _ in range(rows)]
    neighbors = deepcopy(holes)
    sorted_neighbors = defaultdict(set)
    for i in range(1, rows - 1):
        for j in range(1, cols - 1):
            sorted_neighbors[0].add((i, j))

    def update_neighbor(i, j):
        n = neighbors[i][j]
        neighbors[i][j] += 1
        sorted_neighbors[n].remove((i, j))
        sorted_neighbors[n + 1].add((i, j))

    def update(i, j):
        if holes[i][j]:
            return
        holes[i][j] = 1
        for ni, nj in itertools.product(range(i - 1, i + 2), range(j - 1, j + 2)):
            if 1 <= ni < rows - 1 and 1 <= nj < cols - 1:
                update_neighbor(ni, nj)

    while True:
        for i in range(10):
            if sorted_neighbors[i]:
                i, j = next(iter(sorted_neighbors[i]))
                break
        else:
            assert False
        print_stdout(i + 10, j + 10)
        i, j = readintlist()
        if i == 0:
            return
        elif i < 0:
            sys.exit(1)
        update(i - 10, j - 10)

    return 0


def main():
    tstart = time.clock()

    interactive = True
    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')
        interactive = False

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        if not interactive:
            print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.clock() - tstart))


if __name__ == '__main__':
    main()