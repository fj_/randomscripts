import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'B'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def troublesort(lst):
    swapped = True
    while swapped:
        swapped = False
        for idx in range(len(lst) - 2):
            if lst[idx] > lst[idx + 2]:
                lst[idx], lst[idx + 2] = lst[idx + 2], lst[idx]
                swapped = True
    return lst


def troublesort2(lst):
    even = lst[0 : : 2]
    odd = lst[1 : : 2]
    even.sort()
    odd.sort()
    lst[0 : : 2] = even
    lst[1 : : 2] = odd

def check():
    import random
    for l in (0, 1, 2, 3, 4, 10, 11, 12):
        lst_base = list(range(l))
        for _ in range(10):
            random.shuffle(lst_base)
            lst1 = copy(lst_base)
            lst2 = copy(lst_base)
            troublesort(lst1)
            troublesort2(lst2)
            assert lst1 == lst2


def solvecase():
    cnt = readint()
    lst = readintlist()
    assert cnt == len(lst)
    troublesort2(lst)
    for i in range(len(lst) - 1):
        if lst[i] > lst[i + 1]:
            return i
    return 'OK'


def main(interactive=False):
    #check()

    tstart = time.clock()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        if not interactive:
            print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.clock() - tstart))


if __name__ == '__main__':
    main()