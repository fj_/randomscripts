import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'A'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def round_down(m, n):
    return m * 100 // n

def round_round(m, n):
    return (m * 200 // n + 1) // 2


def solvecase():
    N, language_cnt = readintlist()
    languages = readintlist()
    assert len(languages) == language_cnt

    remaining = N - sum(languages)

    for i in range(remaining + 1):
        to_onepercent = i
        at_onepercent = round_round(i, N)
        if at_onepercent > round_down(i, N):
            break

    need_correction = []
    total_percent = 0
    for l in languages:
        rd, rr = round_down(l, N), round_round(l, N)
        total_percent += rr
        # print(l, total_percent, rd, rr, need_correction)
        if rr > rd:
            # e.g. 17.5% - 18.0%, more efficient to add new 0.5% languages
            continue
        orig_rr = rr
        for correction in range(1, to_onepercent + 1):
            rd, rr = round_down(l + correction, N), round_round(l + correction, N)
            # print('   ', correction, rd, rr)
            if rr > rd:
                need_correction.append((correction, rr - orig_rr))

    need_correction.sort()

    for c, d in need_correction:
        if remaining < c:
            break
        remaining -= c
        total_percent += d

    onepercent = remaining // to_onepercent
    total_percent += onepercent * at_onepercent
    total_percent += round_round(remaining - onepercent * to_onepercent, N)

    return total_percent


def main(interactive=False):
    tstart = time.clock()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        if not interactive:
            print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.clock() - tstart))


if __name__ == '__main__':
    main()