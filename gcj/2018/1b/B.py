import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint
import functools

TASKNAME = 'B'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    S = readint()
    signs = []
    for _ in range(S):
        d, a, b = readintlist()
        signs.append((d + a, d - b))

    @functools.lru_cache(maxsize=None)
    def possibilities(n):
        r = {(None, None, 0)}
        if n < 0:
            return r
        a, b = signs[n]
        for pa, pb, pl in possibilities(n - 1):
            if pa == a or pb == b:
                r.add((pa, pb, pl + 1))
            if pa == None:
                r.add((a, pb, pl + 1))
            if pb == None:
                r.add((pa, b, pl + 1))
        return r

    max_stretch = 0
    max_count = 0
    for n in range(len(signs)):
        l = max(pl for pa, pb, pl in possibilities(n))
        if l == max_stretch:
            max_count += 1
        elif l > max_stretch:
            max_stretch = l
            max_count = 1
    return '{} {}'.format(max_stretch, max_count)








    return 0


def main(interactive=False):
    tstart = time.clock()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        if not interactive:
            print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.clock() - tstart))


if __name__ == '__main__':
    main()