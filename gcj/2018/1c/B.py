import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint
import random

TASKNAME = 'B'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    N = readint()
    if N < 0: sys.exit()

    lollies = {i: 0 for i in range(N)}
    for _ in range(N):
        prefs = readintlist()
        if prefs[0] < 0: sys.exit()

        assert prefs[0] == len(prefs) - 1
        del prefs[0]

        least, least_id = 10000, None
        for p in prefs:
            if p in lollies:
                c = lollies[p]
                if c == least:
                    least_id.append(p)
                elif c < least:
                    least, least_id = c, [p]
                lollies[p] = c + 1
        if least_id is not None:
            # Ia! Ia! Shub-Niggurath
            least_id = random.choice(least_id)
            print_stdout(least_id)
            del lollies[least_id]
        else:
            print_stdout(-1)
    return 0


def main(interactive=False):
    tstart = time.clock()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        interactive = True
    else:
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        if not interactive:
            print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.clock() - tstart))


if __name__ == '__main__':
    main()