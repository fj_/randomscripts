import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'C'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    n_ants = readint()
    ants = readintlist()
    assert len(ants) == n_ants

    n_w = [(0, 0)]

    new = []
    def add(n, w):
        # assume that n never decreases
        nw = (n, w)
        if not new:
            new.append(nw)

        nn, ww = new[-1]
        if w <= ww:
            # always replace same or better weight with same or more ants
            new[-1] = nw
        elif n > nn:
            new.append(nw)

    for ant in ants:
        for n, w in n_w:
            add(n, w)
            if w <= ant * 6:
                add(n + 1, w + ant)
        n_w, new = new, []
        # print(n_w)

    return n_w[-1][0]


def main(interactive=False):
    tstart = time.clock()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        if not interactive:
            print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.clock() - tstart))


if __name__ == '__main__':
    main()