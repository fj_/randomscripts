import pdb
import sys
import re
import time
from collections import namedtuple, defaultdict
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'A'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    N, L = readintlist()
    words = []
    for _ in range(N):
        w = readstr()
        assert len(w) == L
        words.append(w)

    lettersets = [{w[i] for w in words} for i in range(L)]

    def solve_rec(words, idx=0, acc=''):
        if idx >= L:
            return '-'

        d = defaultdict(list)
        for w in words:
            d[w[idx]].append(w)

        if len(d) < len(lettersets[idx]):
            acc += next(iter(lettersets[idx] - d.keys()))
            acc += words[0][idx + 1:]
            return acc

        wmin = min(d.values(), key=len)
        return solve_rec(wmin, idx + 1, acc + wmin[0][idx])

    return solve_rec(words)


def main(interactive=False):
    tstart = time.clock()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        if not interactive:
            print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.clock() - tstart))


if __name__ == '__main__':
    main()