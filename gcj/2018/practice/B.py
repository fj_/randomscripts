import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint
from glob import glob

TASKNAME = 'B'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    party_count = readint()
    parties = readintlist()
    assert len(parties) == party_count
    remaining = sum(parties)
    def get_party():
        nonlocal remaining
        _, idx = max((p, i) for i, p in enumerate(parties))
        s = chr(ord('A') + idx)
        parties[idx] -= 1
        remaining -= 1
        return s

    res = []
    while remaining:
        s = get_party()
        if remaining != 2:
            s += get_party()
        res.append(s)

    return ' '.join(res)


def main(interactive=False):
    tstart = time.clock()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        if not interactive:
            s = "Case #%d: %s" % (case, str(s))
            print_stdout(s)
            print(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.clock() - tstart))


if __name__ == '__main__':
    main()