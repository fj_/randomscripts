import sys
import os
import os.path as os_path
import shutil

def readfile(path):
	f = open(path, 'r')
	data = f.read()
	f.close()
	return data

def writefile(path, data):
	if os_path.exists(path):
		print('Skipping', path)
		return
	print('Writing', path)
	f = open(path, 'w')
	f.write(data)
	f.close()


if __name__ == '__main__':
	dirname = '1c'
	if not os_path.exists(dirname):
		print('Creating', dirname)
		os.mkdir(dirname)
	template = readfile('template.py')
	testcontents = '0\n'
	for task in ('A', 'B', 'C'):
	# for task in ('A', 'B', 'C', 'D'):
		code = template.replace('$taskname$', task)
		assert code != template
		writefile(dirname + '/' + task + '.py', code)
		writefile(dirname + '/' + task + '-example.in', testcontents)


