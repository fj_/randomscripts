import sys
import os
import shutil
from pathlib import Path

def readfile(path):
	f = open(path, 'r')
	data = f.read()
	f.close()
	return data

def writefile(path, data):
	if path.exists():
		print('Skipping', path)
		return
	print('Writing', path)
	path.write_text(data)


if __name__ == '__main__':
	basedir = Path(__file__).parent
	template = readfile(basedir / 'template.py')
	# dirname =  basedir / 'qualification'
	dirname =  basedir / '1B'
	if not dirname.exists():
		print('Creating', dirname)
		dirname.mkdir()
	testcontents = '0\n'
	for task in ('A', 'B', 'C'):
	# for task in ('A', 'B', 'C', 'D'):
		code = template.replace('$taskname$', task)
		assert code != template
		writefile(dirname / (task + '.py'), code)
		writefile(dirname / (task + '-example.in'), testcontents)


