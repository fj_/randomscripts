import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'B'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    return 0


def simulate(rings, day):
    return sum(r * 2 ** (day // (k + 1)) for k, r in enumerate(rings)) % (2 ** 63)


def main():
    T, W = readintlist()
    for case in range(T):
        print_stdout(6 * 35)
        r = readint()
        if r < 0: return
        r6 = (r >> 35) & 0x7F
        r5 = (r >> (35 + 7)) & 0x7F
        r4 = (r >> (35 + 7 + 7 + 3)) & 0x7F

        print_stdout(56)
        r = readint()
        if r < 0: return
        adj = simulate([0, 0, 0, r4, r5, r6], 56)
        r -= adj
        r3 = (r >> 18) & 0x7F
        r2 = (r >> 28) & 0x7F
        r1 = (r >> 56) & 0x7F
        print_stdout(r1, r2, r3, r4, r5, r6)
        r = readint()
        if r < 0: return

    # def solve(rings):
    #     r = simulate(rings, 6 * 35)
    #     r6 = (r >> 35) & 0x7F
    #     r5 = (r >> (35 + 7)) & 0x7F
    #     r4 = (r >> (35 + 7 + 7 + 3)) & 0x7F
    #     print(r4, r5, r6)
    #     rr = [0, 0, 0, r4, r5, r6]
    #     print('{:064b}'.format(simulate(rr, 56)))
    #     rr2 = rings[0:3] + rr[0:3]
    #     print('{:064b}'.format(simulate(rr2, 56)))
    #     r2 = simulate(rings, 56) - simulate(rr, 56)
    #     print('{:064b}'.format(r2))


    # solve([127, 127, 127, 126, 125, 124])
    # # for case in range(1, casecount + 1):


if __name__ == '__main__':
    main()