import pdb
import sys
import re
import time
from collections import namedtuple, defaultdict
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'A'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solve1d(Q, positive, negative):
    delta = [0] * (Q + 2)
    for p in positive:
        delta[p + 1] += 1
    for p in negative:
        delta[p] -= 1
    current = len(negative)
    max, maxarg = -1, -1
    for i, d in enumerate(delta):
        current += d
        if current > max:
            max = current
            maxarg = i
    return maxarg


def solvecase():
    P, Q = readintlist()
    people = defaultdict(list)
    for _ in range(P):
        x, y, d = readstr().split()
        x = int(x)
        y = int(y)
        if d in 'NS':
            people[d].append(y)
        else:
            people[d].append(x)
    x = solve1d(Q, people['E'], people['W'])
    y = solve1d(Q, people['N'], people['S'])
    return '{} {}'.format(x, y)
    return 0


def main():
    tstart = time.perf_counter()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        # example file exists, redirect stdout to example output as well
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.perf_counter() - tstart))


if __name__ == '__main__':
    main()