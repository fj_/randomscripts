import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'C'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    N, K = readintlist()
    c_skills = readintlist()
    d_skills = readintlist()
    assert len(c_skills) == len(d_skills) == N
    count = 0
    for L in range(N):
        c_max = d_max = 0
        for R in range(L, N):
            c_max = max(c_max, c_skills[R])
            d_max = max(d_max, d_skills[R])
            if abs(c_max - d_max) <= K:
                count += 1
    return count


def main():
    tstart = time.perf_counter()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        # example file exists, redirect stdout to example output as well
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.perf_counter() - tstart))


if __name__ == '__main__':
    main()