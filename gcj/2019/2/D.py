import pdb
import sys
import re
import time
from collections import namedtuple, defaultdict
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'D'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]

MOD = 1000000007

def solvecase():
    M = readint()
    products = []
    rev_products = defaultdict(list)
    for i in range(M):
        x1, x2 = readintlist()
        x1 -= 1
        x2 -= 1
        products.append((x1, x2))
        rev_products[x1].append(i)
        rev_products[x2].append(i)
    amounts = readintlist()
    assert M == len(amounts)
    # phase 1: exclude unreachable products to avoid counterfactual infinities
    reachable = set()
    front = list()
    for i, a in enumerate(amounts):
        if a:
            reachable.add(i)
            front.append(i)

    while front:
        i = front.pop()
        x1, x2 = products[i]
        if x1 not in reachable:
            reachable.add(x1)
            front.append(x1)
        if x2 not in reachable:
            reachable.add(x2)
            front.append(x2)
    # phase 1.5: process pathological cases
    if 0 not in reachable:
        return 0

    # can we pump infinitely starting from lead?
    # multiplier_from_lead = [0] * M
    # multiplier_from_lead[0] = 0
    # front = [0]
    # while front:
    #     print(front, multiplier_from_lead)
    #     x = front.pop()
    #     mx = max(multiplier_from_lead[x], 1)
    #     for y in products[x]:
    #         my = multiplier_from_lead[y]
    #         nmy = min(2, mx + my)
    #         if my == nmy:
    #             continue
    #         multiplier_from_lead[y] = nmy
    #         front.append(y)
    # if multiplier_from_lead[0] > 1:
    #     return 'UNBOUNDED'


    # else we can ignore loops involving lead directly
    # for lst in rev_products.values():
    #     if 0 in lst:
    #         lst.remove(0)

    # phase 2: toposort
    topo = []
    stack = [(0, rev_products[0])]
    marks = [0] * M
    marks[0] = 1
    while stack:
        x, rp = stack[-1]
        if not rp:
            topo.append(x)
            marks[x] = 2
            stack.pop()
            continue
        new = rp.pop()
        if new not in reachable:
            continue
        if marks[new] == 2:
            continue
        if marks[new] == 1:
            return 'UNBOUNDED'
        marks[new] = 1
        stack.append((new, rev_products[new]))
    topo.reverse()

    multipliers = [0] * M
    multipliers[0] = 1
    print(topo)
    for x in topo:
        m = multipliers[x]
        for src in rev_products[x]:
            if src not in reachable:
                continue
            multipliers[src] = (multipliers[src] + m) % MOD
    print(multipliers)

    total = 0
    for i, a in enumerate(amounts):
        total = (total + multipliers[i] * a) % MOD

    return total


def main():
    tstart = time.perf_counter()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        # example file exists, redirect stdout to example output as well
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.perf_counter() - tstart))


if __name__ == '__main__':
    main()