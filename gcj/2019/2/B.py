import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint
import random

TASKNAME = 'B'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    return 0


def main():
    tstart = time.perf_counter()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        # example file exists, redirect stdout to example output as well
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.perf_counter() - tstart))


if __name__ == '__main__':
    dups = 0
    tests = 5 # 250
    for t in range(tests):
        vases = [[0, i] for i in range(20)]
        for p in range(40):
            vases[random.randrange(0, 20)][0] += 1
        # s1 = sorted(vases)
        # w = s1[0][1]
        # # for i in range(1, 20):
        # #       vases[i] += i
        # vases.sort()
        # for p in range(40):
        #     vases[random.randrange(0, 20)][0] += 1
        # print(vases)
        if vases[0] == vases[1]:
            dups += 1
    print(dups / tests)



    # main()