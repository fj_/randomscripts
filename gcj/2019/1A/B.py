import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'B'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def solvecase():
    return 0


def solve_interactive():
    T, N, M = readintlist()
    probes = [5, 7, 9, 11, 13, 16, 17]
    for case in range(1, T + 1):
        remainders = []
        for p in probes:
            print_stdout(' '.join(str(p) for _ in range(18)))
            remainders.append(sum(readintlist()) % p)
        for g in range(1000001):
            expected = [g % p for p in probes]
            if expected == remainders:
                print_stdout(g)
                _ = readint()
                break
    return 0


def main(interactive=False):
    tstart = time.perf_counter()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        interactive = True
    else:
        # example file exists, redirect stdout to example output as well
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    if interactive:
        solve_interactive()
    else:
        casecount = readint()

        for case in range(1, casecount + 1):
            s = solvecase()
            s = "Case #%d: %s" % (case, str(s))
            print(s)
            if not interactive:
                print_stdout(s)

        print('%s solved in %.3f' % (sys.stdin.name, time.perf_counter() - tstart))


if __name__ == '__main__':
    main()