import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint
import random
import functools

TASKNAME = 'A'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


@functools.lru_cache(maxsize=None)
def gen_exclusion(R, C, r, c):
    s = set()
    for i in range(R):
        s.add((i, c))
    for i in range(C):
        s.add((r, i))
    m = max(R, C)
    for i in range(-m, m + 1):
        s.add((r + i, c + i))
        s.add((r + i, c - i))
    return s

def trysolve(R, C, field):
    moves = []
    exclusion = set()
    effective_field = field
    while effective_field:
        n = random.choice(list(effective_field))
        field.remove(n)
        moves.append(n)
        excl = gen_exclusion(R, C, n[0], n[1])
        effective_field = field - excl
    return None if field else moves


def solvecase():
    random.seed(295093425)
    R, C = readintlist()
    field = {(r, c) for r in range(R) for c in range(C)}
    for _ in range(1000):
        r = trysolve(R, C, copy(field))
        if r is not None:
            return 'POSSIBLE\n' + ''.join(str(rr[0] + 1) + ' ' + str(rr[1] + 1) + '\n' for rr in r)
    return 'IMPOSSIBLE'

    return 0


def main(interactive=False):
    tstart = time.perf_counter()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        # example file exists, redirect stdout to example output as well
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        if not interactive:
            print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.perf_counter() - tstart))


if __name__ == '__main__':
    main()