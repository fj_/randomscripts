import pdb
import sys
import re
import time
from collections import namedtuple
import itertools
from copy import copy, deepcopy
from pprint import pprint

TASKNAME = 'C'


original_print = print

def print_stdout(*args, **kwargs):
    return original_print(file=sys.stdout, flush=True, *args, **kwargs)

def print(*args, **kwargs):
    return original_print(file=sys.stderr, flush=True, *args, **kwargs)


def readstr():
    return input().strip()


def readintlist():
    lst = list(map(int, readstr().split()))
    return lst


def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]


def gcd(a, b):
	if a < b: a, b = b, a
	while b:
		a, b = b, a % b
	return a


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


def solvecase():
    N, L = readintlist()
    products = readintlist()
    assert len(products) == L
    def find_one_prime():
        for i, (p1, p2) in enumerate(pairwise(products)):
            if p1 != p2:
                return i + 1, gcd(p1, p2)

    idx, value = find_one_prime()

    primes = [None] * (L + 1)
    primes[idx] = value
    for i in reversed(range(0, idx)):
        primes[i] = products[i] // primes[i + 1]

    for i in range(idx, L):
        primes[i + 1] = products[i] // primes[i]

    d = {p : chr(ord('A') + i) for i, p in enumerate(sorted(set(primes)))}
    return ''.join(d[p] for p in primes)


def main(interactive=False):
    tstart = time.perf_counter()

    try:
        sys.stdin = open('{}-example.in'.format(TASKNAME), 'r')
    except:
        pass
    else:
        # example file exists, redirect stdout to example output as well
        sys.stdout = open('{}-example.out'.format(TASKNAME), 'w')

    casecount = readint()

    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s))
        print(s)
        if not interactive:
            print_stdout(s)

    print('%s solved in %.3f' % (sys.stdin.name, time.perf_counter() - tstart))


if __name__ == '__main__':
    main()