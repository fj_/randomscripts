import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *
from copy import copy, deepcopy
from pprint import pprint
from glob import glob
from heapq import heappop, heappush

taskname = 'C'
input = None

def readstr():
    return next(input).strip()

def readintlist():
    lst = map(int, readstr().split())
    return lst

def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]

def read_words():
    with open('garbled_email_dictionary.txt', 'r') as f:
        lst = [s.strip() for s in f]
    assert all(lst)
    # they are already sorted, too
    return lst

words = read_words() 

def solvecase():
    target = readstr()
    best = {}
    wavefront = []
    def add_item(key, score):
        old_score = best.get(key, None)
        if old_score is None or old_score > score:
            best[key] = score
            heappush(wavefront, key)
    add_item((0, -1000), 0)
    while len(wavefront):
        position, prev_change_pos = heappop(wavefront)
    return 0

def solve(input_name, output_name):
    global input
    tstart = time.clock()
    input = open(input_name, 'r')
    output = open(output_name, 'w')
    casecount = readint()
    
    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s)) 
        print >>output, s
        print s 
        
    input.close()
    output.close()
    print '%s solved in %.3f' % (input_name, time.clock() - tstart)

def main():
    input_names = glob(taskname + '-*.in')
    assert len(input_names)
    input_names.sort(reverse = True)
    for input_name in input_names:
        solve(input_name, input_name.replace('.in', '.out')) 
                
if __name__ == '__main__':
    main()
