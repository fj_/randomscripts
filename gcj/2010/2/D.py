import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *
from copy import copy, deepcopy
from math import sqrt, acos, pi, sin

for i in range(1,16):
	d = 1 - 0.1**i
	s1 = sin(2*acos(d))
	s2 = 2*d*sqrt(1-d*d)
	print i,abs(s1-s2)/s1
	
taskname = 'D'
input = None

def readstr():
	return next(input).strip()

def readintlist():
	lst = map(int, readstr().split())
	return lst

def readfloatlist():
	lst = map(float, readstr().split())
	return lst

def readint():
	lst = readintlist()
	assert len(lst) == 1
	return lst[0]

def segment_area(r, d):
	#return r * r * acos(d / r) - d * sqrt(max(0.0, r * r - d * d))
	#return r * r * acos(d / r) - d * sqrt(r * r - d * d)
	theta = 2 * acos(d / r)
	print theta, theta - sin(theta)
	#return r * r * 0.5 * (theta - 2 * d * sqrt(r * r - d * d) / (r * r))
	return r * r * 0.5 * (theta - sin(theta))


def circle_area(r):
	return pi * r * r 
	

def solvecase():
	def format(r):
		return '%0.8f' % r
	n, m = readintlist()
	goats = [readfloatlist() for _ in range(n)]
	poles = [readfloatlist() for _ in range(m)]
	assert n == 2
	g1x, g1y = goats[0]
	g2x, g2y = goats[1]
	d = sqrt((g1x - g2x) ** 2 + (g1y - g2y) ** 2)
	res = []
	for px, py in poles:
		r1 = sqrt((px - g1x) ** 2 + (py - g1y) ** 2)
		r2 = sqrt((px - g2x) ** 2 + (py - g2y) ** 2)
		ipos = (d*d - r2*r2 + r1*r1) / (2 * d)
		if 0 <= ipos <= d:
			h1 = ipos
			h2 = d - ipos
			a1 = segment_area(r1, h1)
			a2 = segment_area(r2, h2)
			print d, r1, r2, ipos, h1, h2, a1, a2
			r = format(a1 + a2)
			res.append(r)
		elif ipos < 0:
			ipos = -ipos
			h1 = ipos
			h2 = d + ipos
			a1 = segment_area(r1, h1)
			a2 = segment_area(r2, h2)
			ca = circle_area(r1)
			r = format(a2 + ca - a1)
			res.append(r)
		else:
			ipos = ipos - d
			h1 = d + ipos
			h2 = ipos
			a1 = segment_area(r1, h1)
			a2 = segment_area(r2, h2)
			ca = circle_area(r2)
#			print ipos, h1, h2, a1, a2, ca 
			r = format(a1 + ca - a2)
			res.append(r)
			
			
			
			
			
	return ' '.join(res)

def solve(suffix):
	global input
	tstart = time.clock()
	input = open(taskname + '-' + suffix + '.in', 'r')
	output = open(taskname + '-' + suffix + '.out', 'w')
	casecount = readint()
	
	for case in range(1, casecount + 1):
		s = solvecase()
		s = "Case #%d: %s" % (case, str(s)) 
		print >>output, s
		print s 
		
	input.close()
	output.close()
	print '%s solved in %.3f' % (suffix, time.clock() - tstart)
			
if __name__ == '__main__':
	solve('small')
	solve('large')
