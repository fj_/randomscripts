import pdb
import sys
import re
import time
import math
from collections import namedtuple
from itertools import *
from copy import copy, deepcopy

taskname = 'B'
input = None

def readstr():
    return next(input).strip()

def readintlist():
    lst = map(int, readstr().split())
    return lst

def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]

def solvecase():
    def calc_height(current, peak, diff):
        distance = (peak - current)
        return math.floor(heights[peak] - distance * diff) - 1
        
    mountain_cnt = readint()
    visibility = readintlist()
    assert len(visibility) == mountain_cnt - 1
    heights = [0 for _ in xrange(mountain_cnt)]
    lines = [(mountain_cnt - 1, 0)]
    for current in xrange(mountain_cnt - 2, -1, -1):
        peak = visibility[current]
        print peak
        while lines:
            test_peak, diff = lines.pop()
            if test_peak > peak: return 'Impossible'
            if test_peak == peak:
                lines.append((test_peak, diff))
                distance = (peak - current)
                height = math.floor(heights[peak] - distance * diff) - 1
                new_diff = (heights[peak] - height) / float(distance)
                heights[current] = height
                lines.append((current, new_diff))
        if not lines:
            return 'Impossible'
    m = min(heights)
    return ' '.join(h + m for h in heights)

def solve(suffix):
    global input
    tstart = time.clock()
    input = open(taskname + '-' + suffix + '.in', 'r')
    output = open(taskname + '-' + suffix + '.out', 'w')
    casecount = readint()
    
    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s)) 
        print >>output, s
        print s 
        
    input.close()
    output.close()
    print '%s solved in %.3f' % (suffix, time.clock() - tstart)
            
if __name__ == '__main__':
    solve('small')
    solve('large')
