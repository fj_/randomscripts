import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *


taskname = 'D'
input = None
output = None

def readstr():
    return next(input).strip()

def readintlist():
    lst = map(int, readstr().split())
    return lst

def readints():
    lst = readintlist()
    return lst[0] if len(lst) == 1 else lst


def solvecase(case):
    print "Case #%d" % case

    print >>output, "Case #%d:" % (case,)

    
    

def solve(suffix):
    global input, output, taskname
    tstart = time.clock()
    input = open(taskname + '-' + suffix + '.in', 'r')
    output = open(taskname + '-' + suffix + '.out', 'w')
    casecount = readints()
    for case in range(1, casecount + 1):
        solvecase(case)
    input.close()
    output.close()
    print '%s solved in %.3f' % (suffix, time.clock() - tstart)
            
if __name__ == '__main__':
    solve('small')
    solve('large')
