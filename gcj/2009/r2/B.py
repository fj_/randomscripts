import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *
from copy import copy, deepcopy
from pprint import pprint

taskname = 'B'
input = None
output = None

def readstr():
    return next(input).strip()

def readintlist():
    lst = map(int, readstr().split())
    return lst

def readints():
    lst = readintlist()
    return lst[0] if len(lst) == 1 else lst

debuglevel = 10
def dprint(*args, **kwargs):
    def extractarg(arg, default):
        if arg in kwargs:
            res = kwargs[arg]
            del kwargs[arg]
            return res
        return default
        
    sep = extractarg('sep', ' ')
    end = extractarg('end', '\n')
    lev = extractarg('lev', 9)
    assert len(kwargs) == 0, 'unrecognized arguments: ' + str(kwargs)
    if lev >= debuglevel:
        print sep.join(str(obj) for obj in args) + end,

def solvecase(case):
    print "Case #%d" % case
    height, width, maxfall = readints()
    field = [[c == '#' for c in readstr()] for i in range(height)]
    field.append([True for i in range(width)])
    field = map(tuple, field)
    assert all(len(row) == width for row in field)

    def fatal_fall(x, y):
        depth = 0
        while depth < maxfall: 
            y += 1
            depth += 1
            if field[y][x]: return False
        return True
    
    if fatal_fall(0, 0):
        print >>output, "Case #%d: No" % (case,)
        return

    def getsolutions(x, y, row, nextrow, digs):
        # get width of the current platform
        steps = 0
        for i in range(x + 1, width):
            if not nextrow[i] or row[i]:
                break
            steps += 1
        dprint('fall from the right', lev = 0)
        nx = x + steps + 1
        if nx < width and not row[nx]:
            yield (nx, nextrow, digs)
        dprint('dig and fall from the right', lev = 0)
        for i in range(steps):
            for j in range(i + 1):
                rr = list(nextrow)
                for k in range(j, i + 1):
                    nx = x + steps - k
                    rr[nx] = False
                nx = x + steps - i
                yield (nx, tuple(rr), digs + i - j + 1)
        dprint('fall from the left', lev = 0)
        nextrow = field[y + 1]
        nx = x - 1 
        if nx >= 0 and not row[nx]:
            yield (nx, nextrow, digs)
        dprint('dig and fall from the left', lev = 0)
        for i in range(steps):
            for j in range(i + 1):
                rr = list(nextrow)
                for k in range(j, i + 1):
                    nx = x + k
                    rr[nx] = False
                nx = x + i
                yield (nx, tuple(rr), digs + i - j + 1)

    def addsolution(x, row, digs, nextrow):
        # normalize
        if not nextrow[x]: 
            if fatal_fall(x, y + 1):
                dprint('fatal fall')
                return
        else:
            while x > 0:
                if not nextrow[x - 1] or row[x - 1]:
                    break
                x -= 1
        x_row = (x, row)
        if x_row not in newsolutions or digs < newsolutions[x_row]: 
            newsolutions[x_row] = digs
    
    solutions = {(0, field[0]): 0} 

    debug = 0
    for y in range(height - 1):
        print '.',
#        print "---", y
#        pprint(solutions)
        newsolutions = {}
        for (x, row), digs in solutions.iteritems():
            dprint('*', x, row, digs)
            nextrow = field[y + 1]
            if not nextrow[x]: # fall
                row = nextrow
                dprint('-', x, row, digs)
                addsolution(x, row, digs, field[y + 2])
            else:
                for x, row, digs in getsolutions(x, y, row, nextrow, digs):
                    dprint(' ', x, row, digs)
                    addsolution(x, row, digs, field[y + 2])
        solutions = newsolutions
        
    if not solutions:
        print >>output, "Case #%d: No" % (case,)
    else:
        best = min(solutions.itervalues())           
        print >>output, "Case #%d: Yes %d" % (case, best)

    
    

def solve(suffix):
    global input, output, taskname
    tstart = time.clock()
    input = open(taskname + '-' + suffix + '.in', 'r')
    output = open(taskname + '-' + suffix + '.out', 'w')
    casecount = readints()
    for case in range(1, casecount + 1):
        solvecase(case)
    input.close()
    output.close()
    print '%s solved in %.3f' % (suffix, time.clock() - tstart)
            
if __name__ == '__main__':
    solve('small')
    solve('large')
