import sys
import os
import os.path as os_path
import shutil

def readfile(path):
    f = open(path, 'r')
    data = f.read()
    f.close()
    return data

def writefile(path, data):
    print 'Writing', path
    f = open(path, 'w')
    f.write(data)
    f.close()
    

if __name__ == '__main__':
#    assert len(sys.argv) == 2
#    dir = sys.argv[1]
    dir = 'r2'
    if not os_path.exists(dir):
        print 'Creating', dir
        os.mkdir(dir)
    template = readfile('template.py')
    testcontents = '0\n'
    for task in ('B', 'C', 'D'):
        code = template.replace('$taskname$', task)
        assert code != template
        writefile(dir + '/' + task + '.py', code)
        writefile(dir + '/' + task + '-small.in', testcontents)
        writefile(dir + '/' + task + '-large.in', testcontents)
        
    
    