import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *


input = None
output = None

def readints():
    for s in islice(input, 1): 
        lst = map(int, s.split())
        return lst[0] if len(lst) == 1 else lst

def readstr():
    for s in islice(input, 1): 
        return s.strip()

def trymove4(row, col, width, height):
    for dr, dc in ((0, 1), (1, 0), (-1, 0), (0, -1)):
        r1 = row + dr
        c1 = col + dc
        if 0 <= r1 < height and 0 <= c1 < width:
            yield (r1, c1)   
    
def solvecase(case):
    print >>output, "Case #%d:" % case
    print "Case #%d:" % case
    
    w, q = readints()
    
    map = [readstr() for i in range(w)]
    queries = readints()
    assert len(queries) == q
    qset = set(queries)
    solutions = {}
    
    values = {}
    edges = {}
    valueset = set()
    freshmoves = {}
    fmnew = {}
    
    for row in range(w):
        for col in range(w):
            c = map[row][col]
            if c not in '+-':
                v = int(c)
                values[row, col] = v
                edges[row, col] = []
                if v in qset:
                    solutions[v] = c
                    qset.remove(v)
                
    for coords, value in values.items():
        row, col = coords 
        valueset.add((value, coords)) 
        freshmoves[value, coords] = str(value)
        for r1, c1 in trymove4(row, col, w, w):
            op = map[r1][c1]
            for r2, c2 in trymove4(r1, c1, w, w):
                nv = values[r2, c2]
                delta = nv if op == '+' else -nv
                strrepr = op + str(nv)
                edges[row, col].append(((r2, c2), delta, strrepr))

    def goto(value, coords, sol):
        nid = (nv, cn) 
        if nid not in valueset:
            if nid not in fmnew or fmnew[nid] > sol:
                fmnew[nid] = sol
                if nv in qset:
                    if nv not in solutions or solutions[nv] > sol:
                        solutions[nv] = sol
                        foundsolutions.add(nv)
                        #print 'Found solution for', nv, ';', len(qset), 'left in qset' 
            
    while qset:
        foundsolutions = set()
        for (value, coords), solution in freshmoves.items():
            for cn, delta, dstr in edges[coords]:
                nv = value + delta
                goto(value + delta, cn, solution + dstr) 
        valueset.update(fmnew)
        freshmoves = fmnew
        fmnew = {}
        qset -= foundsolutions
    for q in queries:
        s = solutions[q]
        #print s
        print >>output, s
    

def solve():
    casecount = readints()
    for case in range(1, casecount + 1):
        solvecase(case)
            
taskname = 'C'
if __name__ == '__main__':
    prevtime = time.clock()
    
    input = open(taskname + '-small.in', 'r')
    output = open(taskname + '-small.out', 'w')
    solve()
    currtime = time.clock()
    print 'Small solved in %.3f' % (currtime - prevtime)
    prevtime = currtime 
    input.close()
    output.close()
    

    input = open(taskname + '-large.in', 'r')
    output = open(taskname + '-large.out', 'w')
    solve()
    currtime = time.clock()
    print 'Large solved in %.3f' % (currtime - prevtime)
    prevtime = currtime 
    input.close()
    output.close()
      
