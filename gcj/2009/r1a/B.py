import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *


taskname = 'B'
input = None
output = None

def readstr():
    return next(input).strip()

def readints():
    lst = map(int, readstr().split())
    return lst[0] if len(lst) == 1 else lst

def grouper(n, iterable, fillvalue=None):
    "grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return izip_longest(fillvalue=fillvalue, *args)

def solvecase(case):
    print "Case #%d" % case
    fcnt = readints()
    ccnt = readints()
    customers = []
    for c in range(ccnt):
        prefs = readints()
        np = prefs[0]
        assert np * 2 + 1 == len(prefs)
        prefs = dict(grouper(2, prefs[1:]))
        customers.append((prefs, any(prefs.values())))
    print customers
    

    print >>output, "Case #%d:" % (case,)

    
    

def solve(suffix):
    global input, output, taskname
    tstart = time.clock()
    input = open(taskname + '-' + suffix + '.in', 'r')
    output = open(taskname + '-' + suffix + '.out', 'w')
    casecount = readints()
    for case in range(1, casecount + 1):
        solvecase(case)
    input.close()
    output.close()
    print '%s solved in %.3f' % (suffix, time.clock() - tstart)
            
if __name__ == '__main__':
    solve('small')
    solve('large')
