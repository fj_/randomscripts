import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *


taskname = 'A'
input = None
output = None

def readstr():
    s = next(input).strip()
#    print s
    return s

def readints():
    lst = map(int, readstr().split())
    return lst[0] if len(lst) == 1 else lst


def solvecase(case):
    print "Case #%d" % case
    l = readints()
    v1 = readints()
    v2 = readints()
    if l == 1:
        v1 = [v1]
        v2 = [v2]
    assert len(v1) == len(v2) == l
    v1.sort()
    v2.sort()
    v2 = reversed(v2)
    dp = sum(x * y for x, y in izip(v1, v2))
    print >>output, "Case #%d: %d" % (case, dp)

    
    

def solve(suffix):
    global input, output, taskname
    tstart = time.clock()
    input = open(taskname + '-' + suffix + '.in', 'r')
    output = open(taskname + '-' + suffix + '.out', 'w')
    casecount = readints()
    for case in range(1, casecount + 1):
        solvecase(case)
    input.close()
    output.close()
    print '%s solved in %.3f' % (suffix, time.clock() - tstart)
            
if __name__ == '__main__':
    solve('small')
    solve('large')
