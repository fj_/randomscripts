import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *
from copy import copy, deepcopy
#import psyco
#psyco.full()

taskname = 'B'
input = None

def readstr():
    return next(input).strip()

def readintlist():
    lst = map(int, readstr().split())
    return lst

def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]

def solvecase():
    rows, cols, base_mass = readintlist()
    weights = [[int(c) + base_mass for c in readstr()] for _ in xrange(rows)]
    assert all(len(row) == cols for row in weights)

    mass, pmassrow, pmasscol = [], [], []
    prev_mass_row = prev_pmassrow_row = prev_pmasscol_row = [0 for _ in range(cols)]
    for row in xrange(rows):
        mass_row = []
        pmassrow_row = []
        pmasscol_row = []
        acc_mass = 0
        acc_pmassrow = 0
        acc_pmasscol = 0
        for col, m in enumerate(weights[row]):
            acc_mass += m 
            mass_row.append(acc_mass + prev_mass_row[col])
            acc_pmassrow += row * m
            pmassrow_row.append(acc_pmassrow + prev_pmassrow_row[col])
            acc_pmasscol += col * m
            pmasscol_row.append(acc_pmasscol + prev_pmasscol_row[col])
        mass.append(mass_row)
        pmassrow.append(pmassrow_row)
        pmasscol.append(pmasscol_row)
        prev_mass_row = mass_row 
        prev_pmassrow_row = pmassrow_row
        prev_pmasscol_row = pmasscol_row 
    
    minsize = -1
    for row in xrange(2, rows):
        for col in xrange(2, cols):
            for d in xrange(min(row, col) + 1, max(2, minsize), -1):
                r1 = row - d
                c1 = col - d
                
                m = mass[row][col]
                pmr = pmassrow[row][col]
                pmc = pmasscol[row][col]
                if r1 >= 0:
                    m -= mass[r1][col]
                    pmr -= pmassrow[r1][col]
                    pmc -= pmasscol[r1][col]
                if c1 >= 0:
                    m -= mass[row][c1]
                    pmr -= pmassrow[row][c1]
                    pmc -= pmasscol[row][c1]
                if r1 >= 0 and c1 >= 0:
                    m += mass[r1][c1]
                    pmr += pmassrow[r1][c1]
                    pmc += pmasscol[r1][c1]
                    
                r1 += 1
                c1 += 1
                m -=   weights[row][col] 
                pmr -= weights[row][col] * row
                pmc -= weights[row][col] * col

                m -=   weights[r1 ][col] 
                pmr -= weights[r1 ][col] * r1
                pmc -= weights[r1 ][col] * col
                
                m -=   weights[r1 ][c1 ] 
                pmr -= weights[r1 ][c1 ] * r1
                pmc -= weights[r1 ][c1 ] * c1
                
                m -=   weights[row][c1 ] 
                pmr -= weights[row][c1 ] * row
                pmc -= weights[row][c1 ] * c1
                
                c_row2 = m * (row + r1)
                c_col2 = m * (col + c1)
                if c_row2 == pmr * 2 and c_col2 == pmc * 2:
                    minsize = d
                    break

    return minsize if minsize > 0 else 'IMPOSSIBLE'

def solve(suffix):
    global input
    tstart = time.clock()
    input = open(taskname + '-' + suffix + '.in', 'r')
    output = open(taskname + '-' + suffix + '.out', 'w')
    casecount = readint()
    
    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s)) 
        print >>output, s
        print s 
        
    input.close()
    output.close()
    print '%s solved in %.3f' % (suffix, time.clock() - tstart)
            
if __name__ == '__main__':
    solve('small')
    solve('large')
