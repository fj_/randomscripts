import pdb
import sys
import re
import time
from collections import namedtuple
from itertools import *
from copy import copy, deepcopy
#import psyco
#psyco.full()

taskname = 'B'
input = None

def readstr():
    return next(input).strip()

def readintlist():
    lst = map(int, readstr().split())
    return lst

def readint():
    lst = readintlist()
    assert len(lst) == 1
    return lst[0]

def solvecase():
    rows, cols, base_mass = readintlist()
    weights = [int(c) + base_mass for _ in xrange(rows) for c in readstr()]
    assert len(weights) == rows * cols

    mass, pmassrow, pmasscol = (
            [0 for _ in xrange((rows + 1) * cols)],
            [0 for _ in xrange((rows + 1) * cols)],
            [0 for _ in xrange((rows + 1) * cols)],)
    for row in xrange(rows):
        acc_mass = 0
        acc_pmassrow = 0
        acc_pmasscol = 0
        for col in xrange(cols):
            idx_old = row * cols + col
            idx_new = row * cols + cols + col
            m = weights[idx_old]
            acc_mass += m
            acc_pmassrow += row * m
            acc_pmasscol += col * m
            mass[idx_new] = acc_mass + mass[idx_old]
            pmassrow[idx_new] = acc_mass + pmassrow[idx_old]
            pmasscol[idx_new] = acc_mass + pmasscol[idx_old]
    del mass[:cols]
    del pmassrow[:cols]
    del pmasscol[:cols]
    minsize = -1
    for row in xrange(2, rows):
        for col in xrange(2, cols):
            for d in xrange(min(row, col) + 1, max(2, minsize), -1):
                r1 = row - d
                c1 = col - d
                
                idx_rc = row * cols + col
                idx_r1c = r1 * cols + col
                idx_rc1 = row * cols + c1
                idx_r1c1 = r1 * cols + c1
                
                m = mass[idx_rc]
                pmr = pmassrow[idx_rc]
                pmc = pmasscol[idx_rc]
                if r1 >= 0:
                    m -= mass[idx_r1c]
                    pmr -= pmassrow[idx_r1c]
                    pmc -= pmasscol[idx_r1c]
                if c1 >= 0:
                    m -= mass[idx_rc1]
                    pmr -= pmassrow[idx_rc1]
                    pmc -= pmasscol[idx_rc1]
                if r1 >= 0 and c1 >= 0:
                    m += mass[idx_r1c1]
                    pmr += pmassrow[idx_r1c1]
                    pmc += pmasscol[idx_r1c1]
                    
                r1 += 1
                c1 += 1
                m -=   weights[idx_rc] 
                pmr -= weights[idx_rc] * row
                pmc -= weights[idx_rc] * col

                m -=   weights[idx_r1c] 
                pmr -= weights[idx_r1c] * r1
                pmc -= weights[idx_r1c] * col
                
                m -=   weights[idx_r1c1] 
                pmr -= weights[idx_r1c1] * r1
                pmc -= weights[idx_r1c1] * c1
                
                m -=   weights[idx_rc1] 
                pmr -= weights[idx_rc1] * row
                pmc -= weights[idx_rc1] * c1
                
                c_row2 = m * (row + r1)
                c_col2 = m * (col + c1)
                if c_row2 == pmr * 2 and c_col2 == pmc * 2:
                    minsize = d
                    break

    return minsize if minsize > 0 else 'IMPOSSIBLE'

def solve(suffix):
    global input
    tstart = time.clock()
    input = open(taskname + '-' + suffix + '.in', 'r')
    output = open(taskname + '-' + suffix + '.out', 'w')
    casecount = readint()
    
    for case in range(1, casecount + 1):
        s = solvecase()
        s = "Case #%d: %s" % (case, str(s)) 
        print >>output, s
        print s 
        
    input.close()
    output.close()
    print '%s solved in %.3f' % (suffix, time.clock() - tstart)
            
if __name__ == '__main__':
    solve('small')
    solve('large')
