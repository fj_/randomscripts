from __future__ import with_statement
from math import *

def processFile(fin, fout):
	numCases = int(fin.readline())
	for caseNumber in xrange(1, numCases + 1):
		lineItems = fin.readline().strip().split()
		print lineItems
		a, b, c = map(int, lineItems)
	
		# do the job
		
		s = "Case #" + str(caseNumber) + ": "
		print s
		print >> fout, s


task = "A"

with open(task + "-small.in") as f:
	with open(task + "-small.out", "w") as fout:
		processFile(f, fout)
print "OK!"
with open(task + "-large.in") as f:
	with open(task + "-large.out", "w") as fout:
		processFile(f, fout)
