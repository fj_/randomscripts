from __future__ import with_statement
#from math import *

def processFile(fin, fout):
	numCases = int(fin.readline())
	for caseNumber in xrange(1, numCases + 1):
		lineItems = fin.readline().strip().split()
		print lineItems
		n, m, a = map(int, lineItems)
		
		if a > n * m:
			res = "IMPOSSIBLE"
		else:
			d, r = divmod(a, n)
			if r == 0:
				res = "0 0 %d 0 %d %d" % (n, n, d)
			else:
				# a = n*y - x
				y = d + 1
				x = n - r
				res = "0 0 %d 1 %d %d" % (n, x, y) 
	
		# do the job
		
		s = "Case #" + str(caseNumber) + ": " + res
		print s
		print >> fout, s


task = "B"

with open(task + "-small.in") as f:
	with open(task + "-small.out", "w") as fout:
		processFile(f, fout)
print "OK!"
with open(task + "-large.in") as f:
	with open(task + "-large.out", "w") as fout:
		processFile(f, fout)
