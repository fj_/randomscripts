from __future__ import with_statement
from math import *


def generatePermutations(l):
	if l == 1:
		return [[0]]
	else:
		res = []
		pp = generatePermutations(l - 1)
		for p in pp:
			for i in range(l):
				np = list(p)
				np.insert(i, l - 1)
				res.append(np)
		return res

def runP(s, p):
	news = []
	for i in xrange(len(s) // len(p)):
		block = s[ i * len(p), (i + 1) * len(p) 
		

def processFile(fin, fout):
	numCases = int(fin.readline())
	for caseNumber in xrange(1, numCases + 1):
		lineItems = fin.readline().strip().split()
		k, = map(int, lineItems)
		s = fin.readline().strip()
	
		perms = generatePermutations(k)
		print perms
		
		s = "Case #" + str(caseNumber) + ": "
		print s
		print >> fout, s


task = "D"

with open(task + "-small.in") as f:
	with open(task + "-small.out", "w") as fout:
		processFile(f, fout)
print "OK!"
with open(task + "-large.in") as f:
	with open(task + "-large.out", "w") as fout:
		processFile(f, fout)
