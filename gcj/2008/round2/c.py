from __future__ import with_statement

import psyco
psyco.full()

import sys
print sys.path
from contestutils import *
from math import *

maxfloat = 10.0 ** 100
minfloat = -maxfloat

def powerToIntersect((x1, y1, z1, p1), (x2, y2, z2, p2)):
	return (abs(x1 - x2) + abs(y1 - y2) + abs(z1 - z2)) / (p1 + p2)

def processFile(fin, fout):
	numCases = int(fin.readline())
	axes = ((1, 1, 1), (-1, 1, 1), (1, -1, 1), (1, 1, -1))  
	for caseNumber in xrange(1, numCases + 1):
		lineItems = fin.readline().strip().split()
		print lineItems
		n, = map(int, lineItems)
		
		ships = []
		for i in xrange(n):
			lineItems = fin.readline().strip().split()
			ship = map(float, lineItems)
			coords = ship[:3]
			p = ship[3]
			ships.append((tuple([dotProduct3(coords, axis) for axis in axes]), p, ship))
		
		# do the job
		def nonemptyIntersection(power):
			maxes = [maxfloat] * 4
			mins = [minfloat] * 4
			for ship in ships:
				d = ship[1] * power
				for i, c in enumerate(ship[0]):
					maxes[i] = min(maxes[i], c + d)   
					mins[i] = max(mins[i], c - d)
			
			for mx, mn in zip(maxes, mins):
				if mx < mn:
					return -1

			maxes[0] = min(maxes[0], sum(maxes[1:])) 
			mins[0] = max(mins[0], sum(mins[1:])) 
			
			for mx, mn in zip(maxes, mins):
				if mx < mn:
					return -1
			return 1
		
		v = bisectf(nonemptyIntersection, 0.0, 10.0**8, 10.0**-8)
		
#		v2 = 0.0
#		for i, s1 in enumerate(ships):
#			for s2 in ships[i:]:
#				v2 = max(v2, powerToIntersect(s1[2], s2[2]))
				
		s = "Case #" + str(caseNumber) + ": " + ("%1.6f" % v)
		print s
		print >> fout, s


task = "C"

with open(task + "-small.in") as f:
	with open(task + "-small.out", "w") as fout:
		processFile(f, fout)
print "\nOK!\n\n"

with open(task + "-large.in") as f:
	with open(task + "-large.out", "w") as fout:
		processFile(f, fout)
print "\nOK!\n\n"
