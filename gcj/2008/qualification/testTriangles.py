from __future__ import with_statement
import math

def singcosa(a, b, c):
	return a + b - c

def processFile(f, outf):
	l = int(f.readline())
	for i in range(l):
		x1, y1, x2, y2, x3, y3 = map(int, f.readline().split())
		l1 = (x2 - x1)**2 + (y2 - y1)**2 
		l2 = (x2 - x3)**2 + (y2 - y3)**2 
		l3 = (x3 - x1)**2 + (y3 - y1)**2
		
		l1sq = math.sqrt(l1)
		l2sq = math.sqrt(l1)
		l3sq = math.sqrt(l1)
		eps = 10e-6
		if l1 == 0 or l2 == 0 or l3 == 0:
			description = "not a triangle"
		elif l1sq + l2sq <= l3sq + eps or l1sq + l3sq <= l2sq + eps or l2sq + l3sq <= l1sq + eps:
			description = "not a triangle"
		else:
			if l1 == l2 or l1 == l3 or l2 == l3:
				description = "isosceles "
			else:
				description = "scalene "
			c = singcosa(l1, l2, l3)
			if c < 0:
				description += "obtuse triangle"
			elif c == 0:
				description += "right triangle"
			else:
				c = singcosa(l1, l3, l2)
				if c < 0:
					description += "obtuse triangle"
				elif c == 0:
					description += "right triangle"
				else:
					c = singcosa(l2, l3, l1)
					if c < 0:
						description += "obtuse triangle"
					elif c == 0:
						description += "right triangle"
					else:
						description += "acute triangle"
		print >> fout, "case #" + str(i + 1) + ": " + description

input = "A-large"
with open(input + ".in") as f:
	with open(input + ".out", "w") as fout:
		processFile(f, fout)
print "OK!"