from __future__ import with_statement
import psyco
psyco.full()

from math import *

import types
import operator
from bisect import *
import time

#######################################
# infinite list of primes
class Primes(object):
	__slots__ = ("primes", "bound")
	
	def __init__(self):
		self.primes = [2, 3, 5, 7]
		self.bound = 11
	
	def _AddPossiblePrime(self, value):
		for j in self.primes:
			if value % j == 0: return False
			if j * j > value: break
			 
		self.primes.append(value)
		return True
		
	def __contains__(self, value):
		if value % 2 == 0:
			return value == 2
		
		if value >= self.bound:
			i = self.bound
			wasPrime = False
			while i <= value:
				wasPrime = self._AddPossiblePrime(i)
				i += 2
			self.bound = i
			return wasPrime
		else:
			index = bisect(self.primes, value)
			return index < len(self.primes) and self.primes[index] == value 
	
	def _extendTo(self, index):
		i = self.bound
		for counter in xrange(len(self.primes), index + 1):
			while not self._AddPossiblePrime(i):
				i += 2
			i += 2 
		self.bound = i

	def extendTo(self, index):
		if index >= len(self.primes):
			self._extendTo(index)
	
	def extendToValue(self, value):
		if value >= self.bound:
			i = self.bound
			wasPrime = False
			while i <= value:
				wasPrime = self._AddPossiblePrime(i)
				i += 2
			self.bound = i
			return wasPrime
			
	def getPrime(self, index):
		if index >= len(self.primes): 
			self._extendTo(index * 2 - index // 2)
		return self.primes[index]
		
	def __getitem__(self, index):
		if isinstance(index, types.SliceType):
			if None == index.step:
				self.extendTo(index.stop)
				return self.primes[index.start : index.stop]
			else:			 
				return [self.getPrime(item) for item in xrange(index.start, index.stop, index.step)]
		else:
			return self.getPrime(index)
		
	def __iter__(self):
		i = 0
		while True:
			yield self.getPrime(i)
			i += 1
	
	def factorize(self, value):
		assert value >= 1
		factors = []
		if value == 1:
			return factors

		for prime in self:
			while True:
				d, m = divmod(value, prime)
				if m == 0:
					factors.append(prime)
					value = d
					if value == 1:
						return factors
				else:
					break
			if value < prime * prime:
				factors.append(value)
				return factors


class DisjointSet(object):
	__slots__ = ("nodes")
	def __init__(self):
		self.nodes = {}

	class Node(object):
		__slots__ = ("parent", "rank", "value")
		def __init__(self, value):
			self.value = value
			self.rank = 0
			self.parent = None
	
	def _findRoot(self, value):
		node = self.nodes[value]
		if node.parent is None:
			return node

		if node.parent.parent is None:
			return node.parent

		compress = [node]
		node = node.parent
		while node.parent is not None:
			compress.append(node)
			node = node.parent
		for n in compress:
			n.parent = node
		return node
	
	def unionRepresentatives(self, rootx, rooty):
		assert rootx.parent is None and rooty.parent is None
		if rootx == rooty:
			return rootx
		elif rootx.rank < rooty.rank:
			rootx.parent = rooty
			return rooty
		else:
			rooty.parent = rootx
			if rootx.rank == rooty.rank:
				rootx.rank += 1
			return rootx
			
	def getRepresentative(self, value):
		"""
		Search a set for the value, if found return the corresponding representative (a wrapper),
		otherwise wrap a value in a new Node, add it to the set and return it 
		"""
		if value in self.nodes:
			return self._findRoot(value)
		node = DisjointSet.Node(value)
		self.nodes[value] = node
		return node

	def iterRootValues(self):
		for value, node in self.nodes.iteritems():
			if node.parent is None:
				yield value

def factorize(primes, value, p):
	assert value >= 1
	factors = []
	if value == 1:
		return factors

	for prime in primes:
		firstTime = True
		while True:
			d, m = divmod(value, prime)
			if m == 0:
				if prime >= p and firstTime:
					factors.append(prime)
					firstTime = False
				value = d
				if value == 1:
					return factors
			else:
				break
		if value < prime * prime:
			if value >= p:
				factors.append(value)
			return factors

	
def processFile(fin, outf):
	numCases = int(fin.readline())
	primes = Primes()

	for caseNumber in xrange(1, numCases + 1):
		a, b, p = map(long, fin.readline().strip().split())
		print a, b, p
		
		numberSet = DisjointSet()
		distinctSets = 0
		for i in range(a, b + 1):
			numberSet.getRepresentative(i)

		diff = b - a + 1
		for prime in primes:
			if prime < p:
				continue
			if prime > diff:
				break
			start = (((a - 1) // prime) + 1) * prime
			startV = numberSet.getRepresentative(start)
			next = start + prime
			nextV = numberSet.getRepresentative(next)
			while next <= b:
				startV = numberSet.unionRepresentatives(startV, nextV)
				start = next
				next += prime
				nextV = numberSet.getRepresentative(next)

		for r in numberSet.iterRootValues():
			if r >= a and r <= b:
				distinctSets += 1
		
		
		s = "Case #" + str(caseNumber) + ": " + str(distinctSets)
		print s
		print >> outf, s


input = "B-large"
with open(input + ".in") as f:
	with open(input + ".out", "w") as fout:
		processFile(f, fout)
print "OK!"