from __future__ import with_statement
import psyco
psyco.full()

from math import *
from blist import *


	
def processFile(fin, outf):
	numCases = int(fin.readline())
	
	for caseNumber in xrange(1, numCases + 1):
		k, = map(int, fin.readline().strip().split())
		indices = map(int, fin.readline().strip().split())
		indices.pop(0)
		indicator = set(indices)
		
		result = {}
		deck = blist([i for i in range(k)])
		
		position = 0
		for card in range(k):
			position = (position + card) % len(deck)
			pos = deck.pop(position) + 1
			if pos in indicator:
				result[pos] = card + 1
			
		s = "Case #" + str(caseNumber) + ": "
		for i in indices:
			s += str(result[i]) + " " 
		print s
		print >> outf, s



input = "C-large"
with open(input + ".in") as f:
	with open(input + ".out", "w") as fout:
		processFile(f, fout)
print "OK!"