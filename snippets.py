class Margin_writer(object):
	def __init__(self, initial_value, line_end, line_start, margin = 80):
		self.buffer = [initial_value]
		self.line_len = len(initial_value)
		self.line_start_len = len(line_start)
		self.adjusted_margin = margin - len(line_end)
		self.line_separator = line_end + '\n' + line_start
	def append(self, value):
		self.line_len += len(value)
		if self.line_len > self.adjusted_margin:
			self.buffer.append(self.line_separator)
			self.line_len = self.line_start_len + len(value)
		self.buffer.append(value)
	def done(self):
		return ''.join(self.buffer)
	
class Separator_writer(object):
	def __init__(self, writer, separator):
		self.writer = writer
		self.separator = separator
		self.pending_value = None
	def append(self, value):
		if self.pending_value: self.writer.append(self.pending_value + self.separator)
		self.pending_value = value
	def done(self):
		if self.pending_value: self.writer.append(self.pending_value)
		return self.writer.done()
