def coroutine_constructor(f):
    def constructor():
        input = object() # gensym lol
        generator = f(input)
        primed = []
        # primed: in Py3k it would have been a boolean flag that is set to True 
        # after the generator has been run to its first input request
        
        # note that wrapper is an iterator that is created for _every session_.
        def wrapper(data):
            while not len(primed): # executes until the first input
                res = generator.send(None)
                if res is not input:
                    yield res
                else:
                    primed.append(None)
            for c in data:
                res = generator.send(c)            
                while res is not input:
                    yield res
                    res = generator.send(None)
        return wrapper
    return constructor

@coroutine_constructor
def Decoder(input):
    buffer = []
    ff_cnt = 0
    while True:
        while True:
            c = yield input
            buffer.append(c)
            if c == 0xFF:
                ff_cnt += 1
            else:
                ff_cnt = 0
            if ff_cnt >= 3: break
        yield buffer
        buffer = []
        ff_cnt = 0
        while True:
            c = yield input
            if c != 0xFF:
                buffer.append(c)
                break

def test():
    decoder = Decoder()
    data = [[1, 2, 3, 255, 255], [4, 5, 255, 255, 255, 255, 7, 8, 255, 255], [255, 6, 255, 255, 255, 255, 1]] 
    for seq in data:
        print 'sending:', seq
        for parsed in decoder(seq):
            print parsed

if __name__ == '__main__':
    test()
