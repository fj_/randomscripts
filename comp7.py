import psyco
psyco.full()

class ComparisonOptimiser(object):

	def __init__(self, edges, n):
		self.graph = dict([(i, []) for i in range(n)]) 
		self.invertedGraph = dict([(i, []) for i in range(n)])
		self.comparisons = set([(j, i) for i in range(n) for j in range(i)])
		for edge in edges:
			self.addEdge(edge)
		
	
	def lastElement(self, edge):
		front = [edge[1]]
		visited = set()
		last = None
		while len(front) > 0:
			last = front.pop(0)
			if last == edge[0]:
				return None # adding the edge would produce a cycle
			for v in self.graph[last]:
				if v not in visited:
					visited.add(v)
					front.append(v)
		return last
	
				
	def DFS(self, current, visited, requiredDepth):
		if requiredDepth == 0:
			return True
		for v in self.invertedGraph[current]:
			if v not in visited or visited[v] > requiredDepth:
				visited[v] = requiredDepth
				if self.DFS(v, visited, requiredDepth - 1):
					return True
				
	def hasOrder(self, last):
		return self.DFS(last, {}, len(self.graph) - 1)
	
	def addEdge(self, edge):
		self.graph[edge[0]].append(edge[1])
		self.invertedGraph[edge[1]].append(edge[0])
		if (edge[0] > edge[1]):
			edge = (edge[1], edge[0])
		self.comparisons.discard(edge)
	
	def removeEdge(self, edge):
		self.graph[edge[0]].remove(edge[1])
		self.invertedGraph[edge[1]].remove(edge[0])
		if (edge[0] > edge[1]):
			edge = (edge[1], edge[0])
		self.comparisons.add(edge)
	
	
	
	def buildSubTree(self, comparisonsLeft, cmp, last):
		self.addEdge(cmp)
		if self.hasOrder(last):
			g1 = True
		else:
			g1 = self.buildTree(comparisonsLeft - 1)
		self.removeEdge(cmp)
		return g1
		
	def buildTree(self, comparisonsLeft):
		if comparisonsLeft == 0:
			return None
		
		for cmp1 in self.comparisons:
			cmp2 = (cmp1[1], cmp1[0])
			last1 = self.lastElement(cmp1)
			if last1 is not None:
				last2 = self.lastElement(cmp2)
				if last2 is not None:
					g1 = self.buildSubTree(comparisonsLeft, cmp1, last1)
					if g1:
						g2 = self.buildSubTree(comparisonsLeft, cmp2, last2)
						if g2:
							return (cmp1, g1, g2) 

def pprintTree(tree, ident = 0):
	if tree is None:
		print "Fail"
		return
	print " " * ident,
	if tree is True:
		print "Done"
	else:
		print tree[0]
		pprintTree(tree[1], ident + 4)
		pprintTree(tree[2], ident + 4)
		
co = ComparisonOptimiser([(0, 1)], 5)
pprintTree(co.buildTree(6))