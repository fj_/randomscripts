#include <windows.h>
#include <stdio.h>

#include <math.h>
#include <stdio.h>

#define DATASIZE        1024
#define WORKER_COUNT    2
#define OPERATIONS      100000000

LONG data[DATASIZE];
int seeds[WORKER_COUNT];

// lifted from Python's stdlib
double clock()
{
    static LARGE_INTEGER ctrStart;
    static double divisor = 0.0;
    LARGE_INTEGER now;
    double diff;

    if (divisor == 0.0) {
        LARGE_INTEGER freq;
        QueryPerformanceCounter(&ctrStart);
        if (!QueryPerformanceFrequency(&freq) || freq.QuadPart == 0) {
            abort();
        }
        divisor = (double)freq.QuadPart;
    }
    QueryPerformanceCounter(&now);
    diff = (double)(now.QuadPart - ctrStart.QuadPart);
    return diff / divisor;
}

int test_function(const char * desc, int (*f)(int), int arg)
{
    double t = clock();
    int tmp = f(arg);
    t = clock() - t;
    printf("%s: %f\n", desc, t);
    return tmp;
}


int worker(int increment)
{
    int idx = 0;
    for (int i = 0; i < OPERATIONS; i++)
    {
        idx = (idx * 17 + increment) & (DATASIZE - 1);
        InterlockedIncrement(data + idx);
    }
    return 0;
}

DWORD WINAPI thread_worker(void * seed)
{
    worker(*(int*)seed);
    return 0;
}

int spawn_workers(int dummy)
{
    HANDLE handles[WORKER_COUNT];
    for (int i = 0; i < WORKER_COUNT; i++)
    {
        seeds[i] = i + 1;
        handles[i] = CreateThread(NULL, 0, thread_worker, seeds + i, 0, NULL);
    }
    WaitForMultipleObjects(WORKER_COUNT, handles, true, -1);
    return 0;
}

int main(void)
{
    test_function("test1", spawn_workers, 0);
    int cnt = 0;
    for (int i = 0; i < DATASIZE; i++)
    {
        cnt += data[i];
    }
    printf("cnt = %d\n", cnt);
}

