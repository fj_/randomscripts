#import gdata.youtube
from gdata.youtube.service import YouTubeService
from utils import cached_in_pickle
import dateutil.parser
from datetime import tzinfo, timedelta, datetime

# Python is retarded
ZERO = timedelta(0)
class UTC(tzinfo):
    def utcoffset(self, dt):
        return ZERO
    def tzname(self, dt):
        return "UTC"
    def dst(self, dt):
        return ZERO
utc = UTC()

service = YouTubeService()
service.ssl = True

@cached_in_pickle('tmp/youtube_playlist.pickle')
def fetch_data():
    return service.GetYouTubeUserFeed(username='ESportsTV')


def date_to_ago(date_str, now=datetime.now(utc)):
    d = dateutil.parser.parse(date_str)
    return (now - d).total_seconds() // 60


def main(): 
    lst = fetch_data()
    print dir(lst)
    print lst.GetNextLink()
    print service.GetNext(lst)
    for it in lst.entry:
        print date_to_ago(it.published.text), repr(it.title.text), it.media.duration.seconds
        print it.link[0].href
        print it.media.player.url
        print
        break


main()