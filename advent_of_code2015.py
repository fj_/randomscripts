#!/usr/bin/env python3.6

from advent_of_code_utils import *
utils_init(2015, globals())

##########

def problem1(data, second):
    data_ = ')())())'
    if not second:
        return data.count('(') - data.count(')')

    pos = 0
    for i, c in enumerate(data):
        if c == '(':
            pos += 1
        else:
            pos -= 1
            if pos == -1:
                return i + 1
    assert False


def problem2(data, second):
    data_ = split_data('4x23x21\n22x29x19')
    # if second: return

    def area(x, y, z):
        areas = [x * y, y * z, z * x]
        return 2 * sum(areas) + min(areas)

    def ribbon(x, y, z):
        pees = [x + y, y + z, z + x]
        return min(pees) * 2 + x * y * z

    rt = ReTokenizer([('{int}x{int}x{int}', ribbon if second else area)])

    parsed = rt.match_all(data)
    return sum(parsed)


def problem3(data, second):
    data_ = '^>v<'
    # if second: return

    pos = pos2 = (0, 0)
    visited = {pos}
    dirc = '^>v<'
    for c in data:
        pos = addv2(pos, directions4[dirc.index(c)])
        visited.add(pos)
        if second:
            pos, pos2 = pos2, pos
    return len(visited)


def problem4(data, second):
    data_ = 'abcdef'
    target = '00000'
    if second: target += '0'

    hash = hashlib.md5(data.encode('ascii'))
    for idx in range(0, 1000000000):
        h = hash.copy()
        h.update(str(idx).encode('ascii'))
        h = h.hexdigest()
        if h.startswith(target):
            return idx


def problem5(data, second):
    data_ = split_data('ugknbfddgicrmopn')
    # if second: return

    def is_nice(s):
        vowels = 0
        for c in s:
            if c in 'aeiou':
                vowels += 1
                if vowels >= 3:
                    break
        else:
            return False

        for i in range(len(s) - 1):
            if s[i] == s[i + 1]:
                break
        else:
            return False

        if re.search('ab|cd|pq|xy', s):
            return False
        return True


    def is_nice2(s):
        pairs = {}
        for i in range(len(s) - 1):
            p = s[i] + s[i + 1]
            if p in pairs:
                if pairs[p] != i - 1:
                    break
            else:
                pairs[p] = i
        else:
            return False

        for i in range(len(s) - 2):
            if s[i] == s[i + 2]:
                break
        else:
            return False

        return True

    if second:
        return sum(1 for s in data if is_nice2(s))

    return sum(1 for s in data if is_nice(s))


def problem6(data, second):
    data_ = split_data('''
        turn on 0,0 through 999,999
        toggle 0,0 through 999,0
        turn off 499,499 through 500,500''')
    # if second: return

    grid = np.zeros([1000, 1000], dtype='int32')

    def slice(f):
        @functools.wraps(f)
        def w(x1, y1, x2, y2):
            f(grid[x1:x2 + 1, y1:y2 + 1])
        return w

    if not second:
        @slice
        def turn_on(a):
            a[...] = 1

        @slice
        def turn_off(a):
            a[...] = 0

        @slice
        def toggle(a):
            a[...] ^= 1 #np.logical_xor(a, 1)

    else:
        @slice
        def turn_on(a):
            a[...] += 1

        @slice
        def turn_off(a):
            np.maximum(np.subtract(a, 1, a), 0, a)

        @slice
        def toggle(a):
            a[...] += 2

    rt = ReTokenizer([
        ('turn on {int},{int} through {int},{int}', turn_on),
        ('turn off {int},{int} through {int},{int}', turn_off),
        ('toggle {int},{int} through {int},{int}', toggle),])

    rt.match_all(data)

    return int(np.sum(grid))


def problem7(data, second):
    data_ = split_data('''
123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i''')
    # if second: return

    class Env(dict):
        def eval(self, name):
            try:
                return int(name) & 0xFFFF
            except ValueError:
                pass
            return self[name](self)
        def assign(self, thunk):
            self[thunk.dst] = thunk

    commands = {
        '': lambda e, t: e.eval(t.arg1),
        'AND': lambda e, t: e.eval(t.arg1) & e.eval(t.arg2),
        'OR': lambda e, t: e.eval(t.arg1) | e.eval(t.arg2),
        'LSHIFT': lambda e, t: (e.eval(t.arg1) << e.eval(t.arg2)) & 0xFFFF,
        'RSHIFT': lambda e, t: (e.eval(t.arg1) >> e.eval(t.arg2)) & 0xFFFF,
        'NOT': lambda e, t: (~e.eval(t.arg1)) & 0xFFFF,
    }

    class Thunk:
        def __init__(self, cmd, dst, arg1, arg2=None):
            self.cmd = cmd
            self.dst = dst
            self.arg1 = arg1
            self.arg2 = arg2
            self.value = None
            self.f = commands[cmd]
        def __call__(self, env):
            if self.value is None:
                self.value = self.f(env, self)
                assert self.value is not None
            return self.value

    rt = ReTokenizer([
        ('{int_or_id} -> {id}', lambda a, d: Thunk('', d, a)),
        ('NOT {int_or_id} -> {id}', lambda a, d: Thunk('NOT', d, a)),
        ('{int_or_id} {:(AND|OR|LSHIFT|RSHIFT)} {int_or_id} -> {id}', lambda a, c, b, d: Thunk(c, d, a, b)),
    ])

    env = Env()
    for line in data:
        env.assign(rt.fullmatch(line))

    # for k in sorted(env):
    #     print(k, env.eval(k))

    res = env.eval('a')
    if second:
        for th in env.values():
            th.value = None
        env.assign(Thunk('', 'b', res))
        res = env.eval('a')
    return res


def problem8(data, second):
    data_ = split_data(r'''
""
"abc"
"aaa\"aaa"
"\x27"
''')
    # if second: return

    code, real, encoded = 0, 0, 0
    for s in data:
        code += len(s)
        real += len(ast.literal_eval(s))
        encoded += 2 + len(s) + s.count('"') + s.count('\\')
    # print(f'{code}, {real}')
    if second:
        return encoded - code
    return code - real

def problem9(data, second):
    data_ = split_data('''
London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141''')
    # if second: return

    def parse(fro, to, d):
        return (fro, to), int(d)

    rt = ReTokenizer([('{id} to {id} = {int}', parse)])
    distances = rt.match_all(data)
    dd = {}
    cities = set()
    for ((k0, k1), d) in distances:
        dd[k0, k1] = dd[k1, k0] = d
        cities.add(k0); cities.add(k1)
    cities = list(cities)

    def lengths():
        for path in itertools.permutations(cities):
            d = sum(dd[path[i], path[i + 1]] for i in range(len(path) - 1))
            yield d

    if second:
        return max(lengths())
    return min(lengths())


def problem10(data, second):
    data_ = split_data('1')
    # if second: return

    def nxt(s):
        return ''.join(str(len(m.group(0))) + m.group(0)[0] for m in re.finditer(r'(.)\1*', s))

    for _ in range(50 if second else 40):
        data = nxt(data)

    return len(data)


def problem11(data, second):
    data_ = split_data('abcdefgh')
    # if second: return

    def from_str(s):
        lst = [ord(c) - ord('a') for c in s]
        lst.reverse()
        return lst

    def to_str(lst):
        return ''.join(chr(c + ord('a')) for c in reversed(lst))

    def nxt(lst):
        forbidden = {ord('i') - ord('a'), ord('l') - ord('a'), ord('o') - ord('a')}
        # sanitize preexisting conditions
        for i in range(len(lst) - 1, -1, -1):
            if lst[i] in forbidden:
                lst[i] += 1
                for j in range(i):
                    lst[j] = 0
                break

        while True:
            # print(to_str(lst))
            for i in range(len(lst)):
                lst[i] += 1
                if lst[i] >= 26:
                    lst[i] = 0
                    continue
                if lst[i] in forbidden:
                    lst[i] += 1
                break

            for i in range(len(lst) - 2):
                if lst[i] == lst[i + 1] + 1 == lst[i + 2] + 2:
                    break
            else:
                # print('no asc')
                continue

            prev_pair = -10
            for i in range(len(lst) - 1):
                if lst[i] == lst[i + 1] and prev_pair < i - 1:
                    if prev_pair >= 0:
                        break
                    prev_pair = i
            else:
                # print('no pairs')
                continue

            break

    lst = from_str(data)
    print(to_str(lst))
    nxt(lst)
    print(to_str(lst))
    if second:
        nxt(lst)
        print(to_str(lst))
    return to_str(lst)


def problem12(data, second):
    # if second: return
    # return sum(int(m.group(0)) for m in re.finditer(r'-?\d+', data))
    def count(o):
        if isinstance(o, int):
            return o
        if isinstance(o, dict):
            if second and 'red' in o.values():
                return 0
            return sum(count(it) for it in o.values())
        if isinstance(o, list):
            return sum(count(it) for it in o)
        if isinstance(o, str):
            return 0
        assert False, repr(o)

    js = json.loads(data)
    return count(js)


def problem13(data, second):
    data_ = split_data('''
Alice would gain 54 happiness units by sitting next to Bob.
Alice would lose 79 happiness units by sitting next to Carol.
Alice would lose 2 happiness units by sitting next to David.
Bob would gain 83 happiness units by sitting next to Alice.
Bob would lose 7 happiness units by sitting next to Carol.
Bob would lose 63 happiness units by sitting next to David.
Carol would lose 62 happiness units by sitting next to Alice.
Carol would gain 60 happiness units by sitting next to Bob.
Carol would gain 55 happiness units by sitting next to David.
David would gain 46 happiness units by sitting next to Alice.
David would lose 7 happiness units by sitting next to Bob.
David would gain 41 happiness units by sitting next to Carol.''')
    # if second: return

    def parse(who, what, how_much, neighbor):
        return who, neighbor, {'gain': 1, 'lose': -1}[what] * int(how_much)

    rt = ReTokenizer([('{id} would {:(gain|lose)} {int} happiness units by sitting next to {id}.', parse)])

    g = nx.DiGraph()
    g.add_weighted_edges_from(rt.match_all(data))

    # import matplotlib.pyplot as plt
    # nx.draw(g)
    # plt.show()
    nodes = sorted(g.nodes())
    def iter_happiness():
        for path in itertools.permutations(nodes):
            s = 0
            for i in range(len(nodes) - 1 if second else len(nodes)):
                a, b = path[i], path[(i + 1) % len(nodes)]
                s += g[a][b]['weight']
                s += g[b][a]['weight']
            yield s

    # print(list(iter_happiness()))
    return max(iter_happiness())


def problem14(data, second):
    data_ = split_data('''
Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.''')
    time_limit = 2503
    rt = ReTokenizer()
    Deer = rt.add_dataclass_old('{name:id} can fly {speed:int} km/s for {fly:int} seconds,'
        ' but then must rest for {rest:int} seconds.', 'Deer')
    deer = rt.match_all(data)
    def fly(d: Any, t: int):
        currt = 0
        pos = 0
        cycle = d.fly + d.rest
        full_cycles = t // cycle
        currt = full_cycles * cycle
        pos = full_cycles * d.fly * d.speed
        while currt < t:
            dt = min(t - currt, d.fly)
            pos += d.speed * dt
            currt += dt
            currt += d.rest
        return pos
    if not second:
        return max(fly(d, time_limit) for d in deer)

    scores = [0] * len(deer)
    for t in range(1, 2503 + 1):
        positions = [fly(d, t) for d in deer]
        m = max(positions)
        for i, p in enumerate(positions):
            if p == m:
                scores[i] += 1
    return max(scores)


def problem15(data, second):
    data_ = split_data('''
        Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
        Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3''')
    properties = 'capacity durability flavor texture calories'.split()
    rt = ReTokenizer([
        ('{id}: ' + ', '.join(f'{n} {{int}}' for n in properties), lambda *t: t[1:])])
    ingredients = rt.match_all(data)
    assert all(ing[-1] >= 0 for ing in ingredients)
    max_score = 0
    for counts in product_up_to(100, len(ingredients)):
        total = [0] * len(properties)
        for n, ing in zip(counts, ingredients):
            for i, prop in enumerate(ing):
                total[i] += prop * n

        if any(x <= 0 for x in total):
            continue

        if second and total[-1] != 500:
            continue

        score = functools.reduce(operator.mul, total[:-1], 1)
        max_score = max(max_score, score)
    return max_score


def problem16(data, second):
    data_ = split_data('Sue 11: goldfish: 1, perfumes: 4, cars: 6\nSue 12: samoyeds: 6, trees: 6, perfumes: 2')
    def parse(n, *args):
        return n, dict(grouper(args, 2))

    rt = ReTokenizer([
        ('Sue {int}: {id}: {int}, {id}: {int}, {id}: {int}', parse)])

    aunts = rt.match_all(data)

    target_str = '''children: 3
        cats: 7
        samoyeds: 2
        pomeranians: 3
        akitas: 0
        vizslas: 0
        goldfish: 5
        trees: 3
        cars: 2
        perfumes: 1'''

    op_dict = dict()
    if second:
        op_dict['cats'] = op_dict['trees'] = operator.lt
        op_dict['pomeranians'] = op_dict['goldfish'] = operator.gt

    target = {}
    for line in target_str.strip().split('\n'):
        k, v = (s.strip() for s in line.split(':'))
        target[k] = int(v)

    res = []
    for n, attrs in aunts:
        for k, v in attrs.items():
            op = op_dict.get(k, operator.eq)
            if not op(target[k], v):
                break
        else:
            res.append(n)
    [res] = res
    return res


def problem17(data, second):
    data_ = split_data('20\n15\n10\n5\n5')
    data = list(map(int, data))

    if second:
        @functools.lru_cache(maxsize=None)
        def rec(idx: int, remaining_volume: int, remaining_cups):
            if remaining_volume == 0 and remaining_cups == 0:
                return 1
            if remaining_volume <= 0 or remaining_cups <= 0 or idx >= len(data):
                return 0
            cup = data[idx]
            idx += 1
            return (rec(idx, remaining_volume, remaining_cups) +
                    rec(idx, remaining_volume - cup, remaining_cups - 1))
        for i in range(1, len(data)):
            res = rec(0, 150, i)
            if res > 0:
                return res
        assert False

    ways = defaultdict(lambda: 0, {0: 1})
    for it in data:
        nways = copy.copy(ways)
        for k, v in ways.items():
            if k + it > 150: continue
            nways[k + it] += v
        ways = nways
    return nways[150]


def problem18(data, second):
    data_ = split_data('.#.\n.#.\n.#.')
    world = np.array([[c == '#' for c in s] for s in data], dtype=np.int8)

    if second:
        def set_always_on():
            world[0, 0] = 1
            world[0, -1] = 1
            world[-1, -1] = 1
            world[-1, 0] = 1
    else:
        def set_always_on():
            ''

    set_always_on()
    nb = None
    for _ in range(100):
        nb = np_add_8neighbors(world, nb)
        world &= (nb == 2)  # alive if it was alive and has 2 neighbors
        world |= (nb == 3)  # alive if it has 3 neighbors
        set_always_on()
        # print(world)
    return int(np.sum(world.data))


def problem19(data, second):
    data_ = split_data('')
    mol = data.pop()
    trans = ReTokenizer([('{id} => {id}', None)]).match_all(data)
    res = set()

    if not second:
        for fr, to in trans:
            for m in re.finditer(fr, mol):
                res.add(mol[ : m.start()] + to + mol[m.end() : ])
        return len(res)

    rt = ReTokenizer([('{:([A-Z][a-z]?)}', lambda x: x)])
    # symbols = set()
    # nonterminals = set()
    # for fr, to in trans:
    #     nonterminals.add(fr)
    #     symbols.add(fr)
    #     for [to] in rt.parse(to):
    #         symbols.add(to)
    pmol = rt.parse(mol)
    # print(f"{len(pmol)} - {pmol.count('Rn')} - {pmol.count('Ar')} - {pmol.count('Y')} * 2 - 1'")
    return len(pmol) - pmol.count('Rn') - pmol.count('Ar') - pmol.count('Y') * 2 - 1


def problem20(data, second):
    data_ = split_data('')
    data = int(data)

    n = data // 40
    print(n)
    arr = np.zeros(n, np.int32)
    if not second:
        for i in range(1, n):
            arr[i::i] += i * 10
            if arr[i] >= data:
                return i
        print(n, max(arr))
    else:
        for i in range(1, n):
            arr[i : (i + 1) * 50 : i] += i * 11
            if arr[i] >= data:
                return i
        print(n, max(arr))


def problem21(data, second):
    data_ = split_data('')
    # if second: return

    def parse(s):
        return [tuple(map(int, line.split())) for line in split_data(s)]

    weapons = parse('''
  8     4       0
 10     5       0
 25     6       0
 40     7       0
 74     8       0''')

    armor = parse('''
  0     0       0
 13     0       1
 31     0       2
 53     0       3
 75     0       4
102     0       5''')

    rings = parse('''
 25     1       0
 50     2       0
100     3       0
 20     0       1
 40     0       2
 80     0       3''')

    boss_hp, boss_dmg, boss_armor = (int(s.split(':')[1]) for s in data)

    def add(x, y):
        return tuple(a + b for a, b in zip(x, y))

    rings_comb = itertools.chain([(0, 0, 0)], rings, itertools.starmap(add, itertools.combinations(rings, 2)))

    def fight_boss(hp, dmg, armor):
        real_boss_hp = boss_hp
        real_player_dmg = max(1, dmg - boss_armor)
        real_boss_dmg = max(1, boss_dmg - armor)
        while hp > 0:
            real_boss_hp -= real_player_dmg
            if real_boss_hp <= 0:
                return True
            hp -= real_boss_dmg
        return False

    mincost = 9999999
    maxcost = 0
    for things in itertools.product(weapons, armor, rings_comb):
        cost, dmg, arm = functools.reduce(add, things)

        if cost >= mincost and cost <= maxcost:
            continue
        result = fight_boss(100, dmg, arm)
        if result and cost < mincost:
            mincost = cost
            # print(cost, dmg, arm, result)
        if not result and cost > maxcost:
            maxcost = cost
            # print(cost, dmg, arm, result)

    return maxcost if second else mincost


def problem22(data, second):
    # data = split_data('')
    # if second: return
    boss_start_hp, boss_dmg = (int(s.split(':')[1]) for s in data)
    player_start_hp, player_start_mana = 50, 500

    class GameEnded(Exception):
        ''

    class Win(GameEnded):
        ''

    class Lose(GameEnded):
        ''

    @dataclass
    class State:
        player_hp: int = player_start_hp
        player_mana: int = player_start_mana
        mana_spent: int = 0
        boss_hp: int = boss_start_hp
        timer_s: int = 0
        timer_p: int = 0
        timer_r: int = 0
        turn_: int = 0

        def check_end(self):
            if self.player_hp <= 0:
                raise Lose()
            if self.boss_hp <= 0:
                raise Win()

        def pay_mana(self, cost):
            if self.player_mana < cost:
                raise Lose()
            self.player_mana -= cost
            self.mana_spent += cost

        def ply(self, log, action):
            if self.timer_s:
                self.timer_s -= 1
            if self.timer_p:
                self.timer_p -= 1
                self.boss_hp -= 3
            if self.timer_r:
                self.timer_r -= 1
                self.player_mana += 101
            self.check_end()
            self.turn_ += 1
            self._log(log, action)

        def _log(self, log, action):
            log.append(f'turn: {self.turn_:>2}, hp: {self.player_hp:>2}, mana: {self.player_mana:>3}, boss: {self.boss_hp:>2}'
                    f' stat: {self.timer_s}{self.timer_r}{self.timer_p} cmd: {action}')

        def turn(self, action: str, log):
            if second:
                self.player_hp -= 1
            self.ply(log, action)
            if action == 'm':
                self.pay_mana(53)
                self.boss_hp -= 4
            elif action == 'd':
                self.pay_mana(73)
                self.boss_hp -= 2
                self.player_hp += 2
            elif action == 's':
                self.pay_mana(113)
                if self.timer_s: raise Lose()
                self.timer_s = 6
            elif action == 'p':
                self.pay_mana(173)
                if self.timer_p: raise Lose()
                self.timer_p = 6
            elif action == 'r':
                self.pay_mana(229)
                if self.timer_r: raise Lose()
                self.timer_r = 5
            else:
                assert False
            self.check_end()
            self.ply(log, '')
            self.player_hp -= boss_dmg - (7 if self.timer_s else 0)
            self.check_end()

    def run(state, program):
        log = []
        program_iter = iter(program)
        try:
            for turn, c in enumerate(program):
                state.turn(c, log)
        except Win:
            log.append('Win')
            res = state.mana_spent
        except Lose:
            log.append('Lose')
            res = None
        return res, '\n'.join(log)

    def search(state, turn, log):
        for c in 'sprmd':
            newstate = copy.copy(state)
            ll = len(log)
            try:
                newstate.turn(c, log)
                if newstate.mana_spent > best:
                    continue
                yield from search(newstate, turn + 1, log)
            except Win:
                yield newstate.mana_spent, '\n'.join(log)
            except Lose:
                continue
            finally:
                while ll < len(log):
                    log.pop()

    best = 9999999

    # res, log = run(State(player_hp=10, player_mana=250, boss_hp=13), 'pm')
    # res, log = run(State(player_hp=10, player_mana=250, boss_hp=14), 'rsdpm')
    # print(log)
    # res, log = run(State(), 'srpmmpmmm')
    # print(res)
    # print(log)
    # return
    for score, log in search(State(), 1, []):
        if score < best:
            print(score)
            print(log)
            print()
            best = score
    return best


def problem23(data, second):
    data_ = split_data('''
    inc a
    jio a, +2
    tpl a
    inc a
    ''')
    # if second: return

    reg = [1, 0] if second else [0, 0]
    pc = 0

    def instruction(f):
        def wrapper(*args):
            @functools.wraps(f)
            def actual():
                f(*args)
            return actual
        return wrapper

    @instruction
    def hlf(r):
        reg[r != 'a'] //= 2

    @instruction
    def tpl(r):
        reg[r != 'a'] *= 3

    @instruction
    def inc(r):
        reg[r != 'a'] += 1

    @instruction
    def jmp(o):
        nonlocal pc
        pc += o - 1

    @instruction
    def jie(r, o):
        nonlocal pc
        if not (reg[r != 'a'] & 1):
            pc += o - 1

    @instruction
    def jio(r, o):
        nonlocal pc
        if reg[r != 'a'] == 1:
            pc += o - 1

    rt = ReTokenizer([
        ('hlf {:(a|b)}', hlf),
        ('tpl {:(a|b)}', tpl),
        ('inc {:(a|b)}', inc),
        ('jmp {int}', jmp),
        ('jie {:(a|b)}, {int}', jie),
        ('jio {:(a|b)}, {int}', jio),
    ])

    prog = rt.match_all(data)
    while 0 <= pc < len(prog):
        # print(f'{pc} {reg}')
        prog[pc]()
        pc += 1
    print(reg)
    return reg[1]


def problem24(data, second):
    # data = split_data('')
    # if second: return

    nums = [int(x) for x in data]
    nset = set(nums)
    assert len(nset) == len(nums)

    s = sum(nums)
    if second:
        target = s // 4
        assert target * 4 == s
    else:
        target = s // 3
        assert target * 3 == s
    print(target)

    def iter_combs(nset, mincount):
        nlst = list(nset)
        for cnt in range(mincount, 9):
            for ns in itertools.combinations(nlst, cnt):
                nx = target - sum(ns)
                if nx in nset and nx not in ns:
                    lst = sorted(list(ns) + [nx])
                    yield lst

    best, bestlen = None, None
    for lst in iter_combs(nset, 1):
        if bestlen and (len(lst) > bestlen or (not second and 1 not in lst)):
            break
        remaining = nset.difference(lst)
        for lst2 in iter_combs(remaining, len(lst)):
            print(lst, sum(lst), lst2, sum(lst2))
            success = False
            if second:
                remaining2 = remaining.difference(lst2)
                for lst3 in iter_combs(remaining2, len(lst)):
                    print(lst3)
                    success = True
                    break
            else:
                success = True
            if success:
                qe = functools.reduce(operator.mul, lst)
                if best is None or qe < best:
                    best = qe
                    bestlen = len(lst)
                break
    return best


def problem25(data, second):
    # data = split_data('')
    if second: return
    row, column = ReTokenizer('row {int}, column {int}').find(data)

    def idx_from(row, column):
        line = row + column - 2 # zero-based.
        before = (1 + line) * line // 2
        return before + column

    def code_from(idx):
        modulo = 33554393
        return (20151125 * pow(252533, idx - 1, modulo)) % modulo

    # for row in range(5):
    #     print([code_from(idx_from(row + 1, col + 1)) for col in range(5)])

    return code_from(idx_from(row, column))


####

def problem(data, second):
    data = split_data('')
    if second: return


###########

if __name__ == '__main__':
    print('Hello')
    # import cProfile
    # cProfile.run('problem13(get_data(13), True)', sort='cumtime')
    # t = time.clock()
    # problem13(get_data(13), True)
    # print(time.clock() - t)
    # solve_all() #{4, 10})
    solve_latest(18)
