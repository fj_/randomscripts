class Metaclass(type):
	def __new__(cls, name, bases, dct):
		print 'Meta.new', name
		return type.__new__(cls, name, bases, dct)
	def __init__(cls, name, bases, dct):
		print 'Meta.init', name
		super(Metaclass, cls).__init__(name, bases, dct)
	@property
	def method1(self):
		print 'getter, lol'
	@method1.setter
	def method1(self, value):
		print 'setter, lol', value
	
		
	
class X(object):
	__metaclass__ = Metaclass
	def foo(self): return 'foo'

X.method = lambda self: 'method'
X.method1 = lambda self: 'method1'
o = X()
print o.foo()
print o.method()
print o.method1() # aaaand fail!

