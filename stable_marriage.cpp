#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

using std::string;
using std::vector;
using std::pair;
using std::cout;
using std::endl;
using std::make_pair;
using std::sort;

template <typename T1, typename T2>
std::ostream & operator<<(std::ostream & out, const std::pair<T1, T2> & p)
{
    out << "(" << p.first << ", " << p.second << ")";
    return out;
}

template <typename TItem>
void print_vector(const vector<TItem> & v)
{
    for (int i = 0, len = v.size(); i < len; i++)
    {
        std::cout << v[i] << " ";
    }
    std::cout << std::endl;
}

// http://en.wikipedia.org/wiki/Stable_marriage_problem except symmetrical in this case.
// basically, it finds a more or less nice matching that can't be locally improved.
void stable_marriages(
        const int m_count,
        const int w_count, 
        const vector< vector<int> > & scores,
        vector<int> & choices) // men's choices
{
    choices.resize(0);
    choices.resize(m_count, -1);

    // men's preferences, sorted by score, p.first = score, p.second = women's index
    vector< vector< pair<int, int> > > preferences(m_count);
    vector<int> free_men(m_count);
    
    for (int m = 0; m < m_count; m++)
    {
        free_men[m] = m;
        preferences[m].reserve(w_count);
        for (int w = 0; w < w_count; w++)
        {
            preferences[m].push_back(make_pair(scores[m][w], w));
        }
        sort(preferences[m].begin(), preferences[m].end());
    }

    vector<int> w_choices(w_count, -1);

    while (free_men.size())
    {
        int m = free_men.back();
        free_men.pop_back();
        vector< pair<int, int> > & pref = preferences[m];
        while (pref.size())
        {
            int score = pref.back().first;
            int w = pref.back().second;
            pref.pop_back();
            int m2 = w_choices[w];
            if (m2 < 0)
            {
                // marry!
                choices[m] = w;
                w_choices[w] = m;
                break;
            }
            else if (score > scores[m2][w])
            {
                // divorce and marry
                choices[m2] = -1;
                free_men.push_back(m2);
                choices[m] = w;
                w_choices[w] = m;
                break;
            }
        }
    }
}



int main(void)
{
    const int m_count = 3;
    const int w_count = 2;
    vector< vector<int> > scores(m_count, vector<int>(w_count));
    scores[0][0] = 30; scores[0][1] = 20;
    scores[1][0] = 15; scores[1][1] = 40;
    scores[2][0] = 20; scores[2][1] = 35;
    vector<int> choices;

    stable_marriages(m_count, w_count, scores, choices);
    print_vector(choices);
    return 0;
}
