"""YES I'M PARSING XML WITH REGEXES!"""

import re

class Matcher(object):
    def init_from_delimiters(self, start, end, startrx = None, endrx = None):
        self.start = start
        self.startrx = startrx 
        if not startrx: self.startrx = re.escape(start)

        self.end = end
        self.endrx = endrx 
        if not endrx: self.endrx = re.escape(end)

        self.rx = re.compile(r'(%s)|(%s)' % (self.startrx, self.endrx), re.I | re.S)

    def from_delimiters(start, end, startrx = None, endrx = None):
        m = Matcher()
        m.init_from_delimiters(start, end, startrx, endrx)
        return m
    from_delimiters = staticmethod(from_delimiters)

    def init_from_xml_tag(self, tag):
        self.init_from_delimiters(*map(lambda s: s.replace('$', tag), [
            '<$>', '</$>', '<\s*$\s*>', '<\s*/\s*$\s*>']))
    def from_xml_tag(tag):
        m = Matcher()
        m.init_from_xml_tag(tag)
        return m
    from_xml_tag = staticmethod(from_xml_tag)

    def find_idx(self, s, start_pos = 0):
        """returns a tuple (startpos, endpos, nextpos) -- coords of the inner match
        and where to start next"""
        depth = 0
        open_pos = None
        while True:
            m = self.rx.search(s, start_pos)
            if m is None:
                if depth: raise Exception('unmatched start tag %s at %d' % (self.start, open_pos))
                return None
            elif m.group(1): # start tag
                if open_pos is None:
                    open_pos = m.end()
                depth += 1
            elif depth == 0: # here and below - an end tag
                raise Exception('unmatched end tag %s at %d' % (self.end, m.start()))
            elif depth == 1:
                return open_pos, m.start(), m.end()
            else:
                depth -= 1
            start_pos = m.end()

    def find(self, s):
        r = self.find_idx(s)
        if r is None: return None
        return s[r[0]:r[1]]

    def replacer(self, s):
        raise NotImplementedError()

    def replace_all(self, s, replacer = None):
        """if replacer is None, self.replacer would be used (raises NotImplementedError 
        unless overloaded.
        replacer can return None to indicate that this match should be left intact"""
        if replacer is None: replacer = self.replacer
        result = []
        startpos = 0
        while True:
            r = self.find_idx(s, startpos)
            if r is None: 
                result.append(s[startpos:])
                return "".join(result)
            start, end, next = r
            rplc = replacer(s[start:end])
            if rplc is None:
                result.append(s[startpos:next])
            else:
                result.append(s[startpos:start])
                result.append(rplc)
                result.append(s[end:next])
            startpos = next

#    def folder(self, acc, s):
#        raise NotImplementedError()
#
#
#    def lfold(self, s, folder = None, acc = None):
#        """acc == None means no initial value, f is applied first time to the two
#        first matches, not to the (acc, first-match)"""
#        if folder is None: folder = self.folder
#        startpos = 0
#        while True:
#            r = self.find_idx(s, startpos)
#            if r is None: return acc
#            start, end, next = r
#            data = s[start:end]
#            if acc is None: acc = data
#            else: acc = f(acc, data)
