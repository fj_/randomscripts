import random

def run_until_fixed(pop):
    for n in range(0, 10**9):
        pop = random.sample(pop + pop, len(pop))
        if len(set(pop)) == 1:
            return n


pop = list(range(1000))
for i in range(10):
    print(run_until_fixed(pop))
