#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
# Copyright 2011 Haxus the Great.
#
# Cannibalized from the Google AppEngine "shell" sample.
# http://code.google.com/p/google-app-engine-samples/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging
import os
import sys
import traceback
import wsgiref.handlers
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template

from base64 import encodestring as base64_encode
from base64 import decodestring as base64_decode
from minesweeper import World
from minesweeper_model import WorldModel
from string import maketrans

# Set to True if stack traces should be shown in the browser, etc.
_DEBUG = True

identity_trans_table = maketrans('', '')

class UserInfo(db.Model):
    date_created = db.DateTimeProperty(required=True, auto_now_add=True)
    date_modified = db.DateTimeProperty(required=True, auto_now=True)
    newgame_cnt = db.IntegerProperty(default=0)
    command_cnt = db.IntegerProperty(default=0)
    sum_turns = db.IntegerProperty(default=0)
    items_examined = db.IntegerProperty(default=0)
    wins = db.IntegerProperty(default=0)
    losses = db.IntegerProperty(default=0)
    quits = db.IntegerProperty(default=0)

def update_user_stats(request, world, updater):
    ip = request.remote_addr or ''
    user_agent = request.headers.get('User-Agent', '')
    accept_language = request.headers.get('Accept-Language', '')
    key_name = '%s|%s|%s' % (ip, user_agent, accept_language)
    logging.info('user_key: %r' % (key_name))
    def txn():
        entity = UserInfo.get_by_key_name(key_name)
        if entity is None:
            entity = UserInfo(key_name=key_name)
        entity.sum_turns += world.command_counter
        if updater:
            updater(entity)
        entity.put()
    db.run_in_transaction(txn)


class FrontPageHandler(webapp.RequestHandler):
    def get(self):
        self.response.headers['Cache-control'] = 'no-cache'
        self.response.headers['Pragma'] = 'no-cache'
        world = World()
        messages = world.begin_game()
        def updater(stats):
            stats.newgame_cnt += 1
        update_user_stats(self.request, world, updater)

        model = WorldModel()
        world.save(model)
        state = model.serialize()

        template_file = os.path.join(os.path.dirname(__file__), 'templates',
                                                                  'minesweeper.html')
        vars = { 'server_software': os.environ['SERVER_SOFTWARE'],
                          'python_version': sys.version,
                          'rand_seed': world.rand_seed,
                          'messages': messages,
                          'state': base64_encode(state),
                          }
        rendered = webapp.template.render(template_file, vars, debug=_DEBUG)
        self.response.out.write(rendered)



class CommandHandler(webapp.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        # extract the statement to run
        command = self.request.get('command').strip()
        self.response.out.write('> ' + command + '\n')

        state64 = self.request.get('state').strip()
        logging.info('Command: %r, state: %s' % (command, state64))
        write = self.response.out.write
        try: # we want to append state after we are done.
            model = WorldModel.deserialize(base64_decode(state64))
            world = World(model=model)
            logging.info('Seed: %r' % world.rand_seed)
            if world.is_alive():
                resp = world.do_command(command)
                events = world.collect_events()
                def updater(stats):
                    stats.command_cnt += 1
                    stats.sum_turns += world.command_counter
                    for ev in events:
                        if ev == World.Event.Win:
                            stats.wins += 1
                        elif ev == World.Event.Loss:
                            stats.losses += 1
                        elif ev == World.Event.Quit:
                            stats.quits += 1
                        elif ev == World.Event.Examine:
                            stats.items_examined += 1

                update_user_stats(self.request, world, updater)

                model = WorldModel()
                world.save(model)
                state64 = base64_encode(model.serialize())
                # encoded string might contain newlines!
                state64 = state64.translate(identity_trans_table, ' \n\r\t')
                write(resp)
            else:
                write('The game is over, refresh the window to start new game.')
        except Exception:
            write('Oops, internal error!\n')
            s = traceback.format_exc()
            if _DEBUG:
                write(s)
            logging.error(s)
        finally:
            write('\nSTATE:' + state64)

app = webapp.WSGIApplication([
    ('/', FrontPageHandler),
    ('/command.do', CommandHandler)
    ], debug=_DEBUG)


def real_main():
    wsgiref.handlers.CGIHandler().run(app)


def profile_main():
    import cProfile, pstats, StringIO
    prof = cProfile.Profile()
    prof = prof.runctx('real_main()', globals(), locals())
    stream = StringIO.StringIO()
    stats = pstats.Stats(prof, stream=stream)
    stats.sort_stats('cumulative')  # time/cumulative
    stats.print_stats(40)
    # The rest is optional.
    # stats.print_callees()
    # stats.print_callers()
    logging.info('Profile data:\n%s', stream.getvalue())

def main():
    real_main()
    
if __name__ == '__main__':
    main()
