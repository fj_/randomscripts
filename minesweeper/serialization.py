from cStringIO import StringIO

class SerializationError(Exception):
    pass

class EmptyClass(object):
    pass

def read_byte(stream):
    c = stream.read(1)
    if c == '':
        raise SerializationError('End of stream encountered while decoding')
    return ord(c)

def pack_int(value, stream):
    while value >= 64 or value < -64:
        stream.write(chr((value & 0x7F) | 0x80))
        value >>= 7;
    if value < 0:
        stream.write(chr(128 + value))
    else:
        stream.write(chr(value))

def unpack_int(stream):
    acc = 0
    shift = 0
    while True:
        x = read_byte(stream)
        if x & 0x80: # has more
            acc |= ((x & 0x7F) << shift)
            shift += 7
        else:
            negative = x & 0x40
            if negative:
                x -= 128
            acc |= (x << shift)
            break
    return acc

def test_int_serialization():
    for i in xrange(-65538, 65538):
        io = StringIO()
        pack_int(i, io)
        io.seek(0)
        unpacked = unpack_int(io)
        print unpacked
        assert i == unpacked
    

_counter = 0 # used to preserve ordering of fields

class Serialized_field(object):
    def __init__(self):
        global _counter
        self.counter = _counter
        _counter += 1
        # field_name is required for accessor protocol and is set in the decorator
    def __get__(self, obj, cls):
        if obj is None: return self
        return obj.__dict__[self.field_name]  
    def __set__(self, obj, value):
        obj.__dict__[self.field_name] = value
    def __delete__(self, obj):
        del obj.__dict__[self.field_name]
    def pack(self, value, stream):
        raise NotImplementedError()
    def unpack(self, stream):
        raise NotImplementedError()

class Int_field(Serialized_field):
    def pack(self, value, stream):
        return pack_int(value, stream)
    def unpack(self, stream):
        return unpack_int(stream)

class Tuple_field(Serialized_field):
    def __init__(self, *item_types):
        Serialized_field.__init__(self)
        self.item_types = item_types
        
    def pack(self, value, stream):
        assert len(value) == len(self.item_types)
        for it, val in zip(self.item_types, value):
            it.pack(val, stream)
            
    def unpack(self, stream):
        return tuple(it.unpack(stream) for it in self.item_types)

class List_field(Serialized_field):
    def __init__(self, item_type):
        Serialized_field.__init__(self)
        self.item_type = item_type
        
    def pack(self, value, stream):
        packer = self.item_type.pack
        pack_int(len(value), stream)
        for it in value:
            packer(it, stream)
            
    def unpack(self, stream):
        unpacker = self.item_type.unpack
        cnt = unpack_int(stream)
        return [unpacker(stream) for _ in xrange(cnt)]

class Dict_field(Serialized_field):
    def __init__(self, key_type, value_type):
        Serialized_field.__init__(self)
        self.key_type = key_type
        self.value_type = value_type
        
    def pack(self, value, stream):
        key_packer = self.key_type.pack
        value_packer = self.value_type.pack
        pack_int(len(value), stream)
        for k, v in value.iteritems():
            key_packer(k, stream)
            value_packer(v, stream)
            
    def unpack(self, stream):
        key_unpacker = self.key_type.unpack
        value_unpacker = self.value_type.unpack
        cnt = unpack_int(stream)
        return dict((key_unpacker(stream), value_unpacker(stream)) for _ in xrange(cnt))

class Bitlist_field(Serialized_field):
    def pack(self, value, stream):
        pack_int(len(value), stream)
        x = 0
        shift = 0
        for it in value:
            if it:
                x |= 1 << shift
            shift += 1
            if shift > 7:
                stream.write(chr(x))
                shift = 0
                x = 0
        if shift:
            stream.write(chr(x))
            
    def unpack(self, stream):
        length = unpack_int(stream)
        res = []
        bytes, reminder = divmod(length, 8)
        if reminder: bytes += 1        
        for _ in range(bytes):
            x = read_byte(stream)
            for shift in range(8):
                res.append(True if x & (1 << shift) else False)
        if reminder:
            del res[reminder - 8:]
        return res

def check_stream_empty(stream, length):
    remaining = length - stream.tell()
    if remaining:
        raise SerializationError('%d bytes remaining in the stream.' % remaining) 

class With_serialized_fields(type):
    '''
    I don't understand metaclasses.

    This thing kinda works.
    It is two-step, this results in `With_serialized_fields.__new__` called for
    derived classes, but _not_ for Serializable itself.
    '''
    def __new__(self, name, bases, fields):
        serialized_fields = []
        for k, v in fields.iteritems():
            if not isinstance(v, Serialized_field): continue
            v.field_name = k
            serialized_fields.append(v)
        serialized_fields.sort(key = lambda x: x.counter)
        assert len(serialized_fields), 'No serializable fields found in class, you are probably doing something wrong!'
        fields['serialized_fields'] = serialized_fields
        return type.__new__(self, name, bases, fields)
    
def add_serializable_metaclass(name, bases, fields):
    return type.__new__(With_serialized_fields, name, bases, fields)

class Serializable(object):
    __metaclass__ = add_serializable_metaclass
    serialized_fields = None 
    def serialize(self, stream = None):
        to_string = stream is None
        if to_string:
            stream = StringIO()
        for field in self.serialized_fields:
            field.pack(field.__get__(self, None), stream)
        if to_string:
            return stream.getvalue()
    def deserialize_here(self, stream):
        from_string = isinstance(stream, basestring) 
        if from_string:
            s = stream
            stream = StringIO(stream)
        for field in self.serialized_fields:
            field.__set__(self, field.unpack(stream))
        if from_string:
            check_stream_empty(stream, len(s))
    @classmethod
    def deserialize(cls, stream, call_init = False, *args, **kwargs):
        if call_init:
            obj = cls(*args, **kwargs)
        else:
            obj = EmptyClass()
            obj.__class__ = cls 
        obj.deserialize_here(stream)
        return obj

if __name__ == '__main__':
    import random
    randlist = [random.randrange(2) for _ in range(37)]
    class Model(Serializable):
        some_int = Int_field()
        some_list = List_field(Int_field())
        some_list_of_tuples = List_field(Tuple_field(Int_field(), Int_field())) 
        some_dict = Dict_field(Int_field(), List_field(Int_field()))
        some_bitlist = Bitlist_field()
        def __init__(self):
            self.ephemeral_int = 20
            self.some_int = 10
            self.some_list = [11, 22, 33]
            self.some_list_of_tuples = [(11, 22), (33, 44)]
            self.some_dict = { 123 : [1, 2, 3], 0 : [] }
            self.some_bitlist = randlist
    model = Model()
    model.some_int *= 111111111111111111
    s = model.serialize()
    m2 = Model.deserialize(s)
    print repr(s)
    print m2.__dict__
    assert m2.some_bitlist == randlist
