import random
from bisect import bisect_left

__all__ = ['weighted_choice', 'abs_weighted_choice', 
        'weighted_distribution', 'abs_weighted_distribution']

def cumulative_weighted_distribution_impl(items, cumulative_weights):
    '''The base function for doing this stuff, requires preprocessed input.  
    make sure that the last cumulative weight is >= 1.0''' 
    def sample(rnd = None):
        x = (rnd or random).random()
        i = bisect_left(cumulative_weights, x)
        return items[i]
    return sample

def abs_weighted_distribution(*args):
    '''Accepts plain items and two-tuples of the form
    (weight, item), where weight is the absolute probability.
    Plain items must be present and are assigned the remaining probability 
    uniformly.
    
    Use tuple (None, my_tuple) if you want to pass a tuple as an undecorated
    item.
    
    See also weighted_distribution'''

    cumulative_weights = []
    weighted_args = []
    rest_args = []
    s = 0.0
    # extract weighted args converting weights to cumulative weights
    for arg in args:
        if isinstance(arg, tuple):
            p, v = arg
            if p is None:
                rest_args.append(arg)
            else:
                s += p
                cumulative_weights.append(s)
                weighted_args.append(v)
        else:
            rest_args.append(arg)
    assert s <= 1.0
    assert len(rest_args) 
    
    # add unweighted args using the rest of the probability
    while rest_args:
        p = (1.0 - s) / len(rest_args)
        assert p > 0.0
        s += p
        cumulative_weights.append(s)
        weighted_args.append(rest_args.pop())
    cumulative_weights[-1] = 2.0 # to guarantee that bisect never returns out-of-range index  
    return cumulative_weighted_distribution_impl(weighted_args, cumulative_weights)

def weighted_distribution(*args):
    '''Accepts plain items and two-tuples of the form
    (weight, item), where weight is used to adjust probability for that item.
    Plain items are considered to have weight = 1.0.
    
    See also abs_weighted_distribution
    '''
    weights = []
    weighted_args = []
    unscaled_s = 0.0
    # prepare non-cumulative weights and total weight 
    for arg in args:
        if isinstance(arg, tuple):
            p, v = arg
            if p is None:
                p = 1.0
        else:
            p, v = 1.0, arg
        unscaled_s += p
        weights.append(p)
        weighted_args.append(v)
    # convert weights to scaled cumulative weights
    s = 0.0
    q = 1.0 / unscaled_s
    cumulative_weights = []
    for weight in weights:
        s += weight * q
        cumulative_weights.append(s)
    cumulative_weights[-1] = 2.0 # to guarantee that bisect never returns out-of-range index  
    return cumulative_weighted_distribution_impl(weighted_args, cumulative_weights)

def test_abs_weighted_distribution():
    rnd = random.Random()
    d = abs_weighted_distribution((0.3, 'a'), (0.01, 'b'), 'c', 'd')
    from collections import Counter
    counter = Counter(d(rnd) for _ in xrange(100000))
    print counter

def test_weighted_distribution():
    rnd = random.Random()
    d = weighted_distribution((2.0, 'a'), 'b', 'c', (0.5, 'd'))
    from collections import Counter
    counter = Counter(d(rnd) for _ in xrange(100000))
    print counter
#test_abs_weighted_distribution()
#test_weighted_distribution()
#sys.exit()

# convenience functions
def weighted_choice(rnd, *args):
    return weighted_distribution(*args)(rnd)

def abs_weighted_choice(rnd, *args):
    return abs_weighted_distribution(*args)(rnd)

