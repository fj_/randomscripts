// Copyright 2007 Google Inc.
// Copyright 2011 Haxus the Great.
//
// Cannibalized from the Google AppEngine "shell" sample.
// http://code.google.com/p/google-app-engine-samples/
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var shell = {}

// XmlHttpRequest done state
shell.DONE_STATE = 4;

strip = function(s) {
    return s.replace(/^\s+|\s+$/g, '')
}

shell.history = [''];
shell.historyCursor = 0;

// for terminal-like output
shell.buffered_output = '';
shell.buffered_position = 0;

shell.doBufferedOutput = function() {
  var s = shell.buffered_output;
  var pos = shell.buffered_position
  if (pos < s.length) {
    end = Math.min(pos + 50, s.length);
    
    var output = document.getElementById('output');
    output.value += shell.buffered_output.substring(pos, end);
    output.scrollTop = output.scrollHeight;
    
    if (end < s.length) {
      shell.buffered_position = end;
      setTimeout(shell.doBufferedOutput, 20);
    }
    else {
      shell.buffered_output = '';
      shell.buffered_position = 0;
    }
  }
}

shell.getXmlHttpRequest = function() {
  if (window.XMLHttpRequest) {
    return new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    try {
      return new ActiveXObject('Msxml2.XMLHTTP');
    } catch(e) {
      return new ActiveXObject('Microsoft.XMLHTTP');
    }
  }
  return null;
};

shell.onPromptKeyPress = function(event) {
  var command = document.getElementById('command');
  
  if (this.historyCursor == this.history.length - 1) {
    // we're on the current command. update it in the history before doing
    // anything.
    this.history[this.historyCursor] = command.value;
  }
  
  // should we pull something from the history?
  if (event.keyCode == 38 /* up arrow */) {
    if (this.historyCursor > 0) {
      command.value = this.history[--this.historyCursor];
    }
    return false;
  } else if (event.keyCode == 40 /* down arrow */) {
    if (this.historyCursor < this.history.length - 1) {
      command.value = this.history[++this.historyCursor];
    }
    return false;
  } else if (!event.altKey) {
    // probably changing the command. update it in the history.
    this.historyCursor = this.history.length - 1;
    this.history[this.historyCursor] = command.value;
  }
  
  if (event.keyCode == 13 /* enter */ && !event.altKey && !event.shiftKey) {
    var s = strip(command.value);
    if (s != '') {
      // add current command to history
      this.history.push('');
      this.historyCursor = this.history.length - 1;
      this.runStatement();
    } else {
      command.value = '';
    }
    return false;
  }
};

shell.done = function(req) {
  if (req.readyState == this.DONE_STATE) {
    var command = document.getElementById('command');
    command.className = 'prompt';
    command.value = '';

    var result = strip(req.responseText);
    if (result == '') return;

    // extract the state (last line of the response)
    newline_pos = result.lastIndexOf('\n');
    if (newline_pos >= 0) {
        state = strip(result.substr(newline_pos));
        result = strip(result.substr(0, newline_pos));
        var sigil = 'STATE:';
        if (state.substr(0, sigil.length) == sigil) {
            state = state.substr(sigil.length);
        } else {
            // Might be an error message, restore everything back
            result = result + '\n' + state;
            state = '';
        }
    }
    
    if (state) {
        document.getElementById('state').value = state;
    }
    shell.buffered_output += result + '\n\n';
    shell.doBufferedOutput();
  }
};

shell.runStatement = function() {
  var form = document.getElementById('form');

  // build a XmlHttpRequest
  var req = this.getXmlHttpRequest();
  if (!req) {
    document.getElementById('ajax-status').innerHTML =
        "<span class='error'>Your browser doesn't support AJAX. :(</span>";
    return false;
  }

  req.onreadystatechange = function() { shell.done(req); };

  // build the query parameter string
  var params = '';
  for (i = 0; i < form.elements.length; i++) {
    var elem = form.elements[i];
    if (elem.type != 'submit' && elem.type != 'button' && elem.id != 'caret') {
      var value = escape(elem.value).replace(/\+/g, '%2B'); // escape ignores +
      params += '&' + elem.name + '=' + value;
    }
  }

  // send the request and tell the user.
  document.getElementById('command').className = 'prompt processing';
  req.open(form.method, form.action + '?' + params, true);
  req.setRequestHeader('Content-type',
                       'application/x-www-form-urlencoded;charset=UTF-8');
  req.send(null);

  return false;
};
