from random import Random, getrandbits as random_getrandbits
import inspect
from random_utils import weighted_distribution
from items import Item, capitalize
from formatter import DumbWriter
import logging

def v_add(v1, v2):
    return tuple(x + y for x, y in zip(v1, v2))


directions = ('n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw')
directions_delta = ((-1,  0), (-1,  1), ( 0,  1), ( 1,  1), ( 1,  0), ( 1, -1), ( 0, -1), (-1, -1))
directions_long = ('north', 'northeast', 'east', 
        'southeast', 'south', 'southwest', 'west', 'northwest') 
dir_to_delta = dict(zip(directions, directions_delta))
dir_to_long = dict(zip(directions, directions_long))

def filter_nearby(pos, lst):
    return filter(lambda p: max(abs(pos[0] - p[0]), abs(pos[1] - p[1])) <= 1, lst)  

def diff_to_direction(position, other_position):
    return (('', 's', 'n')[other_position[0] - position[0]] +
            ('', 'e', 'w')[other_position[1] - position[1]]) 

numeral_strings = ['no', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten',
        'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen', 'twenty']

def n_objects_str(cnt, item):
    return '%s %s%s' % (numeral_strings[cnt], item, 's' if cnt != 1 else '')


class World(object):
    class Event(object):
        Examine = object()
        Win = object()
        Loss = object()
        Quit = object()

    def __init__(self, width = 5, height = 5, mine_count = 5, model = None):
        # basic initialization
        self.init_commands()
        self.output_buffer = []
        self.event_buffer = []
        if model is None:
            self.width = width
            self.height = height
            self.unflagged_mines = mine_count
            self.rand_seed = random_getrandbits(128) # random.seed uses 16 bytes as seed, too.
        else: 
            self.width = model.width
            self.height = model.height
            self.unflagged_mines = model.unflagged_mines
            self.rand_seed = model.rand_seed
        self.cell_count = self.width * self.height
            
        rnd = Random(self.rand_seed)
        def rand_pos():
            return rnd.randrange(self.height), rnd.randrange(self.width)
        
        # put mines
        self.mines = mines = set()
        for _ in xrange(mine_count):
            while True:
                pos = rand_pos()
                if pos not in mines:
                    mines.add(pos)
                    break
        
        # load or create items
        self.items = {}     
        if model:
            for id, loc in enumerate(model.item_locations):
                self.add_item_id(loc, id + 1)
        else:
            # ok, the idea is that we use self.rand_seed and item_id to seed 
            # the random generator for each item.  
            # Then we put items with with duplicate names into a special unaccessible
            # location (offset = -2 (-1 is inventory)). 
            # Afterwards we only need to store item locations.
            # Which should be a good balance between the session size and the 
            # time we would otherwise spend regenerating and skipping all items each time.
            name_db = set()
            item_id = 0 # important: ids are 1-based.
            gen_item_cnt = weighted_distribution((2.0, 0), (2.0, 1), (1.0, 2), (0.5, 3), (0.2, 4))
            for offset in xrange(self.cell_count):
                position = self.offset_to_position(offset)
                if position in self.mines: continue
                for _ in range(gen_item_cnt(rnd)):
                    while True:
                        item_id += 1
                        item = self.generate_item(item_id)
#                        self.out(item_id, item.name)
                        if item.name not in name_db: 
                            name_db.add(item.name)
                            self.add_item_id(offset, item_id)
                            break
                        else:
                            self.add_item_id(-2, item_id)
        
        # make first step or restore the state
        if model is None:
            self.command_counter = 0
            self.out('You gingerly make your first step...')
            while True:
                if rnd.randrange(2):
                    # Top or bottom row
                    row = 0 if rnd.randrange(2) else height - 1
                    col = rnd.randrange(width)                    
                else:
                    # first or last column
                    row = rnd.randrange(height)
                    col = 0 if rnd.randrange(2) else width - 1                    
                pos = row, col
                if pos not in self.mines: break
            self.position = pos
            self.flags = set()
            self.vision = [False for _ in range(self.cell_count)]
            self.reveal_cell(pos, self.position_to_offset(pos))
            self.cmd_look()
        else:
            self.command_counter = model.command_counter
            self.position = self.offset_to_position(model.position)
            self.flags = set(map(self.offset_to_position, model.flag_offsets))
            self.vision = model.vision
    
    def save(self, model):
        model.width = self.width
        model.height = self.height
        model.rand_seed = self.rand_seed
        model.position = self.position_to_offset(self.position)
        model.vision = self.vision
        model.unflagged_mines = self.unflagged_mines
        model.flag_offsets = map(self.position_to_offset, self.flags)
        model.command_counter = self.command_counter
        id_loc = sorted((v, k) for k, lst in self.items.iteritems() for v in lst)
        model.item_locations = map(lambda x: x[1], id_loc) 

    def out(self, *args):
        s = ' '.join(str(arg) for arg in args)
        self.output_buffer.append(s)
    
    def collect_output(self):
        s = '\n'.join(self.output_buffer)
        if s: s += '\n'
        del self.output_buffer[:]
        return s

    def collect_events(self):
        tmp = self.event_buffer
        self.event_buffer = []
        return tmp
    
    # offset_to_position only checks for negative offsets,
    # position_to_offset also checks bounds, in case we are using it to
    # iterate around the cell.
    def offset_to_position(self, offset):
        if 0 <= offset < self.cell_count:
            return divmod(offset, self.width)
    
    def position_to_offset(self, position):
        if position is None:
            return -1
        row, col = position
        width, height = self.width, self.height
        if 0 <= row < height and 0 <= col < width:
            return row * width + col
        return -1
    
    def is_valid_position(self, position):
        return 0 <= position[0] < self.height and 0 <= position[0] < self.width
    
    def iter8neighbours(self, position):
        for delta in directions_delta:
            npos = (position[0] + delta[0], position[1] + delta[1])
            offset = self.position_to_offset(npos) 
            if offset >= 0: 
                yield npos, offset

    def iter8neighbours_with_dir(self, position):
        for delta, dir in zip(directions_delta, directions):
            npos = (position[0] + delta[0], position[1] + delta[1])
            offset = self.position_to_offset(npos) 
            if offset >= 0: 
                yield npos, offset, dir
                
    def is_alive(self):
        return self.position is not None
    
    def flags_left(self):
        return len(self.mines) - len(self.flags) 
    
    def generate_item(self, item_id):
        rnd = Random(self.rand_seed ^ item_id)
        return Item(rnd)
    
    def add_item_id(self, offset, id):
        self.items.setdefault(offset, []).append(id)
    
    def get_item_ids(self, offset):
        return self.items.get(offset, [])
    
    class Item_tuple(object):
        def __init__(self, name, name_location, attributes, id, item):
            self.name, self.name_location, self.attributes, self.id, self.item = (
                    name, name_location, attributes, id, item)
    
    def get_items(self, offset):
        Item_tuple = self.Item_tuple
        def f(id):
            loc_attr = ('ground', 'inventory')[offset < 0]
            loc_str = (' (on the ground)', ' (in the inventory)')[offset < 0]
            item = self.generate_item(id)
            attrs = item.name.split()
            attrs.append(loc_attr)
            return Item_tuple(item.name, item.name + loc_str, attrs, id, item)
        return map(f, self.get_item_ids(offset))
             
    def reveal_cell(self, position, offset):
        mines = self.mines
        vision = self.vision
        assert not vision[offset]
        
        vision[offset] = True
        stack = [(position, offset)]
        additional_cnt = 0
        while len(stack):
            position, offset = stack.pop()
            if 0 == len(filter_nearby(position, mines)):
                additional_cnt += 1
                for p1, o1 in self.iter8neighbours(position):
                    if not vision[o1]:
                        vision[o1] = True
                        stack.append((p1, o1))
                        
        if additional_cnt > 0:
            self.out('A plot of earth settles down, revealing more safe spots around you.')
        if additional_cnt > 1:
            self.out('You see earth settling down all around you, revealing more safe spots.')
        if additional_cnt > 5:
            self.out('You hear a lot of earth settling down in a distance.')
            
    def cmd_move(self, dir):
        delta = dir_to_delta[dir]
        logging.info('%r %r %r' % (self.position, dir, delta))
        position = v_add(self.position, delta)
        offset = self.position_to_offset(position) 
        if offset < 0:
            self.out('You can\'t leave! YOU CAN\'T LEAVE!')
            return
        if position in self.flags:
            self.out('There\'s a red flag there!')
            return
        if self.vision[offset]:
            self.position = position
            self.cmd_look()
            return
        self.out('You cautiously step %swards...' % (dir_to_long[dir]))
        if position in self.mines:
            self.out('BAM!')
            self.out('You have lost the game.')
            self.event_buffer.append(self.Event.Loss)
            self.cmd_blacksheepwall()
            self.position = None
            return
        self.position = position
        self.reveal_cell(position, offset)
        self.cmd_look()
        
    def cmd_look(self):
        pos = self.position
        self.out('You sense ' + n_objects_str(len(filter_nearby(pos, self.mines)), 'mine') + ' nearby.')
        pod_list = list(self.iter8neighbours_with_dir(pos))
        
        safe = [direction for _, offset, direction in pod_list if self.vision[offset]]
        if len(safe):
            self.out('To the ' + ', '.join(safe) + ' is safe ground.')
        flags = filter_nearby(pos, self.flags)
        str_flags = 'is a flag' if len(flags) == 1 else 'are flags' 
        if len(flags):
            self.out('There ' + str_flags + ' to the ' +
                    ', '.join(diff_to_direction(self.position, f) for f in flags) + '.')
        available_dirs = set(dir for _, _, dir in pod_list)
        for test_dir in 'nwse':
            if test_dir not in available_dirs:
                self.out('To the %s is the edge of the minefield' % (dir_to_long[test_dir]))
        for item_id in self.get_item_ids(self.position_to_offset(pos)):
            item = self.generate_item(item_id)
            self.out('A %s is lying on the ground here' % item.name)

    def match_items(self, keywords, *item_priority_groups):
        '''Successful match: the first priority group that has any matches has
        exactly one match. Other possibilities: no matches at all, or more 
        than one match in the first matching group (but then we still 
        calculate and display all matches)'''  
        def match_group(items):
            for item in items:
                attributes = list(item.attributes) # clone so we can remove matched
                for keyword in keywords:
                    for i, attr in enumerate(attributes):
                        if attr.startswith(keyword):
                            del attributes[i]
                            break
                    else: # keyword not matched
                        break
                else: # all keywords matched
                    yield item
        
        matched = []
        for group in item_priority_groups:
            m = list(match_group(group))
            if not matched and len(m) == 1:
                return m[0]
            matched.extend(m)
            
        if not len(matched):
            self.out('No items match your description')
        else:
            self.out('Ambiguous or no description, these items match:')
            for m in matched:
                self.out('  ' + m.name_location)

    def cmd_examine(self, *args):
        offset = self.position_to_offset(self.position)
        match = self.match_items(args, self.get_items(offset), self.get_items(-1))
        if match:
            self.event_buffer.append(self.Event.Examine)
            self.out(match.item.description.replace('\n', '\n\n').strip())
    
    def cmd_inventory(self, *args):
        if not len(args):
            items = self.get_items(-1)
            self.out('Your inventory:')
            for it in items:
                self.out('A %s.' % it.name)
            self.out('%s.' % capitalize(n_objects_str(self.flags_left(), 'flag')))
            return
        match = self.match_items(args, self.get_items(-1))
        if match:
            self.event_buffer.append(self.Event.Examine)
            self.out(match.item.description.replace('\n', '\n\n'))
        

    def cmd_get(self, *args):
        offset = self.position_to_offset(self.position)
        item = self.match_items(args, self.get_items(offset))
        if item:
            self.get_item_ids(offset).remove(item.id)
            self.add_item_id(-1, item.id)
            self.out('You picked up a %s' % item.name)

    def cmd_drop(self, *args):
        offset = self.position_to_offset(self.position)
        item = self.match_items(args, self.get_items(-1))
        if item:
            self.get_item_ids(-1).remove(item.id)
            self.add_item_id(offset, item.id)
            self.out('You put a %s on the ground' % item.name)

    def cmd_flag(self, dir):
        delta = dir_to_delta.get(dir, None)
        if not delta:
            self.out('I don\'t understand the direction!')
            return
        position = v_add(self.position, delta)
        offset = self.position_to_offset(position)
        if offset < 0:
            self.out('You can\'t put flags outside the field.')
            return
        if position in self.flags:
            self.out('You remove a flag from the plot of land to the %s.' % dir_to_long[dir])
            self.flags.remove(position)
            if position in self.mines:
                self.unflagged_mines += 1
            return
        if self.vision[offset]:
            self.out('You already know there\'s no mine there!')
            return
        if not self.flags_left():
            self.out('You have no flags left.')
            return
        self.out('You mark a plot of land to the %s with a red flag.' % dir_to_long[dir])
        self.flags.add(position)
        self.out('You have %s left.' % n_objects_str(self.flags_left(), 'flag'))
        if position in self.mines:
            self.unflagged_mines -= 1
            if not self.unflagged_mines:
                self.out('Congratulations, you have won the game!')
                self.event_buffer.append(self.Event.Win)
                self.cmd_blacksheepwall()
                self.position = None
                
    def cmd_quit(self):
        self.out('Bye!')
        self.out()
        self.event_buffer.append(self.Event.Quit)
        self.cmd_blacksheepwall()
        self.position = None

    def cmd_help(self):
        help = '''\
Available commands:
  %s - move in that direction
  flag [direction] - put or remove a flag in that direction
  look - report on current surroundings
  examine [parts of item name] - examine an item on the 
     ground or in the inventory. For example, "ex ir arr inv" 
     would examine an iron arrow in the inventory.
     Items on the ground get precedence.
  inventory [parts of item name] - show items in inventory
     or examine an item in the inventory.
  get/pick [parts of item name] - pick an item.
  drop [parts of item name] - drop an item.
  quit - quit
  help - this

Abbreviations are also recognised.''' % ', '.join(directions)
        self.out(help)

    def cmd_blacksheepwall(self):
        # secret command!
        def cell(*pos):
            if pos == self.position: return 'X'
            if pos in self.mines: return '*' 
            return str(len(filter_nearby(pos, self.mines)))

        self.out('Turn: %d' % self.command_counter)
        self.out('Field:')
        for row in xrange(self.height):
            self.out(' '.join(cell(row, col) for col in xrange(self.width)))
        self.out('Vision:')
        for row in xrange(self.height):
            start = row * self.width
            self.out(' '.join(('*' if self.vision[start + col] else '-') for col in xrange(self.width)))
        self.out('Flagged mines: %d of %d' % (len(self.mines) - self.unflagged_mines, len(self.mines)))
#        self.out('Position: %r' % (self.position,))
            
    def init_commands(self):
        def compose(name, func):
            args, varargs, varkw, defaults = inspect.getargspec(func)
            assert defaults is None
            assert varkw is None
            if varargs is not None:
                cnt =  -1
            else:
                cnt = len(args) - 1 # exclude 'self'
            return (name, func, cnt)
            
        self.commands = [
                compose('look', self.cmd_look),
                compose('examine', self.cmd_examine),
                compose('inventory', self.cmd_inventory),
                compose('get', self.cmd_get),
                compose('pick', self.cmd_get),
                compose('drop', self.cmd_drop),
                compose('flag', self.cmd_flag),
                compose('quit', self.cmd_quit),
                compose('help', self.cmd_help),
                ]

    def begin_game(self):
        return self.collect_output()
    
    def do_command(self, s):
        words = s.split()
        if not len(words): return self.collect_output()
        cmd, args = words[0], words[1:]
        def assert_n_args(full_name, n):
            if len(args) == n:
                return True
            self.out('I expect %s for command "%s", and you gave %d (%r)'
                    % (n_objects_str(n, 'argument'), full_name, len(args), args))
            return False
        
        # directions are special cased
        if cmd in dir_to_delta:
            if assert_n_args(cmd, 0):
                self.cmd_move(cmd)
        elif cmd == 'blacksheepwall':
            # cheat should only match the whole word
            self.cmd_blacksheepwall()
        else:
            for name, func, arg_cnt in self.commands:
                if name.startswith(cmd):
                    if arg_cnt < 0 or assert_n_args(name, arg_cnt):
                        func(*args)
                    break
            else:
                self.out('unrecognized command: %r, use "help" for a list of commands' % cmd)
        self.command_counter += 1
        return self.collect_output()
    
def mainloop():
    writer = DumbWriter()
    def prn(s):
        ss = s.split('\n')
        for s in ss:
            writer.send_flowing_data(s)
            writer.send_paragraph(1)
        writer.flush()
    world = World()
    prn(world.begin_game())
    while world.is_alive():
        s = raw_input('> ')
        prn(world.do_command(s))

if __name__ == '__main__':
    mainloop()