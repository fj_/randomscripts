from base64 import decodestring as base64_decode
from pprint import pprint
import time
from minesweeper import World
from minesweeper_model import WorldModel

t = time.clock()
for i in range(1000):
    model = WorldModel.deserialize(base64_decode('BQXtra3F0ryRmYXShqjjiJqxhvUAChkABAAABQAXGwAAAgIDAwQFBQcKDg4ODg8PEhISEhMTExgYGA=='))
    world = World(model = model)
    #print world.do_command('blacksheepwall')
    print world.do_command('n')
    print world.do_command('n')
    print world.do_command('ex Ca')
    
t1 = time.clock()
print t1 - t

