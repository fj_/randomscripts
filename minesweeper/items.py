from pprint import pformat
from formatter import DumbWriter
from random import Random
from string import capitalize
#import sys

from random_utils import weighted_choice, abs_weighted_choice, abs_weighted_distribution
    
def capitalize(s):
    if s: return s[0].upper() + s[1:]
    return s

def make_sentence(s):
    # special hax
    s = s.rstrip(' ,')
    return capitalize(s) + '.'
            
material_adjectives = ['polished', 'raw', 'battered', 'varnished', 'worn', 'discoloured']

materials = ['flint', 'dolomite', 'diamond', 'silver', 'adamantium', 'iron', 'steel', 'ebony', 
        'glass', 'trollbone', 'trollboner', 'maggot', 'soap', 'cheese']

item_types = ['arrow', 'bow', 'knife', 'dagger', 'sword', 'axe', 
        'barrel', 'chair', 'cabinet', 'teapot', 'door', 'loincloth',
        'cudder', 'codan', 'toy cdr']

quality_kinds = ['the higest', 'superior', 'enterprise']

def generate_decorations(rnd, material, type, epic = False):
    decoration_templates = [
            'is encircled with bands of',
            'is adorned with hanging rings of',
            'is decorated with',
            'is encrusted with',
            'is studded with']
    assert len(decoration_templates) == 5, 'fix numbers below if you want to change it'
    if epic:
        extra_decorations_cnt = rnd.randint(3, 5)
    else:
        extra_decorations_cnt = rnd.randrange(0, 3)
    decoration_fragments = rnd.sample(decoration_templates, extra_decorations_cnt)
    decoration_fragments.append('menaces with spikes of')
    decoration_materials = abs_weighted_distribution(
            (0.3, material),  
            *list(rnd.choice(materials) for _ in range(extra_decorations_cnt + 2)))

    expanded_fragments = []
    for df in decoration_fragments:
        s = [df]
        dec_adjective = rnd.choice([None, 'exceptionally worked', 'masterfully worked', 'sharpened'])
        if dec_adjective: s.append(dec_adjective)
        m1, m2 = decoration_materials(rnd), decoration_materials(rnd)
        s.append(m1)
        if m1 != m2 and rnd.random() < 0.8: s.append('and'); s.append(m2)
        expanded_fragments.append(' '.join(s))
    
    expanded_fragments.reverse() # we are going to pop stuff from it, but still want 'menaces' last.
    while len(expanded_fragments):
        s = []
        s.append(rnd.choice(['it', 'it', 'this object', 'the ' + type]))
        s.append(expanded_fragments.pop())
        if len(expanded_fragments) and rnd.random() < 0.5:
            s.append('and')
            s.append(expanded_fragments.pop())
        yield ' '.join(s)

class ActorInstance(object):
    def __init__(self, 
            name, # Urist Thunderfist
            genus, # dwarf 
            description = None, # male dwarf
            details = None, # Blah Blah, the god of trolls
            affection = None, # trolls
            sex = 2, # ['he', 'she', 'it'] 
            ):
        self.name = name
        self.genus = genus 
        self.description = description or genus
        self.details = details or name + ' the ' + genus
        self.affection = affection
        self.sex = sex
    def __str__(self):
        return pformat(self.__dict__)

def generate_dwarf(rnd):
    sex = rnd.randint(0, 1)
    sex_s = ('male', 'female')[sex]
    surname = (rnd.choice(['strong', 'hard', 'stone', 'long']) +
            rnd.choice(['arm', 'beard', 'head', 'anus']))
    return ActorInstance('Urist ' + capitalize(surname), 'dwarf', sex_s + ' dwarf', sex = sex)

def generate_wizard(rnd):
    name = rnd.choice(['The Sussman', 'GJS Jay Sussman', 'Hal Abelson', 'Haxus the Great'])
    return ActorInstance(name, 'wizard', 'male wizard', sex = 0)

def generate_magic_animal(rnd):
    sex = rnd.choice(['male', 'female'])
    name, genus, description = rnd.choice([
            ('Python', 'snake', sex + ' snake'), 
            ('Proggles', 'snake', 'retarded black snake'),
            ('Haskell', 'dead dog', 'dead dog'),
            ('Pazuzu', 'purring maggot', 'purring maggot')])
    return ActorInstance(name, genus, description, sex = 2)

def generate_neckbeard(rnd):
    sex = rnd.randint(0, 1)
    if sex:
        name = rnd.choice(['Leah', 'Nixie', 'Audrey']) + ' ' + rnd.choice(['Culver', 'Pixel', 'Tang']) 
    else:
        name = abs_weighted_choice(rnd, 
                (0.6, capitalize(''.join(rnd.choice(['cairn', 'nair', 'var', 'non', 'nar', 'von']) for _ in range(4)))),
                'RMS Michael Stallman', 'SPJ Python Jones')
    return ActorInstance(name, 'neckbeard', ['male', 'female'][sex] + ' neckbeard', sex = sex)
    
god_names = ('El Eld Tir Nef Eth Ith Tal Ral Ort Thul Amn Sol Shael Dol Hel ' + 
        'Io Lum Ko Fal Lem Pul Um Mal Ist Gul Vex Ohm Lo Sur Ber Jah Cham Zod'
        ).split() 

def generate_god(rnd):
    god_things = {'fire':'', 'death':'', 'plagues':'plague', 
        'famine':'', 'suffering':'', 'madness':'', 'serpents':'serpent', 'maggots':'maggot',
        'trolls':'troll', 'synergy':'synergy', 'five nines availability':'availability', 
        'cutting edge solutions':'severance'}

    a1, a2 = rnd.sample(god_things, 2)
    affection = rnd.choice([a1, a2])
    affection_singular = god_things[affection]
    if not affection_singular: affection_singular = affection
    
#    vowels = 'aeiou'
#    consonants = 'bcdfgklmnprstvz'
#    name = capitalize(rnd.choice(consonants) + rnd.choice(vowels) + rnd.choice(consonants))
    name = rnd.choice(god_names)
    name += ' the ' + capitalize(affection_singular + rnd.choice(['bringer', 'bearer', 'lord', 'god'])) 
    
    details = name + ', the deity of '
    if rnd.random() < 0.3:
        details += affection
    else:
        details += a1 + ' and ' + a2
    details += ',' # is fixed by make_sentence if necessary 
    return ActorInstance(name, 'god', 'terrifying divine presence', details, affection = affection, sex = 2)

def wrap_reference(rnd, s):
    tmp = rnd.random() 
    date = str(rnd.randrange(128))
    if tmp > 0.2:
        adj = 'the ' 
        if tmp < 0.45:
            adj += 'early '
        elif tmp < 0.7:
            adj += 'late ' 
        date = (adj + rnd.choice(['spring', 'summer', 'fall', 'winter']) +
                ' of ' + date) 
    return 'The artwork relates to the ' + s + ' in ' + date 

def wrap_image_description(rnd, s):
    quality = rnd.choice(['a well-designed', 'a masterfully designed', 
            'an exceptionally designed'])
    return 'engraved on the item is %s image of %s' % (quality, s)

def generate_happy_god_engraving(rnd):
    god = generate_god(rnd)
    s = god.details
    incarnation = weighted_choice(rnd, 
            (0.7, generate_wizard), 
            generate_dwarf, 
            generate_magic_animal,
            generate_neckbeard)(rnd)

    if rnd.random() < 0.8:
        s += ' depicted as a ' + incarnation.description 
    yield wrap_image_description(rnd, s)
    
    happy_things = ['laughing', 'smiling enigmatically', 'striking a menacing pose', 'contemplating']
    
    yield '%s is %s' % (god.name, rnd.choice(happy_things))
    def gen_contemplation():
        return ('contemplation of ' + 
                abs_weighted_choice(rnd, 
                        (0.6, god.affection), 
                        'synergy', 
                        'five nines availability', 
                        'cutting edge solutions') +
                ' by ' + god.name)
    def gen_ascension():
        # todo -- generate randomly as well? Or the probability of getting here is too insignificant to bother?
        synergy_msg = (
                'getting to the next level by achieving five nines availability ' +
                'across the entire enterprise through world-class team synergy ' + 
                'and cutting edge solutions')
        return (weighted_choice(rnd, 
                        'ascension to transcendence', 
                        'immanentization of eschaton',
                        synergy_msg) + 
                ' by ' + god.name)
    def gen_incarnation():
        return ('incarnation of ' + god.name +
                ' as ' + incarnation.details)
    
    generator = abs_weighted_choice(rnd, (0.45, gen_incarnation), (0.35, gen_contemplation), gen_ascension) 
    yield wrap_reference(rnd, generator()) 
#    yield rnd.choice([gen_incarnation])()

def generate_construction_engraving(rnd):
    creator = weighted_choice(rnd,
            generate_god, 
            (0.7, generate_wizard), 
            generate_dwarf, 
            generate_magic_animal,
            generate_neckbeard)(rnd)
    item = Item(rnd, True)
    
    s = 'a %s and a %s' % (creator.description, item.shortname)
    yield wrap_image_description(rnd, s)
    
    their = ('his', 'her', 'its')[creator.sex]
    yield '%s is %s' % (
            creator.details,
            rnd.choice(['laboring', 'haxing', 
                    'conjuring the spirits of the computer with %s spells' % their]))
    
    if item.epic:
        it_s = item.name
    else:
        it_s = 'the ' + item.name 
    yield wrap_reference(rnd, 'creation of ' + it_s +
                ' by ' + creator.name)

def generate_battle_engraving(rnd):
    winner_generator = weighted_choice(rnd,
            generate_god, 
            (0.7, generate_wizard), 
            generate_dwarf, 
            generate_magic_animal,
            generate_neckbeard)
    loser_gen_lst = filter(lambda x: x != winner_generator, (
            generate_dwarf, 
            generate_magic_animal,
            generate_neckbeard))
    winner = winner_generator(rnd)
    loser = rnd.choice(loser_gen_lst)(rnd)
    
    s = 'a %s and a %s' % (winner.description, loser.description)
    yield wrap_image_description(rnd, s)
    
    def gen_single_description():
        template = rnd.choice([
                'The %s is striking down the %s',
                'The %s is glaring menacingly at the %s'])
        return [template % (
                winner.genus + ' ' + winner.name, 
                loser.genus + ' ' + loser.name)]

    def gen_double_description():
        action1 = rnd.choice(['laughing', 'striking a menacing pose', 'playing with a codan meaningfully'])
        action2 = rnd.choice(['in the fetal position', 'making a plaintive gesture', 'dead'])
        s1 = 'The ' + winner.genus + ' ' + winner.name + ' is ' + action1
        s2 = 'The ' + loser.genus + ' ' + loser.name + ' is ' + action2
        x = [s1, s2]
        rnd.shuffle(x)
        return x

    generator = weighted_choice(rnd, gen_single_description, (2.0, gen_double_description))
            
    for s in generator(): yield s
    
    s = rnd.choice([
            'mortal wounding of %s by %s',
            'killing of %s by %s',
            'death of %s by the hand of %s',
            'application of a cutting-edge solution to %s by %s',
            'haxing of the anus of %s by %s'])
    s = s % (loser.details, winner.details)
    yield wrap_reference(rnd, s)

def generate_engraving(rnd):
    generator = weighted_choice(rnd,
            (0.5, generate_construction_engraving),
            generate_happy_god_engraving,
            generate_battle_engraving,
            )
    for s in generator(rnd): yield s
    
class Item(object):
    def __init__(self, rnd, epic_boost = False):
        p = [0.15, 0.7][epic_boost]
        self.epic = rnd.random() < p
        
        adjective = rnd.choice(material_adjectives)
        self.material = rnd.choice(materials)
        self.type = rnd.choice(item_types)
        self.name = ' '.join((adjective, self.material, self.type))
        self.shortname = self.name
        
        if self.epic:
            name_len = rnd.randrange(1, 5)
            name_bits = rnd.randrange(2 ** name_len)
            own_name = 'C' + ''.join('ad'[(name_bits >> i) & 1] for i in range(name_len)) + 'r '
            own_name = own_name + capitalize(
                    rnd.choice(['walled', 'stoned', 'haxed', 'morning', 'night', 'boat']) +
                    rnd.choice(['bears', 'mist', 'anii', 'light', 'pleasure', 'murdered']))
            
            self.name = '%s the %s' % (own_name, self.name)
        self.state = rnd.getstate()

    @property
    def description(self):
        rnd = Random(0)
        rnd.setstate(self.state)
        description = [[]]
        def sentence(s):
            description[-1].append(make_sentence(s))
        def end_paragraph():
            description.append([])
        
        s = self.name
        if not self.epic:
            s = 'a ' + s
        sentence('this is ' + s)
        sentence('all craftsdwarfship is of %s quality' % rnd.choice(quality_kinds))
        for s in generate_decorations(rnd, self.material, self.type, self.epic):
            sentence(s)
        end_paragraph()
        if self.epic:
            engravings_cnt = rnd.choice([3, 4])
        else:
            engravings_cnt = weighted_choice(rnd, 0, 1, (0.5, 2), (0.15, 3))
        for _ in range(engravings_cnt):
            for s in generate_engraving(rnd):
                sentence(s)
            end_paragraph()
                
        description = '\n'.join(' '.join(s for s in p) for p in description)
        return description


if __name__ == '__main__':
    def print_wrapped(s):
        writer = DumbWriter()
        ss = s.split('\n')
        for s in ss:
            writer.send_flowing_data(s)
            writer.send_paragraph(2)
        writer.send_paragraph(1)
        writer.flush()
        
    rnd = Random()
    for i in range(5):
        rnd = Random()
#        s = pformat(generate_person(rnd).__dict__)
        it = Item(rnd, True) 
        s = it.name + '\n' + it.description
        print_wrapped(s)
        
    