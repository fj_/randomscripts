from serialization import Int_field, List_field, Bitlist_field
from serialization import Serializable

class WorldModel(Serializable):
    width = Int_field()
    height = Int_field()
    rand_seed = Int_field()
    position = Int_field()
    vision = Bitlist_field()
    unflagged_mines = Int_field()
    flag_offsets = List_field(Int_field())
    command_counter = Int_field()
    item_locations = List_field(Int_field())
