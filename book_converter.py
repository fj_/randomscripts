import sys
import re
from itertools import takewhile
from lxml import etree
import os.path as os_path
import codecs
import glob
from multiprocessing import Process, Queue
import urllib

def set_pdb_hook():
    def info(type, value, tb):
        import traceback, pdb
        traceback.print_exception(type, value, tb)
        pdb.pm()
    sys.excepthook = info
set_pdb_hook()

def describe_image(source):
    from Tkinter import Tk, Label, Entry, StringVar
    import Image, ImageTk
    root = Tk()
    root.title(source)
    img = Image.open(source)
    img = ImageTk.PhotoImage(img)
    label = Label(root, image = img)
    label.pack()
    var = StringVar()
    text = Entry(root, textvariable = var)
    text.pack()
    root.bind('<Return>', lambda ev: root.destroy())
    ws = root.winfo_screenwidth()
    hs = root.winfo_screenheight()
    root.geometry('+%d+%d' % (ws * 0.4, hs * 0.4))
    text.focus()
    root.mainloop()
    return var.get()

# for some reason when I terminate the tkinter mainloop, then recreate it
# anew, its window doesn't get focus. And my internet connection is down
# at the moment, so I'm going to run with what works: doing that in a
# subprocess.
def _subprocess_runner(f, q, args, kwargs):
    q.put(f(*args, **kwargs))

def run_in_subprocess(f, *args, **kwargs):
    q = Queue()
    p = Process(target = _subprocess_runner, args = (f, q, args, kwargs))
    p.start()
    res = q.get()
    p.join()
    return res

image_descriptions = None
book_path = None

def get_image_description(src):
    if not os_path.exists(src):
        return src
    global image_descriptions, book_path
    desc_fname = book_path + '.imagedb'
    src = os_path.join(os_path.dirname(book_path), src)
    if image_descriptions is None:
        if os_path.exists(desc_fname):
            with open(desc_fname, 'r') as f:
                image_descriptions = eval(f.read())
            assert isinstance(image_descriptions, dict)
        else:
            image_descriptions = {}
    if src in image_descriptions: return image_descriptions[src]
    desc = run_in_subprocess(describe_image, src)
    image_descriptions[src] = desc
    from pprint import pformat
    with open(desc_fname, 'w') as f:
        f.write(pformat(image_descriptions))
    return desc

# these are the only characters considered whitespace by HTML specification
# http://www.w3.org/TR/html401/struct/text.html#whitespace
html_space_chars = u'\u0020\u0009\u000C\u200B\u000A\u000D' 
html_space_re = re.compile('[%s]+' % html_space_chars)
def html_condense_space(s):
    return html_space_re.sub(u' ', s)

class Container(object):
    preformatted = False
    def __init__(self, items, node):
        self.items = items
        self.node = node
        self.in_repr = False # a dirty hack for make repr work
        self.post_init()
    def post_init(self):
        """You can override this to avoid calling super().__init__"""
        pass
    def __getitem__(self, index):
        return self.items[index]
    def __setitem__(self, index, value):
        self.items[index] = value
    def __delitem__(self, index):
        del self.items[index]
    def __len__(self):
        return len(self.items)
    def insert(self, index, item):
        return self.items.insert(index, item)
    def compose_inner(self):
        """Composes subnodes. Subclasses should override __unicode__, call
        self.compose() and whatever add decorations"""
        if self.in_repr: 
            return ' '.join(repr(item) for item in self.items)
        else:
            return ''.join(unicode(item) for item in self.items)
    def __unicode__(self):
        """Override this"""
        return self.compose_inner()
    def __repr__(self):
        """Do not override this"""
        self.in_repr = True
        try:
            return '$' + self.__class__.__name__ + ': ' + unicode(self)
        finally:
            self.in_repr = False

class Paragraph(Container):
    def __unicode__(self):
        return '\n    ' + self.compose_inner() + '\n'

class Pre(Container):
    preformatted = True

class Break(Container):
    def __unicode__(self):
        return '\n    '

class Style(Container):
    def post_init(self):
        self.type = self.node.attrib.get('type', None)
    def __unicode__(self):
        if self.type == 'text/css': return u''
        return self.compose_inner()

class Emphasis(Container):
    def __unicode__(self):
        return '_' + self.compose_inner() + '_'

class Strong(Container):
    def __unicode__(self):
        return '*' + self.compose_inner() + '*'
    
class Link(Container):
    def post_init(self):
        self.href = self.node.attrib.get('href', None)
    def __unicode__(self):
        if self.href:
            return '{' + self.compose_inner() + '}'
        else:
            return self.compose_inner();

class Image(Container):
    def post_init(self):
        src = urllib.unquote(self.node.attrib.get('src', None))
        self.desc = get_image_description(src)
    def __unicode__(self):
        return '[' + self.desc + ']'
    
class Header(Container):
    def __unicode__(self):
        return '\n       |' + self.compose_inner() + '|\n'

class HorizontalLine(Container):
    def __unicode__(self):
        return '\n------------------\n'


special_tags = {
                'p' : Paragraph,
                'br' : Break,
                'pre' : Pre,
                'style' : Style,
                'em' : Emphasis, 'i' : Emphasis,
                'strong' : Strong, 'b' : Strong,
                'a' : Link,
                'h1' : Header, 'h2' : Header, 'h3' : Header, 
                'h4' : Header, 'h5' : Header, 'h6' : Header,
                'hr' : HorizontalLine,
                'img' : Image,
                }

ignored_tags = { 'title', 'link' } 

def parse_html_node(node, preformatted=False):
    """The core parsing function. Returns a list of Container objects and strings, 
    for more efficient pruning of useless stuff.
    
    Note that all strings in the list are non-empty (but may contain a single space).
    
    Also note that when I would want to support <pre> tag I would need a parameter to disable
    pruning. 
    
    Pruning works thusly: all adjacent strings are joined, all whitespace 
    is condensed into single spaces, then if leftmost (rightmost) element is a string, it is
    left(right)-trimmed (and possibly removed if becomes empty).
    
    By the way, the point of pruning is not for performance, but to remove as all empty tags 
    so that I wouldn't render '<i><b> </b></i>' as '_* *_'
    """
    # determine the future container class first
    tag = node.tag 
    if tag in special_tags:
        container_class = special_tags[tag]
    elif tag in ignored_tags:
        return []
    else:
        container_class = Container
    
    preformatted = preformatted or container_class.preformatted
    # retrieve contents
    contents = []
    if node.text: contents.append(node.text)
    for item in node:
        parsed = parse_html_node(item, preformatted)
        if parsed: contents.extend(parsed)
        if item.tail: contents.append(item.tail)
    
    if not preformatted:
        # join all strings, condensing spaces as we do
        prev_str = False
        tmpcontents = []
        for item in contents:
            if isinstance(item, basestring):
                if prev_str:
                    tmpcontents[-1] = html_condense_space(tmpcontents[-1] + item)
                else:
                    tmpcontents.append(html_condense_space(item))
                    prev_str = True
            else:
                tmpcontents.append(item)
                prev_str = False
        contents = tmpcontents
        # trim first and last strings
        if len(contents) and isinstance(contents[0], basestring):
            s = contents[0].lstrip(html_space_chars)
            if s: contents[0] = s
            else: del contents[0]

        if len(contents) and isinstance(contents[-1], basestring):
            s = contents[-1].rstrip(html_space_chars)
            if s: contents[-1] = s
            else: del contents[-1]
    
    if container_class is Container:
        return contents
    else:
        return [container_class(contents, node)]

def remove_headers_extract_next(container):
    def get_next_in_section(lst):
        """Returns link to the next page.
        Returns False if this is not a legitimate navigation section.
        Returns None if there's no next link"""
        next = None
        for item in lst:
            if isinstance(item, basestring):
                if len(item.strip(' |')):
                    return False
            elif isinstance(item, Link):
                if item.compose_inner().strip().lower() in (u'next', u'appendices'):
                    next = item.href
            else:
                return False
        return next
    section = list(takewhile(lambda x: not isinstance(x, HorizontalLine), container))
    next1 = next = get_next_in_section(section)
    if next is not False:
        del container[:len(section) + 1]
    
    section = list(takewhile(lambda x: not isinstance(x, HorizontalLine), reversed(container)))
    next = get_next_in_section(section)
    if next is not False:
        del container[-len(section) - 1:]

    return next or next1

def process_page(name):
    tree = etree.parse(name, etree.HTMLParser())
    root = tree.getroot()
    #print etree.tostring(root)
    parsed = Container(parse_html_node(root), root)
    next = remove_headers_extract_next(parsed)
    return parsed, next

downgrade_to_ascii_map = {
        u'\u2011' : u'-', # non-breaking hyphen
        u'\u2013' : u'-', # figure dash (used in number ranges)
        u'\u2018' : u'\'',
        u'\u2019' : u'\'',
        u'\u2022' : u'*', # bullet
        u'\u00A2' : u'c', # cent sign
        u'\u00A9' : u'(c)', # copyright sign
        u'\u00A0' : u' ', # nbsp
        u'\u2014' : u' -- ', # mdash
        u'\u201c' : u'"',
        u'\u201d' : u'"',
        u'\u00b4' : '`', # acute accent (printable).
        u'\u00e0' : u'a', # a'
        u'\u00e9' : u'e', # e'
        u'\u00ea' : u'e', # e^
        u'\u00ef' : u'i', # i:
        u'\u2227' : u'^', # logical conjunction
        u'\xb2'   : u'(2)', # <sup>2</sup>
        u'\u2217' : u'*', # "astersk operator", lol
        u'\u2026' : u'...', # horisontal ellipsis
        
        u'\x85' : u'...', 
        u'\x91' : u'\'', 
        u'\x92' : u'\'', 
        u'\x93' : u'"', 
        u'\x94' : u'"', 
        u'\x97' : u'--', # end of guarded area (wtf?)
        u'\xa1' : u'!', # inverted exclamation mark
        u'\xa7' : u'#', # paragraph actually
        u'\xad' : u'-', # soft hyphen
        u'\xb7' : u'*', # middle dot
        u'\xc0' : u'A', # A with grave
        u'\xc1' : u'A', # A with acute
        u'\xc8' : u'E', # E grave
        u'\xc9' : u'E', # E acute
        u'\xd1' : u'N~', # 
        u'\xe1' : u'a', # a acute
        u'\xe2' : u'a', # a with ^ 
        u'\xe4' : u'a:', # a with diaresis
        u'\xe6' : u'ae', # ae
        u'\xe7' : u'c', # c with cedilla
        u'\xe8' : u'e', # e grave
        u'\xeb' : u'e', # e diaresis
        u'\xec' : u'a', # i grave
        u'\xed' : u'i', # i acute
        u'\xfc' : u'u:', # u with diaresis
        u'\xf9' : u'u', # u with grave
        u'\xf1' : u'n~', # n with tilde
        u'\xf2' : u'o', # o with grave
        u'\xf3' : u'o', # o with acute
        u'\xf4' : u'o', # o with circumflex
        u'\xf6' : u'o:', # o with diaresis
        u'\xfa' : u'u', # u with acute
        
        u'\u0100' : u'A', # A with macron (bar on top)
        u'\u0101' : u'a', # a with macron
        u'\u011b' : u'e', # e with caron
        u'\u012b' : u'i', # i with macron
        u'\u014d' : u'o', # o with macron
        u'\u016b' : u'u', # u with macron
        u'\u01d0' : u'i', # i with caron (like 'v' on top)
        u'\u01d2' : u'o', # o with caron
        u'\u01d4' : u'u', # u with caron
        u'\u01ce' : u'a', # a with caron
        
        u'\u02c6' : '', # MODIFIER LETTER CIRCUMFLEX ACCENT
}

def downgrade_to_ascii(s):
    def downgrade_char(c, i):
        if ord(c) < 128: return c
        mapped = downgrade_to_ascii_map.get(c)
        if mapped is not None: return mapped
        print 'Character not found', repr(c)
        print repr(s[max(0, i - 50) : min(len(s), i + 50)]) 
        import webbrowser
        webbrowser.open_new_tab('http://www.fileformat.info/info/unicode/char/%x/index.htm' % ord(c))
        raise StopIteration()
    return u''.join(downgrade_char(c, i) for i, c in enumerate(s))

def dump_utf8(name, parsed):
    with codecs.open(name + '.utf8.txt', 'w', 'utf-8') as f:
        f.write(parsed)

def martin3():
    path = 'book' 
    next = 'toc.html'
    with codecs.open('book.txt', 'w', 'ascii') as outfile:
        while next:
            print next
            # name = os_path.join(path, os_path.splitext(next)[0])
            parsed, next = process_page(os_path.join(path, next))
            parsed.insert(0, HorizontalLine(None, None)) # prettier
            parsed = unicode(parsed)
            parsed = downgrade_to_ascii(parsed)
            outfile.write(parsed)

def martin4():
    print 'parsing'
    with codecs.open('tmp/feast.txt', 'w', 'ascii') as outfile:
        name = 'tmp/feast.html'
        parsed, next = process_page(name)
        parsed = unicode(parsed)
        parsed = downgrade_to_ascii(parsed)
        outfile.write(parsed)
    print 'done'

def separate_books():
    global book_path
    sources = glob.glob('book/*.htm*')
    for source in sources:
        print source
        dest = re.sub('\.html?$', '.txt', source)
        assert dest != source
        book_path = source
        with codecs.open(dest, 'w', 'ascii') as outfile:
            parsed, next = process_page(source)
            parsed = unicode(parsed)
            parsed = downgrade_to_ascii(parsed)
            outfile.write(parsed)

def separate_chapters(book_name = 'book'):
    global book_path; book_path = os_path.join('book', book_name + '.txt')
    sources = sorted(glob.glob('book/*.htm*'))
    with codecs.open(book_path, 'w', 'ascii') as outfile:
        for source in sources:
            print source
            outfile.write('\n\n--------------------\n[' + source + ']\n\n')
            parsed, next = process_page(source)
            parsed = unicode(parsed)
            parsed = downgrade_to_ascii(parsed)
            outfile.write(parsed)
    
if __name__ == '__main__':
    separate_books()
