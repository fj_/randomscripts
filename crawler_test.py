# -*- coding: utf-8 -*-

import doctest, unittest
import crawler
import time 
from StringIO import StringIO
from utils import inject_attributes, run_tests
import contextlib
from pprint import pprint, pformat

test_dir_name = '!test_dir'

    
    
class TestCrawler(unittest.TestCase):
    test_site_src = {
'http://user:pass@example.com/stuff/%d1%82%d0%b5%d1%81%d1%82/' : """
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
 <head>
  <title>Index of /stuff/тест</title>
 </head>
 <body>
<h1>Index of /stuff/тест</h1>
<table><tr><th><img src="/icons/blank.gif" alt="[ICO]"></th><th><a href="?C=N;O=D">Name</a></th><th><a href="?C=M;O=A">Last modified</a></th><th><a href="?C=S;O=A">Size</a></th><th><a href="?C=D;O=A">Description</a></th></tr><tr><th colspan="5"><hr></th></tr>
<tr><td valign="top"><img src="/icons/back.gif" alt="[DIR]"></td><td><a href="/stuff/">Parent Directory</a></td><td>&nbsp;</td><td align="right">  - </td></tr>
<tr><td valign="top"><img src="/icons/folder.gif" alt="[DIR]"></td><td><a href="%d1%82%d0%b5%d1%81%d1%821/">тест1/</a></td><td align="right">10-Jan-2010 16:46  </td><td align="right">  - </td></tr>
<tr><td valign="top"><img src="/icons/folder.gif" alt="[DIR]"></td><td><a href="~%d1%82%d0%b5%d1%81%d1%822/">~тест2/</a></td><td align="right">10-Jan-2010 16:50  </td><td align="right">  - </td></tr>
<tr><th colspan="5"><hr></th></tr>
</table>
<address>Apache/2.2.9 (Debian) PHP/5.2.6-1+lenny3 with Suhosin-Patch Server at example.com Port 80</address>
</body></html>
""",

'http://user:pass@example.com/stuff/%d1%82%d0%b5%d1%81%d1%82/%d1%82%d0%b5%d1%81%d1%821/' : """
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
 <head>
  <title>Index of /stuff/тест/тест1</title>
 </head>
 <body>
<h1>Index of /stuff/тест/тест1</h1>
<table><tr><th><img src="/icons/blank.gif" alt="[ICO]"></th><th><a href="?C=N;O=D">Name</a></th><th><a href="?C=M;O=A">Last modified</a></th><th><a href="?C=S;O=A">Size</a></th><th><a href="?C=D;O=A">Description</a></th></tr><tr><th colspan="5"><hr></th></tr>
<tr><td valign="top"><img src="/icons/back.gif" alt="[DIR]"></td><td><a href="/stuff/%d1%82%d0%b5%d1%81%d1%82/">Parent Directory</a></td><td>&nbsp;</td><td align="right">  - </td></tr>
<tr><th colspan="5"><hr></th></tr>
</table>
<address>Apache/2.2.9 (Debian) PHP/5.2.6-1+lenny3 with Suhosin-Patch Server at example.com Port 80</address>
</body></html>
""",

'http://user:pass@example.com/stuff/%d1%82%d0%b5%d1%81%d1%82/~%d1%82%d0%b5%d1%81%d1%822/' : """
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
 <head>
  <title>Index of /stuff/тест/~тест2</title>
 </head>
 <body>
<h1>Index of /stuff/тест/~тест2</h1>
<table><tr><th><img src="/icons/blank.gif" alt="[ICO]"></th><th><a href="?C=N;O=D">Name</a></th><th><a href="?C=M;O=A">Last modified</a></th><th><a href="?C=S;O=A">Size</a></th><th><a href="?C=D;O=A">Description</a></th></tr><tr><th colspan="5"><hr></th></tr>
<tr><td valign="top"><img src="/icons/back.gif" alt="[DIR]"></td><td><a href="/stuff/%d1%82%d0%b5%d1%81%d1%82/">Parent Directory</a></td><td>&nbsp;</td><td align="right">  - </td></tr>
<tr><td valign="top"><img src="/icons/text.gif" alt="[TXT]"></td><td><a href="%d1%82%d0%b5%d1%81%d1%82.txt">тест.txt</a></td><td align="right">10-Jan-2010 16:47  </td><td align="right">  4 </td></tr>
<tr><th colspan="5"><hr></th></tr>
</table>
<address>Apache/2.2.9 (Debian) PHP/5.2.6-1+lenny3 with Suhosin-Patch Server at example.com Port 80</address>
</body></html>
""",

'http://user:pass@example.com/stuff/%d1%82%d0%b5%d1%81%d1%82/~%d1%82%d0%b5%d1%81%d1%822/%d1%82%d0%b5%d1%81%d1%82.txt' 
: 'test' 
    }
    
    def get_page(self, url):
        return contextlib.closing(StringIO(self.test_site_src[url]))
    # extremely incomplete FS emulation (but sufficient for testing I suppose)
    def path_exists(self, path):
        return path in self.dirs or path in self.files
    def makedirs(self, path):
        self.dirs.add(path)
    def urlretrieve(self, url, path, hook = None):
        # code below takes 1 second and can be checked visually only. When uncommented.
        if hook:
	        for i in range(10):
	            time.sleep(0.1)
	            hook(i, 1024, 10000)   
        self.files[path] = self.test_site_src[url]
        
    
    def setUp(self):
        self.dirs = set()
        self.files = {}
        self.restore = inject_attributes(crawler, 
            urlopen = self.get_page,
            path_exists = self.path_exists,
            makedirs = self.makedirs,
            urlretrieve = self.urlretrieve)
        
    def tearDown(self):
        self.restore()
        
    def test_walk(self):
        res = list(crawler.walk('http://user:pass@example.com/stuff/%d1%82%d0%b5%d1%81%d1%82/',
                                '.',
                                crawler.simple_extractor))
        expected =\
			[('http://user:pass@example.com/stuff/%d1%82%d0%b5%d1%81%d1%82/',
			  '.',
			  [('http://user:pass@example.com/stuff/%d1%82%d0%b5%d1%81%d1%82/%d1%82%d0%b5%d1%81%d1%821/',
			    'тест1/'),
			   ('http://user:pass@example.com/stuff/%d1%82%d0%b5%d1%81%d1%82/~%d1%82%d0%b5%d1%81%d1%822/',
			    '~тест2/')],
			  []),
			 ('http://user:pass@example.com/stuff/%d1%82%d0%b5%d1%81%d1%82/%d1%82%d0%b5%d1%81%d1%821/',
			  '.\\тест1/',
			  [],
			  []),
			 ('http://user:pass@example.com/stuff/%d1%82%d0%b5%d1%81%d1%82/~%d1%82%d0%b5%d1%81%d1%822/',
			  '.\\~тест2/',
			  [],
			  [('http://user:pass@example.com/stuff/%d1%82%d0%b5%d1%81%d1%82/~%d1%82%d0%b5%d1%81%d1%822/%d1%82%d0%b5%d1%81%d1%82.txt',
			    'тест.txt')])]

        for a, b in zip(res, expected):
            self.assertEqual(a, b, pformat((a, b)))
            
    def test_dump(self):
        walker = crawler.walk('http://user:pass@example.com/stuff/%d1%82%d0%b5%d1%81%d1%82/',
                              '.',
                              crawler.simple_extractor)
        crawler.dump(walker)
        self.assertEqual(self.dirs, set((u'.', u'.\\тест1/', u'.\\~тест2/')))
        self.assertEqual(self.files, 
                         {u'.\\~тест2/тест.txt': 'test'})

def run():
    run_tests(doctest.DocTestSuite(crawler),
              unittest.TestLoader().loadTestsFromTestCase(TestCrawler),
              exit = True)
    

if __name__ == '__main__':
    run()
