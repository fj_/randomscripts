from collections import defaultdict
import vobject

def read(fname):
    with open(fname, 'rb') as f:
        return list(vobject.readComponents(f, validate=True))

def write(fname, components):
    with open(fname, 'wb') as f:
        for it in components:
            it.serialize(f)

class Warning(object):
    def __init__(self, msg):
        self.msg = msg
    def serialize(self, f):
        f.write('\r\n!!! ' + self.msg + '\r\n') 

def get_single_value(v, name):
    items = v.contents[name]
    assert len(items) == 1
    return items[0].value

def main():
    vcards = read('tmp/00003.vcf')
    assert all(it.name == 'VCARD' for it in vcards)
    tel_map = defaultdict(list)
    result = []
    unique_reprs = set()
    for v in vcards:
        # filter exact duplicates
        r = repr(v)
        if r in unique_reprs:
            continue
        unique_reprs.add(r)
        
        # fix weird names
        fn = get_single_value(v, 'fn')
        if fn.endswith('6'):
            n = get_single_value(v, 'n')
            assert str(n).strip() == fn
            assert n.given == fn
            fn = fn.rstrip('6')
            n.given = fn # patches inplace
            v.contents['fn'][0].value = fn
#             v.prettyPrint()
        
        tel = v.contents.get('tel')
        if not tel:
            # no telephone, some weird contact
            result.append(v)
            continue
            
        assert len(tel) == 1 # no PITA with many-to-many mapping
        for t in tel:
            tel_map[t.value].append(v)
    
    rest = []
    for t, v in tel_map.iteritems():
        if len(v) > 1:
            result.append(Warning('{} Duplicates for number {}'.format(len(v), t)))
            result.extend(v)
        else:
            rest.extend(v)
    result.append(Warning('{} OK entries follow'.format(len(rest))))
    result.extend(rest)

    write('tmp/00004.vcf', result)
        

if __name__ == '__main__':
    print 'Start'
    main()
    print 'Done'
    