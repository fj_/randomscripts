from __future__ import division
import psyco
psyco.full()

from itertools import *
from math import *
import pickle
import os

#p = [[0, 0.5, 0.5, 0], [0.5, 0, 0, 0.5], [0.5, 0, 0, 0.5], [0, 0.5, 0.5, 0]]
#p = ((0.1, 0.3, 0.2, 0.4),
#	 (0.4, 0.2, 0.3, 0.1),
#	 (0.6, 0.1, 0.2, 0.1),
#	 (0.25, 0.25, 0.25, 0.25))
#lp = len(p)

width = height = 30
msize = width * height
iterations = 50


def mul(lst):
	r = 1.0
	for i in lst: r *= i
	return r

#print sum(mul(1 - p[j][i] for j in range(lp)) for i in range(lp))

#ptotal = 0
#expectedvalue = 0
#for indices in product(range(lp), repeat = lp):
#	prob = mul(p[i][indices[i]] for i in range(lp))
#	ptotal += prob
#	empty = lp - len(set(indices))
#	expectedvalue += empty * prob
	
#print expectedvalue

def printSometimes(d, modulo = 64, formatstring = "%d"):
	if d % modulo == 0:
		print formatstring % d

def xy2i(x, y):
	if 0 <= x < width and 0 <= y < height:
		return x + y * width

def genProbs(starti):
	def makeMatrix():
		return [0.0 for _ in range(msize)]
	m0 = makeMatrix()
	m0[starti] = 1
	for _ in range(iterations):
		m1 = makeMatrix()
		for x in range(width):
			for y in range(height):
				d = m0[xy2i(x, y)]
				adds = []
				cnt = [0]
				def addTo(dx, dy):
					index = xy2i(x + dx, y + dy)
					if index is not None:
						adds.append(index)
						cnt[0] += 1
				addTo( 1,  0) 
				addTo( 0,  1) 
				addTo(-1,  0) 
				addTo( 0, -1)
				p = d / cnt[0]
				for i in adds:
					m1[i] += p
		m0 = m1
	return m0

fname = 'probs.p'

if not os.path.exists(fname):
	probs = []
	for i in range(msize):
		probs.append(genProbs(i))
		printSometimes(i, 10)
		
	pickle.dump(probs, open(fname, "wb"), 2)
else:
	probs = pickle.load(open(fname, "rb"))
	
print sum(mul(1 - probs[j][i] for j in range(msize)) for i in range(msize))
