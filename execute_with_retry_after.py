#!/usr/bin/env python3
from contextlib import contextmanager


def execute_with_retry_after(fix_things):
    def wrapper(f):
        with fix_things():
            f()
            return
        f()
    return wrapper


@contextmanager
def recreate_mirror():
    try:
        yield
    except Exception as exc:
        print('Recreating mirror')


def execute_with_retry_after2(fix_things):
    def wrapper(f):
        try:
            f()
            return
        except Exception as exc:
            fix_things(exc)
        f()
    return wrapper

def recreate_mirror2(exception):
    if isinstance(exception, TypeError):
        print('Recreating mirror')
    else:
        raise

# @execute_with_retry_after(recreate_mirror)
# def _():
#     print('Trying')
#     raise Exception('tried')


@execute_with_retry_after2(recreate_mirror2)
def _():
    print('Trying')
    raise Exception('tried')
