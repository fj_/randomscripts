from itertools import permutations

people = range(4)
Jane, Alison, Ben, David = people
Botlang, Python, Java, Ruby = people

def who(lst, what):
    return lst.index(what)

# for example, finish[person] says that person finished nth
for finish in permutations(people):
    for language in permutations(people):
        if (
                # Ben wrote his own language: Bot.lang
                language[Ben] == Botlang and
                # David will finish 20 minutes before Alison
                finish[David] + 1 == finish[Alison] and
                # Alison will finish sometime before the dev using Bot.Lang
                finish[Alison] < finish[who(language, Botlang)] and
                #The developer coding with Ruby will finish 20 minutes after the dev using Java.
                finish[who(language, Ruby)] - 1 == finish[who(language, Java)] and
                # The four devs are: the one finishing 3rd, the one using Bot.Lang, the one using Java and Alison
                # note: finish is 0-based.
                4 == len({who(finish, 2), who(language, Botlang), who(language, Java), Alison})
                ):
            print(finish, language)
            res = (finish, language)

key = [ 'H9J8 4XKX OQ27 9J41'.split(),
        'A0AF SQKB FT6Y 5K3L'.split(),
        'G96G AHA0 D0MR KEDU'.split()]

def encode(finish, language):
    for p in people:
        yield key[0][finish[p]][p]
    for p in people:
        yield key[1][finish[p]][language[p]]
    for p in people:
        yield key[2][language[p]][p]

print(''.join(encode(*res)))
