from fractions import Fraction
from itertools import combinations

def simulate(players, evils):
    res = 0
    n = 0
    def neighbors(x, lst):
        lst.append((x - 1) % players)
        lst.append((x + 1) % players)

    for epos in combinations(range(players), evils):
        lst = []
        for p in epos:
            neighbors(p, lst)
        n += 1
        res += len(frozenset(lst))
    return Fraction(res, n * players)

def do(p, e):
    loss = simulate(p, e)
    print(f'{e} evil of {p}:\t{loss}\t{float(loss)}')

for p in range(7, 14):
    e = (p - 1) // 3
    do(p, e)

do(4, 2)







