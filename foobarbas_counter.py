digits = ('foo', 'bar', 'bas')

def from_dcb(bcd, base):
    'decimal-coded-base, actually :)'
    return sum((base ** n) * c for n, c in enumerate(split_base_iter(bcd, 10))) 

def split_base_iter(n, base):
    while n:
        n, m = divmod(n,base)
        yield m

def suffix(order):
    if order == 0: return ''
    if order == 1: return 'by'
    return 'b' + 'id' * (order - 2) + 'ity'
    

def foobarbas_repr(n, base = 4):
    assert n >= 0
    assert 2 <= base <= 4
    if n == 0:
        return 'xip'
    lst = [digits[d - 1] + suffix(n) 
            for n, d in enumerate(split_base_iter(n, base))
            if d]
    lst.reverse()
    return '-'.join(lst)

def main():
    test_data = [
            (0, 'xip'),
            (1, 'foo'),
            (2, 'bar'),
            (3, 'bas'),
            (10, 'fooby'),
            (11, 'fooby-foo'),
            (12, 'fooby-bar'),
            (13, 'fooby-bas'),
            (20, 'barby'),
            (21, 'barby-foo'),
            # ...
            (33, 'basby-bas'),
            (100, 'foobity'),
            (101, 'foobity-foo'),
            # ...
            (110, 'foobity-fooby'),
            (111, 'foobity-fooby-foo'),
            # ...
            (1000, 'foobidity'),
            # ...
            (10000, 'foobididity'),
            (30201, 'basbididity-barbity-foo'),
            ]
    for k, v in test_data:
        real = from_dcb(k, 4)
        my = foobarbas_repr(real)
        print '{}({}) : "{}" "{}"'.format(k, real, my, v)
        assert my == v


if __name__ == '__main__':
    main()
