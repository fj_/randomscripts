import tkinter as tk
import tkinter.font as font

SAMPLE_TEXT = '''Pedicabo ego vos et irrumabo,
Aureli pathice et cinaede Furi,
Qui me ex uersiculis meis putastis,
Quod sunt molliculi, parum pudicum.
Nam castum esse decet pium poetam
Ipsum, uersiculos nihil necesse est,
Qui tum denique habent salem ac leporem,
Si sunt molliculi ac parum pudici
Et quod pruriat incitare possunt,
Non dico pueris, sed his pilosis,
Qui duros nequeunt mouere lumbos.
Vos quod milia multa basiorum
Legistis, male me marem putatis?
Pedicabo ego vos et irrumabo.'''


class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.grid()
        self.createWidgets()

    def createWidgets(self):
        self.main_text = tk.Text(self)
        self.main_text.grid(columnspan=99)

        # Anal touring
        default_font = font.Font(name=self.main_text.config()['font'][4], exists=True)
        bold_font = default_font.copy()
        bold_font.config(weight='bold')

        self.main_text.tag_config('bold', font=bold_font)

        v = tk.StringVar()
        v.trace('w', lambda *args: self.mark())
        self.search_entry = tk.Entry(self, textvariable=v)
        self.search_entry.grid(row=1, column=0, columnspan=2)
        self.search_entry.v = v

        self.mark_button = tk.Button(self, text='Mark',
            command=self.mark)
        self.mark_button.grid(row=1, column=2)

        self.mark_button = tk.Button(self, text='Export',
            command=self.exportmd)
        self.mark_button.grid(row=1, column=3)

        self.quit_button = tk.Button(self, text='Quit',
            command=self.quit)
        self.quit_button.grid(row=1, column=4)

        self.main_text.insert(tk.END, SAMPLE_TEXT)
        self.search_entry.insert(0, 'iesus')


    def mark(self):
        t = self.main_text
        contents : str = t.get(1.0, tk.END)
        t.tag_remove('bold', 1.0, tk.END)

        searchstr = self.search_entry.get()
        searchstr = searchstr.lower()

        pos = 0
        cnt = 0
        for c in searchstr:
            pos = contents.find(c, pos)
            if pos < 0:
                break
            t.tag_add('bold', f'1.0 + {pos} chars')
            pos += 1
            cnt += 1
        print(f'{cnt}/{len(searchstr)} found')


    def exportmd(self):
        t = self.main_text

        lst = ['1.0']
        lst.extend(t.tag_ranges('bold'))
        lst.append(tk.END)

        it = iter(lst)
        res = []
        end = next(it)
        while True:
            # unbolded text
            start, end = end, next(it)
            res.append(t.get(start, end))
            # bolded text
            start, end = end, next(it, None)
            if end is None: break
            res.append('**')
            res.append(t.get(start, end).upper())
            res.append('**')
        print(''.join(res))








app = Application()
app.master.title('Sample application')
app.mainloop()
