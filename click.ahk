toggle := false
^F1::
if(toggle) {
    toggle := false
    SetTimer, click, Off
} else {
    toggle := true
    ; Choose a delay here!
    SetTimer, click, 150 
}
return

click:
SendInput, {Click}
Return
