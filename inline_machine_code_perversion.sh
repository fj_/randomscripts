#!/bin/sh

set -o errexit

if [ ! -f perversion.s ] ; then cat >perversion.s <<EOF
USE32

push byte 0x74
pop eax
sub al, 0x70 ; eax = 04, also set up flags for jnz

jnz hello_string
got_string_address:
pop ecx         ; address of the string

push byte 0x77
push esp        ; address of the byte we will decode
pop edi         ; in edi
pop esi         ; esi = the value to xor with

;push byte 0x44
;xor [edi], esi  
;pop eax         ; eax = 04h

push byte 0x76
xor [edi], esi  
pop ebx         ; ebx = 01h

push byte 0x66
xor [edi], esi  
pop edx         ; edx = 14h



int 0x80

push byte 0x76
xor [edi], esi  
pop eax         ; eax = 01h
push eax
pop ebx
dec ebx         ; ebx = 00h
int 0x80

db ' =) '       ; filler to make the jump offset fit in ASCII

hello_string:
call got_string_address
db 'Hello, World!',10
EOF
fi

# assemble and show disassembly
nasm perversion.s -o perversion.o -f elf 
objdump -M intel -d perversion.o 

# assemble to flat file and convert to string
nasm perversion.s -o perversion.raw
(python <<"EOF"
known = dict([(eval('"%s"' % s), s) for s in ['\\' + c for c in 'abfnrtv']])
known['\\'] = '\\\\'
known['"'] = '\\"'
def encode(c, nc):
    if c in known: return known[c]
    if 32 <= ord(c) <= 127: return c
    if nc.isdigit(): return '\\%03o' % ord(c) # octal
    return '\\x%02x' % ord(c)
data = open('perversion.raw', 'rb').read()
print 'const char main[]="%s";\n' % ''.join(map(encode, data, data[1:]))
EOF
) | tee perversion.c

# compile the result!
gcc perversion.c -o perversion

