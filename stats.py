from collections import Counter
import re

source1 = 'Esp qtcde nzyqpcpynp zy esp ezatn zq Lcetqtntlw Tyepwwtrpynp hld spwo le Olcexzfes Nzwwprp ty estd jplc.'

source2 ='''
|de|  | f|Cl|nf|ed|au| i|ti|  |ma|ha|or|nn|ou| S|on|nd|on|
|ry|  |is|th|is| b|eo|as|  |  |f |wh| o|ic| t|, |  |he|h |
|ab|  |la|pr|od|ge|ob| m|an|  |s |is|el|ti|ng|il|d |ua|c |
|he|  |ea|of|ho| m| t|et|ha|  | t|od|ds|e |ki| c|t |ng|br|
|wo|m,|to|yo|hi|ve|u | t|ob|  |pr|d |s |us| s|ul|le|ol|e |
| t|ca| t|wi| M|d |th|"A|ma|l |he| p|at|ap|it|he|ti|le|er|
|ry|d |un|Th|" |io|eo|n,|is|  |bl|f |pu|Co|ic| o|he|at|mm|
|hi|  |  |in|  |  | t|  |  |  |  |ye|  |ar|  |s |  |  |. |
'''

def parse_s2(source2):
    lines = source2.strip().split('\n')
    lines = [line.split('|')[1:-1] for line in lines]
    return map(None, *lines) # transpose

def all_same(items):
    it = iter(items)
    first = next(it, None)
    return all(x == first for x in it)

def normalize(text):
    table = [' '] * 256
    shift = ord('A') - ord('a')
    for i in range(ord('a'), ord('z') + 1):
        table[i] = chr(i)
        table[i + shift] = chr(i)
    table = ''.join(table)
    text = text.translate(table)
    return ' '.join(text.split())

def letter_frequency(text):
    everything = [chr(i) for i in range(ord('a'), ord('z') + 1)]
    everything.append(' ')
    counter = Counter(everything) 
    counter.update(text)
    total = sum(counter.values())
    return {k : float(v) / total for k, v in counter.iteritems()}


def frequency_diff(f1, f2):
    return sum((f2[k] - v) ** 2 for k, v in f1.items())

def rotN_frequency(src, n):
    def rot(letter):
        if 'a' <= letter <= 'z':
            return chr(ord('a') + (ord(letter) - ord('a') + n) % 26)
        return letter
    return {rot(k) : v for k, v in src.items()}

def rotN_decode(src, n):
    def rot(letter):
        if 'a' <= letter <= 'z':
            return chr(ord('a') + (ord(letter) - ord('a') + n) % 26)
        if 'A' <= letter <= 'Z':
            return chr(ord('A') + (ord(letter) - ord('A') + n) % 26)
        return letter
    return ''.join(map(rot, src))
    
def part1(reference_text):
    ref_f = letter_frequency(reference_text)
    s_f = letter_frequency(normalize(source1))
    score, n = min((frequency_diff(ref_f, rotN_frequency(s_f, n)), n)
            for n in range(26))
    print score, n
    print rotN_decode(source1, n)
    
def letter2_frequency(text):
    everything = [chr(i) for i in range(ord('a'), ord('z') + 1)]
    everything.append(' ')
    everything = [a + b for a in everything for b in everything]
    counter = Counter(everything)

    def twograms():
        for i in xrange(len(text) - 1):
            yield text[i] + text[i + 1]
 
    counter.update(twograms())
    total = sum(counter.values())
    return {k : float(v) / total for k, v in counter.iteritems()}

def strip_diff(strip1, strip2):
    def iter_scores():
        for item1, item2 in zip(strip1, strip2):
            c1, c2 = item1[-1], item2[0]
            # A tricksy moment: we should ignore space-to-space comparisons produced by
            # text wrapping (since we do not have statistics for them),
            # but do not ignore things like 'a ':' b' (impossible) 
            # or ' l':'  ' (compare normally).
            if c1 == c2 == ' ':
                if not (item1.isspace() or item2.isspace()):
                    yield 0.0
                continue
            
    scores = list(iter_scores())
    return reduce(__scores)

def part2(reference_text):
    ref_f = letter2_frequency(reference_text)
    strips = parse_s2(source2)
    height = len(strips[0])
    assert all(len(s) == height for s in strips)
    empty = ['  '] * height
    for s in strips:
        print strip_diff(empty, s), s
     
    
        
        
    


def main():
    with open('book/Orson Scott Card - The Lost Boys.txt', 'r') as f:
        reference_text = normalize(f.read())
#    part1(reference_text)
    part2(reference_text)
    
    

if __name__ == '__main__':
    main()

