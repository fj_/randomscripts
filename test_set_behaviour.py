import sys
import timeit
import gc

with_gc = False

def allocmem(len):
    if with_gc: gc.enable()
    lst = []
    for i in xrange(len):
        lst.append((i,))
    return lst 

def run(n):
    print timeit.timeit('allocmem(%d)' % n, 
                        'from __main__ import allocmem', 
                        number=1);

for n in [1000000, 5000000, 10000000]:
    with_gc = False
    run(n)
    with_gc = True
    run(n)           
                    
sys.exit()


# the original version
from time import clock
from timeit import Timer

def generator(N):
    return ((x,y,z) for z in xrange(1,N+1) for y in xrange(z+1) for x in xrange(y+1))

def clockenize(f):
    def wrapped(*args, **kwargs):
        if with_gc: gc.enable() # HAHAHAHA!
        t = clock()
        res = f(*args, **kwargs)
        print '%0.3f' % (clock() - t)
        return res
    return wrapped
    

@clockenize
def benchmark0(N):
    return set([t for t in generator(N)])

@clockenize
def benchmark1(N):
    return set(generator(N))

@clockenize
def benchmark2(N):
    return set(list(generator(N)))

def test_benchmark(N, benchmark):
    t = Timer("test(%d)" % N, "from test_set_behaviour import benchmark%d as test" % benchmark)
    print "%0.3f" % t.timeit(1)

if __name__ == '__main__':
    for i in range(6):
        test_benchmark(300, i % 3)
        
        
