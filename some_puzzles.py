from __future__ import division
from math import sqrt, factorial, sin, cos, tan
from contextlib import contextmanager
import functools
import itertools, sys
print 'start'

@contextmanager
def time_it():
    from time import clock
    t = clock()
    yield
    print 'Done in {:.3f} seconds'.format(clock() - t)
    

def memoize(f):
    class memodict(dict):
        def __init__(self, f):
            self.f = f
        def __call__(self, *args):
            return self[args]
        def __missing__(self, key):
            ret = self[key] = self.f(*key)
            return ret
    return memodict(f)

# for x, y, z in itertools.product(*[xrange(9)] * 3):
#     if x * 100 + y * 10 + z == z * 81 + y * 9 + x:
#         print x, y, z

# a, b = 2 ** 6, 5 ** 6
# print a, b, a * b

# def generate():
#     digits = '9876543210'
#     for l in xrange(1, 5):
#         for s in itertools.combinations(digits, l):
#             s = ''.join(s)
#             r = ''.join(c for c in digits if c not in s)
#             s, r = int(s), int(r)
#             yield s * r, s, r
#              
# print max(generate())

# digits = '987654321'
# for s in itertools.combinations(digits, 4):
#     for s in itertools.permutations(s):
#         s = ''.join(s)
#         for r in itertools.permutations(c for c in digits if c not in s):
#             s, r = int(s), int(''.join(r))
#             if r == s * 3:
#                 print s, '/', r 


# cnt, total = 0, 0
# for x, y, z in itertools.product(*[xrange(1, 7)] * 3):
#     total += 1
#     cnt += x * y * z % 2
# 
# print cnt / total

# def generate():
#     for x in xrange(1, 10001):
#         for n in str(x).replace('0', ' ').split():
#             yield int(n)
# print sum(generate())

# # humans = 72 - dogs
# # legs = 4 * dogs + 2 * (72 - dogs)
# # legs = 2 * dogs + 144
# # dogs = (legs - 144) / 2
# dogs = (200 - 144) / 2
# humans = 72 - dogs
# print dogs, dogs * 4 + humans * 2

class ApproximateFloat(float):
    __slots__ = ()
    def __hash__(self):
        return round(self)
    
    def __eq__(self, other):
        assert isinstance(other, ApproximateFloat) # to be sure that the algorithm itself works properly
        return abs(self - other) <= max(abs(self), abs(other)) * 10**-6
    
    def __ne__(self, other):
        return not self == other
    
    def __le__(self, other):
        return self == other or float(self) < float(other)
    
    def __lt__(self, other):
        return self != other and float(self) < float(other)
    
    def __ge__(self, other):
        return self == other or float(self) > float(other)
    
    def __gt__(self, other):
        return self != other and float(self) > float(other)

global_exp_cache = {} # avoid recomputing stuff

@memoize
def compute_ops(op_count, available_numbers):
    assert len(available_numbers)
    if op_count == 0:
        return dict((ApproximateFloat(n), str(n)) for n in available_numbers)
                     
    result = {}
    def add_expression(compute, representation):
        try:
            r = ApproximateFloat(compute())
        except:
            return
        if r is None: return
        if abs(r) > 10**9: return
        
#         if r not in result:
#             result[r] = representation
        
        # this makes it slightly faster
        global_key = (available_numbers, r)
        if global_key not in global_exp_cache:
            global_exp_cache[global_key] = representation 
            result[r] = representation

    # unary ops.
    for x, expression in compute_ops(op_count - 1, available_numbers).iteritems():
        add_expression(lambda: -x, '-(' + expression + ')')
        add_expression(lambda: sqrt(x), 'sqrt(' + expression + ')')
        add_expression(lambda: factorial(x) if float(x) < 20 else None, '(' + expression + ')!')
    # binary ops
    for first_op_count in xrange(op_count):
        second_op_count = op_count - first_op_count - 1
        for available_num_count_x in xrange(1, len(available_numbers)):
            for available_nums_x in itertools.combinations(available_numbers, available_num_count_x):
                available_nums_x = frozenset(available_nums_x)
                available_nums_y = frozenset(available_numbers - available_nums_x)
                for x, exp_x in compute_ops(first_op_count, available_nums_x).iteritems():
                    for y, exp_y in compute_ops(second_op_count, available_nums_y).iteritems():
                        add_expression(lambda: x + y, '(' + exp_x + ' + ' + exp_y + ')')
                        add_expression(lambda: x - y, '(' + exp_x + ' - ' + exp_y + ')')
                        add_expression(lambda: x * y, '(' + exp_x + ' * ' + exp_y + ')')
                        add_expression(lambda: x / y, '(' + exp_x + ' / ' + exp_y + ')')
    return result

def find_expr(x, available_ops):
    x = ApproximateFloat(x)
    available_ops = frozenset(available_ops)
    for i in xrange(6):
        r = compute_ops(i, available_ops)
        if x in r:
            return r[x]
    compute_ops.clear()
        
print find_expr(71, [1, 7])
print find_expr(19, [1, 2, 3])

print 'end'
