from cStringIO import StringIO
import re
import sys
import operator
import functools
from contextlib import contextmanager
from decimal import self

'''
This was an attempt to make a Forth-like system, but instead of compile/immediate
mode switch between "interpreting" and "skipping" modes. The interpreter should
store the entire input stream intact and use it for interpretation directly. For
example, word definition (":") creates a word entry which points just after the
colon and switches to the "skipping" mode until the ";" word is encountered, 
which is marked as IMMEDIATE (i.e. executes both in skipping and in normal mode)
and it completes the definition in skipping mode or returns from the call in 
normal mode. All other constructs like "... if ... else ... then" were thought to
be defined as IMMEDIATE as well.

First problem: I need a complex protocol for working with control structures, 
which all should be lexically symmetrical so to speak. For example, implementing 
a word for premature exit from a procedure is no trivial task: it can't just return
because the special stack is full of stuff left by IFs etc, so it should somehow
switch the mode to "skipping" until the end of the function, but then every closing
part of a control structure would try to switch mode back...

Second problem: I can't see how I could implement basic input primitives
in FORTH. Because my Instruction Pointer _is supposed to be_ the current input 
position. This sucks, also without that blocking execution of the VM until more
input is available (i.e. returning from the execute_string function) is going to
be quite ugly.

For the same reason I don't understand how words that read words from the input 
stream are supposed to behave in general. Should they maybe advance the stored
IP of the calling word? Then it's not composable.
'''


'''
Stack visually grows to the right. I.e. subtraction operator has stack effect
described as (a b -- a-b) and "5 2 -" produces 3.

Python functions are converted to words in the same way, the last argument is 
taken from the top of the stack and so on. This allows using all built-in 
binary operators as-is, for example.

Jonesforth places strings on stack like ('s' 't' 'r' 3), I place a Python string
as a single item.    
''' 

re_int = re.compile(r'^\d+$')    

class Word(object):
    def __init__(self, name, codeword, codeptr = None, dataptr = None):
        self.name = name
        self.codeword = codeword
        self.codeptr = codeptr
        self.dataptr = dataptr
        self.immediate = False
    def __call__(self):
        self.codeword(self)
    def __str__(self):
        return 'Word{}({!r}, {!r}, {}, {})'.format(
                '<IMMEDIATE>' if self.immediate else '', 
                self.name, self.codeword, self.codeptr, self.dataptr)
        
class Machine(object):
    def __init__(self, default_output = sys.stdout, init_base_dictionary = True):
        self.stack = [] # data stack
        self.rstack = [] # return stack
        self.executing = True 
        self.executing_stack = []
        self.code = StringIO() # program code -- should only be appended to
        self.dictionary = {}
        self.latest = None
        self.calls = 0
        self.waiting_for_input = None # callback to call when input becomes available
        
        # word
        self.current_token = ''
        self.word_reader_state = 0
        
        self.output_listeners = set()
        if default_output is not None:
            self.output_listeners.add(default_output)
        if init_base_dictionary:
            self.init_base_dictionary()
    
    # output 
    def write(self, s):
        for listener in self.output_listeners:
            listener.write(s)
        
    @contextmanager
    def capture_output(self, listener):
        assert listener not in self.output_listeners
        self.output_listeners.add(listener)
        try:
            yield
        finally:
            self.output_listeners.remove(listener)

    # data stack
    def push(self, *x): self.stack.extend(x)
    def pop(self): return self.stack.pop()
    def popn(self, count):
        assert count >= 0
        if count == 0: return ()
        x = self.stack[-count:]; 
        del self.stack[-count:]; 
        return x
    

    # code methods
    @property
    def ip(self):
        return self.code.tell()
    @ip.setter
    def ip(self, ip):
        self.code.seek(ip)
        
    def get_char(self):
        return self.code.read(1)
    
    def add_word(self, name, codeword, codeptr = None, dataptr = None):
        w = Word(name, codeword, codeptr, dataptr)
        self.dictionary[name] = w
        self.latest = w

    def init_base_dictionary(self):
        self.dictionary.clear()
        # helper methods for native functions
        def get_arity(f):
            return f.__code__.co_argcount
        
        def pyfunc(name, arity = None):
            '''Add a native function which returns a single result.
            Can be used as decorator or called in the decorator fashion, like
            pyfunc(name)(function).
            
            Returns the _undecorated_ version of the function!'''
            def curried(f):
                _ar = arity or get_arity(f)  
                @functools.wraps(f)
                def wrapper(entry):
                    args = self.popn(_ar)
                    self.push(f(*args))
                self.add_word(name, wrapper)
                return f
            return curried
        
        def pyproc(name, arity = None):
            'Add a native function which returns a (possibly empty) tuple of results or None'
            def curried(f):
                _ar = arity or get_arity(f)  
                @functools.wraps(f)
                def wrapper(entry):
                    args = self.popn(_ar)
                    res = f(*args)
                    if res: self.push(*res)
                self.add_word(name, wrapper)
                return f
            return curried

        pyproc('drop')    (lambda x: ())
        pyproc('dup')     (lambda x: (x, x))
        pyproc('swap')    (lambda x, y: (y, x))
        pyproc('over')    (lambda x, y: (x, y, x))
        pyproc('rot')     (lambda x, y, z: (y, z, x))
        pyproc('-rot')    (lambda x, y, z: (z, x, y))
    
        pyproc('2drop')   (lambda x, y: ())
        pyproc('2dup')    (lambda x, y: (x, y, x, y))
        pyproc('2swap')   (lambda x1, x2, y1, y2: (y1, y2, x1, x2))
    
        pyproc('?dup')    (lambda x: (x, x) if x else (x,))
    
        pyfunc('1+')      (lambda x: x + 1)
        pyfunc('1-')      (lambda x: x - 1)
        pyfunc('4+')      (lambda x: x + 4)
        pyfunc('4-')      (lambda x: x - 4)
        
        binary_ops = {
                '+':operator.add, '-':operator.sub, '*':operator.mul, 
                '/':operator.div, 'mod':operator.mod,
                '=':operator.eq, '!=':operator.ne, 
                '<':operator.lt, '<=':operator.le, '>':operator.gt, '>=':operator.ge,
                'and':operator.and_, 'or':operator.and_, 'xor':operator.and_, 
                }
        
        unary_ops = {
                '0=':lambda x: x == 0,  '0<':lambda x: x < 0, '0<=':lambda x: x <= 0, 
                '0<>':lambda x: x != 0, '0>':lambda x: x > 0, '0>=':lambda x: x >= 0,
                'invert': operator.invert,
                'negate':operator.neg, 'not':operator.not_,
                }
        for k, v in binary_ops.iteritems(): pyfunc(k, 2)(v)
        for k, v in unary_ops.iteritems(): pyfunc(k, 1)(v)
    
        pyproc('/mod')(lambda x, y: reversed(divmod(x, y)))
    
            
        # IO
        
        @pyproc('emit')
        def emit(c):
            self.write(chr(c))
        
        @pyproc('.')
        def dot(x):
            self.write(str(x) + ' ')

        @pyproc('key')
        def key():
            c = self.get_char()
            if not c:
                self.waiting_for_input = self.dictionary['key'] # safe way
            else:
                self.push(ord(c))
        
        @pyproc('word')
        def word():
            # skip comment
            if self.skipping_comment:
                while True:
                    c = self.get_char()
                    if not c:
                        self.waiting_for_input = self.dictionary['word']
                        return
                    if c == '\n':
                        if self.current_token:
                            res = ''.join(self.current_token)
                            del self.current_token[:]
                            return res
                        else:
                            self.skipping_comment = False
                            break
            
            # skip spaces (unless we are reading a token)
            if not self.current_token:
                c = ' '
                while c.isspace():
                    c = self.get_char()
                    if not c: 
                        self.waiting_for_input = self.dictionary['word']
                        return
                self.current_token.append(c)
            
            # read 
            while True:
                c = self.get_char()
                if not c:
                    self.waiting_for_input = self.dictionary['word']
                    return
                if c.isspace():
                    res = ''.join(self.current_token)
                    del self.current_token[:]
                    return res
                if c == '\\':
                    self.skipping_comment = True
                    return word()
                self.current_token.append(c)
        
        # infrastructure
        
        pyfunc('find')(lambda s: self.dictionary[s] if s in self.dictionary else False) # no hidden words
    
        def docol(entry):
            self.rstack.append((self.ip, self.executing))
            self.ip = entry.codeptr

            
        @pyproc('immediate')
        def immediate():
            self.latest.immediate = True
        
#        @pyproc('create')
#        def create(name):
#            self.add_word(name, docol, self.get_ip())

        @pyproc(':')
        def colon():
            assert self.executing
            name = self.get_token()
            self.add_word(name, docol, self.ip)
            self.rstack.append((-1, self.executing))
            self.executing = False
        immediate()
            
        @pyproc(';')
        def semicolon():
            ip, self.executing = self.rstack.pop()
            if ip != -1:
                # we are returning from the call
                self.ip = ip
        immediate()

        @pyproc('>r')
        def to_rstack(value):
            self.rstack.append(value)
        
        pyfunc('r>')
        def from_rstack():
            return self.rstack.pop()
        
        # I thought about calling it 'HERE', but that would be confusing
        pyfunc('get_ip')(lambda: self.ip)

        @pyproc('branch')
        def branch(addr):
            self.set_ip(addr)
        
        @pyproc('0branch')
        def zbranch(addr, cond):
            if not cond:
                self.ip = addr

        @pyproc('if')
        def _if():
            if self.executing:
                cond = bool(self.pop())
                to_rstack(cond) # save condition -- for 'else' part
                to_rstack(self.executing)
                self.executing = cond
            else:
                to_rstack(self.executing)
        immediate()

        @pyproc('else') # should be implementable in Forth
        def _else():
            self.executing = from_rstack()
            if self.executing:
                cond = from_rstack()
                self.push(not cond)
                _if()
            else:
                to_rstack(self.executing)
        immediate()
                
        @pyproc('then')
        def then():
            self.executing = from_rstack()
            if self.executing:
                from_rstack() # condition
        immediate()
            
        
        @pyproc('(')
        def begin_comment():
            nesting = 1
            while nesting:
                token = self.get_token() # don't support end of input for now!
                if token == '(':
                    nesting += 1
                elif token == ')':
                    nesting -= 1
        immediate()
        
        with open('dforth.f', 'r') as f:
            self.execute_string(f.read())

    #def terminate():
    #    if stack:
    #        sys.stdout.flush()
    #        sys.stderr.flush()
    #        print >>sys.stderr, 'Terminated with nonempty stack', stack
    #        sys.stderr.flush()
    #    print 'Done!'
    #    sys.exit()

    def execute_string(self, s):
        assert not self.get_char()
        ip = self.ip
        self.code.write(s)
        self.code.write('\n')
        self.ip = ip
        while self.calls < 1000:
            if self.waiting_for_input:
                tmp = self.waiting_for_input
                self.waiting_for_input = None
                tmp()
            if self.waiting_for_input: break
            
            token = self.get_token()
            if not token: break
            
            print 'TRACE: {:03} {} {!r} {!r}  {!r}'.format(
                    self.ip,
                    ' |' if not self.executing else '',
                     token, self.stack, self.rstack)
            
            word = self.dictionary.get(token.lower())
            if word is None:
                word = int(token)
                if self.executing:
                    self.push(word)
            else:
                if self.executing or word.immediate:
                    word()
            self.calls += 1
        
def test_cmd_output(s, answer, machine = None):
    print 'Running: ', s
    if machine is None:
        machine = Machine()
    res = StringIO()
    with machine.capture_output(res):
        machine.execute_string(s)
        
    assert machine.waiting_for_input is None
    assert not len(machine.stack)
    assert not len(machine.rstack)
    res = res.getvalue()
    
    if res != answer:
        print 'Expected:', answer
        print 'Got:', res
        assert False
    print 'Result:', res

if __name__ == '__main__':
#    test_cmd_output('2 3 + .', '5 ')
#    test_cmd_output('2 3 - 4 * .', '-4 ')
#    test_cmd_output('73 5 /mod . .', '14 3 ')
#    
#    test_cmd_output('10 get_ip swap 1 - dup . dup -rot over swap not 0branch 2drop', 
#            '9 8 7 6 5 4 3 2 1 0 ')
#
#    test_cmd_output(': double dup + ; 10 double .', 
#            '20 ')
#    
#    test_cmd_output(': double dup + ; : quad double double ; 10 quad .', 
#            '40 ')

    test_cmd_output('''
            : fac ( n -> n! ) 
            dup if dup 1 - fac * else drop 1 then ;
            : facp fac . ; 
            0 facp 1 facp 2 facp 3 facp 4 facp''', 
            '1 1 2 6 24 ')

#    test_cmd_output('''
#            : " 
#            string_new get_ip key
#            : facp fac . ; 
#            0 facp 1 facp 2 facp 3 facp 4 facp''', 
#            '1 1 2 6 24 ')
    
    
    
    print 'done'
