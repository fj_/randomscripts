using System;
using System.IO;
using System.Collections.Generic;

class Zzz
{
	public static void Main(string[] argv)
	{
        var files = new Dictionary<string, DateTime>()
        {
            {"Morrowind.bsa", new DateTime(2002, 1, 5)},
            {"Morrowind.esm", new DateTime(2002, 1, 5)},
            {"Tribunal.bsa", new DateTime(2002, 6, 11)},
            {"Tribunal.esm", new DateTime(2002, 6, 11)},
            {"Bloodmoon.bsa", new DateTime(2003, 3, 6)},
            {"Bloodmoon.esm", new DateTime(2003, 3, 6)},
        };
        foreach (var item in files)
        {
            File.SetCreationTime(item.Key, item.Value);
            File.SetLastWriteTime(item.Key, item.Value);
            File.SetLastAccessTime(item.Key, item.Value);
        }
	}
}
	