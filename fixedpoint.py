fac = lambda recur: lambda x: lambda acc: acc if x < 2 else recur(x - 1)(x * acc)

fix = (
	lambda f: (
		(lambda g: g(g)) (
			lambda self: f(lambda x: self(self)(x))
		)
	)
)

print(fix(fac)(6)(1))

