def is_bipartite(graph): # graph is {id : [id], ...}
    colors = {} # {id : bool, ...}
    def paint(node, color):
        current_color = colors.get(node, None)
        if current_color is not None: return current_color == color
        colors[node] = color
        return all(paint(edge, not color) for edge in graph[node]) 

    return all(paint(node, True) for node in graph if node not in colors)

if __name__ == '__main__':
    assert is_bipartite({0 : [1],
                         1 : [0, 2, 8],
                         2 : [1, 3],
                         3 : [2, 6],
                         4 : [5, 7],
                         5 : [4],
                         6 : [3],
                         7 : [4, 8],
                         8 : [1, 7]})

    assert not is_bipartite(
                        {0 : [1, 2],
                         1 : [0, 2, 8],
                         2 : [1, 3],
                         3 : [2, 6],
                         4 : [5, 7],
                         5 : [4],
                         6 : [3],
                         7 : [4, 8],
                         8 : [1, 7]})

    assert is_bipartite({0 : [1],
                         1 : [0, 2, 8],
                         2 : [1, 3],
                         3 : [2, 6],
                         4 : [5, 7],
                         5 : [4],
                         6 : [3],
                         7 : [4, 8],
                         8 : [1, 7],
                         9 : []})
    
    print 'Yay!'
                         
            
                