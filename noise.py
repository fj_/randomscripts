from __future__ import division
import pyaudio
import numpy as np
import time
import scipy.io.wavfile
import matplotlib.pyplot as plt

output_rate = 44100
device = pyaudio.PyAudio()
stream = device.open(format = pyaudio.paFloat32, channels = 1, rate = output_rate, output = True)

noise_volume = 0.9
message_volume_min_offset = 0.0

def to_t(samples):
    return samples * (1.0 / output_rate)

def from_t(t):
    return int(t * output_rate)

def int16_to_float64(data):
    data = data.astype(np.float64)
    # Internet tells me this is the least incorrect way.
    return data * (1.0 / 32768)

def play(src):
    # pyaudio doesn't exactly support numpy, so I have to
    # specify the number of frames explicitly
    stream.write(src.astype(np.float32), len(src))
    stream.stop_stream() #wait for the sound to terminate

def generate_noise(samples):
#    freq = 10000.0
#    end = 2 * np.pi * freq * samples / output_rate
#    noise = np.sin(np.linspace(0, end, samples)) * noise_volume
#    return noise
    noise = np.random.rand(samples)
    return (noise - 0.5) * (2 * noise_volume)

def main1():
    freq, message = scipy.io.wavfile.read('red queen.wav')
    assert freq == output_rate
    message = int16_to_float64(message)

#    msg = message * 10
#    msg = np.maximum(message, 0)
    msg = abs(message) * 10

    msg = (1 - msg) * (1 - message_volume_min_offset) + message_volume_min_offset
    noise = generate_noise(len(message))
    noise *= msg

#    noise = np.concatenate((
#            generate_noise(from_t(2.0)),
#            noise,
#            generate_noise(from_t(1.0))))

    play(noise)

#########

def time_sample(duration):
    return np.linspace(0, duration, int(duration * output_rate), dtype=np.float32)


def tone(t, frequency, volume=1.0):
    return np.sin(2 * np.pi * frequency * t) * volume


def harmonic_power(base, freq):
    return 1.0 / (freq / base)


def harmonics(base, n):
    res = []
    for i in range(n):
        f = base * (i + 1)
        res.append((f, harmonic_power(base, f)))
    return res


def normalize(s, volume=1.0):
    return s * (volume / np.abs(s).max())


def main():
    t = time_sample(1.0)
    t_long = time_sample(3.0)
    base = 440
    base = 27.5 / 2
    def n(s):
        return normalize(s, 0.5)

    def note_with_harmonics(base, i, t=t):
        return n(np.sum(tone(t, f, v) for f, v in harmonics(base, i)))

    s = []
    s.append(note_with_harmonics(base, 8, t_long))
    # s.append(note_with_harmonics(base*2, 8, t_long) +
    #         note_with_harmonics(base*3, 8, t_long) +
    #         note_with_harmonics(base*5, 8, t_long) * 0.2)
    # s.append(note_with_harmonics(base, 1, t_long))
    # s.append(note_with_harmonics(base, 8, t_long))
    s.append(n(np.sum(tone(t_long, f, v) for f, v in harmonics(base, 8)[1:])))
    # s.append(note_with_harmonics(base*2, 8, t_long))

    # s.append(note_with_harmonics(base, 8, t_long))
    # s.append(note_with_harmonics(base*2, 8, t_long))

    # for i in range(1, 8):
    #     s.append(note_with_harmonics(base, i))

    s = np.concatenate(s)

    play(normalize(s, 0.5))


if __name__ == '__main__':
    print('hello')
    main()
    print('bye')
