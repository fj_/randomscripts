primes = []
primeind = [False, False]

for i in range(2, 32):
    for p in primes:
        if (i % p) == 0: 
            primeind.append(False);
            break;
    else:
        primes.append(i)
        primeind.append(True)

numberVectors = [[1]]

for i in range(30):
    newVector = numberVectors[-1] + [0]
    for i in reversed(range(1, len(newVector))):
        newVector[i] += newVector[i - 1]
    numberVectors.append(newVector);

def maxPower(n):
    """(n > 1) -> k, 2^k : 2^k <= n"""
    power = 0
    value = 1
    vtemp = 2
    while vtemp <= n:
        power += 1
        value = vtemp
        vtemp *= 2
    return (power, value)

def safeadd(x, y):
   return (x if x != None else 0) + (y if y != None else 0)

def getTotalNumberVector1(n):
    if n == 0: return numberVectors[0]
    power, value = maxPower(n)
    base = numberVectors[power]
    remainder = getTotalNumberVector1(n - value)
    remainder = [0] + remainder
    return map(safeadd, base, remainder)
    
    
        
def getTotalNumberVectorRecursive(n, accumulator, power):
        
    if n == 1:
        return accumulator
    else:
        n, r = divmod(n, 2)
        accumulator = [0] + accumulator
        
        if r : accumulator = map(safeadd, accumulator, numberVectors[power])
            
        return getTotalNumberVectorRecursive(n + 1, accumulator, power + 1)

def getTotalNumberVector(n):
    return getTotalNumberVectorRecursive(n, [], 0)

def getSimplePrimeBitsCount(n):
    vec = getTotalNumberVector1(n)
    result = 0
    for i, cnt in enumerate(vec):
        if primeind[i] : result += cnt
    return result;

def getPrimeBitsCount(start, end):
    c1 = getSimplePrimeBitsCount(start - 1) if start > 0 else 0
    c2 = getSimplePrimeBitsCount(end)
    return c2 - c1

for i in range(20):
    print i, getSimplePrimeBitsCount(i)

