#include <stdio.h>

typedef int (*f_ptr)(const int x, const int y);

// error: uninitialized const member `<anonymous struct>::f'
// struct {
// 	  const char * s;
//    const f_ptr f;
// } test00 = { }; 

struct {
    const char * s;
    f_ptr f; // non-const, OK too.
} test0 = { }; 

struct {
    const char * s;
    const f_ptr f;
} test1 = { "lol", NULL}; // OK

struct {
    const char * s;
    const f_ptr * f; // double ptr apparently.
} test2 = { }; // OK

struct {
    const char * s;
    const f_ptr ** f; // triple ptr?
} test3 = { }; // OK


int main(void)
{
    int x = 0;
//    x += (&(test0.f))(0, 0); // error: `(((int (**)(int, int))(&test0)) + 4u)' cannot be used as a function
    x += test0.f(0, 0);
    x += (*(test0.f))(0, 0);
    x += (**(test0.f))(0, 0);
    x += (***(test0.f))(0, 0);

//    x += (&(test1.f))(0, 0); // error: `(((int (* const*)(int, int))(&test1)) + 4u)' cannot be used as a function
    x += test1.f(0, 0);
    x += (*(test1.f))(0, 0);
    x += (**(test1.f))(0, 0);
    x += (***(test1.f))(0, 0);

//    x += (&(test2.f))(0, 0); // error: `(((int (* const**)(int, int))(&test2)) + 4u)' cannot be used as a function
//    x += test2.f(0, 0); // error: `test2.<anonymous struct>::f' cannot be used as a function
    x += (*(test2.f))(0, 0);
    x += (**(test2.f))(0, 0);
    x += (***(test2.f))(0, 0);

//    x += (&(test3.f))(0, 0); // error: `(((int (* const***)(int, int))(&test3)) + 4u)' cannot be used as a function
//    x += test3.f(0, 0); // error: test3.<anonymous struct>::f' cannot be used as a function
//    x += (*(test3.f))(0, 0); //  `*test3.<anonymous struct>::f' cannot be used as a function
    x += (**(test3.f))(0, 0);
    x += (***(test3.f))(0, 0);

    return x;
}
