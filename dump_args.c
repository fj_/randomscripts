
#include <stdio.h>

int main(int argc, char ** argv)
{
    int i = 0;
    const char * fname = "dump_args.out";
    FILE * f = fopen(fname, "wb");
    if (!f) 
    {
        printf("Can't open '%s' for writing\n", fname);
        return 1;
    }
    fprintf(f, "argc: %d\n", argc);
    printf("argc: %d\n", argc);

    for (i = 0; i < argc; i++)
    {
        fprintf(f, "argv[%d]: %s\n", i, argv[i]);
        printf("argv[%d]: %s\n", i, argv[i]);
    }
    
    fclose(f);
}
