from collections import defaultdict
import csv, graphviz
from pathlib import Path

POWERBITERS=frozenset(['Sasanka_of_Gauda', 'August'])

def collect_descendants(d, g: graphviz.Digraph, source, visited):
    if source in POWERBITERS:
        g.node(source, shape='tripleoctagon')

    for it in d[source]:
        g.edge(source, it)
        if it in POWERBITERS:
            g.node(it, shape='tripleoctagon')
            continue
        if it not in visited:
            visited.add(it)
            collect_descendants(d, g, it, visited)


def main():
    data = list(csv.DictReader(Path(__file__).with_name('bites-all.csv').open()))
    d = defaultdict(list)
    for r in data:
        d[r['from_username']].append(r['to_username'])

    print(d['dramasexual'])
    return

    sources = set(d.keys()) - set(name for lst in d.values() for name in lst)

    # print(sources)

    for src in sources | POWERBITERS:
        g = graphviz.Digraph(f'bittening_{src}', format='png')
        g.attr(rankdir='LR', nodesep='0.1')
        collect_descendants(d, g, src, set())
        # g = g.unflatten(stagger=3)
        g.view()


main()
input()
