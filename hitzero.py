probabilities = [1.0]
count = 250
for i in range(1, count):
    current = 0;
    for d in range(1, 7):
        if i - d >= 0:
            current += probabilities[i - d] / 6.0
    probabilities.append(current)
#print probabilities
s = ",".join(["%1.2f" % (p * 100) for p in probabilities])
#print s

expectation = [0.0]
roundTripExpectation = count
for n,p in enumerate(probabilities):
    e = (p * n / 3.5) + (1 - p) * roundTripExpectation
    expectation.append(e)
expectation.reverse()

s = ",".join(["%1.2f" % e for e in expectation])
print ("http://chart.apis.google.com/chart?cht=lc&chs=600x500&chtt=Cyclic+(N+=+250)&chd=t:"
    + s + "&chds=125,250&chxt=x,y&chxr=0,0,249|1,0,250")
