from __future__ import with_statement
from __future__ import division
from __future__ import print_function

import psyco
psyco.full()

from zipfile import ZipFile
from urllib import FancyURLopener
import __builtin__
import string
import numpy
import operator
import gzip
import hashlib
import zlib
import difflib
import urllib
import urllib2
from StringIO import StringIO
import bz2

from itertools import *
import re
import os.path
from contextlib import closing
import pickle
import sys
from PIL import Image, ImageDraw
import ImagePalette 
import xmlrpclib
import calendar
import wave
from math import *

from numpy import array, fromiter, zeros, ones


def implies(a, b):
	return b or not a 

def takeafter(predicate, iterable):
	i = iter(iterable)
	while not predicate(i.next()):
		pass
	for v in i: 
		yield v

def grouper(n, iterable, fillvalue=None):
	"grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"
	args = [iter(iterable)] * n
	return izip_longest(fillvalue=fillvalue, *args)


def readAll(fileName):
	with open(fileName) as f:
		return "".join(f.readlines())
		
def readAllUrl(url):
	with closing(urllib.urlopen(url)) as f:
		return "".join(f.readlines())

def readAllUrlCached(url, reload = False):
	fname = re.search("/([^/]+)$", url).group(1) 
	if reload or not os.path.exists(fname):
		print("Downloading data")
		urllib.urlretrieve(url,	fname)
	print("Reading file\n")
	return readAll(fname)

	
def getChallengeData(code, reload = False):
	lines = readAllUrlCached("http://www.pythonchallenge.com/pc/def/" + code + ".html",
						reload)
	lines = takewhile(lambda l: l != '-->\n', 
					takeafter(lambda l: l == '<!--\n', lines))

	return lines
		
	

def level3():
	data = getChallengeData("equality")
	
	for m in re.finditer(r"[^A-Z][A-Z]{3}[a-z][A-Z]{3}[^A-Z]", data):
		print(m.group(0))

def nextNothingZip(nothing):
	data = readAll("zip/" + nothing + ".txt")
	print(data)
	m = re.match(r"Next nothing is (\d+)\n*", data)
	if m:
		return m.group(1)
	
def nextNothing(nothing):
	data = readAllUrl("http://www.pythonchallenge.com/pc/def/"
					"linkedlist.php?nothing=" + nothing)
	print(data)
	m = re.search(r"and the next nothing is (\d+)", data)
	if m:
		return m.group(1)


def level4():
	nothing = str(92118 / 2) 
	while nothing:
		nothing = nextNothing(nothing)

def level5():
	data = readAllUrlCached("http://www.pythonchallenge.com/pc/def/banner.p")
	obj = pickle.loads(data)
	for line in obj:
		for c, l in line:
			sys.stdout.write(c * l)
		sys.stdout.write("\n")
	
def level6():
	nothings = set()
	nothing = str(90052) 
	while nothing:
		nothings.add(nothing)
		nothing = nextNothingZip(nothing)
	
	files = os.listdir("zip")
	for f in files:
		f = f.split('.')[0]
		if not f in nothings:
			print(f)

def level7():
	img = Image.open("oxygen.png")
	pix = img.load();
	print(img.size)
	def strDecode(seq):
		return "".join([chr(i) for i in seq])
		
	print(strDecode(pix[i*7, 47][0] for i in range(620/7)))
	print(strDecode([105, 110, 116, 101, 103, 114, 105, 116, 121]))
	

def level8():
	un = 'BZh91AY&SYA\xaf\x82\r\x00\x00\x01\x01\x80\x02\xc0\x02\x00 \x00!\x9ah3M\x07<]\xc9\x14\xe1BA\x06\xbe\x084'
	pw = 'BZh91AY&SY\x94$|\x0e\x00\x00\x00\x81\x00\x03$ \x00!\x9ah3M\x13<]\xc9\x14\xe1BBP\x91\xf08'
	print(bz2.decompress(un), bz2.decompress(pw)) 
		
def level9():
	first = (
			146,399,163,403,170,393,169,391,166,386,170,381,170,371,170,355,169,346,167,335,170,329,170,320,170,
			310,171,301,173,290,178,289,182,287,188,286,190,286,192,291,194,296,195,305,194,307,191,312,190,316,
			190,321,192,331,193,338,196,341,197,346,199,352,198,360,197,366,197,373,196,380,197,383,196,387,192,
			389,191,392,190,396,189,400,194,401,201,402,208,403,213,402,216,401,219,397,219,393,216,390,215,385,
			215,379,213,373,213,365,212,360,210,353,210,347,212,338,213,329,214,319,215,311,215,306,216,296,218,
			290,221,283,225,282,233,284,238,287,243,290,250,291,255,294,261,293,265,291,271,291,273,289,278,287,
			279,285,281,280,284,278,284,276,287,277,289,283,291,286,294,291,296,295,299,300,301,304,304,320,305,
			327,306,332,307,341,306,349,303,354,301,364,301,371,297,375,292,384,291,386,302,393,324,391,333,387,
			328,375,329,367,329,353,330,341,331,328,336,319,338,310,341,304,341,285,341,278,343,269,344,262,346,
			259,346,251,349,259,349,264,349,273,349,280,349,288,349,295,349,298,354,293,356,286,354,279,352,268,
			352,257,351,249,350,234,351,211,352,197,354,185,353,171,351,154,348,147,342,137,339,132,330,122,327,
			120,314,116,304,117,293,118,284,118,281,122,275,128,265,129,257,131,244,133,239,134,228,136,221,137,
			214,138,209,135,201,132,192,130,184,131,175,129,170,131,159,134,157,134,160,130,170,125,176,114,176,
			102,173,103,172,108,171,111,163,115,156,116,149,117,142,116,136,115,129,115,124,115,120,115,115,117,
			113,120,109,122,102,122,100,121,95,121,89,115,87,110,82,109,84,118,89,123,93,129,100,130,108,132,110,
			133,110,136,107,138,105,140,95,138,86,141,79,149,77,155,81,162,90,165,97,167,99,171,109,171,107,161,
			111,156,113,170,115,185,118,208,117,223,121,239,128,251,133,259,136,266,139,276,143,290,148,310,151,
			332,155,348,156,353,153,366,149,379,147,394,146,399
			)
	second = (
			156,141,165,135,169,131,176,130,187,134,191,140,191,146,186,150,179,155,175,157,168,157,163,157,159,
			157,158,164,159,175,159,181,157,191,154,197,153,205,153,210,152,212,147,215,146,218,143,220,132,220,
			125,217,119,209,116,196,115,185,114,172,114,167,112,161,109,165,107,170,99,171,97,167,89,164,81,162,
			77,155,81,148,87,140,96,138,105,141,110,136,111,126,113,129,118,117,128,114,137,115,146,114,155,115,
			158,121,157,128,156,134,157,136,156,136
			)
		
	img = Image.open("good.jpg")
	draw = ImageDraw.Draw(img)
	draw.line(first, "#FF0000", width = 2)
	draw.line(second, "#00FF00", width = 2)
	img.save("good_out.jpg")
	
	
def level10():
	def next(s):
		r = []
		for k, g in groupby(s):
			r += [len(list(g)), k]
		return r
	
	s = [1]
	
	for _ in range(31):
		s = next(s)
		print(len(s), s)
		
		
def level11():
	img = Image.open("cave.jpg")
	pxlz = img.load()
	s1, s2 = Image.new(img.mode, img.size), Image.new(img.mode, img.size)
	px1, px2 = s1.load(), s2.load()
	for x in range(img.size[0]):
		for y in range(img.size[1]):
			if (x + y) % 2:
				px1[x, y] = pxlz[x, y]
			else:
				px2[x, y] = pxlz[x, y]
	s1.save("cave1.jpg");
	s2.save("cave2.jpg");
	print("Yay!")

def level12():
	data = open("evil2.gfx", "rb").read()
	for i in range(5):
		open("12_f%d.dat" % i, "wb").write(data[i::5])
	print("Yay!")

def level13():
	server = xmlrpclib.ServerProxy("http://www.pythonchallenge.com/pc/phonebook.php")
	print(server.system.listMethods())
	print(server.phone("Bert"))
	

def level14():
	img = Image.open("wire.png")
	px = img.load()
	r = Image.new(img.mode, (100, 100))
	rpx = r.load()
	
	x1, y1, x2, y2 = 0, 0, 100, 100
	p = 0
	
	def nextPxl():
		return px[p, 0], p + 1
	
	while x1 < x2:
		for i in range(x1, x2):
			rpx[i, y1], p = nextPxl()
			
		for i in range(y1 + 1, y2):
			rpx[x2 - 1, i], p = nextPxl()
			
		for i in range(x2 - 2, x1 - 1, -1):
			rpx[i, y2 - 1], p = nextPxl()
			
		for i in range(y2 - 2, y1, -1):
			rpx[x1, i], p = nextPxl()
		
		x1 += 1
		x2 -= 1
		y1 += 1
		y2 -= 1
		
	r.save("unwired.png");
	print("Yay!")


def level15():
	for m in range(0, 100):
		y = 1000 + m * 10 + 6;
		if calendar.isleap(y) and calendar.weekday(y, 1, 26) == 0:
			print(y)


def level16():
		
	img = Image.open("mozart.gif").convert('RGB')
	px = img.load()
	sizex, sizey = img.size
	
	for y in range(sizey):
		line = [px[x, y] for x in range(sizex)]
		for x in range(sizex):
			if line[x] == (255, 0, 255):
				line = line[x:] + line[:x]
				for x in range(sizex):
					px[x, y] = line[x]
				break
		else:
			print("Fail!")
	img.save("mozart.png")
	print("Yay")

#############################

cookiejar = None
response = None

def initUrllib():
	global cookiejar
	cp = urllib2.HTTPCookieProcessor()
	cookiejar = cp.cookiejar;
	
	password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
	#password_mgr.add_password(None, "www.pythonchallenge.com", "huge", "file")
	password_mgr.add_password(None, "www.pythonchallenge.com", "butter", "fly")
		
	opener = urllib2.build_opener(cp, urllib2.HTTPBasicAuthHandler(password_mgr))
	urllib2.install_opener(opener)
#initUrllib()		
def passwordHack(host, realm):
	return ("butter", "fly")

def openUrl(url, headers = {}):
	global response
	opener = urllib.FancyURLopener()
	opener.prompt_user_passwd = passwordHack

	for h, v in headers.iteritems():
		opener.addheader(h, v)	

	response = opener.open(url)
	
	return response.read()

def getCookies():
	return dict((c.name, c.value) for c in cookiejar)

def getHeaders():
	d = {}
	msg = response.info()
	for i in msg:
		d[i] = msg[i]
	return d
		

##############################


def nextBusyNothing(nothing):
	data = openUrl("http://www.pythonchallenge.com/pc/def/"
					"linkedlist.php?busynothing=" + nothing)
	print(data)
	m = re.search(r"and the next busynothing is (\d+)", data)
	if m:
		return m.group(1)

def level17():

#	data = openUrl("http://www.pythonchallenge.com/pc/def/"
#					"linkedlist.php")
#	print getCookies()
#	nothing = str(12345)
#	cstr = ""
#	while nothing:
#		nothing = nextBusyNothing(nothing)
#		cmap = getCookies()
#		if "info" in cmap:
#			c = cmap["info"]
#			cstr += c
#			print c
#	print cstr

	cstr = "BZh91AY%26SY%94%3A%E2I%00%00%21%19%80P%81%11%00%AFg%9E%A0+%00hE%3DM%B5%23%D0%D4%D1%E2%8D%06%A9%FA%26S%D4%D3%21%A1%EAi7h%9B%9A%2B%BF%60%22%C5WX%E1%ADL%80%E8V%3C%C6%A8%DBH%2632%18%A8x%01%08%21%8DS%0B%C8%AF%96KO%CA2%B0%F1%BD%1Du%A0%86%05%92s%B0%92%C4Bc%F1w%24S%85%09%09C%AE%24%90"
	bstr = urllib.unquote_plus(cstr)
	print(bz2.decompress(bstr))
						
#	server = xmlrpclib.ServerProxy("http://www.pythonchallenge.com/pc/phonebook.php")
#	print server.system.listMethods()
#	print server.phone("Leopold")
	data = openUrl("http://www.pythonchallenge.com/pc/stuff/violin.php");
	print(data)
	print(getCookies())
	#http://www.pythonchallenge.com/pc/return/balloons.html	

def dumphex(file, lines):
	for l in lines:
		for code in l.split():
			file.write(chr(int(code, 16)))

def level18():
#	img = Image.open("balloons.jpg")
#	px = img.load()
#	halfwidth, height = img.size
#	halfwidth /= 2
#	r = Image.new(img.mode, (halfwidth, height))
#	rpx = r.load()
#	for y in range(height):
#		for x in range(halfwidth):
#			p1, p2 = px[x, y], px[x + halfwidth, y]
#			rpx[x, y] = tuple([p1[i] - p2[i] for i in range(3)])  
#	r.show()
	data = gzip.open("deltas.gz", "r").read()
	d1, d2 = [], []
	for line in data.split('\n'):
		p1, _, p2 = line.rpartition('   ')
		d1.append(p1)
		d2.append(p2)
		
	diff = difflib.ndiff(d1, d2)
	p1, p2, p3 = [], [], []
	for line in diff:
		code, data = line[:2], line[2:]
		if code == "+ ":
			p1.append(data)
		elif code == "- ":
			p2.append(data)
		else:
			p3.append(data)
	dumphex(open("p1.png", "wb"), p1)			
	dumphex(open("p2.png", "wb"), p2)	
	dumphex(open("p3.png", "wb"), p3)

def level19():
	data = open("lvl19.txt", "r").read()
	decoded = data.decode("base64");
	header, data = decoded[:44], decoded[44:]
	result = []

	for a, b in izip(*[iter(data)]*2):
		result.append(b)
		result.append(a) 
	
	decoded =  header + "".join(list(result))
	open("lvl19.wav", "wb").write(decoded)
	
def level20():
#	rrx = re.compile(r"bytes (\d+)-(\d+)/(\d+)")
#	start = 30203
#	for i in range(10):
#		print openUrl("http://www.pythonchallenge.com/pc/hex/unreal.jpg", 
#					{"range":"bytes=%d-" % start})
#		h = getHeaders()
#		print h
#		start, stop, total = map(int, rrx.match(h["content-range"]).groups())
#		start = stop + 1
	#1152983631
	start = 1152983631
	data = openUrl("http://www.pythonchallenge.com/pc/hex/unreal.jpg", 
				{"range":"bytes=%d-" % start})
	print(data)
	h = getHeaders()
	print(h)

class BreakWriter(object):
	def __init__(self, width):
		self.width = width
		self.pos = 0
	def write(self, data):
		for c in data:
			sys.stdout.write(c)
			self.pos += 1
			if self.pos >= self.width:
				self.pos = 0
				sys.stdout.write('\n')

def level21():
	def tryUnpack(decompressor, data):
		try:
			return decompressor.decompress(data) 
		except:
			pass
	
	def unpackZlib(data):
		return tryUnpack(zlib, data) or tryUnpack(zlib, data[::-1]) 
	def unpackBz2(data):
		return tryUnpack(bz2, data) or tryUnpack(bz2, data[::-1])

	data = open("21/package.pack", "rb").read()


	str = []
	while True:
		p2 = unpackZlib(data)
		if p2:
			code = ' '
		else:
			p2 = unpackBz2(data)
			if p2:
				code = '#'
			else:
				break
		data = p2
		str.append(code)
		
	for i in range(60, 80):
		print("\n\n", i)
		BreakWriter(i).write(str) 

def getFramesFromGif(gifImage):
	frame = 0
	frames = []
	
	while 1:
		try:
			gifImage.seek(frame)
			frames.append(gifImage.convert("RGB"))
			frame += 1
		except EOFError:
			break
	return frames

def addtuples(t1, t2):
	return tuple([i1 + i2 for (i1, i2) in zip(t1, t2)])

		
def level22():
	img = Image.open("white.gif")
	print(img.info)
	
	result = Image.new("RGB", (500, 500))
	
	frames = getFramesFromGif(img)
	v = []
	for i, frame in enumerate(frames):
#		name = "zzz%03d.png" % i
		#print name

		data = list(frame.getdata())

		for i in range(len(data)):
			if data[i] != (0, 0, 0):
				#data[i] = (255, 255, 255)
				v.append(((i // 200 - 100), (i % 200 - 100)))
		#result.putdata(data);
		#result.save(name)
	print(v)
	pos = (250, 250)
	draw = ImageDraw.Draw(result)
	for d in v:
		if d == (0, 0):
			pos = addtuples(pos, (20, 0))
		else:
			pos1 = addtuples(pos, d)
			draw.line((pos, pos1), "white")
			pos = pos1
	result.show()

#import this
def rot13(s):
	d = {}
	for c in (65, 97):
		for i in range(26):
			d[chr(i+c)] = chr((i+13) % 26 + c)
	
	return "".join([d.get(c, c) for c in s])
	

def level23():
	print(rot13("va gur snpr bs jung"))
	
def level24():
	# http://www.pythonchallenge.com/pc/hex/ambiguity.html
	origimg = Image.open("maze.png").convert("RGB")
	origpx = origimg.load()
	
	img = origimg.convert("L")
	px = img.load()
	
	szx, szy = img.size
	
	start = (szx-2, 0)
	end = (1, szy - 1)

	pathdict = { start : None }
	front = [start]
	nextFront = []
	pixelz = []
	while front:
		while front:
			current = front.pop()
			def tryToGo(xy, prev):
				x, y = xy
				if x >= 0 and y >= 0 and x < szx and y < szy:
					if px[x, y] < 200 and xy not in pathdict:
						pathdict[xy] = prev
						nextFront.append(xy)
			tryToGo(addtuples(current, (1, 0)), current)
			tryToGo(addtuples(current, (-1, 0)), current)	
			tryToGo(addtuples(current, (0, 1)), current)	 
			tryToGo(addtuples(current, (0, -1)), current)
			
		front = nextFront
		if end in pathdict:
			break
	
#	for current in pathdict:
#		origpx[current[0], current[1]] = (0, 255, 0)
#	if end in pathdict:
#		current = end
#		while current:
#			origpx[current[0], current[1]] = (0, 255, 255)
#			current = pathdict[current]
#	origimg.show()
#		
#	origimg.save("mazepath.png")
	if end in pathdict:
		current = end
		while current:
			pixelz.append(origpx[current[0], current[1]][0])
			current = pathdict[current]
			
	pixelz = pixelz[::-1][1::2]
	open("24.zip", "wb").write("".join(chr(r) for r in pixelz))


	

def level25():
	#http://www.pythonchallenge.com/pc/hex/lake.html
	waves = []
	for i in range(1, 26):
		name = "lake%d.wav" % i
		print(name)
		waves.append(wave.open("25/" + name, "rb"))
		#urllib.urlretrieve("http://www.pythonchallenge.com/pc/hex/" + name,	"25/" + name)
	bigimage = Image.new("RGB", (60*5, 60*5))
	for i, w in enumerate(waves):
		data = w.readframes(10800)
		img = Image.fromstring("RGB", (60, 60), data)
		x = i % 5 * 60
		y = i // 5 * 60
		bigimage.paste(img, (x, y))
	bigimage.show()
		
#From: Leopold Mozart <leopold.moz@pythonchall >
#Subject: Re: sorry
#Never mind that.
#
#Have you found my broken zip?
#md5: bbb8b499a0eef99b52c7f13f4e78c24b
#
#Can you believe what one mistake can lead to?

def level26():
	rawdata = open("mybroken.zip", "rb").read()
	
	rightmd5 = "bbb8b499a0eef99b52c7f13f4e78c24b"
	#rightmd5 = "1e930498c1296b98de18fbf9d25f6f04"

	data = array("B", rawdata)
#	data[23] ^= 0x40
#	print hashlib.md5(data).hexdigest()
#	return
	
	currentMd5 = hashlib.md5()
	for bp in range(len(rawdata)):
		if not bp % 64: print(bp)
		
		for b in range(256):
			data[bp] ^= b
			m = currentMd5.copy()
			m.update(data[bp:])
			if m.hexdigest() == rightmd5:
				print("Yay, byte %d wrong" % bp)
				open("24_fix.zip", "wb").write(data)
				return
			data[bp] ^= b
			
		currentMd5.update(chr(data[bp]))

def hexefy(seq):
	return " ".join("%02x" % i for i in seq)

def level27():
		
	img = Image.open("zigzag.gif")
	p = img.palette.palette
	data = list(img.getdata())

#	s = ""
#	for i in range(256):
#		r, c = i // 16, i % 16 
#		s += p[r*16 + c]*3 
#	img.putpalette(s)
#	img.show()
	pp = p[::3]
	pmap = [ord(i) for i in pp]
	print(pmap)
	
	def evolve(seed):
		while True:
			yield seed
			seed = pmap[seed]
	
#	d2 = []
	seed = 0xd7
	diff = []
	positions = []
	for i, d in enumerate(data):
		if seed != d:
			diff.append(d)
			positions.append(i)
			seed = d
		seed = pmap[seed]
	
	i2 = img.convert("RGB")
	data2 = list(i2.getdata())
	for i in positions:
		data2[i] = (255, 0, 0)
	i2.putdata(data2)
	i2.show() 

	print(hexefy(diff))
	s = "".join(chr(i) for i in diff)
	dc = bz2.decompress(s)
	print(dc[:500])
	sp = dc.split()
	print(set(sp))
	
	# http://www.pythonchallenge.com/pcc/ring/bell.html:repeat:switch	


def level28():
	img = Image.open("bell.png")
	data = list(img.getdata())
	
#	print data[:10]
#	return

	d2 = []
	res = []
	for t1, t2 in izip(data[::2], data[1::2]):
		r1, g1, b1 = t1
		r2, g2, b2 = t2
		if abs(g1 - g2) == 42:
			d2.append((0, 0, 0));
			d2.append((0, 0, 0));
		else: 
			d2.append(t1)
			d2.append(t2)
			res.append((t1, t2, g1 - g2))
			print (chr(abs(g1 - g2)), end=' ')
		
	#gdata = [(0, g, 0) for (r, g, b) in data]
	img.putdata(d2)
	#img.putdata(gdata)
	img.show()
	


def level29():
	# http://www.pythonchallenge.com/pc/ring/guido.html
	data = open("silence.txt").read()
	lines = data.split("\n")
	s = "".join(chr(len(i)) for i in lines[1:])
	print(bz2.decompress(s))

def factorizeSlow(n):
	factors = []
	p = 2
	while n > 1:
		if p * p > n:
			factors.append(n)
			break
		
		while n % p == 0:
			factors.append(p)
			n //= p

		p = p + 2 if p != 2 else 3
	return factors
		
def level30():
	values = [s.strip() for s in open("yankeedoodle.csv", "rb").read().split(",")]

	floats = map(float, values)
	print(len(values)) 
	# factorizeSlow(7367) = 53, 139
	img = Image.new("L", (53, 139))
	img.putdata([int(x*256) for x in floats])
	img = img.rotate(270, expand = 1).transpose(Image.FLIP_LEFT_RIGHT)
	print(values)
	#img.show()
	d2 = []
	for i in range(0, len(values) - 2, 3):
		d2.append(int(values[i][5] + values[i + 1][5] + values[i + 2][6]) % 256)
	print(d2)
	print("".join(chr(i) for i in d2))
#	zz = ""
#	for s in d2:
#		zz+= chr(int(s) % 256)
	
def mpeek(x, y, maxIterations):
	xx = 0
	yy = 0
	i = 0
	while xx*xx + yy*yy < 4 and i < maxIterations:
		xtemp = xx*xx - yy*yy + x
		yy = 2*xx*yy + y
		xx = xtemp
		i += 1
	return i - 1

def distribute(start, width, n):
	for i in range(n):
		yield i, start + width * i / n 

def printhistogram(img):
	for i, n in enumerate(img.histogram()):
		if n != 0:
			print (i, n)
	
# "kohsamui" + "thailand"
def level31():
	img = Image.open('mandelbrot.gif')
	px0 = img.load()
	width, height = img.size
	i2 = Image.new('P', img.size)
	i2.putpalette(img.getpalette())
	px = i2.load()
	
	#print(list(enumerate(grouper(3, img.getpalette()))))
	
	#printhistogram(img) 
	
	r = ""
	for j, y in reversed(list(distribute(0.57, 0.027, 480))):
		jj = 479 - j
		for i, x in distribute(0.34, 0.036, 640):
			res = mpeek(x, y, 128)
			if px0[i, jj] == res:
				pass
			elif px0[i, jj] < res:
				r += " "
			else:
				r += "#"

	print(len(r), factorizeSlow(len(r)))
	
	for line in grouper(23, r):
		print(*line, sep='')


def level32():
	data = readAllUrlCached('http://kohsamui:thailand@www.pythonchallenge.com/pc/rock/up.txt')
	print(len(data))
	skipper = [[int(i) for i in k] for k in 
			   (s.split() for s in data.split('\n') if not s.startswith('#')) if len(k) > 0]
	
	rows, cols = skipper[0]
	horColumns = skipper[1 : rows + 1]
	verColumns = skipper[rows + 1 : rows + cols + 1]
	
	# now we add fake border to avoid complex checks.
	rows += 2
	cols += 2
	horColumns = [[0]] + horColumns + [[0]]
	verColumns = [[0]] + verColumns + [[0]]
	horSums = fromiter((sum(l) for l in horColumns), numpy.int)
	verSums = fromiter((sum(l) for l in verColumns), numpy.int)
	
	field = zeros((rows, cols), numpy.byte)
	field[0,:] = field[rows - 1,:] = -1
	field[:,0] = field[:,cols - 1] = -1
	level = [0]
	
	def printSolution():
		print('\n' + str(level[0]) + '\n')
		for row in field:
			for c in row:
				print('#' if c > 0 else ' ' if c == 0 else '.', end = '')
			print()
	
	
	def checkLine(line, expected):
		def makeColumn():
			cnt = 0
			for c in line:
				if c > 0: cnt += 1
				elif cnt > 0:
					yield cnt
					cnt = 0
			if cnt > 0:
				yield cnt
		return all(map(operator.eq, makeColumn(), expected))
	
	def putBlocks(row, field, sums, columns, sums2, columns2, solve):
		line = field[row,:]
		column = columns[row]
		def putBlocksRec(start, index, reserved):
			if index >= len(column):
				tmp = sums[row]
				sums[row] = 0
				solve()
				sums[row] = tmp
				return
			#else go deeper
			length = column[index]
			reserved -= length + 1
			def putBlock():
				free = 0
				i = start
				limit = len(line) - reserved
				while i < limit:
					c = line[i]
					if c <= 0:
						if free >= length:
							# can put a line.
							bs = i - length
							be = i
							
							lineBackup = array(line[bs - 1 : be + 1])
							sums2Backup = array(sums2[bs : be])
							
							
							for k in range(bs, be):
								if line[k] == 0:
									line[k] = 1
									s = sums2[k]
									if s == 1:
										if not checkLine(field[:, k], columns2[k]):
											# so we could never use this position
											free = min(free, i - k - 1)
											break							
									elif s == 0:
										break
									sums2[k] = s - 1
							else:
								line[bs - 1] = line[be] = -1
								yield i 
							#restore everything
							line[bs - 1 : be + 1] = lineBackup
							sums2[bs : be] = sums2Backup
							
						if c < 0:
							free = 0
						else: free += 1
					else:
						# can't put a line adjanced to a nonempty cell
						limit = min(limit, i + length + 1)
						free += 1
				
					i += 1
			# end of putBlock 
			for newstart in putBlock():
					putBlocksRec(newstart + 1, index + 1, reserved)
		# end of putBlocksRec
		putBlocksRec(0, 0, sum(column) + len(column))

	def analyzeLine(row, field, sums, columns, sums2, columns2):
		line = field[row,:]
		fullArr = ones(len(line), numpy.byte)
		emptyArr = ones(len(line), numpy.byte)
		counter = [-1]
		def callback():
			c = counter[0] + 1
			if (c & 0xFFF) == 0:
				printSolution() 
			counter[0] = c
			
			for i, c in enumerate(line):
				if c > 0:
					emptyArr[i] = 0
				else:
					fullArr[i] = 0
					
		putBlocks(row, field, sums, columns, sums2, columns2, callback)
		if counter[0] < 0:
			return None
		
		changed = []
		for i in range(len(line)):
			if line[i] == 0:
				assert not (fullArr[i] and emptyArr[i])
				if fullArr[i]:
					line[i] = 1
					sums2[i] -= 1
					sums[row] -= 1
					changed.append(i)
				if emptyArr[i]:
					line[i] = -1
					changed.append(i)
			else:
				if not (((fullArr[i] > 0) == (line[i] > 0)) and ((emptyArr[i] > 0) == (line[i] < 0))):
					print(line, fullArr, emptyArr, sep='\n')
					assert false  
		return changed
	
	def analyzeField():
		stored = field.copy()
		storedhs = horSums.copy()
		storedvs = verSums.copy()
		def internal():
			changed = True
			dirtyRows = set(range(1, rows - 1))
			dirtyCols = set(range(1, cols - 1))
			changed = True
			while changed: #dirtyRows or dirtyCols:
				#ndirtyRows, ndirtyCols = set(), set()
				# actually I don't want to do that since every change that decreases some of 
				# the sums to 1 or 0 is nonlocal
				changed = False
				
				for i in dirtyRows:
					c = analyzeLine(i, field, horSums, horColumns, verSums, verColumns)
					if c is None:
						return False
					elif c:
						changed = True
#						for k in c:
#							ndirtyCols.add(k)
#						ndirtyRows.add(i)
					
				f = field.swapaxes(0, 1)
				
				for i in dirtyCols:
					c = analyzeLine(i, f, verSums, verColumns, horSums, horColumns)
					if c is None:
						return False
					elif c:
						changed = True
#						for k in c:
#							ndirtyRows.add(k)
#						ndirtyCols.add(i)
				
				#dirtyRows, dirtyCols = ndirtyRows, ndirtyCols
			return True
		
		if internal():
			solve()
			
		field[...] = stored
		horSums[...] = storedhs
		verSums[...] = storedvs
		
		
					
	solutions = []
	solveCalls = [0]
	
	def solve():
		solveCalls[0] += 1
		if level[0] < 8: 
			print("Level:", level[0])
			printSolution()
		#else: print("zomg")
		level[0] += 1
		#printSolution()
		
		def getMax(lines, sums):
			return max((
					-len(lines[i])*4 + 100*sums[i] if sums[i] != 0 else None
						, i) for i in range(len(lines))) 
			
		hv, hi = getMax(horColumns, horSums)
		vv, vi = getMax(verColumns, verSums)
		
		if hv >= vv and hv is not None:
			putBlocks(hi, field, horSums, horColumns, verSums, verColumns, analyzeField)
		elif vv > hv:
			putBlocks(vi, field.swapaxes(0, 1), verSums, verColumns, horSums, horColumns, analyzeField)
		else:
			print("\n\n!!!FFound!!!\n\n")
			printSolution()
			with open('solutions.txt', 'a') as f:
				f.write(str(field));
			sys.exit(0)
		level[0] -= 1
	
	analyzeField()		
	print ("Solve calls:", solveCalls[0])






def level33():
	#If you are blinded by the light,
	#remove its power, with its might.
	#Then from the ashes, fair and square,
	#another truth at you will glare.
	
	img = Image.open("beer2.png")
	print(img.getcolors())
	
	px = img.load()
	data = list(img.getdata())
	#print(data[:100])
	def filterdata(data, filterSet, targetValue):
		for c in data:
			if c == targetValue: yield 255
			elif c in filterSet: yield 0

	fset = set()
	images = []
	totalWidth = 0
	totalHeight = 0
	for i in range(33):
		i = 1 + 6*i
		fset.add(i)
		fset.add(i + 1)
		d = list(filterdata(data, fset, i + 1))
		l = len(d)
		s = int(sqrt(l) + 0.5)
		print(s, s*s, l)
		img2 = Image.new("I", (s, s))
		img2.putdata(d)
		images.append(img2)
		totalWidth += s
		totalHeight = max(totalHeight, s)
	
	res = Image.new("I", (totalWidth, totalHeight))
	index = 0
	for img in images:
		res.paste(img, (index, 0))
		index += img.size[1]
	res.show()
		
	
	

#print(rot13("Fgneg ol trggvat evq bs gur k."))
level33()
