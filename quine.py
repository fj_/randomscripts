# A surprisingly nice quine which uses two kinds of quotes instead
# of escapes.
s="'" # single quote
d='"' # double quote
code="print's='+d+s+d+';d='+s+d+s+';code='+d+code+d+';'+code"
print's='+d+s+d+';d='+s+d+s+';code='+d+code+d+';'+code
