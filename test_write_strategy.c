#include <stdio.h>

#if !defined(_MSC_VER)
#include <sys/time.h>
#endif
   

char * test_file = "test.out";

void write1(int cnt)
{
	int i;
	for (i = 0; i < cnt; i++) {
		FILE * f = fopen(test_file, "a");
		fprintf(f, "%d\n", i);
		fclose(f);
	}
}

void write2(int cnt)
{
	int i;
	FILE * f = fopen(test_file, "a");
	for (i = 0; i < cnt; i++) {
		fprintf(f, "%d\n", i);
		fflush(f);
	}
	fclose(f);
}

void write3(int cnt)
{
	int i;
	FILE * f = fopen(test_file, "a");
	for (i = 0; i < cnt; i++) {
		fprintf(f, "%d\n", i);
	}
	fclose(f);
}

int cnt = 40000;

// lifted from Python's stdlib
double clock()
{
#if defined(_MSC_VER)
	static long long ctrStart;
	static double divisor = 0.0;
	long long now;
	double diff;

	if (divisor == 0.0) {
		long long freq;
		QueryPerformanceCounter(&ctrStart);
		if (!QueryPerformanceFrequency(&freq) || freq == 0) {
			abort(-1);
		}
		divisor = (double)freq;
	}
	QueryPerformanceCounter(&now);
	diff = (double)(now - ctrStart);
	return diff / divisor;
#else // Linux
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec + tv.tv_usec * 0.000001;
#endif
}


void test_one(const char * desc, void (*f)(int))
{
	double t = clock();
	f(cnt);
	t = clock() - t;
	printf("%s: %f\n", desc, t);
}


void test(const char * desc, void (*f)(int))
{
	test_one(desc, f);
	test_one(desc, f);
	remove(test_file);
}

int main()
{
	test("write1", write1);
	test("write2", write2);
	test("write3", write3);
	return 0;
}