# This is the first way, just creating a proxy object with accessors and stuff
# It's not very memory efficient (this object + accessor methods (always all
# three, for nicer error reporting), but I don't see a way to fix it: accessor
# is defined on the class level at the class creation, its __get__ receives 
# instance as a parameter at call time and should return something that
# implements index access protocol and holds that instance reference until 
# it's called.
#
# It's not even a property: at least this way I don't need to create that 
# temporary holder object every time.

class indexed_property(object):
	__slots__ = ('getter', 'setter', 'deleter')
	def __init__(self, getter, setter = None, deleter = None):
		self.getter = getter
		self.setter = setter or self.setter_error 
		self.deleter = deleter or self.deleter_error
	def __getitem__(self, key): return self.getter(key)
	def __setitem__(self, key, value): return self.setter(key, value)
	def __delitem__(self, key): return self.deleter(key)
	def setter_error(self, key, value):
		raise TypeError(str(self.getter.im_class) + ' instance does not support item assignment')
	def deleter_error(self, key):
		raise TypeError(str(self.getter.im_class) + ' instance does not support item deletion')

class Sample(object):
	def __init__(self):
		self.someproperty = indexed_property(self.get_some_item)
	def get_some_item(self, key):
		return '<<<' + str(key) + '>>>'

print Sample().someproperty[12]

