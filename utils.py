from contextlib import contextmanager
import os.path as os_path
import sys
from itertools import chain
import time
import re


@contextmanager
def db_execute(db, *args, **kwargs):
    '''
    with db_execute(db, sql): execute sql in a new cursor and return it for fetching results

    Never ever call outside the `with` statement (nothing will happen).

    Use `with db:` block to commit/rollback the transaction (this doesn't close the connection).

    With psycopg2, always use `with db:`, because even simple selects start a transaction,
    so that will avoid idling transactions.
    '''
    cur = db.cursor()
    try:
        cur.execute(*args, **kwargs)
        yield cur
    finally:
        cur.close()

def set_pdb_hook():
    def info(type, value, tb):
        import traceback, pdb
        traceback.print_exception(type, value, tb)
        pdb.pm()
    sys.excepthook = info

class Tee:
    def __init__(self, f1, f2):
        self.f1 = f1
        self.f2 = f2
    def write(self, text) :
        self.f1.write(text)
        self.f2.write(text)
    def flush(self) :
        self.f1.flush()
        self.f2.flush()

@contextmanager
def log_stdout(log_name):
    with open(log_name, 'w') as f:
        old_stdout = sys.stdout
        try:
            sys.stdout = Tee(old_stdout, f)
            yield
        finally:
            sys.stdout = old_stdout

class Object(object):
    def __init__(self, **kwargs): self.__dict__.update(kwargs)
    def __repr__(self): return 'Object(%s)' % repr(self.__dict__)
    def __contains__(self, name): return name in self.__dict__
    def __getitem__(self, name): return self.__dict__[name]
    def __setitem__(self, name, value): self.__dict__[name] = value
    def __delitem__(self, name): del self.__dict__[name]

def bind_kwargs(src, **kwargs):
    """For when you have *args and also some optional arguments!

    >>> def f(x, *args, **kwargs):
    ...     kwargs = bind_kwargs(kwargs, optarg = None, optarg2 = 1)
    ...     print kwargs
    ...
    >>> f(1, 2, 3, optarg = True)
    Object({'optarg2': 1, 'optarg': True})
    >>> f(1, not_an_arg = True)
    Traceback (most recent call last):
        ...
    TypeError: f got an unexpected keyword argument 'not_an_arg'
    """
    for name, value in src.items():
        if name not in kwargs:
            # Try to mimic the real keyword argument binding behaviour
            f = '?'
            try:
                import inspect
                f = inspect.stack()[1][3] # up one level, third element is function name
            except: pass
            raise TypeError('%s got an unexpected keyword argument \'%s\'' % (f, name))
        kwargs[name] = value
    return Object(**kwargs)

def inject_attributes(obj, **attrs):
    """Sets attributes of a given object to attrs.
    Returns a function that will restore the affected attributes
    to their original state.

    >>> o = Object(a = 1)
    >>> restore = inject_attributes(o, a = 2, b = 2)
    >>> o
    Object({'a': 2, 'b': 2})
    >>> restore()
    >>> o
    Object({'a': 1})
    """
    original = {}
    undefined = object()
    for name, attr in attrs.items():
        original[name] = getattr(obj, name, undefined)
        setattr(obj, name, attr)
    def restore():
        for name, attr in original.items():
            if attr != undefined:
                setattr(obj, name, attr)
            else:
                delattr(obj, name)
    return restore

@contextmanager
def with_attributes(obj, **attrs):
    """The same as inject_attributes, usable with 'with' statement

    >>> o = Object(a = 1)
    >>> with with_attributes(o, a = 2, b = 2):
    ...     print o
    ...
    Object({'a': 2, 'b': 2})
    >>> print o
    Object({'a': 1})
    """
    restore = inject_attributes(obj, **attrs)
    yield
    restore()

def run_tests(*suites, **kwargs):
    """Runs tests with a suitably verbose runner, returns True on success.
    If a nonzero optional argument 'exit' is given, calls sys.exit(exit) on failure.

    I'm not writing doctests for this one, sorry"""
    from unittest import TestSuite, TextTestRunner
    tests = TestSuite(suites)
    runner = TextTestRunner(verbosity=4)
    success = runner.run(tests).errors = 0
    if not success:
        exit = bind_kwargs(kwargs, exit = None).exit
        if exit:
            sys.exit(exit)


def cached_call(callable, name = None, mod_dependencies = [],
                hash_args = True, check_existence_only = False):
    """Quick and dirty persistent memoization. Given a callable and optional
    arguments returns a curried wrapper, that, when called, checks if a file
    with a generated name exists and is more recent than the module where the
    callable was defined (and optional dependency modules), if so, returns
    the results of unpickling, if not, calls the callable, dumps the result
    to that file and returns the result.

    Specify `hash_args = False` to not add a hash of the arguments to the file
    name (or just don't pass any arguments to the function you want to wrap).

    Specify `check_existence_only = True` to load the file unconditionally if
    it exists (if you are working on a different code in the same file where
    the callable is defined and prefer to delete it manually if needed).
    """
    from os.path import getmtime, exists
    from pickle import load, dump

    def modification_time(mod):
        if isinstance(mod, basestring):
            mod = sys.modules[mod]
        try:
            f = mod.__file__
            return getmtime(f)
        except:
            return 0.0

    def wrapper(*args, **kwargs):
        if not name: name = callable.__name__
        h = ''
        if hash_args and len(args) + len(kwargs):
            h = hash(tuple(args + sorted(kwargs.iteritems())))
            h = '_' + str(h)
        fname = name + h + '.pickled'
        if exists(fname):
            should_load = True
            if not check_existence_only:
                ftime = getmtime(fname)
                modtime = max(map(modification_time, chain([callable.__module__], mod_dependencies)))
                should_load = modtime <= ftime
            if should_load:
                with open(fname, 'rb') as f:
                    return load(f)
        # doesn't exist or is older
        obj = callable(*args, **kwargs)
        with open(fname, 'wb') as f:
            dump(obj, f)
        return obj
    return wrapper

def cached_in_pickle(file_name):
    '''A quicker and dirtier persistent memoization'''
    import pickle
    from functools import wraps
    file_name = os_path.realpath(file_name)
    def fwrapper(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if os_path.exists(file_name):
                with open(file_name, 'rb') as file:
                    return pickle.load(file)
            obj = f(*args, **kwargs)
            with open(file_name, 'wb') as file:
                pickle.dump(obj, file)
            return obj
        return wrapper
    return fwrapper

class Progress_printer(object):
    """Allows printing progress information by using backspace character
    to erase previous message (on Windows).

    Can't write multiline messages (command window limitation). For the same
    reason doesn't normally write EOL, or, when asked to, also resets
    internal state.
    """
    def __init__(self, out = sys.stdout):
        self.out = out
        self.prev_string = u''
    def update(self, s, eol = False):
        if not isinstance(s, unicode): s = s.decode('ascii')
        prev_string = self.prev_string
        new_string = s
        common_prefix_len = len(os_path.commonprefix([prev_string, new_string]))
        # erase stuff
        chars_to_erase = len(self.prev_string) - common_prefix_len
        eraser = u'\x08' * chars_to_erase
        spaces = u' ' * chars_to_erase
        self.out.write(eraser + spaces + eraser)
        # write stuff
        self.out.write(new_string[common_prefix_len:].encode(self.out.encoding))

        if eol:
            self.out.write(u'\n')
            self.prev_string = u''
        else:
            self.prev_string = s
        self.out.flush()

def format_float_adjusting_precision(value, precision):
    """Adjusts precision to fit the float to width.

    >>> for exp in range(-3, 6): format_float_adjusting_precision(1.6 * 10 ** exp, 3)
    '0.002'
    '0.016'
    '0.160'
    '1.600'
    '16.00'
    '160.0'
    '1600'
    '16000'
    '160000'
    """
    assert precision > 0
    s = '%.*f' % (precision, value)
    diff = len(s) - (precision + 2)
    if diff <= 0: return s
    if diff >= precision: diff = precision + 1 # cut the trailing dot as well
    return s[:-diff]

def parse_simple_list(src, comment_char = '#'):
    """Parse simple newline-separated lists of things.
    if comment_char is specified ('#' by default') then it acts as a single
    line comment char and must be escaped by doubling it.

    Lines are stripped of whitespace, empty lines are removed.
    >>> parse_simple_list('\\n\\n  \\n first line \\n#commen#ted\\n'
    ...                   'second#commented\\n##escaped###com###ment\\n')
    ['first line', 'second', '#escaped#']
     """
    doubled_comment_char = comment_char * 2
    result = []
    for line in src.split('\n'):
        if comment_char:
            line = line.replace(doubled_comment_char, '\n') # \n is safe after split
            line = line.split(comment_char, 1)[0]
            line = line.replace('\n', comment_char)
        line = line.strip()
        if line: result.append(line)
    return result

def closing_contextmanager(cls):
    """Generates __enter__ and __exit__ for a class implementing close().

    >>> @closing_contextmanager
    ... class Test(object):
    ...     def __init__(self, x):
    ...         self.x = x
    ...     def close(self):
    ...         print 'closed'
    ...
    >>> with Test('abc') as t:
    ...     print t.x
    abc
    closed
    """
    cls.__enter__ = lambda self: self
    def exit(self, exc_type, exc_val, exc_tb):
        self.close()
        return False
    cls.__exit__ = exit
    return cls

class CyclicEvent(object):
    """Note that it's not strictly cyclic, it starts counting afresh after
	each update that returns True, so it is guaranteed that there would be
	at least the specified interval between consecutive reported events
	but the timings will slowly drift depending on the refresh rate.

	First event occurs after interval elapses since the first update, not
	immediately.

	>>> e = CyclicEvent(1.0)
	>>> e.update(0.1)
	False
	>>> e.update(1.0)
	False
	>>> e.update(1.2)
	True
	>>> e.update(2.19)
	False
	>>> e.update(2.2)
	True
	"""
    def __init__(self, interval):
        self.t = None
        self.interval = interval
    def update(self, t):
        if self.t is None:
            self.t = t
            return False
        elif t - self.t > self.interval:
            self.t = t
            return True
        return False
    def reset(self):
        self.t = None


def create_enum(*field_names):
    """ Probably wrong. """
    class Enum(object):
        __slots__ = field_names
    obj = Enum()
    for i, name in enumerate(field_names):
        setattr(obj, name, i)
    return obj

class Timeit(object):
    def __init__(self, description):
        self.description = description
    def __enter__(self):
        self.t = time.time()
    def __exit__(self, arg1, arg2, arg3):
        print (self.description + ': ' +
            format_float_adjusting_precision(time.time() - self.t, 3))


def float_to_binary(f):
    return '{:064b}'.format(ctypes.c_ulonglong.from_buffer(ctypes.c_double(f)).value)


if __name__ == '__main__':
    import doctest
    run_tests(doctest.DocTestSuite(), exit = 1)

