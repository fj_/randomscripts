#!/usr/bin/sh
rm -rf testrepo
mkdir testrepo
cd testrepo

git init
echo -e "A\nB\nC\nD\nE" > test.txt
git add .
git commit -am "a"

git branch c
git checkout c
echo -e "A\nB\nX\nD\nE" > test.txt
git commit -am "c1"

git checkout master
echo -e "G\nG\nG\nA\nB\nC\nD\nE" > test.txt
git commit -am "b1"
echo -e "A\nB\nC\nD\nE\nG\nG\nG\nA\nB\nC\nD\nE" > test.txt
git commit -am "b2"

git checkout c

if [ -z $1 ]; then
    # this produces incorrect result
    git rebase master
else
    # but if we go through revisions manually instead, we get the correct result
    git rebase master~1
    git rebase master
fi

cat test.txt
