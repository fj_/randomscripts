import functools
from typing import NamedTuple
import itertools
from dataclasses import dataclass, replace as dc_replace, field as dc_field

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


# @functools.total_ordering
@dataclass
class State:
    territory: int
    money: int
    units: int = 0
    farms: int = 0
    prev: object = dc_field(default=None, repr=False)
    move: str = ''

    @property
    def income_per_turn(self):
        return self.territory + self.farms * 4 - self.units * 2


    def inferior_to(self, other):
        if self.income_per_turn + 10 < other.income_per_turn:
            return True
        return  self.income_per_turn <= other.income_per_turn and \
                self.units <= other.units and \
                self.money <= other.money

    # def __repr__(self):
    #     return '{' + ', '.join(f'{s}={getattr(self, s)}' for s in self._fields if s != 'prev') + '}'



def make_turn(state):
    money = state.money + state.income_per_turn
    if money < 0:
        return None
    return dc_replace(state, prev=state, money=money, territory=state.territory + state.units, move='.')


def make_farm(state):
    if state.farms + 1 + state.units >= state.territory:
        return None
    money = state.money - 12 - state.farms * 2
    if money < 0:
        return None
    return dc_replace(state, prev=state, money=money, farms=state.farms + 1, move='F')


def make_unit(state):
    money = state.money - 10
    if money < 0:
        return None
    return dc_replace(state, prev=state, money=money, units=state.units + 1, territory = state.territory + 1, move='U')


def make_stuff(state, stuff_factory):
    while state is not None:
        yield state
        state = stuff_factory(state)


def make_all_stuff(state):
    for state in make_stuff(state, make_farm):
        yield from make_stuff(state, make_unit)


def remove_inferior_states(states):
    best = []
    for s in states:
        i = 0
        while i < len(best):
            b = best[i]
            if s.inferior_to(b):
                # inferiority is transitive, so no reason to consider this state further
                break
            elif b.inferior_to(s):
                best[i] = best[-1]
                del best[-1]
            else:
                i += 1
        else:
            best.append(s)
    return best




def main():
    states = [State(7, 10, 0, 0, None)]
    for turn in range(15):
        print(f'--------{turn}----------')
        if turn:
            states = [make_turn(s) for s in states]
        new_states = []
        for state in states:
            new_states.extend(make_all_stuff(state))
        states = remove_inferior_states(new_states)
        for state in states:
            print(state)
        print()
    print('-------')
    states = sorted(states, key=lambda s: s.income_per_turn)
    for state in states:
        print(state.income_per_turn, state, end=': ')
        extra = []
        while state is not None:
            extra.append(state)
            state = state.prev
        extra.reverse()
        print (''.join(s.move for s in extra))


if __name__ == '__main__':
    main()


