import fnmatch
import sys
import os
from os import path
import re
 
def CleanVSFiles(targetPath = ""):
    oldwd = os.getcwdu()
    if targetPath: os.chdir(targetPath)
    try:
        i = 1
        while True:
            backup = "!!VS Files Backup " + str(i)
            if not path.exists(backup): break;
            i = i + 1
    
        createBackup = True
        for dirpath, dirnames, filenames in os.walk(""):
            # a hack for not including new backup dir in the walk
            if createBackup:
                os.mkdir(backup)
                print "Backup directory created: " + path.abspath(backup);
                createBackup = False
                
            for i, d in enumerate(dirnames):
                if d == "bin" or d == "obj":
                    current = path.join(dirpath, d)
                    print "Removing " + current
                    
                    newName = re.sub(r"\\|/", "__", current)
                    newName = path.join(backup, newName, d)
                    
                    os.renames(current, newName)
    finally:
        os.chdir(oldwd)
    
    print "\n\nDone!"

if __name__ == '__main__':
    #print CleanVSFiles(*sys.argv[1:])
    CleanVSFiles("c:/programming")
    