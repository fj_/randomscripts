// warning: contains a race condition (but I forgot where :( )

#include <stdio.h>
#include <string>
#include <queue>
#include <vector>
#include <process.h>
#include "cpp_utils.hpp"

using std::string;
using std::queue;
using std::vector;

enum Priority
{
    PR_NORMAL,
    PR_HIGH,
    PR_LOW,
};

struct ITask
{
    virtual void Execute() = 0;
    virtual ~ITask() {};
};

const int MAX_PENDING_REQUESTS = 100000;

class Executor
{
private:
    queue<ITask*> high_priority, normal_priority, low_priority;
    vector<HANDLE> threads;
    int high_priority_counter;
    HANDLE semaphore;
    HANDLE terminate_requested;
    volatile bool terminate_requested_flag; // technically I could've used WaitForSingleObject 
    // on terminate_requested, but that would've been messy and slow
    CRITICAL_SECTION critical_section;
    bool critical_section_initialized;

    bool fetch_and_execute_request()
    {
        ITask * task(NULL);
        EnterCriticalSection(&critical_section);
        __try
        {
            printf("Counts: %d %d %d\n", high_priority.size(), normal_priority.size(), low_priority.size());

            high_priority_counter = (high_priority_counter + 1) & 3;
            queue<ITask*> * queues[] = {&high_priority, &normal_priority, &low_priority};
            if (!high_priority_counter) // check normal priority first
            {
                swap(queues[0], queues[1]);
                // funny thing is, I don't have a slightest idea why `swap` is visible here =)
            }
            for (int i = 0; i < array_length(queues); i++)
            {
                if (queues[i]->size())
                {
                    task = queues[i]->front();
                    queues[i]->pop();
                    break;
                }
            }
        }
        __finally
        {
            LeaveCriticalSection(&critical_section);
        }
        if (task)
        {
            task->Execute();
            return true;
        }
        return false;
    }

    unsigned int thread_routine()
    {
        printf("Running lol\n");
        HANDLE handles[] = { this->semaphore, this->terminate_requested };
        bool running = true;
        while (running)
        {
            DWORD n = WaitForMultipleObjects(array_length(handles), handles, false, INFINITE);
            switch (n)
            {
            case WAIT_OBJECT_0:
                // have job to do!
                printf("Woken up!\n");
                fetch_and_execute_request();
                break;
            case WAIT_OBJECT_0 + 1: 
                // terminate requested.
                Sleep(100);
                printf("Terminate requested!\n");
                running = false;
                break;
            default:
                CHECK_NZ(false);
            }
        }
        // execute any remaining requests
        while (fetch_and_execute_request());
        return 0;
    }

    static unsigned int __stdcall thread_routine_wrapper(void * arg)
    {
        return static_cast<Executor*>(arg)->thread_routine();
    }

    void cleanup()
    {
        for (vector<HANDLE>::iterator it = threads.begin(), end = threads.end();
            it != end; ++it)
        {
            safe_close_handle(*it);
        }
        threads.resize(0);
        safe_close_handle(semaphore);
        safe_close_handle(terminate_requested);
        if (critical_section_initialized)
        {
            DeleteCriticalSection(&critical_section);
            critical_section_initialized = false;
        }
    }

public:
    Executor(int num_threads) 
        try
        :high_priority_counter(0),
        semaphore(NULL),
        terminate_requested(NULL),
        terminate_requested_flag(false),
        critical_section(),
        critical_section_initialized(false)
    {
        threads.resize(num_threads);
        InitializeCriticalSection(&critical_section);
        critical_section_initialized = true;
        CHECK_NZ(semaphore = CreateSemaphore(NULL, 0, MAX_PENDING_REQUESTS, NULL));
        CHECK_NZ(terminate_requested = CreateEvent(NULL, true, false, NULL));

        // finally, launch threads
        for (vector<HANDLE>::iterator it = threads.begin(), end = threads.end();
            it != end; ++it)
        {
            CHECK_NZ(*it = (HANDLE)_beginthreadex(NULL, 0, thread_routine_wrapper, this, 0, NULL));
        }
    }
    catch (...)
    {
        cleanup();
        throw;
    }

    // Destroying the object without calling Stop first still results in undefined behaviour.
    ~Executor()
    {
        cleanup();
    }

    bool Execute(ITask * task, Priority priority)
    {
        if (terminate_requested_flag) return false;
        EnterCriticalSection(&critical_section);
        switch (priority)
        {
        case PR_NORMAL: 
            normal_priority.push(task); 
            break;
        case PR_HIGH: 
            high_priority.push(task); 
            break;
        case PR_LOW: 
            low_priority.push(task); 
            break;
        }
        LeaveCriticalSection(&critical_section);
        CHECK_NZ(ReleaseSemaphore(semaphore, 1, NULL));
        return true;
    }

    void Stop()
    {
        printf("Signaling terminate event.\n");
        terminate_requested_flag = true;
        SetEvent(terminate_requested);
        printf("Joining threads.\n");
        DWORD n = WaitForMultipleObjects(threads.size(), &threads[0], true, INFINITE);
        CHECK_NZ(n >= WAIT_OBJECT_0 && n < WAIT_OBJECT_0 + threads.size());
        printf("Yay!\n");
        cleanup();
    }
};


void main()
{
    Executor executor(5);
    struct Task : ITask
    {
        const string msg;
        const DWORD delay_ms;
        const Priority priority;
        Task(Priority a_priority, const string & a_msg, DWORD a_delay_ms)
            :priority(a_priority),
            msg(a_msg),
            delay_ms(a_delay_ms)
        {}
        virtual void Execute()
        {
            printf("{ Executing %s\n", msg.c_str());
            Sleep(delay_ms);
            printf("} // Executing %s\n", msg.c_str());
        }
    };

    printf("Created, lol\n");

    vector<Task*> tasks;
    for (int i = 0; i < 200; i++)
    {
        Task * task(NULL);
        switch (i % 3)
        {
        case 0:
            tasks.push_back(new Task(PR_HIGH, "#" + to_string(i) + " high_priority", 50)); break;
        case 1:
            tasks.push_back(new Task(PR_NORMAL, "#" + to_string(i) + " normal_priority", 50)); break;
        case 2:
            tasks.push_back(new Task(PR_LOW, "#" + to_string(i) + " low_priority", 50)); break;
        }
    }
    
    for (size_t i = 0; i < tasks.size(); i++)
    {
        executor.Execute(tasks[i], tasks[i]->priority);
        Sleep(1);
    }
    executor.Stop();
}