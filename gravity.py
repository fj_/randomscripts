from math import sqrt, acos

G = 6.67428e-11
earth_mass = 5.9736e24
earth_radius = 6.371e6

def potential_energy(m1, m2, r):
    return -G * m1 * m2 / r

def orbital_speed(m, r):
    return sqrt(G * m / r)

def orbital_kinetic_energy(m, m2, r):
    return m * orbital_speed(m2, r) ** 2 / 2

def free_fall_time(y0, y, m):
    return (sqrt(y0 ** 3 / (2 * G * m)) *
            (sqrt(y / y0 * (1 - y / y0)) + acos(sqrt(y / y0))))

earth_surface_p = potential_energy(1.0, earth_mass, earth_radius)

def print_params(orbit_name, orbit_radius):
    print(orbit_name)
    my_potential_energy = (potential_energy(1.0, earth_mass, orbit_radius) - earth_surface_p)
    my_kinetic_energy = orbital_kinetic_energy(1.0, earth_mass, orbit_radius)
    my_total_energy = my_potential_energy + my_kinetic_energy
    my_impact_velocity = sqrt(2 * my_potential_energy)
    my_free_fall_time = free_fall_time(orbit_radius, earth_radius, earth_mass)
    print('Potential energy: {} MJ/kg'.format(my_potential_energy / 1e6))
    print('Kinetic energy: {} MJ/kg'.format(my_kinetic_energy / 1e6))
    print('Total energy: {} MJ/kg'.format(my_total_energy  / 1e6))
    print('Impact velocity: {} km/s'.format(my_impact_velocity / 1e3))
    print('Free fall time: {} minutes ({} days)'.format(my_free_fall_time / 60, my_free_fall_time / (60 * 60 * 24)))
    print()

print_params('ISS (400km)', earth_radius + 0.4e6)
print_params('2000 km', earth_radius + 2e6)
print_params('3000 km', earth_radius + 3e6)
print_params('4000 km', earth_radius + 4e6)
print_params('Geostationary (35,786 km)', earth_radius + 35.786e6)
print_params('Moon (385,000 km)', 385e6)