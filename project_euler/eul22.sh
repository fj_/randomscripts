#!/bin/sh

cat names.txt | tr -d \" | tr , \\n | sort \
    | xargs -i echo "echo \$((\`echo {} | sed \"s/./&\n/g\" | xargs -iinput printf '%d - 64 + ' \"'input\"; echo 0\`))" \
    | bash | awk '{ if (NR % 10 == 0) print NR; sum += NR * $0}; END {printf "%d", sum}'

#    | head -n 20 \
