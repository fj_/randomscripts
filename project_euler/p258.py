import sys
from numpy import array, zeros, identity, ones, dot, sum
# element_type = int
from numpy import uint64 as element_type



from time import clock
_t = clock()
def op_completed(s):
    global _t
    oldt, _t = _t, clock()
    print "%0.3f: %s" % (_t - oldt, s) 

  
#shift = 10
#modulo = 20092010

shift = 2000
modulo = element_type(20092010)

def memoized(f):
    d = {}
    notfound = object()
    def wrapper(*args):
        r = d.get(args, notfound)
        if r is notfound:
            r = f(*args)
            d[args] = r
        return r
    return wrapper

@memoized
def naive(k):
    return 1  if k < shift else (naive(k - shift) + naive(k - shift + 1)) % modulo

def get_matrix():
    m = zeros((shift, shift), dtype = element_type)
    m[0, -1] = m[0, -2] = 1
    for i in range(shift - 1):
        m[i + 1, i] = 1
    return m

def fastexp(value, power):
    assert power > 0
    result = identity(shift, element_type)
    while True:
        print power
        if power & 1: 
            result = dot(result, value.T)
            result %= modulo
        power >>= 1
        if not power: break
        value = dot(value, value.T.copy().T) # :)
        value %= modulo
    return result.T.copy()

def fast(k):
    if k < shift:
        return 1
    m = get_matrix()
    m = fastexp(m, k - shift + 1)
    r = sum(m[0]) % modulo
    print r
    print sum(m[1]) % modulo
    print sum(m[2]) % modulo
    return r

#n = 50
#
#m = get_matrix()
#print m
#
#initial = ones(shift)
#current = initial
#expm = m
#for i in range(shift, n):
#    current = dot(m, current) % modulo
#    fexpm = fastexp(m, i - shift + 1)
#    fast = dot(expm, initial) % modulo
#    print '---------------'
#    print '%3d: %5d %5d %5d' % (i, naive(i), current[0], fast[0])
#    expm = dot(m, expm)

#m = get_matrix()
#value = ones(shift, uint64)
#m = fastexp(m, 10**18 - shift + 2)
#value = dot(m, value)   
#print value

#for i in [1999, 2000, 2001] :
#    n = naive(i)
#    f = fast(i)
#    print n, f
#    assert n == f

print fast(10**18 + 1)