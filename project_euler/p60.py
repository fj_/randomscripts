from mathutils import PrimeGenerator
from collections import defaultdict

primes = PrimeGenerator()

all_cliques = []

def is_edge(p1, p2):
    return primes.isprime(int(p1 + p2)) and primes.isprime(int(p2 + p1)) 

def add_clique(*vertices):
    vertices = sorted(vertices, key = lambda v: int(v))
    n = len(vertices) - 1
    while len(all_cliques) < n: all_cliques.append(defaultdict(lambda k: []))
    all_cliques[n][vertices[0]] = vertices[1:]
    

def add_edge(p1, p2):
    # assert int(p1) < int(p2)
    for n, cliques in enumerate(all_cliques):
        for clique in cliques[p1]:
            if all(is_edge(p, p2) for p in clique):
                add_clique(p1, p2, *clique)
    add_clique(p1, p2)
                
print sorted(str(i) for i in range(20))
