from collections import defaultdict
import re
import itertools

def tarjan(graph):
    '''graph is {node: [node]}'''
    result = []
    current_index = [0] # Python2 doesn't have "nonlocal" variables, emulate using a singleton list.
    index, lowlink = {}, {}
    # for small graphs list lookup is faster than set lookup and stuff, so don't bother. 
    stack = []
    def strongconnect(v):
        # Set the depth index for v to the smallest unused index
        index[v] = current_index[0]
        lowlink[v] = current_index[0]
        current_index[0] += 1
        stack.append(v)
        # Consider successors of v
        for w in graph[v]:
            if w not in index:
                # Successor w has not yet been visited; recurse on it
                strongconnect(w)
                lowlink[v] = min(lowlink[v], lowlink[w])
            elif w in stack:
                # Successor w is in stack S and hence in the current SCC
                lowlink[v] = min(lowlink[v], index[w])

        # If v is a root node, pop the stack and generate an SCC
        if lowlink[v] == index[v]:
            # start a new strongly connected component
            current_scc = []
            while True:
                w = stack.pop()
                current_scc.append(w)
                if w == v: break
            result.append(current_scc)

    for v in graph:
        if v not in index:
            strongconnect(v)
    return result

def tarjan2(graph):
    '''graph is {node: [node]}'''
    result = []
    current_index = [0] # Python2 doesn't have "nonlocal" variables, emulate using a singleton list.
    index, lowlink = {}, {}
    # for small graphs list lookup is faster than set lookup and stuff, so don't bother. 
    stack = []
    def strongconnect(v):
        # Set the depth index for v to the smallest unused index
        index[v] = current_index[0]
        lowlink[v] = current_index[0]
        current_index[0] += 1
        stack.append(v)
        # Consider successors of v
        for w in graph[v]:
            if w not in index:
                # Successor w has not yet been visited; recurse on it
                strongconnect(w)
                lowlink[v] = min(lowlink[v], lowlink[w])
            elif w in stack:
                # Successor w is in stack S and hence in the current SCC
                lowlink[v] = min(lowlink[v], lowlink[w])

        # If v is a root node, pop the stack and generate an SCC
        if lowlink[v] == index[v]:
            # start a new strongly connected component
            current_scc = []
            while True:
                w = stack.pop()
                current_scc.append(w)
                if w == v: break
            result.append(current_scc)

    for v in graph:
        if v not in index:
            strongconnect(v)
    return result

def tarjan3(graph):
    '''graph is {node: [node]}'''
    result = []
    current_index = [0] # Python2 doesn't have "nonlocal" variables, emulate using a singleton list.
    index = {}
    # for small graphs list lookup is faster than set lookup and stuff, so don't bother. 
    stack = []
    def strongconnect(v):
        # Set the depth index for v to the smallest unused index
        index[v] = current_index[0]
        lowlink = current_index[0]
        current_index[0] += 1
        stack.append(v)
        # Consider successors of v
        for w in graph[v]:
            if w not in index:
                # Successor w has not yet been visited; recurse on it
                lowlink = min(lowlink, strongconnect(w))
            elif w in stack:
                # Successor w is in stack S and hence in the current SCC
                lowlink = min(lowlink, index[w])

        # If v is a root node, pop the stack and generate an SCC
        if lowlink == index[v]:
            # start a new strongly connected component
            current_scc = []
            while True:
                w = stack.pop()
                current_scc.append(w)
                if w == v: break
            result.append(current_scc)
        return lowlink

    for v in graph:
        if v not in index:
            strongconnect(v)
    return result

def make_graph(edges):
    graph = defaultdict(list)
    for fro, to in edges:
        graph[fro].append(to)
        # create the destination, too
        graph[to]
    return dict(graph)
    

def parse_graph(s):
    def edges():
        for line in re.split(r'[\n,;]', s):
            line = line.strip()
            if not line: continue
            fro, to = line.split()
            yield fro, to
    return make_graph(edges())


def iterate_all_graphs(size, self_links=False):
    possible_edges = []
    for i in xrange(1, size + 1):
        for j in xrange(1, size + 1):
            if i == j and not self_links: continue
            possible_edges.append((None, (i, j)))
                
    for edges in itertools.product(*possible_edges):
        yield make_graph(e for e in edges if e is not None)


def main():
    print 'yo' 
    # g = parse_graph('1 2, 1 3, 3 4, 4 2')
    # print g, tarjan(g)
    # 
    # g = parse_graph('1 2, 1 3, 3 4, 4 2, 2 3')
    # print g, tarjan(g)

    for g in iterate_all_graphs(4):
        r1 = tarjan(g)
        r2 = tarjan3(g)
        if r1 != r2:
            print g
            print r1
            print r2
            break
    
    print 'done'
    

if __name__ == '__main__':
    main()